package com.example.admin.daveai.database.models;

public class MediaTableRowModel {
    public String url = "";
    public String localPath = "";
    public long lastSynced = 0;
    public String type = "";

    public MediaTableRowModel(String url, String localPath, long lastSynced, String type) {
        this.url = url;
        this.localPath = localPath;
        this.lastSynced = lastSynced;
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public long getLastSynced() {
        return lastSynced;
    }

    public void setLastSynced(long lastSynced) {
        this.lastSynced = lastSynced;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
