package com.example.admin.daveai.dynamicView;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import com.example.admin.daveai.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchableDialog extends DialogFragment implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private static final String TAG = "SearchableDialog";
    private static final String ITEMS = "items";

    private  ProgressBar progressBar;
    private SearchView _searchView;
    private ListView _listViewItems;
    private ArrayAdapter listAdapter;



    private ArrayList itemsList = new ArrayList() ;

    private String _strTitle;
    private String _strPositiveButtonText;

    private OnSearchViewCreated onSearchViewCreated;
    private SearchableItem _searchableItem;
    private OnSearchTextChanged _onSearchTextChanged;
    private DialogInterface.OnClickListener _onClickListener;



    public SearchableDialog() {

    }



    public static SearchableDialog newInstance() {

        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SearchableDialog:-----------");

        SearchableDialog multiSelectExpandableFragment = new SearchableDialog();

      /*  Bundle args = new Bundle();
        args.putSerializable(ITEMS, (Serializable) items);
        multiSelectExpandableFragment.setArguments(args);*/

        return multiSelectExpandableFragment;
    }

    public interface OnSearchViewCreated{
        void onSearchViewCreated();
    }
    public void setOnSearchViewCreatedListener(OnSearchViewCreated onSearchViewCreated) {
        this.onSearchViewCreated = onSearchViewCreated;
    }


    public interface SearchableItem<T> extends Serializable {
        void onSearchableItemClicked(T item, int position);
    }

   /* public interface SearchableItem  {
        void onSearchableItemClicked(Object item, int position);
    }*/
    public void setOnSearchableItemClickListener(SearchableItem searchableItem) {
        this._searchableItem = searchableItem;
    }


    public interface OnSearchTextChanged {
        void onSearchTextChanged(String strText);
    }
    public void setOnSearchTextChangedListener(OnSearchTextChanged onSearchTextChanged) {
        this._onSearchTextChanged = onSearchTextChanged;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //itemsList = (ArrayList) getArguments().getSerializable(ITEMS);
        Log.i(TAG,"onCreate:------------------------------------"+itemsList);

    }
/*
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.i(TAG,"onViewCreated:------------------------------------");

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        _searchView = (SearchView) view.findViewById(R.id.searchView);
        _listViewItems = (ListView) view.findViewById(R.id.listView);

        _searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        _searchView.setIconifiedByDefault(false);
        _searchView.setOnQueryTextListener(this);
        _searchView.setOnCloseListener(this);
        _searchView.clearFocus();

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(_searchView.getWindowToken(), 0);


       // List items = (List) getArguments().getSerializable(ITEMS);

        //create the adapter by passing your ArrayList data and attach the adapter to the list
        listAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, itemsList);
        _listViewItems.setAdapter(listAdapter);
        _listViewItems.setTextFilterEnabled(true);
        _listViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                _searchableItem.onSearchableItemClicked(listAdapter.getItem(position), position);
                getDialog().dismiss();

            }
        });

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }*/


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if(onSearchViewCreated!=null)
            onSearchViewCreated.onSearchViewCreated();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Getting the layout inflater to inflate the view in an alert dialog.
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        // Crash on orientation change #7
        // Change Start
        // Description: As the instance was re initializing to null on rotating the device,
        // getting the instance from the saved instance
        if (null != savedInstanceState) {
            _searchableItem = (SearchableItem) savedInstanceState.getSerializable("item");
        }
        // Change End

        View rootView = inflater.inflate(R.layout.searchable_list_dialog, null);
        itemsList = new ArrayList();
        setData(rootView);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);

        String strPositiveButton = _strPositiveButtonText == null ? "CLOSE" : _strPositiveButtonText;
        alertDialog.setPositiveButton(strPositiveButton, _onClickListener);

        String strTitle = _strTitle == null ? "Select Item" : _strTitle;
        alertDialog.setTitle(strTitle);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    // Crash on orientation change #7
    // Change Start
    // Description: Saving the instance of searchable item instance.
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("item", _searchableItem);

        super.onSaveInstanceState(outState);
    }
    // Change End

    public void setTitle(String strTitle) {
        _strTitle = strTitle;
    }

    public void setPositiveButton(String strPositiveButtonText) {
        _strPositiveButtonText = strPositiveButtonText;
    }

    public void setPositiveButton(String strPositiveButtonText, DialogInterface.OnClickListener onClickListener) {
        _strPositiveButtonText = strPositiveButtonText;
        _onClickListener = onClickListener;
    }

    private void setData(View rootView) {

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        _searchView =  rootView.findViewById(R.id.searchView);
        _searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        _searchView.setIconifiedByDefault(false);
        _searchView.setOnQueryTextListener(this);
        _searchView.setOnCloseListener(this);
        _searchView.clearFocus();

        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(_searchView.getWindowToken(), 0);

        progressBar = rootView.findViewById(R.id.progressBar);


        //List items = (List) getArguments().getSerializable(ITEMS);

        _listViewItems = (ListView) rootView.findViewById(R.id.listView);


        //create the adapter by passing your ArrayList data and attach the adapter to the list
        listAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, itemsList);
        _listViewItems.setAdapter(listAdapter);
        _listViewItems.setTextFilterEnabled(true);
        _listViewItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                _searchableItem.onSearchableItemClicked(listAdapter.getItem(position), position);
                getDialog().dismiss();

            }
        });
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        _searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
//        listAdapter.filterData(s);
        if (TextUtils.isEmpty(s)) {
//                _listViewItems.clearTextFilter();
            ((ArrayAdapter) _listViewItems.getAdapter()).getFilter().filter(null);
        } else {
            ((ArrayAdapter) _listViewItems.getAdapter()).getFilter().filter(s);
        }
        /*if (null != _onSearchTextChanged) {
            _onSearchTextChanged.onSearchTextChanged(s);
        }*/
        //todo make sever call and update listview
        return true;
    }


    public void clearData(){
        itemsList.clear();
        listAdapter.clear();
        listAdapter.notifyDataSetChanged();

    }

    public void updatedData(List itemsArrayList) {
        Log.i(TAG," Before update Data:------------------"+itemsArrayList.size()+" "+ itemsArrayList );

        progressBar.setVisibility(View.GONE);
        if(itemsList!=null && itemsList.size()>0)
            itemsList.clear();
        if(itemsList ==null)
            itemsList = new ArrayList();

        itemsList.addAll(itemsArrayList);
        listAdapter.notifyDataSetChanged();

        Log.e(TAG," After update Data:------------------"+listAdapter.getCount() );



       /* listAdapter.clear();
        if (itemsArrayList != null){

            for (Object object : itemsArrayList) {

                listAdapter.insert(object, listAdapter.getCount());
            }
        }
        listAdapter.notifyDataSetChanged();*/

    }

    public void onRequestFailed(){
        progressBar.setVisibility(View.GONE);
    }

}
