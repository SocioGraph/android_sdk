package com.example.admin.daveai.model;


import java.util.ArrayList;
import java.util.List;


public class InteractionStages {


    private String interaction_time_attribute;
    private String next_stage;
    private String previous_stage;
    private String interaction_group_attribute;
    private boolean experienced_product;
    private String share_button_name;
    private String name;
    //dict nested_object
    private PositivityScoreFunction positivity_score_function;
    private String share_button_template;

    //double
    private double default_positivity_score;
    private boolean display;
    //stringlist list
    private List<String> product_attributes;
    private List<String> quantity_attributes;
    private List<String> customer_attributes;
    private List<String> new_interaction_attributes = new ArrayList<>();

    private String tagged_products_attribute;
    private String product_model;
    private String customer_model;
    private String store_model;
    private String interaction_channel_attribute;
    private String time_period;
    private String description;

    //objectlist
    private String pre_actions;
    //private String post_actions;
    private Object post_actions;




    public String getInteraction_time_attribute() {
        return interaction_time_attribute;
    }

    public void setInteraction_time_attribute(String interaction_time_attribute) {
        this.interaction_time_attribute = interaction_time_attribute;
    }

    public String getNext_stage() {
        return next_stage;
    }

    public void setNext_stage(String next_stage) {
        this.next_stage = next_stage;
    }

    public String getPrevious_stage() {
        return previous_stage;
    }

    public void setPrevious_stage(String previous_stage) {
        this.previous_stage = previous_stage;
    }

    public String getInteraction_group_attribute() {
        return interaction_group_attribute;
    }

    public void setInteraction_group_attribute(String interaction_group_attribute) {
        this.interaction_group_attribute = interaction_group_attribute;
    }

    public boolean isExperienced_product() {
        return experienced_product;
    }

    public void setExperienced_product(boolean experienced_product) {
        this.experienced_product = experienced_product;
    }

    public String getShare_button_name() {
        return share_button_name;
    }

    public void setShare_button_name(String share_button_name) {
        this.share_button_name = share_button_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PositivityScoreFunction getPositivity_score_function() {
        return positivity_score_function;
    }

    public void setPositivity_score_function(PositivityScoreFunction positivity_score_function) {
        this.positivity_score_function = positivity_score_function;
    }

    public String getShare_button_template() {
        return share_button_template;
    }

    public void setShare_button_template(String share_button_template) {
        this.share_button_template = share_button_template;
    }

    public double getDefault_positivity_score() {
        return default_positivity_score;
    }

    public void setDefault_positivity_score(double default_positivity_score) {
        this.default_positivity_score = default_positivity_score;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public List<String> getProduct_attributes() {
        return product_attributes;
    }

    public void setProduct_attributes(List<String> product_attributes) {
        this.product_attributes = product_attributes;
    }

    public List<String> getQuantity_attributes() {
        return quantity_attributes;
    }

    public void setQuantity_attributes(List<String> quantity_attributes) {
        this.quantity_attributes = quantity_attributes;
    }

    public List<String> getCustomer_attributes() {
        return customer_attributes;
    }

    public void setCustomer_attributes(List<String> customer_attributes) {
        this.customer_attributes = customer_attributes;
    }

    public String getTagged_products_attribute() {
        return tagged_products_attribute;
    }

    public void setTagged_products_attribute(String tagged_products_attribute) {
        this.tagged_products_attribute = tagged_products_attribute;
    }

    public List<String> getNew_interaction_attributes() {
        return new_interaction_attributes;
    }

    public void setNew_interaction_attributes(List<String> new_interaction_attributes) {
        this.new_interaction_attributes = new_interaction_attributes;
    }

    public static class PositivityScoreFunction {
        /**
         * max_value : 1
         * min_value : -1
         * quantity_attribute : qty
         */

        private int max_value;
        private int min_value;
        private String quantity_attribute;

        public int getMax_value() {
            return max_value;
        }

        public void setMax_value(int max_value) {
            this.max_value = max_value;
        }

        public int getMin_value() {
            return min_value;
        }

        public void setMin_value(int min_value) {
            this.min_value = min_value;
        }

        public String getQuantity_attribute() {
            return quantity_attribute;
        }

        public void setQuantity_attribute(String quantity_attribute) {
            this.quantity_attribute = quantity_attribute;
        }
    }


}
