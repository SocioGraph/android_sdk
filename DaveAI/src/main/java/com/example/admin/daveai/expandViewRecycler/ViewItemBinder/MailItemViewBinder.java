package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.admin.daveai.R;
import com.example.admin.daveai.dialogbox.SMSTemplate;
import com.example.admin.daveai.expandViewRecycler.ViewItem.MailItem;
import me.drakeet.multitype.ItemViewBinder;


public class MailItemViewBinder extends ItemViewBinder<MailItem, MailItemViewBinder.TextHolder> {


    private Context context;

    public MailItemViewBinder(Context context) {
        this.context = context;

    }

    @Override
    protected @NonNull
    TextHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.mail_item_text, parent, false);
        return new TextHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull TextHolder holder, @NonNull final MailItem mailItem) {

        holder.mail_value.setText(mailItem.getText_value());
        holder.mail_title.setText(mailItem.getText_key());
        holder.mail_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ClikingText", "onClick: ");

                //TODO inplement call back func to open SMS Template

                SMSTemplate cdd = new SMSTemplate(context,mailItem.getText_value(),SMSTemplate.SEND_MAIL_TEMPLATE);
                cdd.show();

             /*   Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{mailItem.getText_value()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(i, "Email:"));*/

            }
        });

    }


    static class TextHolder extends RecyclerView.ViewHolder {

        private TextView key_title, key_value, mail_title, mail_value;

        TextHolder(@NonNull View itemView) {
            super(itemView);
            this.mail_title = itemView.findViewById(R.id.mail_title);
            this.mail_value = itemView.findViewById(R.id.mail_value);


        }
    }
}
