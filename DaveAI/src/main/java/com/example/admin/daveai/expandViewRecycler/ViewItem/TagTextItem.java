package com.example.admin.daveai.expandViewRecycler.ViewItem;


import org.json.JSONArray;

public class TagTextItem {

    private String text_key;
    private String text_value;
    private JSONArray jsonArray;


    public TagTextItem(String text_key, JSONArray jsonArray) {
        this.text_key = text_key;
        this.jsonArray = jsonArray;
    }


    public TagTextItem(String text_key, String text_value) {
        this.text_key = text_key;
        this.text_value = text_value;
    }

    public String getText_key() {
        return text_key;
    }

    public String getText_value() {
        return text_value;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }
}
