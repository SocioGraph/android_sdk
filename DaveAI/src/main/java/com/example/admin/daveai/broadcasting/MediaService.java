package com.example.admin.daveai.broadcasting;

import android.Manifest;
import android.app.Activity;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.MediaQueueTableRowModel;
import com.example.admin.daveai.database.models.MediaTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;

public class MediaService extends IntentService implements DatabaseConstants {

    private  final static  String TAG = "MediaService";
    //context and database helper object
    private Context context;
    private DatabaseManager databaseManager;
    private int cacheTableType = 0 ;
    private String updateTableName = "";
    private String updateTableRowId ="";
    private String updateAttributeName ="";

    public static boolean isIntentServiceRunning = false;

    public MediaService() {
        super(MediaService.class.getName());
        Log.d(TAG, "****************Initialize  Service!");

    }


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param context
     */
    public static void verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"NO PERMISSION");
            // We don't have permission so prompt the user

        }else{
            Log.e(TAG," WRITE PERMISSION GRANTED");

        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "*********Media Service Started!");
        this.context = MediaService.this;
        isIntentServiceRunning = true;
        if(CheckNetworkConnection.networkHasConnection(context)) {
            databaseManager = DatabaseManager.getInstance(context);
            databaseManager.createTable(DatabaseConstants.MEDIA_TABLE_NAME,CacheTableType.MEDIA_TABLE);
            databaseManager.createTable(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME,CacheTableType.MEDIA_QUEUE_TABLE);

                Cursor cursor = databaseManager.getDataFromTableWhere(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME, new HashMap<>(), 1, POST_ID);
                while(cursor.getCount()>0) {
                    syncDataWithServer();
                    cursor.close();
                    cursor = databaseManager.getDataFromTableWhere(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME, new HashMap<>(), 1, POST_ID);
                }
                sendMyBroadCast(MEDIA_QUEUE_EMPTY);
                cursor.close();
                this.stopSelf();

        }

        isIntentServiceRunning = false;
        /*else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);

        }*/
        //this.stopSelf();
        // Log.e(TAG, "****************Service Stopping!");
    }

    //TODO:call post queue api from databaseManager
    private void syncDataWithServer(){
        try {

            Cursor cursor = databaseManager.getDataFromTableWhere(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME, new HashMap<>(), 1, POST_ID);
//            Log.e(TAG, "****************syncDataWithServer cursor count*************" + cursor.getCount());
            if (cursor.moveToFirst()) {
                int postId = cursor.getInt(cursor.getColumnIndex(POST_ID));
                String url = cursor.getString(cursor.getColumnIndex(URL));
                String type = cursor.getString(cursor.getColumnIndex(TYPE));
                updateTableName = cursor.getString(cursor.getColumnIndex(UPDATE_TABLE_NAME));
                updateTableRowId = cursor.getString(cursor.getColumnIndex(UPDATE_TABLE_ROW_ID));
                updateAttributeName = cursor.getString(cursor.getColumnIndex(UPDATE_ATTRIBUTE_NAME));
                String status = cursor.getString(cursor.getColumnIndex(STATUS));
                // String  onError =  cursor.getString(cursor.getColumnIndex(ON_ERROR));

                Log.i(TAG, "APIQueue Row Details:- 1.) URL:- " + url + " 2.)UPDATE_TABLE_NAME:- " + updateTableName +
                        "  3.)UPDATE_TABLE_ROW_ID :- " + updateTableRowId+" 4)UPDATE_ATTRIBUTE_NAME:-"+updateAttributeName);

                HashMap<String, Object> pars = new HashMap<>();
                pars.put(URL, url);
                Cursor c = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, pars, new HashMap<>(), "=", 0, "");

                String finalLocalPath = downloadFIle(url);
                if(!(finalLocalPath.contains("http://")||finalLocalPath.contains("https://"))){
                    updateParticularTable(url,finalLocalPath,type);
                }

                Log.d(TAG," BEFORE media queue size = "+databaseManager.getDataFromTable(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME).getCount());
                ArrayList<String> whereClauseArray = new ArrayList<>();
                whereClauseArray.add(POST_ID);
                databaseManager.deleteRow(
                        DatabaseConstants.MEDIA_QUEUE_TABLE_NAME,
                        databaseManager.createWhereClause(whereClauseArray),
                        new String[]{Integer.toString(postId)}
                );
                Log.d(TAG," BEFORE media queue size after = "+databaseManager.getDataFromTable(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME).getCount());


                cursor.close();
                c.close();
                Log.e(TAG,"post queue size == "+databaseManager.getQueueSize());
               /* if(databaseManager.getQueueSize()<=0){
                    Log.e("LOGGER","calling again!!");
                    syncDataWithServer();
                }*/

            }
        }catch (Exception e) {
            Log.e(TAG,"Error syncDataWithServer:-------------  "+e.getMessage());
            e.printStackTrace();

        }



    }

    public static String MEDIA_QUEUE_EMPTY = "MEDIA_QUEUE_EMPTY";

    private void sendMyBroadCast(String broadcastId)
    {
        try
        {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(broadcastId);

//            broadCastIntent.putExtra("data", "abc");

            sendBroadcast(broadCastIntent);

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private String downloadFIle(String url){
        String finalFileName = "";

        verifyStoragePermissions(this);
        int count;
        HttpURLConnection connection = null;
        try {
            URL urlObj = new URL(url);
            connection = (HttpURLConnection)urlObj.openConnection();

            connection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = connection.getContentLength();
            Log.d(TAG,"get Header :----------------"+connection.getResponseCode());
            // download the file
            InputStream input = new BufferedInputStream(urlObj.openStream(),8192);
            Log.d(TAG,"DIR = "+Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/cached_files/");
            // Output stream
            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/cached_files/");
            if(!folder.exists())
                folder.mkdirs();
            finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/cached_files/"
                    + generateNameFromUrl(url);




            FileOutputStream output = new FileOutputStream(finalFileName);

            byte data[] = new byte[1024];

            long total = 0;
            //Log.d(TAG,"before media save while loop");
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                // writing data to file
                output.write(data, 0, count);
            }
            //Log.e(TAG,"after media save while loop");

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
            connection.disconnect();
            return finalFileName;
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
            if(connection!=null){
                connection.disconnect();
            }
            return url;
        }
    }



    private void updateParticularTable(String url, String finalFileName, String fileType){
        //inserting data in the media map table
        try {
            if(!TextUtils.isEmpty(finalFileName)&&!TextUtils.isEmpty(url)) {
                databaseManager.insertMediaMapData(new MediaTableRowModel(url, finalFileName, System.currentTimeMillis(), fileType));
                HashMap<String, Object> params = new HashMap<>();
                params.put(OBJECT_ID, updateTableRowId);

                Cursor curs = databaseManager.getDataFromTable(updateTableName, params, new HashMap<>(), "=", 1, "");
                curs.moveToFirst();
                ContentValues updateCursor = new ContentValues();
                JSONObject jobj = new JSONObject();
                jobj.put("asdf", url);

                String oldJson = curs.getString(curs.getColumnIndex(DATA_CACHED));
                Log.d(TAG, "oldJSON = " + oldJson);
                String oldUrl = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);
                Log.d(TAG, "oldURL = " + oldUrl);
                String newJson = oldJson;
                Log.d(TAG, "newJSON = " + newJson);
                Log.d(TAG, "index of oldURL = " + newJson.indexOf(oldUrl));
                Log.d(TAG, "index of url = " + newJson.indexOf(url));

                while (newJson.indexOf(oldUrl) >= 0) {

                    newJson = newJson.replace(oldUrl, finalFileName);
                }
                while (newJson.indexOf(url) >= 0) {

                    newJson = newJson.replace(url, finalFileName);
                }
                Log.i(TAG, "newJSON altered = " + newJson);

                updateCursor.put(DATA_CACHED, newJson);
                databaseManager.updateRow(updateTableName, updateCursor, OBJECT_ID + "=?", new String[]{String.valueOf(updateTableRowId)});
                HashMap<String, Object> param = new HashMap<>();
                param.put(OBJECT_ID, updateTableRowId);
                Cursor asd = databaseManager.getDataFromTable(updateTableName, params, new HashMap<>(), "=", 0, "");
                asd.moveToFirst();
                Log.d(TAG, "altered data = " + asd.getString(asd.getColumnIndex(DATA_CACHED)));
                curs.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {


            return null;
        }


        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            Log.e("FILE_DOWNLOADED","downloaded file path ---------- "+file_url);

        }

    }

    public static String generateNameFromUrl(String url){

        // Replace useless chareacters with UNDERSCORE
        String uniqueName = url.replace("://", "_").replace(".", "_").replace("/", "_");
        // Replace last UNDERSCORE with a DOT
        uniqueName = uniqueName.substring(0,uniqueName.lastIndexOf('_'))
                +"."+uniqueName.substring(uniqueName.lastIndexOf('_')+1,uniqueName.length());
        return uniqueName;
    }


    public String getFileNameFromURL(String url) {
        if (url == null) {
            return "";
        }
        try {
            URL resource = new URL(url);
            String host = resource.getHost();
            if (host.length() > 0 && url.endsWith(host)) {
                // handle ...example.com
                return "";
            }
        }
        catch(MalformedURLException e) {
            return "";
        }

        int startIndex = url.lastIndexOf('/') + 1;
        int length = url.length();

        // find end index for ?
        int lastQMPos = url.lastIndexOf('?');
        if (lastQMPos == -1) {
            lastQMPos = length;
        }

        // find end index for #
        int lastHashPos = url.lastIndexOf('#');
        if (lastHashPos == -1) {
            lastHashPos = length;
        }

        // calculate the end index
        int endIndex = Math.min(lastQMPos, lastHashPos);
        return url.substring(startIndex, endIndex);
    }

    public void startBackgroundService(Context mContext){
        if(CheckNetworkConnection.networkHasConnection(mContext)) {
            Log.i(TAG," check Is Media service running >>>>>>>>>>>>>>>>>"+isIntentServiceRunning );
            if(!isIntentServiceRunning){
                Intent msgIntent = new Intent(mContext,MediaService.class);
                mContext.startService(msgIntent);
            }else {
                Log.i(TAG, "<<<<<<<<<<<<<<< Service already Running>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }

    }

    @Override
    public void onDestroy() {
        isIntentServiceRunning = false;
        super.onDestroy();
    }
}