package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.R;
import com.example.admin.daveai.activity.Visualization;

import org.json.JSONArray;


public class GridAdapter extends RecyclerView.Adapter<GridAdapter.SimpleViewHolder> {

    private final Context mContext;
    private JSONArray jsonGridList;



    public GridAdapter(Context context, JSONArray jsonGridList ) {
        mContext = context;
        this.jsonGridList = jsonGridList;
    }

    public GridAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.grid_image_row, parent, false);
        return new GridAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridAdapter.SimpleViewHolder holder, final int position) {

        String imageinGrid="";
       /* holder.imageTitle.setText(title.get(position));
        Glide.with(mContext)
                .load(strarray.get(position))
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(holder.imageGrid);*/



      /*  try {
            imageTitle=jsonGridList.getJSONObject(position).getString("title");
            image = jsonGridList.getJSONObject(position).getString("image");
            if(jsonGridList.getJSONObject(position).has("thumbnail")){
                imageinGrid= jsonGridList.getJSONObject(position).getString("thumbnail");

            }else
                imageinGrid= image;

        } catch (Exception e) {
            e.printStackTrace();
        }*/
        holder.imageTitle.setText(readDataFromJSONArray("title",position));
        Log.e("IMAGE LOAD","thumbnail loading from = "+readDataFromJSONArray("thumbnail",position));
        Glide.with(mContext)
                .load(readDataFromJSONArray("thumbnail",position))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.no_image)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.imageGrid);
    }

    @Override
    public int getItemCount() {
        return jsonGridList.length();
    }


    public class SimpleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageGrid;
        TextView imageTitle;

        public SimpleViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            imageGrid = view.findViewById(R.id.imageGrid);
            imageTitle = view.findViewById(R.id.imageTitle);
            imageGrid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                Bundle b=new Bundle();
                b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.THUMBNAILS.toString());
                b.putInt(Visualization.GRID_POSITION, getAdapterPosition());
                b.putString(Visualization.GRID_LIST, jsonGridList.toString());

                //TODO implement call back function to show  visualization
                Intent intent = new Intent(mContext, Visualization.class);
                intent.putExtra("IMAGE", readDataFromJSONArray("image",getAdapterPosition()));
                intent.putExtra("TITLE", readDataFromJSONArray("title",getAdapterPosition()));
                intent.putExtras(b);
                mContext.startActivity(intent);

                }
            });
        }

        @Override
        public void onClick(View v) {


        }
    }

    private String readDataFromJSONArray(String keyName, int position){

        String jsonValue="";
        try {
            switch (keyName) {
                case "title":
                    jsonValue = jsonGridList.getJSONObject(position).getString("title");
                    break;
                case "image":
                    jsonValue = jsonGridList.getJSONObject(position).getString("image");
                    break;
                case "thumbnail":
                    if(jsonGridList.getJSONObject(position).has("thumbnail")){
                        jsonValue = jsonGridList.getJSONObject(position).getString("thumbnail");

                    }else
                        readDataFromJSONArray("image",position);

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonValue;
    }
}


