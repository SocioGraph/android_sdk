package com.example.admin.daveai.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.admin.daveai.R;
import com.example.admin.daveai.others.MyScaleGestures;
import org.json.JSONArray;


public class VisualizationAdapter extends PagerAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private JSONArray jsonGridList;
    private LayoutInflater layoutInflater;


    public VisualizationAdapter(Context context, JSONArray jsonGridList) {
        this.context = context;
        this.jsonGridList=jsonGridList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return jsonGridList.length();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        View itemView = layoutInflater.inflate(R.layout.visualization_view_pager_item, container, false);
        final ProgressBar progressBar = itemView.findViewById(R.id.progressBar);
        ImageView visualizationImage = itemView.findViewById(R.id.visualizationImage);
        TextView visualizationTitle = itemView.findViewById(R.id.visualizationTitle);
        progressBar.setVisibility(View.VISIBLE);
        visualizationImage.setOnTouchListener(new MyScaleGestures(context));
        //ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(this,new ScaleListener());

        try {
            String titleValue =jsonGridList.getJSONObject(position).getString("title");
            visualizationTitle.setText(titleValue);
            String imageURL= jsonGridList.getJSONObject(position).getString("image");
            Log.e("IMAGE LOAD","image loading from = "+imageURL);
            Glide.with(context)
                    .load(imageURL)
                    .placeholder(com.example.admin.daveai.R.drawable.placeholder)
                    .error(com.example.admin.daveai.R.drawable.no_image)
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);

                            return false;
                        }
                    })
                    .into(visualizationImage);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error During loading Image in Visualization"+e.getLocalizedMessage());
        }
        container.addView(itemView,0);
        return itemView;
    }
}
