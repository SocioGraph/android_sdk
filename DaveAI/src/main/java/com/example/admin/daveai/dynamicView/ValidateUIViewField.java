package com.example.admin.daveai.dynamicView;


import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.util.Patterns;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import mabbas007.tagsedittext.TagsEditText;



public class ValidateUIViewField {

    private final String TAG = getClass().getSimpleName();

    private Context context;
    private String modelName;
    private JSONObject currentObjectDetails;
    HashMap<String, Boolean> requiredFieldHashMap;


    public ValidateUIViewField(Context context){
        this.context=context;


    }

    public ValidateUIViewField(Context context, String modelName){
        this.context=context;
        this.modelName = modelName;

    }

    /**
     * View is Can't Validate for now :---  image, viewPager, GridView
     *
     * Validation is done for following Ui View:--
     *        1.) EditText   2.) TextView :- switch, button   3.) TextInputLayout
     *        4.) Spinner    5.) ListView        6.) RatingBar
     */

    public JSONObject methodValidateDynamicView(ViewGroup parentLayout,HashMap<String, Boolean> requiredFieldHashMap,String preObjectDetails ,JSONObject newObjectDetails){

        Log.e(TAG, "methodValidateDynamicView****requiredFieldHashMap"+requiredFieldHashMap +"\n newObjDetails:-  "+ newObjectDetails);
        this.requiredFieldHashMap = requiredFieldHashMap;
        this.currentObjectDetails = newObjectDetails;
        boolean isFieldValid = true;
        String errorMsg="";

        try {
            //add attrs which not mentioned in form view List but exist in objectDetails
            if(preObjectDetails!= null && !preObjectDetails.isEmpty()){
                JSONObject jsonObject = new JSONObject(preObjectDetails);
                Iterator<String> keys = jsonObject.keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    if(!currentObjectDetails.has(key))
                        currentObjectDetails.put(key,jsonObject.get(key));
                }
            }
            if (parentLayout.getParent() != null)
            {
                for (int i = 0; i < parentLayout.getChildCount(); i++) {
                   View child = parentLayout.getChildAt(i);
                    //Log.e(TAG, "Validate View**************ChildView :-"+child+ " isRequired:- " + fieldRequired );
                    if (child instanceof EditText) {
                       isFieldValid = validateEditText(child);

                    }
                    else if (child instanceof TextView) {
                        isFieldValid= validateTextView(child);

                    }
                    else if (child instanceof TextInputLayout) {
                        TextInputLayout til = (TextInputLayout) child;
                        EditText et = til.getEditText();
                        assert et != null;
                        child = et;
                        isFieldValid = validateTextInputLayout(et);

                    }
                    else if (child instanceof Spinner) {
                        isFieldValid =  validateSpinner(child);

                    }
                    else if (child instanceof ListView) {
                        isFieldValid =  validateListView(child);

                    }
                    else if (child instanceof RatingBar) {
                        isFieldValid = validateRatingBar(child);
                    }
                    else if (child instanceof ViewGroup)
                    {
                        ViewGroup group = (ViewGroup) child;
                        methodValidateDynamicView(group, requiredFieldHashMap,preObjectDetails,currentObjectDetails);
                    }
                    else {
                        Log.e(TAG, "************ Default Other Child of parent ************** : ");
                    }

                    if(!isFieldValid) {
                        errorMsg = child.getTag().toString();
                        break;
                    }
                }
            }
            if (isFieldValid ) {
                Log.e(TAG, "<<<<<<<<<<<<<,MethodValidateDynamicView Object Details >>>>>>>>>>  " + currentObjectDetails);
                return currentObjectDetails;
            } else
                Toast.makeText(context, "Please Fill " + errorMsg, Toast.LENGTH_SHORT).show();

        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "<<<<<<<<<<<<<<<<<<<<<Error during Validate Field>>>>>>>>>>>>>> : " + e.getMessage());
        }
        return null;
    }

    private void saveViewValue(String childName,Object viewData){
        try {
            Log.i(TAG," saveViewValue View NAme:- "+childName+" and value:- "+viewData);
            currentObjectDetails.put(childName,viewData);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Store User Input Data:- " + e.getMessage());
        }
    }

    private boolean validateEditText(View child){

        EditText et = (EditText) child;
        boolean fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
        String editValue = et.getText().toString();
        Log.i(TAG, "Validate EditText***Name :-"+child.getTag().toString()+ " isRequired:-" + fieldRequired + " && value:-"+editValue);
        if (fieldRequired && editValue.isEmpty()) {
            return false;
        }
        if (!editValue.isEmpty()) {
            if(et instanceof TagsEditText){
                JSONArray jsonArray = new JSONArray();
                String[] items = editValue.split(" ");
                for (String item : items) {
                    jsonArray.put(item);
                }
                saveViewValue(et.getTag().toString(), jsonArray);

            }else {
                saveViewValue(et.getTag().toString(), editValue);
            }


        }else{
            saveViewValue(et.getTag().toString(), JSONObject.NULL);
        }

        return true;
    }

    private boolean validateTextView(View child ){

        boolean isFieldRequired = requiredFieldHashMap.get(child.getTag().toString());
        if(child instanceof android.widget.Switch ){
            Switch switchView = (Switch) child;
            Log.i(TAG, "Validate SwitchView**************Name :-"+child.getTag().toString()+ " isRequired:- " + isFieldRequired);
            saveViewValue(child.getTag().toString(), switchView.isChecked());

        }
        else if(child instanceof Button)
        {
            Button button = (Button) child;
            // String textValue = button.getText().toString();
            String textValue = button.getHint().toString();
            Log.i(TAG, "Validate ButtonView**************Name :-"+child.getTag().toString()+ " isRequired:- " + isFieldRequired +" textValue:- "+textValue);
            if (isFieldRequired && textValue.isEmpty()) {
                return false;

            }
            if (!textValue.isEmpty()) {
                saveViewValue(button.getTag().toString(), textValue);
            }
            else{
                saveViewValue(button.getTag().toString(), JSONObject.NULL);
            }

        }else {

            TextView tV = (TextView) child;
            if (child.getTag() != null) {
                String textValue = tV.getText().toString();
                Log.i(TAG, "Validate TextView**************Name :- " + child.getTag().toString() + " isRequired:- " + isFieldRequired);
                if (isFieldRequired && textValue.isEmpty()) {
                    return false;
                }
                if (!textValue.isEmpty()) {
                    saveViewValue(tV.getTag().toString(), textValue);
                }else{
                    saveViewValue(tV.getTag().toString(), JSONObject.NULL);
                }
            }
        }
        return true;
    }

    private boolean validateTextInputLayout(EditText editText){

        boolean isFieldRequired = requiredFieldHashMap.get(editText.getTag().toString());
        Log.i(TAG, "Validate TextInputLayout Edit Text**************Name :-"+editText.getTag().toString()+ " isRequired:- " + isFieldRequired);
        String textValue = editText.getText().toString();
        if (isFieldRequired && textValue.isEmpty()) {
            return false;
        }
        if (!textValue.isEmpty()) {

            saveViewValue(editText.getTag().toString(), textValue);

        }
        else{
            saveViewValue(editText.getTag().toString(), JSONObject.NULL);
        }

     return true;
    }

    private boolean validateSpinner(View child){

        Spinner spinner = (Spinner) child;
        boolean isFieldRequired = requiredFieldHashMap.get(child.getTag().toString());
        Log.e(TAG, "Validate Spinner**************Name :-"+child.getTag().toString()+ " isRequired:- " + isFieldRequired);
        int pos = spinner.getSelectedItemPosition();
        if (isFieldRequired && pos == 0) {
            return false;
        }
        if (pos != 0) {
            saveViewValue(child.getTag().toString(), spinner.getSelectedItem());

        }else{
            saveViewValue(child.getTag().toString(), JSONObject.NULL);
        }
        return true;
    }

    private boolean validateListView(View child){

        ListView mListView = (ListView) child;
        boolean fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
        Log.i(TAG, "Validate ListView MultiSelect**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
        JSONArray selectedList = new JSONArray();
        SparseBooleanArray checked = mListView.getCheckedItemPositions();
        for (int k = 0; k < checked.size(); k++) {
            int item_position = checked.keyAt(k);
            if (checked.valueAt(k)) {
                selectedList.put(mListView.getAdapter().getItem(item_position).toString());
            }
        }
        if (fieldRequired && selectedList.length()==0) {
            return false;

        }
        if (selectedList.length()> 0) {
            saveViewValue(child.getTag().toString(), selectedList);

        } else{
            saveViewValue(child.getTag().toString(), JSONObject.NULL);
        }

        return true;
    }

    private boolean validateRatingBar(View child){

        RatingBar rB = (RatingBar) child;
        boolean fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
        Log.i(TAG, "Validate RatingBar**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
        float ratingBAr = rB.getRating();
        if (fieldRequired && ratingBAr == 0.0) {
            return false;
        }
        if (ratingBAr != 0.0) {
            saveViewValue(child.getTag().toString(), ratingBAr);
        }
        else{
            saveViewValue(child.getTag().toString(), JSONObject.NULL);
        }


        return true;
    }






    /**
     * validate your email address format. Ex-akhi@mani.com
     */
    private boolean emailValidator(String email) {

        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[a-zA-Z0-9#_~!$&'()*+,;=:.\"(),:;<>@\\[\\]\\\\]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*$";
        // final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        Log.e(TAG, " is valid Email: " +matcher.matches());
        return matcher.matches();
    }

    private static boolean isValidEmail(CharSequence target) {
        //return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        return Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    private boolean isValidMobile(String phone) {

        Boolean matches = android.util.Patterns.PHONE.matcher(phone).matches();
        Log.e(TAG, "isValidMobile: " + matches);
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }



    /*private  void validateEmailPhone(EditText editText,String editTextValue){

        if(editText.getInputType()== InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {
            // if (!emailValidator(editText.getText().toString()))
            if (!isValidEmail(editText.getText().toString()))
                Toast.makeText(context, editText.getTag() + " is not valid", Toast.LENGTH_SHORT).show();
            else {
                try {
                    storeCustomerDetails.put(editText.getTag().toString(), editTextValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(editText.getInputType()==InputType.TYPE_CLASS_PHONE) {
            if (!isValidMobile(editText.getText().toString()))
                Toast.makeText(context, editText.getTag()+" is not valid", Toast.LENGTH_SHORT).show();
            else {
                try {
                    storeCustomerDetails.put(editText.getTag().toString(), editTextValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }*/

}
