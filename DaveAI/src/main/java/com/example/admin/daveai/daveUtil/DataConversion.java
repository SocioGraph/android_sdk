package com.example.admin.daveai.daveUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DataConversion {



    public static <T> T dataConversion(Object key, Class<T> returnClass, Object defaultValue){
        try {
            if(key != null ){
                return returnClass.cast(key);
            }
            else{
                return returnClass.cast(defaultValue);
            }
        }catch (Exception e){
            e.printStackTrace();
            return returnClass.cast(defaultValue);
        }
    }


    public static Boolean toBoolean(Object value) {
        if (value instanceof Boolean) {
            return (Boolean) value;
        } else if (value instanceof String) {
            String stringValue = (String) value;
            if ("true".equalsIgnoreCase(stringValue)) {
                return true;
            } else if ("false".equalsIgnoreCase(stringValue)) {
                return false;
            }
        }
        return null;
    }

    public static Double toDouble(Object value) {
        if (value instanceof Double) {
            return (Double) value;
        } else if (value instanceof Number) {
            return ((Number) value).doubleValue();
        } else if (value instanceof String) {
            try {
                return Double.valueOf((String) value);
            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }

    public static Float toFloat(Object value) {
        if (value instanceof Double) {
            return  ((Double) value).floatValue();
        } else if (value instanceof Number) {
            return ((Number) value).floatValue();
        } else if (value instanceof String) {
            try {
                return Float.valueOf((String) value);
            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }

    public static Integer toInteger(Object value) {
        if (value instanceof Integer) {
            return (Integer) value;
        } else if (value instanceof Number) {
            return ((Number) value).intValue();
        } else if (value instanceof String) {
            try {
                return (int) Double.parseDouble((String) value);
            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }

    public static Long toLong(Object value) {
        if (value instanceof Long) {
            return (Long) value;
        } else if (value instanceof Number) {
            return ((Number) value).longValue();
        } else if (value instanceof String) {
            try {
                return (long) Double.parseDouble((String) value);
            } catch (NumberFormatException ignored) {
            }
        }
        return null;
    }

    public static String toString(Object value) {
        if (value instanceof String) {
            return (String) value;
        } else if (value != null) {
            return String.valueOf(value);
        }
        return null;
    }


    public static double getDoubleWithTwoDecimal( double value){
        //DecimalFormat formater = new DecimalFormat("#.00");
        DecimalFormat df2 = new DecimalFormat("#.##");
        return toDouble(df2.format(value));
    }

    public static ArrayList convertObjectToArrayList(Object object){

        ArrayList<String> list = new ArrayList<String>();
        if(object instanceof JSONArray ) {
            JSONArray jsonArray = (JSONArray) object;
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public JSONObject convertStringTOJSONObject(String passString) {
        try {
            return new JSONObject(passString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList convertJsonArrayToList(JSONArray jsonArray){
        try {
            ArrayList<String> list = new ArrayList<String>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return list;
        }catch (Exception e){
            e.printStackTrace();
            return new ArrayList();
        }
    }

    public static JSONArray arrayToJSONArray(ArrayList arrayList){
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < arrayList.size(); i++) {
            jsonArray.put(arrayList.get(i).toString());
        }
        return jsonArray;

    }

    public static JSONObject hashmapToJSONObject(HashMap<String,Object> hashMap){
        JSONObject jsonObject = new JSONObject();
        try {
            for (Map.Entry<String,Object> entry : hashMap.entrySet()) {
                jsonObject.put(entry.getKey(),entry.getValue());
            }
        }catch (Exception e){
            e.printStackTrace();
            return jsonObject;
        }


        return jsonObject;

    }
    public static JSONObject mapListToJSONObject(HashMap<String,ArrayList<String>> hashMap){
        JSONObject jsonObject = new JSONObject();
        try {
            for (Map.Entry<String,ArrayList<String>> entry : hashMap.entrySet()) {
                JSONArray jsonArray = arrayToJSONArray(entry.getValue());
                jsonObject.put(entry.getKey(),jsonArray);
            }
        }catch (Exception e){
            e.printStackTrace();
            return jsonObject;
        }


        return jsonObject;

    }

    public static JSONObject mapObjectToJSONObject(HashMap<String,Object> hashMap){
        JSONObject jsonObject = new JSONObject();
        try {
            for (Map.Entry<String,Object> entry : hashMap.entrySet()) {
                jsonObject.put(entry.getKey(),entry.getValue());
            }
        }catch (Exception e){
            e.printStackTrace();
            return jsonObject;
        }


        return jsonObject;

    }

    /*public static JSONArray toJSONArray(Object object){
        JSONArray jsonArray = new JSONArray();
        try {
            if(object instanceof ArrayList){
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        jsonArray.add(jsonArray.get(i).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            return jsonArray;
        }catch (Exception e){
            e.printStackTrace();
            return jsonArray;
        }
    }*/
}
