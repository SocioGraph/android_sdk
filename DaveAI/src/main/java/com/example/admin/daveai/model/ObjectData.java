package com.example.admin.daveai.model;

import android.content.Context;
import android.util.Log;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;




public class ObjectData {
     //ids - list of objects/model ids
    //id_names - ids of model
            private static String TAG = "ObjectData";
            private Model model;
            private List<Object> ids;
            private List<String> id_names;
            private HashMap<String,Object> object_data;
            private List<JSONObject> data_value;
            private HashMap<String, List<Object>> resultSetMap;
           // private DaveAIPerspective daveAIPerspective;


    public ObjectData(){
       // daveAIPerspective = DaveAIPerspective.getInstance();

    }

    public HashMap<String, Object> getObject_data() {
        return object_data;
    }

    public void setObject_data(HashMap<String, Object> object_data) {
        this.object_data = object_data;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public List<Object> getIds() {
        return ids;
    }

    public void setIds(List<Object> ids) {
        this.ids = ids;
    }

    public List<String> getId_names() {
        return id_names;
    }

    public void setId_names(List<String> id_names) {
        this.id_names = id_names;
    }

    public List<JSONObject> getData_value() {
        return data_value;
    }

    public void setData_value(List<JSONObject> data_value) {
        this.data_value = data_value;
    }

    public HashMap<String, List<Object>> getResultSetMap() {
        return resultSetMap;
    }

    public void setResultSetMap(HashMap<String, List<Object>> resultSetMap) {
        this.resultSetMap = resultSetMap;
    }

    public static JSONArray getKeysAsJSONArrayOfJSONObject(JSONObject jsonObject){
        JSONArray key_names = new JSONArray();
        for(int i=0;i<jsonObject.length();i++){
            try{
                key_names.put(jsonObject.names().getString(i));
            }
            catch (Exception e){
                Log.e(TAG, "Error Getting Key Name from JsonObject" + e.getMessage());
            }
        }
        return key_names;
    }

   /* public static JSONArray getKeysOfJSONObject(JSONObject jsonObject){
        JSONArray key_names = new JSONArray();
        for(int i=0;i<jsonObject.length();i++){
            try{
                key_names.put(jsonObject.names().getString(i));
            }
            catch (Exception e){
                Log.e(TAG, "Error Getting Key Name from JsonObject" + e.getMessage());
            }
        }
        return key_names;
    }*/
    public static ArrayList getKeyAsArrayListOfJSONObject(JSONObject jsonObject){
        ArrayList keyNames = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try{
                keyNames.add(jsonObject.names().getString(i));
            }
            catch (Exception e){
                Log.e(TAG, "Error Getting Key Name from JsonObject" + e.getMessage());
            }
        }
        return keyNames;
    }


    public static ArrayList<String> getAllKeysNameOfObject(JSONObject jsonObject){
        ArrayList<String> key_names = new ArrayList<>();
        for(int i=0;i<jsonObject.length();i++){
            try{
                key_names.add(jsonObject.names().getString(i));
            }
            catch (Exception e){
                Log.e(TAG, "Error Getting Key Name from JsonObject" + e.getMessage());
            }
        }
        return key_names;
    }

    public static Object getValueOfKeyFromJSONObject(JSONObject jsonObject,String key){
        for(int i=0;i<jsonObject.length();i++){
            try{
                if(jsonObject.has(key))
                    return jsonObject.get(key);
            }catch (Exception e){
                Log.e(TAG, "Error Getting Value of Key from JsonObject" + e.getMessage());
            }
        }
        return null;
    }


    public static  List<JSONObject> getData(Context context, String url){
        final   List<JSONObject> jsonObjectsList = new ArrayList<>();
        if (CheckNetworkConnection.networkHasConnection(context)){
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    if(response!=null) {
                        try {
                            JSONObject data = new JSONObject(response);
                            Iterator<String> keys = data.keys();
                            while (keys.hasNext()) {
                                String key = keys.next();
                                if (data.get(key) instanceof JSONArray) {
                                    JSONArray data_array = data.getJSONArray(key);
                                    for (int i = 0; i < data_array.length(); i++) {
                                        jsonObjectsList.add(data_array.getJSONObject(i));
                                    }
                                }
                            }
                        }
                        catch (Exception exception){
                            Log.e(TAG, "Error in InteractionList+++++++++++" + exception.getMessage());
                        }
                        //return jsonObjectsList;
                    }
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {

                }
            };
            new APICallAsyncTask(postTaskListener, context,"GET",true).execute(url);

        } else{
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
        return null;
    }

    public ArrayList getJSONObjectList(String response,Model model_instance){
        ArrayList jsonResponse = new ArrayList<>();
        if(response!=null) {
            try {
                ArrayList<JSONObject> json_list = new ArrayList<>();
                //List<Object> object_ids = new ArrayList<>();
                Type type = new TypeToken<HashMap<String,Object>>(){}.getType();
                HashMap<String, Object> objectsMap = new Gson().fromJson(response,type);

                JSONObject data = new JSONObject(response);
                Iterator<String> keys = data.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray data_array = data.getJSONArray(key);
                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject get_data = data_array.getJSONObject(i);
                            //object_ids.add(get_data.get(model_instance.getIds().get(0)));
                            json_list.add(data_array.getJSONObject(i));
                        }
                    }
                }
               // Log.e(TAG, "Size of ObjectsList+++++++++++" + json_list.size());
                jsonResponse.add(data.get("total_number"));
                jsonResponse.add(data.get("is_last"));
                jsonResponse.add(json_list);
                setModel(model_instance);
                //setIds(object_ids);
                setId_names(model_instance.getIds());
                setObject_data(objectsMap);
                setData_value(json_list);
            }
            catch (Exception exception){
                Log.e(TAG, "Error in Objects List +++++++++++ " + exception.getMessage());
            }
        }
        return jsonResponse;
    }


    public static List<JSONObject> getJSONObjectDataList(String response){
        if(response!=null) {
            List<JSONObject> json_list = new ArrayList<>();
            try {
                JSONObject data = new JSONObject(response);
                Iterator<String> keys = data.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray data_array = data.getJSONArray(key);
                        for (int i = 0; i < data_array.length(); i++) {
                            json_list.add(data_array.getJSONObject(i));
                        }
                    }
                }
                Log.e(TAG, "Size of ObjectsList+++++++++++" + json_list.size());
                return json_list;
            }
            catch (Exception exception){
                Log.e(TAG, "Error in JsonArrayList+++++++++++" + exception.getMessage());
            }
        }
        return null;
    }

    public static List<JSONObject> getJSONObjectDataList(JSONObject response){
        if(response!=null) {
            List<JSONObject> jsonList = new ArrayList<>();
            try {
                Iterator<String> keys = response.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (response.get(key) instanceof JSONArray) {
                        JSONArray data_array = response.getJSONArray(key);
                        for (int i = 0; i < data_array.length(); i++) {
                            jsonList.add(data_array.getJSONObject(i));
                        }
                    }
                }
                Log.e(TAG, "Size of JSONObjects List+++++++++++" + jsonList.size());
                return jsonList;
            }
            catch (Exception exception){
                Log.e(TAG, "Error in JsonArrayList+++++++++++" + exception.getMessage());
            }
        }
        return null;
    }

    public static List<JSONObject> getJSONObjectDataList(JSONArray jsonArray){

        List<JSONObject> jsonList = new ArrayList<>();
        if(jsonArray!=null && jsonArray.length()>0) {
            try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonList.add(jsonArray.getJSONObject(i));
                }
                Log.e(TAG, "Size of JSONObjects List+++++++++++" + jsonList.size());
                return jsonList;
            }
            catch (Exception exception){
                Log.e(TAG, "Error in JsonArrayList+++++++++++" + exception.getMessage());
            }
        }
        return null;
    }

    public static List convertJsonArrayToList(JSONArray jsonArray){
        List<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++){
            try {
                list.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static ArrayList<String> convertObjectToArrayList(Object object){
        ArrayList<String> list = new ArrayList<String>();
        try {
            if(object instanceof JSONArray) {
               JSONArray jArray = (JSONArray) object;
               if (jArray != null && jArray.length()>0) {
                   for (int i = 0; i < jArray.length(); i++) {

                           list.add(jArray.get(i).toString());

                   }
               }
           }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static ArrayList<String> convertJsonArrayToArrayList(JSONArray jsonArray){
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++){
            try {
                list.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static ArrayList<Object> convertJsonArrayToArrayObject(JSONArray jsonArray){
        ArrayList<Object> list = new ArrayList<Object>();
        for(int i = 0; i < jsonArray.length(); i++){
            try {
                list.add(jsonArray.get(i).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }




}
