package com.example.admin.daveai.database.models;

/**
 * Created by soham on 14/9/18.
 */

public class ModelTableRowModel {

   // public long id;
    public String model_type = "";
    public String model_name = "";
    public String data_cached = "";
    public long last_synced;
    public String error_message = "";



    //insert model
    public ModelTableRowModel(String model_type, String model_name, String data_cached, long last_synced) {
        this.model_type = model_type;
        this.model_name = model_name;
        this.data_cached = data_cached;
        this.last_synced = last_synced;

    }


    //update model
    public ModelTableRowModel(String model_type, String model_name, String data_cached, long last_synced, String error_message) {
        this.model_type = model_type;
        this.model_name = model_name;
        this.data_cached = data_cached;
        this.last_synced = last_synced;
        this.error_message = error_message;
    }



}
