package com.example.admin.daveai.broadcasting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;




public class DaveReceiver extends BroadcastReceiver {

    private final static String TAG = "DaveReceiver";
    //context and database helper object
    private Context context;
    private OnSmsReceivedListener listener = null;


    public interface OnSmsReceivedListener {
        public void onSmsReceived(Intent intent, String message);
    }

    public void setOnSmsReceivedListener(Context context) {
        this.listener = (OnSmsReceivedListener) context;
    }


    @Override
    public void onReceive(Context context, Intent intent) {

        this.context=context;
        Log.e(TAG, "On DAVE.AI Receiver: *********broadcast_msg************ "+ intent.getAction());
        Bundle bundle = intent.getExtras();
        if(bundle!= null){
            String messageContent = bundle.getString("broadcast_msg");
            assert messageContent != null;
            if (listener != null) {
                listener.onSmsReceived(intent, messageContent);
            }
        }

        //todo start service if network is Connected
       /* if(CheckNetworkConnection.networkHasConnection(context)) {
            new DaveService().startBackgroundService(context);

        }*/



    }



}

  /*  public void downloadAndUpdateApp(String appDownloadUrl){

        //Broadcast receiver for the download manager
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mContext.registerReceiver(downloadReceiver, filter);

        pDialog = new ProgressLoaderDialog(mContext);
        pDialog.setMessage("Please wait application is updating...");
        pDialog.setCancelable(false);
        pDialog.show();
        //start downloading the file using the download manager
        downloadManager = (DownloadManager)mContext.getSystemService(DOWNLOAD_SERVICE);
        Uri Download_Uri = Uri.parse(appDownloadUrl);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        // request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
        request.setAllowedOverRoaming(false);
        request.setTitle(R.string.app_name+" Download");
        request.setDestinationInExternalFilesDir(mContext, Environment.DIRECTORY_DOWNLOADS,"MyAndroidApp.apk");
        downloadReference = downloadManager.enqueue(request);
    }

    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (pDialog != null){
                pDialog.dismiss();
            }
            //check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if(downloadReference == referenceId){
                //start the installation of the latest version
                Uri uri = downloadManager.getUriForDownloadedFile(downloadReference);
                String filePath="";
                if ("content".equals(uri.getScheme())) {
                    Cursor cursor = mContext.getContentResolver().query(uri, new String[]
                            { android.provider.MediaStore.Images.ImageColumns.DATA }, null, null, null);
                    assert cursor != null;
                    cursor.moveToFirst();
                    filePath = cursor.getString(0);
                    cursor.close();
                    uri = Uri.fromFile(new File(filePath));
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    Uri fileUri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".provider", new File(filePath));
                    Intent intent1 = new Intent(Intent.ACTION_VIEW, fileUri);
                    intent1.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    intent1.setDataAndType(fileUri, downloadManager.getMimeTypeForDownloadedFile(downloadReference));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    mContext.startActivity(intent1);

                }else{
                    Intent intent2 = new Intent(Intent.ACTION_VIEW);
                    intent2.setDataAndType(uri, downloadManager.getMimeTypeForDownloadedFile(downloadReference));
                    intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent2);
                }
            }
        }
    };*/

