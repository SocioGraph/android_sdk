package com.example.admin.daveai.dialogbox;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.admin.daveai.R;


public class ProgressLoaderDialog {

    AlertDialog alertDialog;
    ProgressBar progressBar;
    TextView textViewLoaderMsg;

    Dialog dialog;
    Context context;

    public ProgressLoaderDialog(Context context){
        this.context = context;

    }

  /*  public void showProgressDialog(String loaderMsg) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService( Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_progress, null);
        AlertDialog.Builder  dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    public void hideProgressDialog(){
        alertDialog.dismiss();
    }*/

  /*  public AlertDialog showProgressDialog(String loaderMsg) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService( Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_progress, null);
        AlertDialog.Builder  dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        alertDialog = dialogBuilder.create();
        return alertDialog;
    }*/

    public Dialog progressDialog(){

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_progress);
        progressBar =  dialog.findViewById(R.id.progressBar);
        textViewLoaderMsg =  dialog.findViewById(R.id.progressBarMsg);

        return dialog;
    }

    public void setMessage(String message){
        textViewLoaderMsg.setText(message);
    }

    public void setStyle(){

    }





}
