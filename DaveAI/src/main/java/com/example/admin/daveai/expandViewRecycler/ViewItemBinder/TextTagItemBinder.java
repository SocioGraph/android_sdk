package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TagTextItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TextItem;

import org.json.JSONException;

import me.drakeet.multitype.ItemViewBinder;


public class TextTagItemBinder extends ItemViewBinder<TagTextItem, TextTagItemBinder.TextHolder> {


    @Override
    protected @NonNull
    TextTagItemBinder.TextHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_tag_text, parent, false);
        return new TextTagItemBinder.TextHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull TextTagItemBinder.TextHolder holder, @NonNull TagTextItem textItem) {

        //holder.key_value.setText(textItem.getText_value());
        holder.key_title.setText(textItem.getText_key());
        holder.key_value.setText("");
        try {
            for (int j = 0; j < textItem.getJsonArray().length(); j++){
                holder.key_value.append(textItem.getJsonArray().getString(j) + "\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull TextTagItemBinder.TextHolder holder) {
        holder.itemView.clearAnimation();
    }


    static class TextHolder extends RecyclerView.ViewHolder {

        private TextView key_title, key_value;

        TextHolder(@NonNull View itemView) {
            super(itemView);
            this.key_title = itemView.findViewById(R.id.key_title);
            this.key_value = itemView.findViewById(R.id.key_value);
        }
    }
}

