package com.example.admin.daveai.database;

/**
 * Created by soham on 12/9/18.
 */

public interface DatabaseConstants {


    String DATABASE_NAME = "dave_data_cache.db";
    String META_TABLE_NAME = "meta_table";
    String POST_QUEUE_TABLE_NAME = "post_queue_table";
    String INTERACTIONS_TABLE_NAME = "interactions_table";
    String SINGLETON_TABLE_NAME = "singleton_table";
    String MODEL_TABLE_NAME = "model_table";
    String MEDIA_TABLE_NAME = "media_table";
    String MEDIA_QUEUE_TABLE_NAME = "media_queue_table";
    String S3_UPLOAD_QUEUE_TABLE_NAME = "sthree_upload_queue_table";
    String INTERACTION_STAGES_TABLE_NAME = "interaction_stages_table";
    String RECOMMENDATION_TABLE_NAME = "recommendation_table";
    //String MODEL_UTIL_TABLE = "model_util_table";

    String TABLE_NAME = "table_name";
    String HOT_UPDATE = "hot_update";
    String COLD_UPDATE = "cold_update";
    String MODEL_ID_NAME = "model_id_name";
    String IS_DEFAULT = "is_default";
    String MASTER_TIMESTAMP = "master_timestamp";
    String OBJECT_ID = "object_id";
    String DATA_CACHED = "data_cached";
    String LAST_SYNCED = "last_synced";
    String MODEL_NAME = "model_name";
    String MODEL_TYPE = "model_type";
    String REQUEST_TYPE = "request_type";

    String API_URL = "api_url";
    String POST_ID = "post_id";
    String MEDIA_ID = "media_id";
    String API_METHOD = "api_method";
    String API_BODY = "api_body";
    String UPDATE_TABLE_NAME = "update_table_name";
    String UPDATE_ATTRIBUTE_NAME = "update_attribute_name";
    String UPDATE_TABLE_ROW_ID = "update_table_row_id";
    String STATUS = "status";
    String PRIORITY = "priority";
    String ON_ERROR = "on_error";
    String ERROR_MESSAGE = "error_message";
    String CACHE_TABLE_TYPE = "cache_table_type";
    String EXTRA_DATA = "extra_data";
    String URL = "url";
    String LOCAL_PATH = "local_path";
    String UPLOAD_ID = "upload_id";
    String UPLOAD_FILE_NAME = "upload_file_name";
    String UPLOAD_FOLDER_PATH = "upload_folder_path";
    String UPLOAD_BUCKET = "upload_bucket";
    String TYPE = "type";
    String ERROR_MESSAGE_TEXT = "Sync in Progress";

    //COMPARATORS
    String LIKE_OPERATOR = "LIKE";
    String NOT_LIKE_OPERATOR = "NOT LIKE";
    String EQUALS = "=";
    String NOT_EQUALS = "<>";
    String GREATER_THAN = ">";
    String LESS_THAN = "<";
    String TILDE = "~";
    String NOT_TILDE = "!~";




    String CREATE_POST_QUEUE_TABLE = "CREATE TABLE IF NOT EXISTS "+POST_QUEUE_TABLE_NAME+" (" +
            POST_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
            API_URL+" TEXT, " +
            API_METHOD+" TEXT, " + //POST, PATCH, GET, DELETE
            API_BODY+" TEXT, " +
            UPDATE_TABLE_NAME+" TEXT, " +
            UPDATE_TABLE_ROW_ID+" TEXT, " +
            STATUS+" TEXT, " +
            PRIORITY+" INT, " +
            CACHE_TABLE_TYPE+" INT, " +
            EXTRA_DATA+" TEXT  DEFAULT '', " +
            ON_ERROR+" TEXT" +
            ")";
    String CREATE_INTERACTIONS_TABLE = "CREATE TABLE IF NOT EXISTS "+INTERACTIONS_TABLE_NAME+" (" +
            OBJECT_ID+" TEXT PRIMARY KEY," +
            DATA_CACHED+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            ERROR_MESSAGE+" TEXT DEFAULT ''" +
            ")";
    String CREATE_OBJECTS_TABLE = "CREATE TABLE IF NOT EXISTS %s (" +
            OBJECT_ID+" TEXT PRIMARY KEY," +
            DATA_CACHED+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            ERROR_MESSAGE+" TEXT DEFAULT ''" +
            ")";
    String CREATE_RECOMMENDATION_TABLE = "CREATE TABLE IF NOT EXISTS %s (" +
            OBJECT_ID+" TEXT PRIMARY KEY," +
            DATA_CACHED+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            ERROR_MESSAGE+" TEXT DEFAULT ''" +
            ")";
    String CREATE_MODEL_TABLE = "CREATE TABLE IF NOT EXISTS "+MODEL_TABLE_NAME+" (" +
            MODEL_NAME+" TEXT PRIMARY KEY," +
            MODEL_TYPE+" TEXT," +
            DATA_CACHED+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            ERROR_MESSAGE+" TEXT DEFAULT ''" +
            ")";
    String CREATE_SINGLETON_TABLE = "CREATE TABLE IF NOT EXISTS "+SINGLETON_TABLE_NAME +"(" +
            MODEL_NAME+" TEXT PRIMARY KEY," +
            DATA_CACHED+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            REQUEST_TYPE+" TEXT," +
            ERROR_MESSAGE+" TEXT DEFAULT ''" +
            ")";
    String CREATE_META_TABLE = "CREATE TABLE IF NOT EXISTS "+META_TABLE_NAME+" (" +
            TABLE_NAME+" TEXT PRIMARY KEY," +
            HOT_UPDATE+" BIGINT," +
            COLD_UPDATE+" BIGINT," +
            MODEL_ID_NAME+" TEXT, " +
            IS_DEFAULT+" INT," +
            MASTER_TIMESTAMP+" BIGINT DEFAULT 0" +
            ")";



    String CREATE_MEDIA_TABLE = "CREATE TABLE IF NOT EXISTS "+MEDIA_TABLE_NAME+" (" +
            URL+" TEXT PRIMARY KEY," +
            LOCAL_PATH+" TEXT," +
            LAST_SYNCED+" BIGINT," +
            TYPE+" TEXT DEFAULT ''" +
            ")";

    String CREATE_S3_UPLOAD_QUEUE = "CREATE TABLE IF NOT EXISTS "+S3_UPLOAD_QUEUE_TABLE_NAME+" (" +

            UPLOAD_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
            LOCAL_PATH+" TEXT, " +
            UPLOAD_FILE_NAME + " TEXT, " +
            UPLOAD_FOLDER_PATH + " TEXT, " +
            UPLOAD_BUCKET + " TEXT" +
            ")";

    String CREATE_MEDIA_QUEUE_TABLE = "CREATE TABLE IF NOT EXISTS "+MEDIA_QUEUE_TABLE_NAME+" (" +
            POST_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
            URL+" TEXT, " +
            TYPE+" TEXT, " +
            UPDATE_TABLE_NAME+" TEXT, " +
            UPDATE_TABLE_ROW_ID+" TEXT, " +
            UPDATE_ATTRIBUTE_NAME+" TEXT, " +
            STATUS+" TEXT DEFAULT ''" +
            ")";



    enum CacheTableType{
        S3_UPLOAD_QUEUE (11), //single table
        MODEL_UTIL (10), //single table
        MEDIA_TABLE  (9),  //single table
        MEDIA_QUEUE_TABLE  (8),  //single table
        POST_QUEUE  (7),  //single table
        INTERACTIONS  (6),  //single table
        OBJECTS  (5),  //different tables for diff models
        MODEL  (4),  //different models in the same table
        RECOMMENDATION  (3),  //different tables created for each category
        META_DATA (2),  //different models in the same table
        SINGLETON (1),  //different models in the same table
        DEFAULT (0),  //different models in the same table
        ;


        private final int levelCode;

        private CacheTableType(int levelCode) {
            this.levelCode = levelCode;
        }

        public int getValue() {
            return levelCode;
        }
    }

    enum APIQueuePriority{
        FOUR (4),  //Alternate cases
        THREE  (3),  //Alternate cases
        TWO (2),  //GET
        ONE (1),  //POST DELETE PATCH
        LOGIN(0)
        ;


        private final int levelCode;

        private APIQueuePriority(int levelCode) {
            this.levelCode = levelCode;
        }

        public int getValue() {
            return levelCode;
        }
    }


    public enum CacheRefreshStatus{

        NO_META_FOUND  (4),  //invalid Meta Doesnt Exist
        HOT_UPDATE_REQUIRED  (3),  //calls constructor with value 3
        COLD_UPDATE_REQUIRED(2),  //calls constructor with value 2
        NOT_REQUIRED(1)   //calls constructor with value 1
        ; // semicolon needed when fields / methods follow


        private final int levelCode;

        private CacheRefreshStatus(int levelCode) {
            this.levelCode = levelCode;
        }
    }

    enum APIQueueStatus{

        PENDING("pending"),
        LAST_UPDATE("_last_update"),
        SUCCESS("success")
        ;
        private final String status;

        /**
         * @param text
         */
        APIQueueStatus(final String text) {
            this.status = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return status;
        }
    }
}
