package com.example.admin.daveai.broadcasting;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.InteractionsHelper;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.MediaTableRowModel;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.ConnectDaveAI;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.daveai.others.DaveHelper;
import com.example.admin.daveai.others.DaveModels;
import com.google.gson.Gson;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;



import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;

/**
 * Created by Rinki on 21/9/18.
 */
public class DaveService extends IntentService  implements  DatabaseConstants , DaveConstants {

    private  final String TAG = getClass().getSimpleName();
    private Context context;
    private DatabaseManager databaseManager;
    private int cacheTableType = 0 ;
    private String updateTableName = "";
    private String updateTableRowId ="";
    private String status ="";
    private String apiMethod ="";
    public static boolean isIntentServiceRunning = false;
    private Model modelInstance;
    private ModelUtil modelUtil;
    private DaveModels daveModels;
    private int priority =-1;
    private ConnectDaveAI connectDaveAI;
    private DaveAIPerspective daveAIPerspective;
    private Handler handler = new Handler();
    long count = 0;
    private Runnable runnable;
    String postBody = "";
    boolean isSyncDataRunning = true;


    public DaveService() {
        super(DaveService.class.getName());

    }

    //Start Dave service to post data
    public void startBackgroundService(Context mContext){
        if(CheckNetworkConnection.networkHasConnection(mContext)) {
            long queueSize = DatabaseManager.getInstance(context).forceUpdateQueueSize();
            Log.i(TAG," check Is service running >>>>>>>>>>>>>>>>>"+isIntentServiceRunning +" ApiQueueSize:- "+ queueSize);
            if(!isIntentServiceRunning && queueSize>0){
                Intent msgIntent = new Intent(mContext,DaveService.class);
                mContext.startService(msgIntent);
            }else {
                Log.i(TAG, "<<<<<<<<<<<<<<< Service already Running>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }

    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        //Log.e(TAG, "<<<<<<<<<<<<<<< Service starting>>>>>>>>>>>>>>>>>>>>>>>>");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        this.context = DaveService.this;
        isIntentServiceRunning = true;
        modelInstance = new Model(DaveService.this);
        modelUtil = new ModelUtil(DaveService.this);
        daveModels = new DaveModels(DaveService.this,true);
        databaseManager = DatabaseManager.getInstance(DaveService.this);
        connectDaveAI = new ConnectDaveAI(DaveService.this);
        daveAIPerspective = DaveAIPerspective.getInstance();
        count = databaseManager.forceUpdateQueueSize();
        //Log.e(TAG, "<<<<<<<<<<<<<<<<<<Service Started>>>>>>>>>>>>>>>>>>>>>>"+isIntentServiceRunning+" Queuecount:-"+ count);
        syncDataWithServer();
        do{
            try {
                synchronized (this) {
                    //for 1sec delay, use this "synchronized!" block,otherwise you will got error.
                    wait(1000);

                    //check queue size and sync Data running status
                    Cursor postQueueCursor = databaseManager.getDataFromTable(POST_QUEUE_TABLE_NAME);
                    if(postQueueCursor!=null) {
                        count = postQueueCursor.getCount();
                        postQueueCursor.close();

                        if(!isSyncDataRunning && count > 0 )
                            syncDataWithServer();
                    }
                    count = databaseManager.forceUpdateQueueSize();
                    if(!isSyncDataRunning && count > 0 )
                        syncDataWithServer();
                    Log.i(TAG," <<<<<<<<onHandleIntent  service is running Get PostQueue Count:------------ "+count);
                    if(!CheckNetworkConnection.networkHasConnection(DaveService.this)) {
                        this.stopSelf();
                    }
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(count > 0);
        this.stopSelf(); //after finishing the service call this for self destroy.


    }

    @Override
    public void onDestroy() {
        isIntentServiceRunning = false;
        super.onDestroy();
        Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<, BackGround Service Destroyed>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }

    private void syncDataWithServer(){
        isSyncDataRunning= true;
        try {

            isFilesToUpload = false;
            listOfFilesToUpload = new ArrayList<>();
            Cursor cursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1, PRIORITY);
            if (cursor.moveToFirst()) {
                int postId = cursor.getInt(cursor.getColumnIndex(POST_ID));
                String apiUrl = cursor.getString(cursor.getColumnIndex(API_URL));
                apiMethod = cursor.getString(cursor.getColumnIndex(API_METHOD));
                String apiBody = cursor.getString(cursor.getColumnIndex(API_BODY));
                updateTableName = cursor.getString(cursor.getColumnIndex(UPDATE_TABLE_NAME));
                updateTableRowId = cursor.getString(cursor.getColumnIndex(UPDATE_TABLE_ROW_ID));
                status = cursor.getString(cursor.getColumnIndex(STATUS));
                priority = cursor.getInt(cursor.getColumnIndex(PRIORITY));
                cacheTableType = cursor.getInt(cursor.getColumnIndex(CACHE_TABLE_TYPE));
                // String  onError =  cursor.getString(cursor.getColumnIndex(ON_ERROR));



                Log.e(TAG, "APIQueue Row Details:- 0.) postId:- "+ postId +"\n APIURL:---" + apiUrl + " \n 2.)MethodNAme:- " + apiMethod +
                        "\n  3.)updateTableName :- " + updateTableName+"  \n 4)apiBody:-"+apiBody);
                if(!TextUtils.isEmpty(apiBody)){
                    postBody = replaceLocalPathWithUrl(updateTableName,new JSONObject(apiBody));
                }

                if(isFilesToUpload){
                    if(listOfFilesToUpload.size()>0){
                        uploadFiles(listOfFilesToUpload.get(0), new OnImageUploadsComplete() {
                            @Override
                            public void onImagesUploaded() {
                                try {
                                    postBody = replaceLocalPathWithUrl(updateTableName, new JSONObject(apiBody));
                                    callAPI(postId, apiUrl, apiMethod, apiBody, postBody, status);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onImagesUploadError(int respCode, String errorMsg) {
                                processErrorInApiCall(respCode,errorMsg,postId, apiUrl, apiMethod, apiBody, postBody, status);
                            }

                        });
                    }
                }else {


                    callAPI(postId, apiUrl, apiMethod, apiBody, postBody, status);
                }
                cursor.close();
            }
        }catch (Exception e) {
             e.printStackTrace();
             Log.e(TAG,"Error syncDataWithServer:-------------  "+e.getMessage());
        }

    }


    interface OnImageUploadsComplete{
        void onImagesUploaded();
        void onImagesUploadError(int respCode, String errorMsg);
    }
    private void uploadFiles(String mediaPath, OnImageUploadsComplete onImageUploadsComplete){

        DaveModels daveModels = new DaveModels(DaveService.this,true);
        daveModels.uploadImageToServer(new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                Log.i(TAG,"uploaded file");
                try {
                    if(!TextUtils.isEmpty(mediaPath)&&!TextUtils.isEmpty(response.getString("path")))
                    databaseManager.insertMediaMapData(new MediaTableRowModel(response.getString("path"), mediaPath, System.currentTimeMillis(), "image"));
                }catch (Exception e){
                    e.printStackTrace();
                }
                listOfFilesToUpload.remove(0);
                if(listOfFilesToUpload.size()>0){
                    uploadFiles(listOfFilesToUpload.get(0),onImageUploadsComplete);
                }else{
                    onImageUploadsComplete.onImagesUploaded();
                }
            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
                onImageUploadsComplete.onImagesUploadError(requestCode,responseMsg);
            }
        },"file","image.png",mediaPath,null,null,false);
    }

    private void callAPI(int postId,String apiUrl,String apiMethod,String apiBody,String postBody,String status){
        RequestBody requestBody = RequestBody.create(MEDIA_TYPE_JSON, postBody);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                try {
                    //JSONObject jsonObject = new JSONObject(response);
                    //1.) update particular table

                    if(!apiMethod.equalsIgnoreCase(APIRequestMethod.DELETE.getMethod()))
                        updateParticularTable(response, "",status);

                    //2.) delete post queue row from post queue table
                    ArrayList<String> attrs = new ArrayList<>();
                    attrs.add(POST_ID);
                    databaseManager.deleteRow(
                            POST_QUEUE_TABLE_NAME,
                            databaseManager.createWhereClause(attrs),
                            new String[]{String.valueOf(postId)}
                    );
                    databaseManager.updateQueueSizeOnDelete();

                    //3.)make next queue call
                    Cursor cursor1 = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1,PRIORITY);
                    if (cursor1.getCount() > 0) {
                        syncDataWithServer();
                    }else{
                        isSyncDataRunning = false;
                    }
                    cursor1.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onTaskFailure(int responseCode, String errorMsg) {
                processErrorInApiCall(responseCode,errorMsg,postId, apiUrl, apiMethod, apiBody, postBody, status);

            }
        };

        if(updateTableName.equals("device_media_played")&&priority!=APIQueuePriority.FOUR.getValue()){
            ArrayList<String> attributes = new ArrayList<>();
            attributes.add(POST_ID);
            databaseManager.deleteRow(
                    POST_QUEUE_TABLE_NAME,
                    databaseManager.createWhereClause(attributes),
                    new String[]{String.valueOf(postId)}
            );

            /*JSONObject jsonObject = databaseManager.getObjectData(updateTableName,updateTableRowId);

            jsonObject.remove(ERROR_MSG_ATTRIBUTE);

            Log.e(TAG,"Sync error for Patch. Posting Object Again with body = "+DaveAIStatic.base_url +"/object/"+updateTableName);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(postId, apiUrl,
                            apiMethod,
                            jsonObject.toString(),
                            updateTableName, updateTableRowId, status,
                            APIQueuePriority.FOUR.getValue(),
                            cacheTableType, "")
            );*/
            Cursor cursor1 = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1,PRIORITY);
            if (cursor1.getCount() > 0) {
                syncDataWithServer();
            }else{
                isSyncDataRunning = false;
            }
            cursor1.close();
        }else {
            new APICallAsyncTask(postTaskListener, context, apiMethod, requestBody).execute(apiUrl);
        }
    }


    private void processErrorInApiCall(int responseCode, String errorMsg,int postId,String apiUrl,String apiMethod,String apiBody,String postBody,String status){
        Log.e(TAG,"processErrorForAPICall called for = "+apiUrl+"  error code = "+responseCode+" priority = "+priority);
        if (responseCode >= 400 && responseCode < 500) {
            // 401  403 authentication....  4XX - notify client,
            //update  particular table with response error
            //1.) update particular table
                updateParticularTable(apiBody, errorMsg,status);
                // 2.) delete row from api queue table
                ArrayList<String> attrs = new ArrayList<>();
                attrs.add(POST_ID);
                databaseManager.deleteRow(
                        POST_QUEUE_TABLE_NAME,
                        databaseManager.createWhereClause(attrs),
                        new String[]{String.valueOf(postId)}
                );

            if(errorMsg.contains("does not have any objects on ids")&&apiMethod.equals(APIRequestMethod.UPDATE.getMethod())){
                // 2.) delete row from api queue table and add post request in queue
                ArrayList<String> attributes = new ArrayList<>();
                attributes.add(POST_ID);
                databaseManager.deleteRow(
                        POST_QUEUE_TABLE_NAME,
                        databaseManager.createWhereClause(attributes),
                        new String[]{String.valueOf(postId)}
                );

                JSONObject jsonObject = databaseManager.getObjectData(updateTableName,updateTableRowId);
                databaseManager.updateDataCached(updateTableName, updateTableRowId, jsonObject, ERROR_MESSAGE_TEXT);

                jsonObject.remove(ERROR_MSG_ATTRIBUTE);

                Log.e(TAG,"Sync error for Patch. Posting Object Again with body = "+DaveAIStatic.base_url +"/object/"+updateTableName);
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(postId, DaveAIStatic.base_url +"/object/"+updateTableName,
                                APIRequestMethod.POST.getMethod(),
                                jsonObject.toString(),
                                updateTableName, updateTableRowId, status,
                                APIQueuePriority.LOGIN.getValue(),
                                cacheTableType, errorMsg)
                );

            }

            //3.)make next queue call
            Cursor cursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1, PRIORITY);
            Log.i(TAG, "****************On request code from 400 to 500 cursor count*************" + cursor.getCount());
            if (cursor.getCount() > 0)
                syncDataWithServer();
            else{
                isSyncDataRunning = false;
            }

        } else if (responseCode == 500) {
            //500 - notify admin and try again...internal server  error make priority:-3
            //update  API post Queue table with response error
            if(priority != APIQueuePriority.LOGIN.getValue()) {
                databaseManager.updateAPIQueueData(
                        new APIQueueTableRowModel(postId, apiUrl, apiMethod, apiBody, updateTableName, updateTableRowId, status,
                                APIQueuePriority.THREE.getValue(),
                                cacheTableType, errorMsg)
                );
            }

        } else if (responseCode > 500) {
            // 502,503,504 - try again
            // stop service and after that don't do anything
           // DaveService.this.stopSelf();
            //todo sleep thread for 10s keep checking it and retry it......
            try {
                Log.e(TAG,"Print code error *************************"+502);
                synchronized (this) {
                    //for 1sec delay, use this "synchronized!" block,otherwise you will got error.
                    wait(10000);
                    //try again same server call after 10s
                    Cursor cursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1, PRIORITY);
                    Log.i(TAG, "****************On request code from 400 to 500 cursor count*************" + cursor.getCount());
                    if (cursor.getCount() > 0)
                        syncDataWithServer();
                    else{
                        isSyncDataRunning = false;
                    }

                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }else if(responseCode == 10) {
            Log.i(TAG,"Time out >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+errorMsg+" == "+ responseCode);
            if(priority != APIQueuePriority.LOGIN.getValue()){
                databaseManager.updateAPIQueueData(
                        new APIQueueTableRowModel(postId, apiUrl, apiMethod, apiBody, updateTableName, updateTableRowId, status,
                                APIQueuePriority.FOUR.getValue(),
                                cacheTableType, errorMsg)
                );

            }
            //3.)make next queue call
            Cursor cursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME, new HashMap<>(), 1, PRIORITY);
            Log.i(TAG, "****************On request code from 400 to 500 cursor count*************" + cursor.getCount());
            if (cursor.getCount() > 0)
                syncDataWithServer();
            else{
                isSyncDataRunning = false;
            }
        }
        else {
            Log.i(TAG,"Else Api Other call error >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+errorMsg+" == "+ responseCode);
            if(priority != APIQueuePriority.LOGIN.getValue()){
                databaseManager.updateAPIQueueData(
                        new APIQueueTableRowModel(postId, apiUrl, apiMethod, apiBody, updateTableName, updateTableRowId, status,
                                APIQueuePriority.THREE.getValue(),
                                cacheTableType, errorMsg)
                );

            }
        }

    }



    private JSONObject mergeTwoJsonObjs(JSONObject jobj1, JSONObject jobj2){
        if(jobj1!=null&&jobj2!=null){
            Iterator<String> iter = jobj2.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    if(!jobj1.has(key)){
                        jobj1.put(key,jobj2.get(key));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jobj1;
    }


    private void updateParticularTable(String response, String errorMsg,String status){
        // response is Python String
        // jsonObject.toString() is Java String
        try {

            if (cacheTableType == CacheTableType.SINGLETON.getValue()) {
                JSONObject jsonObject = new JSONObject(response);
                Log.i(TAG, "************update SINGLETON table************" + updateTableName +" updateTableRowId:- "+updateTableRowId);
                connectDaveAI.setupModelCacheMetaData(updateTableName, updateTableRowId, DaveService.this);
                if(errorMsg!=null && !errorMsg.isEmpty())
                    jsonObject.put(ERROR_MSG_ATTRIBUTE,errorMsg);

                //updateTableName = <model_name>_<attr_name>    updatetablerowid = <pivot_min/max/only pivot>_<attribute name>
                databaseManager.insertOrUpdateSingleton(
                        updateTableName,
                        new SingletonTableRowModel(updateTableName, jsonObject.toString(),System.currentTimeMillis(),updateTableRowId,errorMsg)
                );
                if(updateTableName.equalsIgnoreCase("perspective_settings")||updateTableRowId.equalsIgnoreCase("perspective_settings")){
                    Log.e(TAG,"pers === "+response);
                    daveAIPerspective.savePerspectivePreferenceValue(DaveService.this,response);

                }
                if(updateTableRowId.startsWith("pivot")){
                    saveFilterAttr(response);
                }
            }
            else if (cacheTableType == CacheTableType.MODEL.getValue()) {
                // model_type; model_name; data_cached; last_synced; error_message;
                 List<String> CORE_MODEL_LIST = Arrays.asList("customer", "product", "interaction");
                 JSONObject modelResponse = new JSONObject();
                 String modelName = "";
                 String modelType = "";
                 if(CORE_MODEL_LIST.contains(updateTableName) && priority == APIQueuePriority.LOGIN.getValue()){
                     //here updateTableRowId is modelType
                     JSONArray modelJArray = new JSONArray(response);
                     modelResponse = modelJArray.getJSONObject(0);
                     modelName = modelResponse.getString("name");
                     modelType = modelResponse.getString("model_type");
                     String modelIDAttrName = modelUtil.saveIDAttrNameOfCoreModel(modelType,modelResponse.toString());
                     Log.i(TAG, "getCoreModel model name modelIDAttrName>>>>" + modelName +" ModelType:-- "+ modelType+ " modelIDAttrName:--" + modelIDAttrName);
                     if (!modelIDAttrName.isEmpty()) {
                         connectDaveAI.setupModelCacheMetaData(modelName, modelIDAttrName, context);
                     }
                     if(modelJArray.length()>0) {
                         databaseManager.insertORUpdateModelData(modelName,
                                 new ModelTableRowModel(modelType, modelName, modelResponse.toString(), System.currentTimeMillis(), errorMsg));


                     }

                 }else{
                     Log.i(TAG, "************update Model Table************" + updateTableName);
                     modelResponse = new JSONObject(response);
                     modelName = updateTableName;
                     modelType = modelResponse.getString("model_type");
                     String modelIDAttrName = modelUtil.getIDAttrNameOfModel(modelResponse.toString());
                     Log.e(TAG, "get model name && modelIDAttrName>>>>" + updateTableName +" ModelType:-- "+ modelType +  " modelIDAttrName:--" + modelIDAttrName);
                     if (!modelIDAttrName.isEmpty()) {
                         connectDaveAI.setupModelCacheMetaData(updateTableName, modelIDAttrName, context);
                     }
                     databaseManager.insertORUpdateModelData(updateTableName,
                             new ModelTableRowModel(modelType, updateTableName,
                                     modelResponse.toString(),
                                     System.currentTimeMillis(), errorMsg));
                 }
                 if(priority == APIQueuePriority.LOGIN.getValue() && !updateTableRowId.equalsIgnoreCase("login") &&
                         !updateTableRowId.equalsIgnoreCase(INTERACTION_STAGES_TABLE_NAME)){

                     connectDaveAI.getObjectsList(modelName,true,APIQueuePriority.LOGIN.getValue());
                     if( !modelType.isEmpty() && modelType.equalsIgnoreCase("customer")){
                         connectDaveAI.getCategoryHierarchyList(modelType,modelName,daveAIPerspective.getCustomer_category_hierarchy(),
                                 true,APIQueuePriority.LOGIN.getValue());
                         connectDaveAI.getFilterAttr(modelName,daveAIPerspective.getCustomer_filter_attr(),true,APIQueuePriority.LOGIN.getValue());
                     }
                     if( !modelType.isEmpty() && modelType.equalsIgnoreCase("product")){
                         connectDaveAI.getCategoryHierarchyList(modelType,modelName,daveAIPerspective.getProduct_category_hierarchy(),
                                 true,APIQueuePriority.LOGIN.getValue());
                         connectDaveAI.getFilterAttr(modelName,daveAIPerspective.getProduct_filter_attr(),true,APIQueuePriority.LOGIN.getValue());
                     }
                     if(!modelType.isEmpty() && modelType.equalsIgnoreCase("interaction")){
                         //todo set Interaction details interactionStageName, customerId, productId
                         new ModelUtil(context).saveInteractionModelAttributes(modelName);
                         if(daveAIPerspective.getInvoice_model_name()!=null && !daveAIPerspective.getInvoice_model_name().isEmpty()) {
                             connectDaveAI.getModel(daveAIPerspective.getInvoice_model_name(),true,APIQueuePriority.LOGIN.getValue());
                         }
                     }

                 }
            }
            else if (cacheTableType == CacheTableType.INTERACTIONS.getValue()) {
                InteractionsHelper interactionsHelper = new InteractionsHelper(context);
                interactionsHelper.addObjectsInInteractionsTable(response);

            }
            else if (cacheTableType == CacheTableType.RECOMMENDATION.getValue()) {
                Log.i(TAG, "************update RECOMMENDATION table************" + updateTableName);

            }
            else if (cacheTableType == CacheTableType.OBJECTS.getValue()) {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.has("data")) {
                    Log.i(TAG, "****update OBJECTS table***" + updateTableName + " && Object Details:-" + response);
                    if(updateTableName.equalsIgnoreCase(INTERACTION_STAGES_TABLE_NAME)){
                       connectDaveAI.saveInteractionStagesDetails(response);

                    }else {
                        // do pagination of objects only during login
                        //objectsPagination(response);
                        if(priority == APIQueuePriority.LOGIN.getValue()) {
                            int intNextPage = daveModels.checkPaging(response);
                            Log.i(TAG, "<<<<<<<<<Print check Paging FOr Get Objects:------------------" + updateTableName + " PageNumber>>> " + intNextPage);
                            if (intNextPage != -1) {
                                HashMap<String, Object> pagingParam = new HashMap<String, Object>();
                                pagingParam.put("_page_number", intNextPage);
                                pagingParam.put("_page_size", DaveHelper.getInstance().getObjectPageSize());
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.getObjectsAPI(updateTableName, pagingParam), DaveConstants.APIRequestMethod.GET.name(),
                                                "", updateTableName, "", APIQueueStatus.PENDING.name(),
                                                APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                                        )
                                );

                            }
                        }

                        CacheModelPOJO modelMetaData = databaseManager.getMetaData(updateTableName);
                        modelMetaData.master_timestamp = System.currentTimeMillis();
                        databaseManager.insertMetaData(modelMetaData);
                        List<JSONObject> getList = ObjectData.getJSONObjectDataList(response);
                        if (getList != null) {
                           for (int i = 0; i < getList.size(); i++) {

                               HashMap<String,Object> params = new HashMap<>();
                               params.put(OBJECT_ID,getList.get(i).getString(modelMetaData.model_id_name));

                               Cursor c = databaseManager.getDataFromTable(updateTableName,params,new HashMap<>(),"",0,"");
                               if(c.getCount() == 1){
                                   if(c.getColumnIndex(ERROR_MESSAGE) != -1){
                                       c.moveToFirst();
                                       if(!TextUtils.isEmpty(c.getString(c.getColumnIndex(ERROR_MESSAGE)))){

                                           JSONObject jobj1 = databaseManager.getObjectData(updateTableName, getList.get(i).getString(modelMetaData.model_id_name));

                                           JSONObject finalDataCached = mergeTwoJsonObjs(jobj1,getList.get(i));

                                           databaseManager.insertOrUpdateObjectData(
                                                   updateTableName,
                                                   new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                           finalDataCached.toString(),
                                                           System.currentTimeMillis(), errorMsg)

                                           );
                                       }else {
                                           databaseManager.insertOrUpdateObjectData(
                                                   updateTableName,
                                                   new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                           getList.get(i).toString(),
                                                           System.currentTimeMillis(), errorMsg)

                                           );
                                       }
                                   }
                               }else{

                                   databaseManager.insertOrUpdateObjectData(
                                           updateTableName,
                                           new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                   getList.get(i).toString(),
                                                   System.currentTimeMillis(), errorMsg)

                                   );
                               }

                           }
                        }
                        if(updateTableName.equalsIgnoreCase(modelUtil.getInteractionModelName())){
                            Log.e(TAG," Get interaction model NAme:--------Print updated Table name:-------------"+updateTableName);
                            connectDaveAI.setupModelCacheMetaData(INTERACTIONS_TABLE_NAME,modelInstance.getModelIDAttrName(updateTableName),context);
                            InteractionsHelper interactionsHelper = new InteractionsHelper(context);
                            interactionsHelper.addObjectsAsInteractions(jsonObject,null);

                        }
                    }
                }else {
                    if (cacheTableType == CacheTableType.SINGLETON.getValue()) {
                        databaseManager.createTable(updateTableName,CacheTableType.SINGLETON);
                    }else if (cacheTableType == CacheTableType.OBJECTS.getValue()) {
                        databaseManager.createTable(updateTableName,CacheTableType.OBJECTS);
                    }else if (cacheTableType == CacheTableType.INTERACTIONS.getValue()) {
                        databaseManager.createTable(updateTableName,CacheTableType.INTERACTIONS);
                    }else if (cacheTableType == CacheTableType.MODEL.getValue()) {
                        databaseManager.createTable(updateTableName,CacheTableType.MODEL);
                    }
                    //Log.i(TAG, "****update Single Object of OBJECTS table***" + updateTableName + " && Object Details:-" + response);

                    /*if(jsonObject.has(ERROR_MSG_ATTRIBUTE))
                        jsonObject.remove(ERROR_MSG_ATTRIBUTE);

                    if(errorMsg!= null && !errorMsg.isEmpty())
                        jsonObject.put(ERROR_MSG_ATTRIBUTE,errorMsg);

                    databaseManager.insertOrUpdateObjectData(
                            updateTableName,
                            new ObjectsTableRowModel(updateTableRowId,
                                   jsonObject.toString(),
                                    System.currentTimeMillis(), errorMsg)

                    );*/

                    databaseManager.updateDataCached(updateTableName, updateTableRowId, jsonObject, errorMsg);
                    //convert interaction object to interactions obj
                    if(updateTableName.equalsIgnoreCase(modelUtil.getInteractionModelName())){
                        JSONObject inteactionObj = databaseManager.getObjectData(updateTableName, jsonObject.getString(modelUtil.getInteractionIdAttrName()));
                       // Log.e(TAG," Add object in Interactions TAble:-------------"+updateTableName +""+ inteactionObj);
                        InteractionsHelper interactionsHelper = new InteractionsHelper(context);
                        interactionsHelper.addObjectInInteractionsTable(inteactionObj);

                    }

                }

            }
            else {
                Log.d(TAG, "****************Don't Update Any Table**************************");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error  Update db after syncDataWithServer:-------------  "+e.getMessage());
        }



    }

    private void objectsPagination(String response ){
        if(priority == APIQueuePriority.LOGIN.getValue()) {
            int intNextPage = daveModels.checkPaging(response);
            Log.i(TAG, "<<<<<<<<<Print check Paging FOr Get Objects:------------------" + updateTableName + " PageNumber>>> " + intNextPage);
            if (intNextPage != -1) {
                HashMap<String, Object> pagingParam = new HashMap<String, Object>();
                pagingParam.put("_page_number", intNextPage);
                pagingParam.put("_page_size", DaveHelper.getInstance().getObjectPageSize());
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.getObjectsAPI(updateTableName, pagingParam), DaveConstants.APIRequestMethod.GET.name(),
                                "", updateTableName, "", APIQueueStatus.PENDING.name(),
                                APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                        )
                );

            }
        }

    }

    private void saveFilterAttr(String response ){
        if(response!= null) {
            try {
                JSONObject pivotResponse = new JSONObject(response);
                Iterator<String> keys = pivotResponse.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (pivotResponse.get(key) instanceof JSONObject) {
                        if(updateTableRowId.startsWith("pivot_min_")){
                            String[] parts = updateTableRowId.split("pivot_min_");
                            Log.e(TAG,"Print pivot attr  pivot_min_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                            double d = pivotResponse.getJSONObject(key).getDouble(parts[1]);
                            int price = (int) d;

                           /* JSONArray jsonArray = new JSONArray();
                            jsonArray.put(price);
                            checkModelNameOfPivot(parts[1], jsonArray);*/

                            insertOrUpdateFilterMinMaxValue(updateTableName,parts[1],price,0);


                        }else if(updateTableRowId.startsWith("pivot_max_")){
                            String[] parts = updateTableRowId.split("pivot_max_");
                            Log.d(TAG,"Print pivot attr  pivot_max_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                            double d = pivotResponse.getJSONObject(key).getDouble(parts[1]);
                            int price = (int) d;

                           /* JSONArray jsonArray = new JSONArray();
                            jsonArray.put(price);
                            checkModelNameOfPivot(parts[1], jsonArray);*/

                            insertOrUpdateFilterMinMaxValue(updateTableName,parts[1],price,1);


                        }else if(updateTableRowId.startsWith("pivot_")){
                            String[] parts = updateTableRowId.split("pivot_");
                            Log.d(TAG,"Print pivot attr  pivot_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                            //checkModelNameOfPivot(parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse.getJSONObject(key)));
                            insertOrUpdateFilterValue(updateTableName,parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse.getJSONObject(key)));
                        }

                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Error in Customer Save Filter Attr+++++++++++ " + e.getMessage());
            }


        }

    }

    private void checkModelNameOfPivot(String key , JSONArray value ){

        String customerModelName = new ModelUtil(DaveService.this).getCustomerModelName();
        String productModelName = new ModelUtil(DaveService.this).getProductModelName();
        if(updateTableName.startsWith(productModelName+"_")){
            saveProductFilterAttr(key, value);

        }else if(updateTableName.startsWith(customerModelName+"_")){
            saveCustomerFilterAttr(key, value);
        }

    }

    private void insertOrUpdateFilterValue(String modelName, String key , JSONArray value ){
        try {
            connectDaveAI.setupModelCacheMetaData(modelName+"FilterList", modelName+"FilterList", DaveService.this);
            JSONObject productFil = databaseManager.getSingletonRow(modelName+"FilterList");
            Log.i(TAG," before update saveFilterAttr:------------"+value);

          /*
            if(productFil != null && productFil.length() >0 ){
                productFil.put(key,value);

            }else{
                productFil = new JSONObject();
                productFil.put(key,value);
            }*/

            if(productFil == null)
                productFil = new JSONObject();
            productFil.put(key,value);
            Log.i(TAG,"After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    modelName+"FilterList",
                    new SingletonTableRowModel(modelName+"FilterList",productFil.toString(),
                            System.currentTimeMillis(),modelName+"Pivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    private void insertOrUpdateFilterMinMaxValue(String modelName, String key , int value, int position){
        try {
            connectDaveAI.setupModelCacheMetaData(modelName+"FilterList", modelName+"FilterList", DaveService.this);
            JSONObject productFil = databaseManager.getSingletonRow(modelName+"FilterList");
            Log.i(TAG," before update saveFilterAttr:------------"+value);
            if(productFil != null && productFil.length() >0 ){
                if(productFil.has(key)) {
                    JSONArray priceRange = productFil.getJSONArray(key);
                    priceRange.put(position,value);
                    productFil.put(key, priceRange);
                }else {
                    productFil.put(key,value);
                }

            }else{
                productFil = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(position,value);
                productFil.put(key,jsonArray);
            }
            Log.i(TAG,"After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    modelName+"FilterList",
                    new SingletonTableRowModel(modelName+"FilterList",productFil.toString(),
                            System.currentTimeMillis(),modelName+"Pivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }



    private void saveProductFilterAttr(String key , JSONArray value ){
        try {
            connectDaveAI.setupModelCacheMetaData("productFilterList", "productFilterList", DaveService.this);
            JSONObject productFil = databaseManager.getSingletonRow("productFilterList");
            Log.i(TAG," before update saveProductFilterAttr:------------"+value);
            if(productFil != null && productFil.length() >0 ){
                if(productFil.has(key)) {
                    JSONArray priceRange = productFil.getJSONArray(key);
                    for(int i = 0; i<value.length();i++)
                        priceRange.put(value.get(i));
                    productFil.put(key, priceRange);
                }else {
                    productFil.put(key,value);
                }

            }else{
                productFil = new JSONObject();
                productFil.put(key,value);
            }
            Log.i(TAG,"After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    "productFilterList",
                    new SingletonTableRowModel("productFilterList",productFil.toString(),
                            System.currentTimeMillis(),"productPivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void saveCustomerFilterAttr(String key , JSONArray value ){
        try {
            connectDaveAI.setupModelCacheMetaData("customerFilterList", "customerFilterList", DaveService.this);
            JSONObject productFil = databaseManager.getSingletonRow("customerFilterList");
            Log.i(TAG," before update sproductFil:------------"+productFil);
            if(productFil != null && productFil.length() >0 ){
                if(productFil.has(key)) {
                    JSONArray priceRange = productFil.getJSONArray(key);
                    for(int i = 0; i<value.length();i++)
                        priceRange.put(value.get(i));
                    productFil.put(key, priceRange);
                }else {
                    productFil.put(key,value);
                }

            }else{
                productFil = new JSONObject();
                productFil.put(key,value);
            }
            Log.i(TAG,"After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    "customerFilterList",
                    new SingletonTableRowModel("customerFilterList",productFil.toString(),
                            System.currentTimeMillis(),"customerPivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    boolean isFilesToUpload = true;
    ArrayList<String> listOfFilesToUpload = new ArrayList<>();

    private String replaceLocalPathWithUrl(String modelName, JSONObject body){
        //inserting data in the media map table
        String newBody = body.toString();
        if(!TextUtils.isEmpty(newBody)) {
            try {
                JSONArray cachedAttrs = new JSONArray();
                List<String> modelMediaCache = new ArrayList<>();
                Model mod = new Model(DaveService.this);
                JSONObject modelJsn = databaseManager.getModelData(modelName);
                Gson gson = new Gson();
                mod = gson.fromJson(modelJsn.toString(), Model.class);

                cachedAttrs = modelJsn.getJSONObject("quick_views").getJSONArray("cached");
                modelMediaCache = mod.getQuick_views().getCached();

                for (int i = 0; i < modelMediaCache.size(); i++) {
//                    Log.e("LOGGER", "model_mediaCache attr = " + modelMediaCache.get(i));

                    if (body.has(modelMediaCache.get(i))) {

                        ArrayList<String> mediaLocalPath = new ArrayList<>();
                        if (mod.getAttributeType(modelMediaCache.get(i)).equals("image") || mod.getAttributeType(modelMediaCache.get(i)).equals("video") || mod.getAttributeType(modelMediaCache.get(i)).equals("file") || mod.getAttributeType(modelMediaCache.get(i)).equals("apk") || mod.getAttributeType(modelMediaCache.get(i)).equals("url")) {
                            if(!body.getString(modelMediaCache.get(i)).startsWith("http"))
                            mediaLocalPath.add(body.getString(modelMediaCache.get(i)));
                        } else if (mod.getAttributeType(modelMediaCache.get(i)).equals("list")) {
                            JSONArray jarr = body.getJSONArray(modelMediaCache.get(i));
                            for (int x = 0; x < jarr.length(); x++) {
                                if(!jarr.getString(x).startsWith("http"))
                                mediaLocalPath.add(jarr.getString(x));
                            }
                        } else if (mod.getAttributeType(modelMediaCache.get(i)).equals("media_map") || mod.getAttributeType(modelMediaCache.get(i)).equals("nested_object")) {
                            JSONObject json = new JSONObject();
                            json = body.getJSONObject(modelMediaCache.get(i));
                            Iterator<String> iter = json.keys();
                            while (iter.hasNext()) {
                                String key = iter.next();
                                try {
                                    String value = json.getString(key);
                                    if(!value.startsWith("http"))
                                    mediaLocalPath.add(value);
                                } catch (JSONException e) {
                                    // Something went wrong!
                                }
                            }
                        } else if (mod.getAttributeType(modelMediaCache.get(i)).equals("image_object")) {
                            JSONArray json = new JSONArray();
                            json = body.getJSONArray(modelMediaCache.get(i));
                            for (int l = 0; l < json.length(); l++) {
                                JSONObject jsobj = json.getJSONObject(l);
                                if (jsobj.has("image")) {
                                    if(!jsobj.getString("image").startsWith("http")){
                                        mediaLocalPath.add(jsobj.getString("image"));
                                    }

                                    if (jsobj.has("thumbnail")) {
                                        if(!jsobj.getString("thumbnail").startsWith("http")){
                                            mediaLocalPath.add(jsobj.getString("thumbnail"));
                                        }
                                    }
                                }
                            }
                        }
                        Log.e(TAG,"replacing paths for medialocalpath.size = "+mediaLocalPath.size());
                        for (int l = 0; l < mediaLocalPath.size(); l++) {

                            HashMap<String, Object> param = new HashMap<>();
                            param.put(LOCAL_PATH, mediaLocalPath.get(l));
                            Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                            String oldBody = body.toString();

                            Log.e(TAG,"found local path in table for "+mediaLocalPath.get(l)+" count = "+imagedata.getCount());
                            if (imagedata.getCount() > 0) {
                                imagedata.moveToFirst();
                                String url = imagedata.getString(imagedata.getColumnIndex(URL));

                                Log.e(TAG,"found url table for "+mediaLocalPath.get(l)+" url = "+url);
                                Log.e(TAG," oldBody.contains(mediaLocalPath.get(l)) = "+oldBody.contains(mediaLocalPath.get(l)));
                                Log.e(TAG," oldBody = "+oldBody);
                                JSONObject jobj = new JSONObject();
                                jobj.put("asdf", mediaLocalPath.get(l));
                                String LOCAL_URL = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);

                                while (oldBody.contains(LOCAL_URL)) {
                                    oldBody = oldBody.replace(LOCAL_URL, url);
                                }
//                                oldBody = newBody;
                                newBody = oldBody;
                                Log.e(TAG," newBody = "+oldBody);
                            }else{
                                isFilesToUpload = true;
                                listOfFilesToUpload.add(mediaLocalPath.get(l));
                            }

                        }

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return newBody;
    }

}







