package com.example.admin.daveai.model;


import android.content.Context;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.List;

public class Attribute {

    private String atype;
    //  private Object atype;
    private boolean auto_updater;
    private Converter converter;
    @SerializedName("default")
    // private Default defaultX;
    private Object defaultX;
    // private boolean force_update;
    private Object force_update;
    private boolean id;
    private String list_type;
    private String name;
    private boolean option_others;
    private String placeholder;
    private boolean required;
    private String title;
    private String type;
    private String ui_element;
    private boolean unique;
    private float weight;
    private Jsoner jsoner;
    private Constraint constraint;


    // private List<?> conversation;
    private Object conversation;

    // private List<?> options;
    // private List<String> options;
    private Object options;

    // private String dict_keys;
    // private List<String> dict_keys;
    private Object dict_keys;

    // private List<?> verbs;
    private Object verbs;

    // @SerializedName("defaulty")
    // private List<?> defaultY;

    private String refers;
    // @SerializedName("defaultz")
    private Object defaultZ;

    // private View viewElement;



    /* boolean isValid(){
        if(this.required == true){

        }
        return true;
    }

    Object getValue(){

    }*/


    /**
     * conversation : []
     * options : /unique/product/product_id
     * verbs : []
     */
    // "adder:- Dict","units:-unicode","sequence:-int","conversation:-dict","section:-unicode"

    public String getAtype() {
        return atype;
    }

    public void setAtype(String atype) {
        this.atype = atype;
    }

   /* public Object getAtype() {
        return atype;
    }

    public void setAtype(Object atype) {
        this.atype = atype;
    }*/

    public boolean isAuto_updater() {
        return auto_updater;
    }

    public void setAuto_updater(boolean auto_updater) {
        this.auto_updater = auto_updater;
    }

    public Converter getConverter() {
        return converter;
    }

    public void setConverter(Converter converter) {
        this.converter = converter;
    }



   /* public Default getDefaultX() {
        return defaultX;
    }

    public void setDefaultX(Default defaultX) {
        this.defaultX = defaultX;
    }*/

    public Object getDefaultX() {
        return defaultX;
    }

    public void setDefaultX(Object defaultX) {
        this.defaultX = defaultX;
    }



    public Object isForce_update() {
        return force_update;
    }

    public void setForce_update(Object force_update) {
        this.force_update = force_update;
    }

    public boolean isId() {
        return id;
    }

    public void setId(boolean id) {
        this.id = id;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOption_others() {
        return option_others;
    }

    public void setOption_others(boolean option_others) {
        this.option_others = option_others;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUi_element() {
        return ui_element;
    }

    public void setUi_element(String ui_element) {
        this.ui_element = ui_element;
    }

    public boolean isUnique() {
        return unique;
    }

    public void setUnique(boolean unique) {
        this.unique = unique;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Jsoner getJsoner() {
        return jsoner;
    }

    public void setJsoner(Jsoner jsoner) {
        this.jsoner = jsoner;
    }

    public Constraint getConstraint() {
        return constraint;
    }

    public void setConstraint(Constraint constraint) {
        this.constraint = constraint;
    }

  /*  public List<?> getConversation() {
        return conversation;
    }

    public void setConversation(List<?> conversation) {
        this.conversation = conversation;
    }*/

    public Object getConversation() {
        return conversation;
    }

    public void setConversation(Object conversation) {
        this.conversation = conversation;
    }

    public Object getOptions() {
        return options;
    }

    public void setOptions(Object options) {
        this.options = options;
    }

    public Object getDict_keys() {
        return dict_keys;
    }

    public void setDict_keys(Object dict_keys) {
        this.dict_keys = dict_keys;
    }

/*   public List<String> getDict_keys() {
        return dict_keys;
    }

    public void setDict_keys(List<String> dict_keys) {
        this.dict_keys = dict_keys;
    }*/

  /*  public List<?> getVerbs() {
        return verbs;
    }

    public void setVerbs(List<?> verbs) {
        this.verbs = verbs;
    }*/

    public Object getVerbs() {
        return verbs;
    }

    public void setVerbs(Object verbs) {
        this.verbs = verbs;
    }

 /*   public List<?> getDefaultY() {
        return defaultY;
    }
    public void setDefaultY(List<?> defaultY) {
        this.defaultY = defaultY;
    }*/

    public String getRefers() {
        return refers;
    }

    public void setRefers(String refers) {
        this.refers = refers;
    }

    public Object getDefaultZ() {
        return defaultZ;
    }

    public void setDefaultZ(Object defaultZ) {
        this.defaultZ = defaultZ;
    }

    public static class Converter {

        private String function;

        public String getFunction() {
            return function;
        }

        public void setFunction(String function) {
            this.function = function;
        }
    }

    public static class Default {

        private String function;
        private List<String> args;

        public String getFunction() {
            return function;
        }

        public void setFunction(String function) {
            this.function = function;
        }

        public List<String> getArgs() {
            return args;
        }

        public void setArgs(List<String> args) {
            this.args = args;
        }
    }

    public static class Jsoner {

        private String function;

        public String getFunction() {
            return function;
        }

        public void setFunction(String function) {
            this.function = function;
        }
    }

    public static class Constraint {

        private String function;

        public String getFunction() {
            return function;
        }

        public void setFunction(String function) {
            this.function = function;
        }
    }


    @Override
    public String toString()
    {
        return "AttributePOJO [type = "+type+", title = "+title+", ui_element = "+ui_element+", name = "+name+"]";
    }

    public Object get(String name) throws Exception {
        Field field = this.getClass().getDeclaredField(name);
        field.setAccessible(true);
        return field.get(this);
    }
}


