package com.example.admin.daveai.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;

import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.adapter.ViewPagerAdapter;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.InteractionsHelper;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.DataConversion;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.dialogbox.DateTimePickerDialog;
import com.example.admin.daveai.dynamicView.SearchableDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.others.DaveAI;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.example.admin.daveai.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.RequestBody;

import mabbas007.tagsedittext.TagsEditText;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.M;
import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;


public class DynamicForm extends Fragment implements DaveConstants {

    private Context context;
    private static final String TAG = "DynamicForm";
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final String MODEL_NAME = "modelName";
    private static final String METHOD_NAME = "methodName";
    private static final String BUTTON_NAME = "buttonName";
    private static final String TITLE = "title";
    private static final String SEQUENCE_LIST = "sequenceList";
    private static final String OBJECT_ID_ATTR_NAME = "objectId";
    private static final String OBJECT_DETAILS = "objectDetails";
    private static final String REQUEST_TYPE = "request_type";
    private static final String IS_SINGLETON = "is_singleton";
    private static final String OBJECT_STATUS = "object_status";

    private String title;
    private String buttonName;
    private String methodName;
    private String modelName;
    private ArrayList<String> sequenceList;
    private String objectId;
    private String objectDetails;
    LinearLayout addViewLayout;
    Button buttonAction;
    TextView textTitleView;
    HashMap<String, Boolean> fieldValidationMap = new HashMap<>();
    private JSONObject storeObjectDetails = new JSONObject();
    private boolean isSingleton = false;
    private String requestType= "";
    private  JSONObject jsonObjectUserAndCurrentModel = new JSONObject();
    private boolean isCreateNew = false;

    //todo form optionlist with query param
    // todo options/distributor/distributor_name?area_manager_id={area_manager_id}
    // get user singleton , add all attr value of current model
    // during click on option list replace query param value with actual value of that attr then make server call


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private OnCreateDynamicFormListener mListener;
    public interface OnCreateDynamicFormListener {
        void onCreateOrUpdateView(String objectDetails);
        void onFailureCreateOrUpdate(String errorMsg);
    }

    public DynamicForm() {
    }

    public static DynamicForm createFormView(String title, String buttonName,String methodName,String modelName, ArrayList<String> sequenceList){


        return  newInstance(title,buttonName,methodName,modelName,sequenceList,"","");
    }


    public static DynamicForm updateFormView(String title,String buttonName,String methodName,String modelName, ArrayList<String> sequenceList,
                                             String objectDetails) {
        return newInstance(title,buttonName,methodName,modelName,sequenceList,"",objectDetails);
    }


    public static DynamicForm updateFormView(String title,String buttonName,String methodName,String modelName, ArrayList<String> sequenceList,
                                             String objectId, String objectDetails) {
        return newInstance(title,buttonName,methodName,modelName,sequenceList,objectId,objectDetails);
    }

    public static DynamicForm updateSingletonView(String title,String buttonName,String methodName,String modelName,String requestType,
                                             ArrayList<String> sequenceList,String objectDetails) {

        DynamicForm fragment = new DynamicForm();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(BUTTON_NAME, buttonName);
        args.putString(METHOD_NAME, methodName);
        args.putString(MODEL_NAME, modelName);
        args.putString(REQUEST_TYPE, requestType);
        args.putStringArrayList(SEQUENCE_LIST,sequenceList);
        args.putString(OBJECT_DETAILS, objectDetails);
        args.putBoolean(IS_SINGLETON, true);
        fragment.setArguments(args);
        return fragment;


    }

    public static DynamicForm newInstance(String title,String buttonName,String methodName,String modelName,
                                          ArrayList<String> sequenceList,String objectId, String objectDetails) {
        DynamicForm fragment = new DynamicForm();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(BUTTON_NAME, buttonName);
        args.putString(METHOD_NAME, methodName);
        args.putString(MODEL_NAME, modelName);
        args.putStringArrayList(SEQUENCE_LIST,sequenceList);
        args.putString(OBJECT_ID_ATTR_NAME, objectId);
        args.putString(OBJECT_DETAILS, objectDetails);
       // args.putString(OBJECT_STATUS,"post");
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            title = getArguments().getString(TITLE);
            buttonName = getArguments().getString(BUTTON_NAME);
            methodName = getArguments().getString(METHOD_NAME);
            modelName = getArguments().getString(MODEL_NAME);
            sequenceList = getArguments().getStringArrayList(SEQUENCE_LIST);
            if(getArguments().containsKey(OBJECT_ID_ATTR_NAME))
                objectId = getArguments().getString(OBJECT_ID_ATTR_NAME);
            if(getArguments().containsKey(OBJECT_DETAILS))
                objectDetails = getArguments().getString(OBJECT_DETAILS);

            if(getArguments().containsKey(REQUEST_TYPE))
                requestType = getArguments().getString(REQUEST_TYPE);

            if(getArguments().containsKey(IS_SINGLETON))
                isSingleton = getArguments().getBoolean(IS_SINGLETON);
        }
    }



    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment_dynamic_form, container, false);
        context = getActivity();
        textTitleView =  mainView.findViewById(R.id.textTitleView);
        addViewLayout =  mainView.findViewById(R.id.addViewLayout);
        buttonAction =  mainView.findViewById(R.id.buttonAction);
        buttonAction.setText(buttonName);

        if(methodName.equals("UPDATE")){
            textTitleView.setVisibility(View.VISIBLE);
            if(title.isEmpty() || title==null)
                textTitleView.setText("Edit Profile");
            else
                textTitleView.setText(title);
        }

        createDynamicForm(new OnFormViewCreatedListener() {
            @Override
            public void onFormVewCreated(HashMap<String, Boolean> fieldRequiredMap) {


            }
        },getActivity(), addViewLayout, modelName, sequenceList, objectDetails);

        return mainView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                InputMethodManager imm = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
                assert imm != null;
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                JSONObject  objectDetail = methodValidateDynamicView(addViewLayout,fieldValidationMap,objectDetails);



                    try {
                        ModelUtil modelUtil = new ModelUtil(context);
                        if (modelUtil.getCustomerModelName().equalsIgnoreCase(modelName)) {
                            DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
                            if ((!TextUtils.isEmpty(daveAIPerspective.getCustomer_card_header()) || !daveAIPerspective.getCustomer_card_header().equalsIgnoreCase("_NULL_") || !daveAIPerspective.getCustomer_card_header().equalsIgnoreCase("__NULL__"))
                                    &&
                                    (!TextUtils.isEmpty(daveAIPerspective.getCustomer_mobile_attr()) || !daveAIPerspective.getCustomer_mobile_attr().equalsIgnoreCase("_NULL_") || !daveAIPerspective.getCustomer_mobile_attr().equalsIgnoreCase("__NULL__"))) {
                                String nameAttrName = daveAIPerspective.getCustomer_card_header();
                                String contactAttrName = daveAIPerspective.getCustomer_mobile_attr();

                                Log.e(TAG, "button clicked for name attr =" + nameAttrName );
                                Log.e(TAG, "button clicked for contact # =" + contactAttrName );
                                Log.e(TAG, "button clicked for json  =" + objectDetail);
                                if(objectDetail.has(nameAttrName)&&objectDetail.has(contactAttrName)) {
                                    Log.e(TAG, "button clicked for name attr =" + nameAttrName + " = " + objectDetail.getString(nameAttrName));
                                    Log.e(TAG, "button clicked for contact # =" + contactAttrName + " = " + objectDetail.getString(contactAttrName));

                                    modelUtil.addToContacts(objectDetail.getString(nameAttrName), objectDetail.getString(contactAttrName));
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                Log.i(TAG,"Click on button Action:--------------"+objectDetail);
                if(objectDetail!= null){
                   // postObjectToServer(objectDetail);
                    checkToCallAPI(objectDetail);
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCreateDynamicFormListener) {
            mListener = (OnCreateDynamicFormListener) context;
            this.context= context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnCreateDynamicFormListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void checkToCallAPI(JSONObject objectDetail){ //post,update

        try {
            Log.e(TAG, "postOrUpdateObjectToServer  Method >>>>> " + methodName+" Model name:-"+modelName +" isSingleton"+isSingleton
                    +"\n objectDetail"+objectDetail);
            if (modelName != null && !modelName.isEmpty()) {
                if(objectDetail.has(ERROR_MSG_ATTRIBUTE))
                    objectDetail.put(ERROR_MSG_ATTRIBUTE,JSONObject.NULL);
                if(isSingleton && methodName.equals(APIRequestMethod.UPDATE.name())){
                    Log.e(TAG, "postOrUpdate Singleton  Method >>>>> " + methodName+" Model name"+modelName);
                    postObjectToServer(objectDetail);
                }else {
                    if (methodName.equals(APIRequestMethod.POST.name())) { // post new object  online
                        postObjectOnline(objectDetail);

                    } else if (methodName.equals(APIRequestMethod.UPDATE.name())) {
                        postObjectToServer(objectDetail);
                    }
                }
            }
            else{
                Log.e(TAG, "<<<<Model Name is Null Or Empty>>>>>>>>>>>>>>" + modelName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void postObjectToServer(JSONObject objectDetail)  {
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                if(response!=null && response.length()> 0) {
                    if (mListener != null)
                        mListener.onCreateOrUpdateView(response.toString());
                }
                else
                    Toast.makeText(
                            getActivity(),
                            "Something is wrong with the server connection... please try after sometime",
                            Toast.LENGTH_SHORT).show();
                }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {

            }
        };
        try {
            DaveModels daveModels = new DaveModels(getActivity(),true);
            Log.e(TAG, "postOrUpdateObjectToServer  Method >>>>> " + methodName+" Model name:-"+modelName +" isSingleton"+isSingleton
                    +"\n objectDetail"+objectDetail);
            if (modelName != null && !modelName.isEmpty()) {
                if(objectDetail.has(ERROR_MSG_ATTRIBUTE))
                    objectDetail.put(ERROR_MSG_ATTRIBUTE,JSONObject.NULL);
                if(isSingleton && methodName.equals(APIRequestMethod.UPDATE.name())){
                    Log.e(TAG, "postOrUpdate Singleton  Method >>>>> " + methodName+" Model name"+modelName);
                    //todo update Singleton table
                    daveModels.updateSingleton(modelName,requestType,objectDetail,daveAIListener);
                }else {
                    if (methodName.equals(APIRequestMethod.POST.name())) { // create new object online
                        Log.e(TAG,"asdf postobject called");
                        daveModels.postObject(modelName, objectDetail, daveAIListener);

                    } else if (methodName.equals(APIRequestMethod.UPDATE.name())) {
                        Log.e(TAG,"asdf patchobject called");

                        daveModels.patchObject(true,modelName, objectDetail, daveAIListener);
                        //daveModels.patchAllObject(modelName,objectDetail,daveAIListener);
                    }
                }
            }
            else{
                Log.e(TAG, "<<<<Model Name is Null Or Empty>>>>>>>>>>>>>>" + modelName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void postObjectOnline(JSONObject objectDetail){

        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, objectDetail.toString());
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Log.e(TAG,"live post call");
                if(response!=null && response.length()> 0) {

                    if (mListener != null)
                        mListener.onCreateOrUpdateView(response);

                    //todo check model iD and save in Db
                    String modelId = new ModelUtil(getActivity()).getModelIDAttrName(modelName);
                    Log.e(TAG,"modelName = "+modelName);
                    Log.e(TAG,"modelId = "+modelId);
                    if(modelId!=null && !modelId.isEmpty()){
                        try {
                            DatabaseManager.getInstance(getActivity()).insertOrUpdateObjectData(
                                    modelName,
                                    new ObjectsTableRowModel(new JSONObject(response).getString(modelId),
                                            response,
                                            System.currentTimeMillis(), "")

                            );
                        }catch (Exception e){
                            e.printStackTrace();
                            Toast.makeText(
                                    getActivity(),
                                    e.getMessage(),
                                    Toast.LENGTH_SHORT).show();

                        }
                    }else {
                        Log.e(TAG,"Model id is null or empty.Object Detail doesn't save in local Db.Required id to save in Db. : "+ modelId);
                    }
                }
                else
                    Toast.makeText(
                            getActivity(),
                            "Something is wrong with the server connection... please try after sometime",
                            Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if(requestCode < 500) {
                    Toast.makeText(context, responseMsg, Toast.LENGTH_SHORT).show();
                }else {
                    postObjectToServer(objectDetail);
                }
            }
        };
        if (CheckNetworkConnection.networkHasConnection(getActivity())){
            new APICallAsyncTask(postTaskListener, getActivity(), "POST", body_part, true)
                    .execute(APIRoutes.postObjectAPI(modelName));
        }
        else{
            postObjectToServer(objectDetail);
        }


    }



    public interface OnFormViewCreatedListener {
        void onFormVewCreated( HashMap<String, Boolean> fieldRequiredMap);
    }

    /**
     * method to create multiple view based on ui_element of attributes and add it in parent linear layout
     * @param onFormViewCreatedListener pass listener to get required attrs map
     * @param context context
     * @param parentLayout linear parent layout name where add multiple view
     * @param modelName model name which jsonObject store in local Database
     * @param formSequenceList sequence of attribute which want to show
     * @param objectDetails object details if avaiable
     */
    public void createDynamicForm(OnFormViewCreatedListener onFormViewCreatedListener, Context context,ViewGroup parentLayout,String modelName,
                                  List formSequenceList, String objectDetails){

        this.context = context;
        try {
            Log.e(TAG, " \n \n  ****************createDynamicForm modelName:==  " + modelName +" ListSize:-" + formSequenceList.size() + " \n List:-" + formSequenceList
                    + "  \n objectDetails during edit:== " + objectDetails +"\n \n \n");
            DatabaseManager databaseManager = DatabaseManager.getInstance(getActivity());
            jsonObjectUserAndCurrentModel = databaseManager.getSingletonRow(DaveAIPerspective.getInstance().getUser_login_model_name());
            JSONObject modelResponse = databaseManager.getModelData(modelName);
            Model modelInstance = Model.getModelInstance(modelResponse.toString());
        /*    if(modelResponse== null || modelResponse.length() == 0) {
                DaveModels daveModels = new DaveModels(context, true);
                try {
                    daveModels.getModel(modelName, new DaveAIListener() {
                        @Override
                        public void onReceivedResponse(JSONObject response) {
                            try {
                                Model modelInstance =  Model.getModelInstance(response.toString());
                                if(formSequenceList!=null && formSequenceList.size()>0 && modelInstance!=null) {
                                    totalViewCount = formSequenceList.size();
                                    checkTypeOfUiElement(onFormViewCreatedListener,parentLayout, modelInstance, formSequenceList, objectDetails);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e(TAG, "Error During Show OBject Details :>>>>>>>>>>>>>>" + e.getLocalizedMessage());
                            }

                        }

                        @Override
                        public void onResponseFailure(int requestCode, String responseMsg) {

                        }
                    });
                } catch (DaveException e) {
                    e.printStackTrace();
                }

            }else{
                modelInstance = Model.getModelInstance(modelResponse.toString());
                if(formSequenceList!=null && formSequenceList.size()>0 && modelInstance!=null) {
                    totalViewCount = formSequenceList.size();
                    checkTypeOfUiElement(onFormViewCreatedListener,parentLayout, modelInstance, formSequenceList, objectDetails);
                }
            }*/
            if(formSequenceList!=null && formSequenceList.size()>0 && modelInstance!=null) {
                viewCount =0;
                totalViewCount = formSequenceList.size();
                checkTypeOfUiElement(onFormViewCreatedListener,parentLayout, modelInstance, formSequenceList, objectDetails);
            }else
                Log.e(TAG,"Print formSequenceList:- "+formSequenceList+" modelInstance:- "+modelInstance);
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error  CreateDynamic Form :>>>>>>>>>>>>>>" + e.getMessage());
        }

    }

    //callback for each view created
    private interface OnCreateDynamicViewCallBack {
        void onCreatedDynamicView(View view);
    }
    private int totalViewCount = 0;
    private int viewCount = 0;
    private void checkTypeOfUiElement(OnFormViewCreatedListener onFormViewCreatedListener,ViewGroup parentLayout,Model modelInstance,List formSequenceList,String objectDetails){
        try {
            if(viewCount < totalViewCount){
                String keyName = formSequenceList.get(viewCount).toString();
                String uiElement = modelInstance.getAttributeUIElement(keyName);
                Log.e(TAG, "checkTypeOfUiElement*******attr Name:-" + keyName + "          UI Element:-" + uiElement+"        type:-"+modelInstance.getAttributeType(keyName) + "  required = "+modelInstance.getAttributeRequired(keyName));
                HashMap<String, Object> viewAttributeDetails = new HashMap<>();
                if (objectDetails != null && !objectDetails.isEmpty()) {
                    JSONObject setValueToField = new JSONObject(objectDetails);
                    if (setValueToField.has(keyName) && !setValueToField.isNull(keyName)) {
                        viewAttributeDetails.put("SetValue", setValueToField.get(keyName));
                    }
                }
                viewAttributeDetails.put("setTag", keyName);
                if(modelInstance.getAttributeRequired(keyName)) {
                    Log.e(TAG,"required for "+keyName);
                    viewAttributeDetails.put("setTitle", modelInstance.getAttributeTitle(keyName)+" *");
                    viewAttributeDetails.put("setHint", modelInstance.getAttributeTitle(keyName)+" *");
                }else {
                    viewAttributeDetails.put("setTitle", modelInstance.getAttributeTitle(keyName));
                    viewAttributeDetails.put("setHint", modelInstance.getAttributeTitle(keyName));
                }
                viewAttributeDetails.put("Options", modelInstance.getAttributeOptions(keyName));
                viewAttributeDetails.put("InputType", modelInstance.getAttributeUIElement(keyName));
                viewAttributeDetails.put("AutoUpdate", modelInstance.getAutoUpdate(keyName));
                viewAttributeDetails.put("isRequired", modelInstance.getAttributeRequired(keyName));

                if (uiElement != null) {
                    createViewBasedOnUIElement(uiElement, viewAttributeDetails, new OnCreateDynamicViewCallBack() {
                        @Override
                        public void onCreatedDynamicView(View view) {
                            //Log.i(TAG,"onCreatedDynamicView:--- viewName:-"+ keyName+" isRequired "+ modelInstance.getAttributeRequired(keyName));
                            if (view != null) {
                                parentLayout.addView(view);
                                fieldValidationMap.put(keyName, modelInstance.getAttributeRequired(keyName));
                            }
                            viewCount++;
                            checkTypeOfUiElement(onFormViewCreatedListener,parentLayout,modelInstance,formSequenceList,objectDetails);
                        }
                    });
                }else {
                    viewCount++;
                    checkTypeOfUiElement(onFormViewCreatedListener,parentLayout,modelInstance,formSequenceList,objectDetails);
                }
            }
            else if(viewCount == totalViewCount){
                Log.e(TAG, "Print Attributes Required Map of formview  :>>>>>>>>>>>>>>" + fieldValidationMap);
                if(onFormViewCreatedListener!=null)
                    onFormViewCreatedListener.onFormVewCreated(fieldValidationMap);
            }


        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Error  CreateDynamic Form :>>>>>>>>>>>>>>" + e.getMessage());
        }

    }

    //check and create different  view based on UiElement
    public void createViewBasedOnUIElement(String ui_element,HashMap<String, Object> viewDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {
        try {
            List<String> EDIT_TEXT_OPTION = Arrays.asList("text", "textarea", "password", "email", "number", "tel");
            List<String> DATE_TIME_OPTION = Arrays.asList("date", "time", "datetime");

            if (EDIT_TEXT_OPTION.contains(ui_element)) {
                boolean autoUpdate = false;
                if(viewDetails.containsKey("AutoUpdate"))
                    autoUpdate = (boolean) viewDetails.get("AutoUpdate");

                if(autoUpdate)
                   createTextViewWIthTitle(viewDetails,onCreateDynamicViewCallBack);
                else
                    createInputEditTextLayout(viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("rating")) {
                createRatingBarWIthTitle(viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("tags") ) {
                createTagsView(viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("select")) {

                createSpinnerBasedOnOption(viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("typeahead") ) {

                createTypeAheadView(viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("multiselect")) {
                if(viewDetails.get("Options") instanceof String)
                     createInputEditTextLayout(viewDetails,onCreateDynamicViewCallBack);
                else
                    createAMultiSelectList(viewDetails,onCreateDynamicViewCallBack);
            }

            else if (ui_element.equalsIgnoreCase("typeahead-tags") ) {
                createTypeAheadTagsView(viewDetails,onCreateDynamicViewCallBack);

            }

            else if (ui_element.equalsIgnoreCase("image")) {
                createImageView(viewDetails,onCreateDynamicViewCallBack);
            }
            else if (ui_element.equalsIgnoreCase("slideshow") ) {
                createViewPager(viewDetails,onCreateDynamicViewCallBack);

            }
            /*else if (ui_element.equalsIgnoreCase("thumbnails") ) {
                System.out.println("**************Create Grid View **************" + viewDetails);
                view = createGridView(context, mapDetails);
                return view;
            }*/
            else if (DATE_TIME_OPTION.contains(ui_element)) {
                 createDateTimeView(ui_element,viewDetails,onCreateDynamicViewCallBack);

            }
            else if (ui_element.equalsIgnoreCase("switch") ) {
                createSwitchView(viewDetails,onCreateDynamicViewCallBack);
            }
            else if (ui_element.equalsIgnoreCase("geolocation") ) {
                createMapView(viewDetails,onCreateDynamicViewCallBack);
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during creating view based on ui Element:------"+e.getMessage());
        }

    }

    private void setCreateDynamicViewCallBack(View view ,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack){
        if(onCreateDynamicViewCallBack!=null)
            onCreateDynamicViewCallBack.onCreatedDynamicView(view);
    }

    private Object getEditTextValueInputType(EditText editText){

        String editTextValue = editText.getText().toString();
        Object value = editTextValue;
        int inputType = editText.getInputType();

        Log.i(TAG,"getEditTextValueInputType Print check Input type :- "+ editText.getInputType() +" number: "+ InputType.TYPE_CLASS_NUMBER
                +"\n decimal: "+ InputType.TYPE_NUMBER_FLAG_DECIMAL );
        if (inputType == InputType.TYPE_CLASS_TEXT) {//text ,textarea
            value = editTextValue.toString();
        }
        else if (inputType == 8194) { //password
            String s = editTextValue.replaceAll(",","").trim();
            String f = s.replaceAll(" ", "");
            value = Double.parseDouble(f);
        }
        else if (inputType == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) {//email
            value = editTextValue;
        }

        else if (inputType == InputType.TYPE_CLASS_NUMBER) { //number
            value = Integer.parseInt(editTextValue);
        }
        else if (inputType == InputType.TYPE_NUMBER_FLAG_DECIMAL) {//decimal
           // DecimalFormat df = new DecimalFormat("0.00");
           // df.setMaximumFractionDigits(2);
            //value = Double.parseDouble(editTextValue);
            String s = editTextValue.replaceAll(",","").trim();
            String f = s.replaceAll(" ", "");
            value = Double.parseDouble(f);

        }
        else if (inputType == InputType.TYPE_CLASS_PHONE) { //telephone
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
        }
        else{
            return value;
        }
        return value;
    }

    public JSONObject methodValidateDynamicView(ViewGroup parentLayout,HashMap<String, Boolean> requiredFieldHashMap,String objectDetail){

        //Log.e(TAG, "methodValidateDynamicView****requiredFieldHashMap"+requiredFieldHashMap);
        Boolean fieldRequired = false;
        boolean isFieldValid = true;
        String errorMsg="";
        Object viewData = null;
        View child = null;
        try {
            if(objectDetail!= null && !objectDetail.isEmpty()){
                JSONObject jsonObject = new JSONObject(objectDetail);
                Iterator<String> keys = jsonObject.keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    if(!storeObjectDetails.has(key))
                        storeObjectDetails.put(key,jsonObject.get(key));
                }
            }
            if (parentLayout.getParent() != null) {
                for (int i = 0; i < parentLayout.getChildCount(); i++) {
                    child = parentLayout.getChildAt(i);
                    //Log.e(TAG, "Validate View**************ChildView :-"+child+ " isRequired:- " + fieldRequired );
                    if (child instanceof EditText)
                    {
                        EditText et = (EditText) child;
                        fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                        String editValue = et.getText().toString();
                       // Object editValue = et.getText();
                        Log.i(TAG, "Validate EditText***Name :-"+child.getTag().toString()+ " isRequired:-" + fieldRequired + " && value:-"+editValue);
                        if (fieldRequired && editValue.isEmpty()) {
                            isFieldValid = false;
                            errorMsg = et.getTag().toString();
                            break;
                        }
                        if (!editValue.isEmpty()) {
                            if(et instanceof TagsEditText){
                                JSONArray jsonArray = new JSONArray();
                                String[] items = editValue.split(" ");
                                for (String item : items) {
                                    jsonArray.put(item);
                                }
                                viewData= jsonArray;

                            }else {
                                //todo check input type of edit text
                                //viewData = editValue;
                                viewData = getEditTextValueInputType(et);
                            }
                            saveViewValue(et.getTag().toString(), viewData);

                        }else{
                            saveViewValue(et.getTag().toString(), JSONObject.NULL);
                        }

                    }
                    else if (child instanceof TextView)
                    {
                        if(child instanceof android.widget.Switch ){
                            Switch switchView = (Switch) child;
                            fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                            Log.i(TAG, "Validate Switch**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
                            viewData = switchView.isChecked();
                            saveViewValue(child.getTag().toString(), viewData);

                        }
                        else if(child instanceof Button)
                        {
                            Button button = (Button) child;
                            fieldRequired = requiredFieldHashMap.get(child.getTag().toString());

                            String textValue = button.getText().toString();
                           // String textValue = button.getHint().toString();
                            Log.i(TAG, "Validate ButtonView**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired +" textValue:- "+textValue);
                            if (fieldRequired && textValue.isEmpty()) {
                                isFieldValid = false;
                                errorMsg = button.getTag().toString();
                                break;
                            }
                            if (!textValue.isEmpty()) {
                                viewData = textValue;
                                saveViewValue(button.getTag().toString(), viewData);

                            }
                            else{
                                saveViewValue(button.getTag().toString(), JSONObject.NULL);
                            }
                        }else {

                            TextView tV = (TextView) child;
                            if (child.getTag() != null) {
                                fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                                String textValue = tV.getText().toString();
                                Log.i(TAG, "Validate TextView**************Name :- " + child.getTag().toString() + " isRequired:- " + fieldRequired);
                                if (fieldRequired && textValue.isEmpty()) {
                                    isFieldValid = false;
                                    errorMsg = tV.getTag().toString();
                                    break;
                                }
                                if (!textValue.isEmpty()) {
                                    viewData = textValue;
                                    saveViewValue(tV.getTag().toString(), viewData);
                                }else{
                                    saveViewValue(tV.getTag().toString(), JSONObject.NULL);
                                }
                            }
                        }
                    }
                    else if (child instanceof Spinner)
                    {
                        Spinner spinner = (Spinner) child;
                        fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                        Log.i(TAG, "Validate Spinner**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
                        int pos = spinner.getSelectedItemPosition();
                        if (fieldRequired && pos == 0) {
                            isFieldValid = false;
                            errorMsg = spinner.getTag().toString();
                            break;
                        }
                        if (pos == 0) {
                            saveViewValue(child.getTag().toString(), JSONObject.NULL);

                        }else{
                            viewData = spinner.getSelectedItem();
                            saveViewValue(child.getTag().toString(), viewData);
                        }


                    }
                    else if (child instanceof RatingBar)
                    {
                        RatingBar rB = (RatingBar) child;
                        fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                        Log.i(TAG, "Validate RatingBar**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
                        float ratingBAr = rB.getRating();
                        if (ratingBAr != 0.0) {
                            viewData = ratingBAr;
                            saveViewValue(child.getTag().toString(), viewData);

                        }
                        else{
                            saveViewValue(child.getTag().toString(), JSONObject.NULL);
                        }
                        if (fieldRequired && ratingBAr == 0.0) {
                            isFieldValid = false;
                            errorMsg = rB.getTag().toString();
                            break;
                        }
                    }
                    else if (child instanceof TextInputLayout)
                    {
                        TextInputLayout til = (TextInputLayout) child;
                        EditText et = til.getEditText();
                        assert et != null;
                        if(et.getTag()!=null) {
                            fieldRequired = requiredFieldHashMap.get(et.getTag().toString());
                            Log.i(TAG, "Validate TextInputLayout Edit Text**************Name :-"+et.getTag().toString()+ " isRequired:- " + fieldRequired);
                            String textValue = et.getText().toString();
                            if (fieldRequired && textValue.isEmpty()) {
                                isFieldValid = false;
                                errorMsg = et.getTag().toString();
                                break;
                            }
                            if (!textValue.isEmpty()) {
                                //todo check input type of edit text
                               // viewData = textValue;
                                viewData = getEditTextValueInputType(et);
                                saveViewValue(et.getTag().toString(), viewData);

                            }
                            else{
                                saveViewValue(et.getTag().toString(), JSONObject.NULL);
                            }

                        }
                    }
                    else if (child instanceof ListView)
                    {

                        ListView mListView = (ListView) child;
                        fieldRequired = requiredFieldHashMap.get(child.getTag().toString());
                        Log.i(TAG, "Validate ListView MultiSelect**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired);
                        JSONArray selectedList = new JSONArray();
                        SparseBooleanArray checked = mListView.getCheckedItemPositions();
                        for (int k = 0; k < checked.size(); k++) {
                            int item_position = checked.keyAt(k);
                            if (checked.valueAt(k)) {
                                selectedList.put(mListView.getAdapter().getItem(item_position).toString());
                            }
                        }
                        if (fieldRequired && selectedList.length()==0) {
                            isFieldValid = false;
                            errorMsg = mListView.getTag().toString();
                            break;
                        }
                        if (selectedList.length()> 0) {
                            viewData = selectedList;
                            saveViewValue(child.getTag().toString(), viewData);
                            // storeObjectDetails.put(child.getTag().toString(),viewData);
                        } else{
                            saveViewValue(child.getTag().toString(), JSONObject.NULL);
                        }
                    }
                    else if (child instanceof Button)
                    {
                        Button button = (Button) child;
                        fieldRequired = requiredFieldHashMap.get(child.getTag().toString());

                       // String textValue = button.getText().toString();
                        String textValue = button.getHint().toString();
                        Log.i(TAG, "Validate ButtonView**************Name :-"+child.getTag().toString()+ " isRequired:- " + fieldRequired +" textValue:- "+textValue);
                        if (fieldRequired && textValue.isEmpty()) {
                            isFieldValid = false;
                            errorMsg = button.getTag().toString();
                            break;
                        }
                        if (!textValue.isEmpty()) {
                            viewData = textValue;
                            saveViewValue(button.getTag().toString(), viewData);

                        }
                        else{
                            saveViewValue(button.getTag().toString(), JSONObject.NULL);
                        }

                    }
                    else if (child instanceof ViewGroup)
                    {
                        Log.i(TAG, "Validate ViewGroup*****Name :-"+child);
                        ViewGroup group = (ViewGroup) child;
                        methodValidateDynamicView(group, requiredFieldHashMap,storeObjectDetails.toString());
                    }
                    else {
                        Log.e(TAG, "************ Default Other Child of parent ************** : ");
                    }
                }
            }
            if (isFieldValid ) {
                Log.e(TAG, "<<<<<<<<<<<<<,MethodValidateDynamicView Object Details >>>>>>>>>>  " + storeObjectDetails);
                return storeObjectDetails;
            } else
                Toast.makeText(context, "Please Fill " + errorMsg, Toast.LENGTH_SHORT).show();

        }catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "<<<<<<<<<<<<<<<<<<<<<Error during Validate Field>>>>>>>>>>>>>> : " + e.getMessage());
        }
        return null;
    }

    private void saveViewValue(String childName, Object viewData){
        try {
            Log.i(TAG," saveViewValue View NAme:- "+childName+" and value:- "+viewData +" valueType:- "+ viewData.getClass());
            storeObjectDetails.put(childName,viewData);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Store User Input Data:- " + e.getMessage());
        }
    }


/* ******************* Start To Create  Dynamic View ****************************************************************************************/

    // create switch view
    private void createSwitchView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        Switch switchName = new Switch(context);
        if (mapDetails.containsKey("setTag")) {
            switchName.setTag(mapDetails.get("setTag"));
        }
        if (mapDetails.containsKey("setTitle")) {
            switchName.setText(mapDetails.get("setTitle").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            Object value = mapDetails.get("SetValue");
            Boolean defaultVal = (Boolean)value;
            Log.e(TAG,"Get Switch view Value:--------------"+value+" == "+defaultVal);
            switchName.setChecked(defaultVal);
        }
        setCreateDynamicViewCallBack(switchName,onCreateDynamicViewCallBack);
    }

    // create textview
    private View createTextView(HashMap<String, Object> mapDetails) {

        LinearLayout.LayoutParams textViewTextParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textViewTextParams.setMargins(0, 8, 0, 8);

        TextView textView = new TextView(context);
        textView.setLayoutParams(textViewTextParams);
        textView.setId(View.generateViewId());

        if (mapDetails.containsKey("setTag")) {
            textView.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("setHint")) {
            textView.setHint(mapDetails.get("setHint").toString());
        }

        if (mapDetails.containsKey("setHintTextColor")) {
            int textHintColor = (int) mapDetails.get("setHintTextColor");
            textView.setHintTextColor(context.getResources().getColor(textHintColor));
        }
        if (mapDetails.containsKey("SetValue")) {
            textView.setText(mapDetails.get("SetValue").toString());
        }

        textView.setHintTextColor(context.getResources().getColor(R.color.primary_text));
        textView.setTextSize(16);
        textView.setTextColor(context.getResources().getColor(R.color.primary_text));
        textView.setMaxLines(1);
        textView.setLines(1);
        textView.setLines(1);

        return textView;
    }

    // create textView with Title
    private void createTextViewWIthTitle(HashMap<String, Object> mapDetails, OnCreateDynamicViewCallBack onCreateDynamicViewCallBack){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.textview_layout, null);
        TextView keyTitle=  view.findViewById(R.id.key_title);
        TextView textValue = view.findViewById(R.id.key_value);

        if (mapDetails.containsKey("setTitle")) {
            keyTitle.setText(mapDetails.get("setTitle").toString());
        }

        textValue.setId(View.generateViewId());
        if (mapDetails.containsKey("setTag")) {
            textValue.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("SetValue") && mapDetails.get("SetValue")!=null) {
            textValue.setText(mapDetails.get("SetValue").toString());
            setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
            try {
                jsonObjectUserAndCurrentModel.put(mapDetails.get("setTag").toString(),textValue.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else
            setCreateDynamicViewCallBack(null,onCreateDynamicViewCallBack);
    }

    // create EditText
    @SuppressLint("ResourceAsColor")
    private EditText createAEditText(HashMap<String, Object> mapDetails) {

        LinearLayout.LayoutParams editTextParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        editTextParams.setMargins(0, 8, 0, 8);

        EditText editText = new EditText(context);
        editText.setLayoutParams(editTextParams);
        editText.setId(View.generateViewId());
        if (mapDetails.containsKey("setTag")) {
            editText.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("setHint")) {
            editText.setHint(mapDetails.get("setHint").toString());
        }

        if (mapDetails.containsKey("setHintTextColor")) {
            int textHintColor = (int) mapDetails.get("setHintTextColor");
            editText.setHintTextColor(context.getResources().getColor(textHintColor));
        }
        if (mapDetails.containsKey("SetValue")) {
            editText.setText(mapDetails.get("SetValue").toString());
        }

        if (mapDetails.containsKey("InputType")) {
            String inputType = mapDetails.get("InputType").toString();
            if (inputType.equalsIgnoreCase("text"))
                editText.setInputType(InputType.TYPE_CLASS_TEXT);

            else if (inputType.equalsIgnoreCase("password"))
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());

            else if (inputType.equalsIgnoreCase("textarea"))
                editText.setInputType(InputType.TYPE_CLASS_TEXT);

            else if (inputType.equalsIgnoreCase("email"))
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

            else if (inputType.equalsIgnoreCase("number"))
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);

            else if (inputType.equalsIgnoreCase("tel"))
                editText.setInputType(InputType.TYPE_CLASS_PHONE);

            else
                editText.setInputType(InputType.TYPE_CLASS_TEXT);

        }
        if (mapDetails.containsKey("textPersonName")) {
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        editText.setHintTextColor(context.getResources().getColor(R.color.primary_text));
        editText.setTextSize(16);
        editText.setTextColor(context.getResources().getColor(R.color.primary_text));
        editText.setMaxLines(1);
        editText.setLines(1);
        Drawable drawable = editText.getBackground();
        drawable.setColorFilter(R.color.colorPrimary, PorterDuff.Mode.SRC_ATOP);
        //editText.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorPrimary));

        return editText;
    }

    // create createInputEditTextLayout
    private void createInputEditTextLayout(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

       // Log.i(TAG,"createInputEditTextLayout:-------mapDetails:  "+mapDetails);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view =  inflater.inflate(R.layout.text_input_edit_text_layout, null);
        TextInputLayout textInputLayout = view.findViewById(R.id.textInputLayout);
        TextInputEditText editText = view.findViewById(R.id.textInputEditText);

        if (mapDetails.containsKey("setTag")) {
            editText.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("setHint")) {
           // editText.setHint(mapDetails.get("setHint").toString());
            textInputLayout.setHint(mapDetails.get("setHint").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            editText.setText(mapDetails.get("SetValue").toString());
        }

        /*
        •none •text •textCapCharacters •textCapWords •textCapSentences •textAutoCorrect •textAutoComplete •textMultiLine
        •textImeMultiLine •textNoSuggestions •textUri •textEmailAddress •textEmailSubject •textShortMessage •textLongMessage •textPersonName
        •textPostalAddress •textPassword •textVisiblePassword •textWebEditText •textFilter •textPhonetic •textWebEmailAddress
        •textWebPassword •number •numberSigned •numberDecimal •numberPassword •phone •datetime •date •time*/
        if (mapDetails.containsKey("InputType")) {
            String inputType = mapDetails.get("InputType").toString();

            if (inputType.equalsIgnoreCase("text"))
                editText.setInputType(InputType.TYPE_CLASS_TEXT);

            else if (inputType.equalsIgnoreCase("password"))
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());

            else if (inputType.equalsIgnoreCase("textarea"))
                editText.setInputType(InputType.TYPE_CLASS_TEXT);

            else if (inputType.equalsIgnoreCase("email"))
                editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

            else if (inputType.equalsIgnoreCase("number"))
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                //editText.setInputType(InputType.TYPE_CLASS_NUMBER );

            else if (inputType.equalsIgnoreCase("tel"))
                editText.setInputType(InputType.TYPE_CLASS_PHONE);

            else
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
               // editText.setInputType(InputType.TYPE_NULL);

        }else {
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
        }
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable mEdit)
            {
                //Log.i(TAG,"Edit Text Attr Name:-"+mapDetails.get("setTag").toString()+" Value:-"+ mEdit.toString());
                try {
                    jsonObjectUserAndCurrentModel.put(mapDetails.get("setTag").toString(),mEdit.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after){}

            public void onTextChanged(CharSequence s, int start, int before, int count){}
        });

        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }


    //create rating bar
    private void createRatingBar(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LinearLayout.LayoutParams ratingParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ratingParams.setMargins(0, 8, 0, 8);

        RatingBar ratingBar = new RatingBar(context, null, android.R.attr.ratingBarStyleIndicator);
        ratingBar.setLayoutParams(ratingParams);
        ratingBar.setId(View.generateViewId());
        if (mapDetails.containsKey("setTag")) {
            ratingBar.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            ratingBar.setRating(DataConversion.toFloat(mapDetails.get("SetValue")));
        }
        ratingBar.setNumStars(5);
        ratingBar.setStepSize((float) 1);
        ratingBar.setIsIndicator(false);

        LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

       /* Drawable progress = ratingBar.getProgressDrawable();
        DrawableCompat.setTint(progress, context.getResources().getColor(R.color.colorPrimary));*/

        setCreateDynamicViewCallBack(ratingBar,onCreateDynamicViewCallBack);

    }

    //create rating bar with Title
    private void createRatingBarWIthTitle(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.ratingbar_layout, null);
        TextView keyTitle=  view.findViewById(R.id.keyTitle);
        RatingBar ratingBar = view.findViewById(R.id.ratingBar);

        if (mapDetails.containsKey("setTitle")) {
            keyTitle.setText(mapDetails.get("setTitle").toString());
        }

        ratingBar.setId(View.generateViewId());
        if (mapDetails.containsKey("setTag")) {
            ratingBar.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            ratingBar.setRating(DataConversion.toFloat(mapDetails.get("SetValue")));
        }
        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);

    }


    //create tag view:- edit view with multiple text with cross icon
    private void createTagsView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.type_ahead_tags_layout, null);
        TagsEditText mTagsEditText = view.findViewById(R.id.tagsEditText);

        //To Disable space click (Adds text to tags)
        mTagsEditText.setTagsWithSpacesEnabled(true);
        //mTagsEditText.setTagsListener(context);

        if (mapDetails.containsKey("setTag")) {
            mTagsEditText.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.get("setHint") != null) {
            mTagsEditText.setHint(mapDetails.get("setHint").toString());

        }
        if (mapDetails.containsKey("SetValue")) {
            JSONArray getValue = (JSONArray) mapDetails.get("SetValue");
            ArrayList<String> selectedList = new ArrayList();
            for(int i = 0;i<getValue.length();i++){
                try {
                    selectedList.add(getValue.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // Log.e(TAG,"Set Tags Value :---"+selectedList);
            ArrayAdapter arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line,selectedList);
            // getResources().getStringArray(R.array.country));
            // mTagsEditText.setAdapter(arrayAdapter);
            mTagsEditText.setThreshold(1);
            String [] tag =selectedList.toArray(new String [0]);
            mTagsEditText.setTags(tag);
        }



        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }


    /**
     * method to create spinner based on options
     * if Option type 1) ArrayList :- create spinner with dropdown
     *                2) String url :- a) check internet connection.(net is required).  b) get optionList from server
     *                                 c) edit view:- on type get list of option from server
     *                                 d) need to select option from dropdown only
     *
     * @param mapDetails paas details of attr to create view
     * @param onCreateDynamicViewCallBack to notify view through listener after created view.
     */
    private void createSpinnerBasedOnOption(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack){
        Log.e(TAG,"OPTIONS ====  "+mapDetails.get("Options").toString());
        if(mapDetails.get("Options") instanceof ArrayList) {
            createASpinner(mapDetails,onCreateDynamicViewCallBack);
        }
        else if(mapDetails.get("Options") instanceof String) {
            if (CheckNetworkConnection.networkHasConnection(context))
            {
                createSearchableSpinner(mapDetails, onCreateDynamicViewCallBack);

            }else {
               // CheckNetworkConnection.showNetDisabledAlertToUser(context, "To Get Options need Internet Connection");
                createInputEditTextLayout(mapDetails,onCreateDynamicViewCallBack);
            }
        }
        else {
            createASpinner(mapDetails,onCreateDynamicViewCallBack);
        }
    }

    private void createASpinner(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.dynamic_spinner_layout, null);
        TextView selectTitle = view.findViewById(R.id.selectTitle);
        Spinner spinnerName = view.findViewById(R.id.spinner);

        if (mapDetails.containsKey("setTitle")) {
            selectTitle.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            spinnerName.setTag(mapDetails.get("setTag"));
        }
        if (mapDetails.get("setHint") != null) {
            spinnerName.setPrompt(mapDetails.get("setHint").toString());
        }
        ArrayList<String> optionList = new ArrayList<>();
        if (mapDetails.containsKey("Options")) {
            optionList.add(mapDetails.get("setTitle").toString());
            optionList.addAll((ArrayList) mapDetails.get("Options"));
            if (!optionList.isEmpty() && optionList.size() > 0) {
                ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, optionList);
                /* ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<String>(context,R.layout.text_view_layout, optionList);*/
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerName.setAdapter(spinnerArrayAdapter);
            }

        }
        if (mapDetails.containsKey("SetValue") && !optionList.isEmpty()) {
            optionList.add(mapDetails.get("SetValue").toString());
            int selectedPos = optionList.indexOf(mapDetails.get("SetValue").toString());
            spinnerName.setSelection(selectedPos);
        }
        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }

    //create spinner with search view
    private void createSearchableSpinner(HashMap<String, Object> mapDetails, OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;

        View view = inflater.inflate(R.layout.dynamic_spinner_search, null);
        TextView selectTitle = view.findViewById(R.id.selectTitle);
        Button button = view.findViewById(R.id.button);

        String optionLink = mapDetails.get("Options").toString();
        Log.i(TAG,"<<<<<<<<<<<<<<<<<createSearchableSpinner Print  Option Link:----------"+optionLink);

        if (mapDetails.containsKey("setTitle")) {
            selectTitle.setText(mapDetails.get("setTitle").toString());
            //button.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            button.setTag(mapDetails.get("setTag"));
        }

        if (mapDetails.get("setHint") != null) {
            button.setHint(mapDetails.get("setHint").toString());
        }

        if (mapDetails.containsKey("SetValue") ) {
           button.setText(mapDetails.get("SetValue").toString());
        }

        SearchableDialog _searchableDialog = SearchableDialog.newInstance();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show search view
                _searchableDialog.show(scanForActivity(context).getFragmentManager(), "TAG");
            }
        });

        _searchableDialog.setOnSearchViewCreatedListener(new SearchableDialog.OnSearchViewCreated() {
            @Override
            public void onSearchViewCreated() {
               // getSelectViewOptionsFromServer(spinnerName,spinnerArrayAdapter,optionsList,_searchableDialog,getOptionsUrl(optionLink),getOptionsDefaultQueryParam(optionLink),true);
                getSelectViewOptionsFromServer(_searchableDialog,getOptionsUrl(optionLink),getOptionsDefaultQueryParam(optionLink),false);
            }
        });
        _searchableDialog.setOnSearchableItemClickListener(new SearchableDialog.SearchableItem() {
            @Override
            public void onSearchableItemClicked(Object item, int position) {

                try {
                    Log.e(TAG,"onSearchableItemClicked:---------------------"+ item +" Position:-"+ position );
                    button.setText(item.toString());
                    if(optionListMap.containsKey(item))
                        button.setHint(optionListMap.get(item));
                    else {
                         button.setHint("");
                         Log.i(TAG, "<<<<<<<<<Set Hint empty..................."+button.getHint());
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        _searchableDialog.setOnSearchTextChangedListener(new SearchableDialog.OnSearchTextChanged() {
            @Override
            public void onSearchTextChanged(String strText) {
                try {
                    if(strText!=null && strText.length()>2){
                        HashMap<String ,Object> queryParam = getOptionsDefaultQueryParam(optionLink);
                        // queryParam.put(mapDetails.get("setTag").toString(),"~"+strText);
                        String strSearch = optionLink.split("/")[3];
                        if(strSearch.contains("?"))
                            strSearch = strSearch.split("\\?")[0];

                        queryParam.put(strSearch,"~"+strText);
                        getSelectViewOptionsFromServer(_searchableDialog,getOptionsUrl(optionLink),queryParam,false);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }

    private Activity scanForActivity(Context cont) {

        if (cont == null)
            return null;
        else if (cont instanceof Activity)
            return (Activity) cont;

        else if (cont instanceof ContextWrapper)

            return scanForActivity(((ContextWrapper) cont).getBaseContext());


        return null;
    }

    private HashMap<String ,String> optionListMap = new HashMap<>();
    private void getSelectViewOptionsFromServer(SearchableDialog _searchableDialog,
                                                String optionUrl, HashMap<String,Object> queryParam , boolean loader){
        optionListMap = new HashMap<>();
        if (CheckNetworkConnection.networkHasConnection(context)){
            DaveModels daveModels = new DaveModels(context, false);
            daveModels.getOptions(new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    try {
                        ArrayList optionsList = new ArrayList();

                        if(jsonObject!=null && jsonObject.length()>0  && jsonObject.has("data") && !jsonObject.isNull("data")){

                            if(jsonObject.get("data") instanceof JSONArray)
                            {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for(int i=0; i<jsonArray.length();i++)
                                {
                                    JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                                    optionListMap.put(jsonArray1.getString(1),jsonArray1.getString(0));
                                    optionsList.add(jsonArray1.getString(1));
                                }
                            }
                            else if(jsonObject.get("data") instanceof JSONObject)
                            {
                                optionsList =  ObjectData.getKeyAsArrayListOfJSONObject(jsonObject.getJSONObject("data"));
                            }
                            _searchableDialog.updatedData(optionsList);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, " Error getSelectOptionsFromServer :  " + e.getMessage());
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {
                    _searchableDialog.onRequestFailed();


                }
            }, optionUrl,queryParam,loader);

        }else
            CheckNetworkConnection.showNetDisabledAlertToUser(context,"To Get Options need Internet Connection");

    }
    private String getOptionsUrl(String optionsValue){
        if(optionsValue.contains("?")) {
            return optionsValue.split("\\?")[0];
        }
        else
           return optionsValue;
    }

    //method to get default QueryParam of Options Link sample:- options/distributor/distributor_name?area_manager_id={area_manager_id}
    private HashMap<String ,Object>  getOptionsDefaultQueryParam(String optionUrl){

        HashMap<String ,Object> queryParam = new HashMap<>();
        //Log.i(TAG,"getOptionsDefaultQueryParam Start :----"+optionUrl + "  "+ jsonObjectUserAndCurrentModel);
        if(optionUrl.contains("?")) {
            String[] paramArray = optionUrl.split("\\?")[1].split("&");
            //Log.i(TAG,"getOptionsDefaultQueryParam paramArray:----"+ Arrays.toString(paramArray));
            for (int i = 0; i < paramArray.length; i++) {
                String[] paramDetails = paramArray[0].split("=");
                String key = paramDetails[0];
                String value = paramDetails[1];
                if (jsonObjectUserAndCurrentModel.has(key) && !jsonObjectUserAndCurrentModel.isNull(key)) {
                    try {
                        queryParam.put(key, jsonObjectUserAndCurrentModel.get(key).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        Log.i(TAG,"getOptionsDefaultQueryParam End:-----"+queryParam);
        return queryParam;
    }

    private ArrayList getOptionListFromResponse (JSONObject jsonObject){
        ArrayList optionsList = new ArrayList();
        try {

            if(jsonObject!=null && jsonObject.length()>0  && jsonObject.has("data") && !jsonObject.isNull("data")){

                if(jsonObject.get("data") instanceof JSONArray)
                {
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for(int i=0; i<jsonArray.length();i++)
                    {
                        JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                        optionListMap.put(jsonArray1.getString(1),jsonArray1.getString(0));
                        optionsList.add(jsonArray1.getString(1));
                    }
                }
                else if(jsonObject.get("data") instanceof JSONObject)
                {
                    optionsList =  ObjectData.getKeyAsArrayListOfJSONObject(jsonObject.getJSONObject("data"));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error getSelectOptionsFromServer :  " + e.getMessage());
        }
        Log.i(TAG," getOptionListFromResponse>>>>>>>>>>>>>>>>>>>>>>>>> "+optionsList);
       return optionsList;
    }

    //create createTypeAheadView (Single value) :- on typing something get dropdown list,accept any value including options
    private void createTypeAheadView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.type_ahead_layout, null);
        TextView textView = view.findViewById(R.id.textView);
        AutoCompleteTextView tagsView = view.findViewById(R.id.tagsView);

        if (mapDetails.containsKey("setTitle")) {
            textView.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            tagsView.setTag(mapDetails.get("setTag").toString());
        }

        if (mapDetails.get("setHint") != null) {
            tagsView.setHint(mapDetails.get("setHint").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            tagsView.setText(mapDetails.get("SetValue").toString());
        }

        if (mapDetails.containsKey("Options")) {
            Log.i(TAG,"createTypeAheadView>>>>>>>>>>>>>>"+mapDetails.get("Options").getClass()+" OptionsValue:- "+mapDetails.get("Options"));
            if(mapDetails.get("Options") instanceof ArrayList)
            {
                ArrayList optionList = (ArrayList) mapDetails.get("Options");
                if (optionList != null && optionList.size() > 0) {
                    Log.i(TAG,"createTypeAheadView for  options ArrayList>>>>>>>>>>>>>>"+optionList);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, optionList);
                    tagsView.setAdapter(adapter);
                    tagsView.setThreshold(1);
                    //tagsView.showDropDown();
                    // tagsView.setAdapter(adapter);
                }
                setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);

            }else if(mapDetails.get("Options") instanceof String)
            {
                if (CheckNetworkConnection.networkHasConnection(context)){

                    String optionLink = mapDetails.get("Options").toString();
                    Log.i(TAG,"<<<<<<<<<<<<<<<<<createTypeAheadView Print  Option Link:----------"+optionLink);

                    tagsView.addTextChangedListener(new TextWatcher() {
                        private Timer timer;
                        public void afterTextChanged(Editable s) {

                           /* // user typed: start the timer
                            timer = new Timer();
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    // do your actual work here
                                }
                            }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask
                            */
                            if(s.toString().length() > 1)
                            {
                                HashMap<String ,Object> queryParam = getOptionsDefaultQueryParam(optionLink);
                                String strSearch = optionLink.split("/")[3];
                                if(strSearch.contains("?"))
                                    strSearch = strSearch.split("\\?")[0];

                                queryParam.put(strSearch,"~"+s.toString());
                                getTypeAheadOptionsFromServer(tagsView,getOptionsUrl(optionLink),queryParam);
                            }
                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                        public void onTextChanged(CharSequence query, int start, int before, int count) {

                            // user is typing: reset already started timer (if existing)
                            if (timer != null) {
                                timer.cancel();
                            }

                            /*if(query.toString().length() > 1)
                            {
                                HashMap<String ,String> queryParam = getOptionsDefaultQueryParam(optionLink);
                                String strSearch = optionLink.split("/")[3];
                                if(strSearch.contains("?"))
                                    strSearch = strSearch.split("\\?")[0];

                                queryParam.put(strSearch,"~"+query.toString());
                                getTypeAheadOptionsFromServer(tagsView,getOptionsUrl(optionLink),queryParam);
                            }*/
                        }
                    });

                    setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);

                }else {
                    Log.e(TAG,"No net Connection createTypeAheadView createInputEditTextLayout>>>>>>>>>>>>>>>>... ");
                    createInputEditTextLayout(mapDetails,onCreateDynamicViewCallBack);

                }

            }
        }else {
            setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
        }

    }


    private void getTypeAheadOptionsFromServer(AutoCompleteTextView autoCompleteTextView, String optionUrl, HashMap<String,Object> queryParam)
    {
        if (CheckNetworkConnection.networkHasConnection(context)){
            DaveModels daveModels = new DaveModels(context, false);
            daveModels.getOptions(new DaveAIListener() {
                @Override
                public void onReceivedResponse(JSONObject jsonObject) {
                    try {

                        ArrayList optionList = getOptionListFromResponse(jsonObject);
                        Log.e(TAG,"Print OptionList getTypeAheadOptionsFromServer:-----------------------"+optionList);
                        if(optionList!=null && optionList.size()>0) {
                            ArrayAdapter arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, optionList);
                            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            autoCompleteTextView.setAdapter(arrayAdapter);
                            autoCompleteTextView.setThreshold(2);
                        }
                    } catch (Exception e) {
                        Log.e("Error ", " Error Upload Image in ImageView :  " + e.getMessage());
                    }
                }

                @Override
                public void onResponseFailure(int i, String s) {

                }
            }, optionUrl,queryParam,false);

        }else
            CheckNetworkConnection.showNetDisabledAlertToUser(context,"To Get Options need Internet Connection");

    }


    //create multi select view
    private void createAMultiSelectList(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        ArrayAdapter<String> modeAdapter=null;

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.multi_select_layout, null);
        ListView listView =view.findViewById(R.id.multiSelectListView);
        TextView textView = view.findViewById(R.id.textView);

        if (mapDetails.containsKey("setTitle")) {
            textView.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            listView.setTag(mapDetails.get("setTag").toString());
        }

        if (mapDetails.containsKey("Options")) {
            ArrayList optionList = (ArrayList) mapDetails.get("Options");
            if (optionList != null && optionList.size() > 0) {
                 /*ArrayAdapter<String> modeAdapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_list_item_multiple_choice,
                        android.R.id.text1, optionList);*/

                modeAdapter = new ArrayAdapter<String>(context, R.layout.checked_textview,optionList);
                listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                listView.setAdapter(modeAdapter);
            }
        }
        if (mapDetails.containsKey("SetValue")) {

            ArrayList selectedList = ObjectData.convertObjectToArrayList(mapDetails.get("SetValue")) ;

           // ArrayList selectedList = (ArrayList) mapDetails.get("SetValue");
            for(int i=0;i<selectedList.size();i++){
                assert modeAdapter != null;
                int checked_position = modeAdapter.getPosition(selectedList.get(i).toString());
                listView.setItemChecked(checked_position, true);
            }
        }

        listView.setOnTouchListener(new ListView.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });
        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }


    //create createTypeAheadTagsView (Multiple Value ) :- add multi tags , on typing something get dropdown list,accept any value including options
    private void createTypeAheadTagsView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.type_ahead_tags_layout, null);
        TagsEditText mTagsEditText = view.findViewById(R.id.tagsEditText);
        TextView textView = view.findViewById(R.id.textView);

        if (mapDetails.containsKey("setTitle")) {
            textView.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            mTagsEditText.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.get("setHint") != null) {
            mTagsEditText.setHint(mapDetails.get("setHint").toString());

        }
        //todo set previous value
        if (mapDetails.containsKey("SetValue")) {
            JSONArray getValue = (JSONArray) mapDetails.get("SetValue");
            ArrayList<String> selectedList = new ArrayList();
            for(int i = 0;i<getValue.length();i++){
                try {
                    selectedList.add(getValue.get(i).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            // Log.e(TAG,"Set Tags Value :---"+selectedList);
            ArrayAdapter arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line,selectedList);
            // getResources().getStringArray(R.array.country));
            //mTagsEditText.setAdapter(arrayAdapter);
            mTagsEditText.setThreshold(1);
            String [] tag =selectedList.toArray(new String [0]);
            mTagsEditText.setTags(tag);
        }
        if (mapDetails.containsKey("Options")) {
            if(mapDetails.get("Options") instanceof ArrayList) {
                ArrayList optionList = (ArrayList) mapDetails.get("Options");
                if (optionList != null && optionList.size() > 0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, optionList);
                    mTagsEditText.setAdapter(adapter);
                    mTagsEditText.setThreshold(1);

                }
            }
        }

        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }


    //create image view with title
    private void createImageView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.image_view, null);
        TextView imageTitle = view.findViewById(R.id.imageTitle);
        ImageView imageView = view.findViewById(R.id.imageView);
        ImageView retryView = view.findViewById(R.id.retryView);
        TextView textErrorMsg = view.findViewById(R.id.textErrorMsg);
        retryView.setVisibility(View.GONE);
        textErrorMsg.setVisibility(View.GONE);

        /*if (mapDetails.containsKey("setTag")) {
            imageView.setTag(mapDetails.get("setTag").toString());
        }*/

        if (mapDetails.containsKey("setTitle")) {
            imageTitle.setText(mapDetails.get("setTitle").toString());
        }else
            imageTitle.setVisibility(View.GONE);

        if (mapDetails.containsKey("SetValue")) {
            String imageUrl = mapDetails.get("SetValue").toString();
            MultiUtil.setImageUsingGlide(context,imageUrl,imageView);
            if(!checkIsRemoteUrlPath(imageUrl)) {
                Log.i(TAG,"Imageview  has local File path>>>>>>>>>>>>>>>>>  "+imageUrl);
                retryView.setVisibility(View.VISIBLE);
                textErrorMsg.setVisibility(View.VISIBLE);
            }
        }
        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);

      /*  imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // CropImage.startPickImageActivity(getContext(),DynamicForm.this);
                onSelectImageClick(v);
                imageCroppedListener = new ImageCroppedListener() {
                    @Override
                    public void onImageCropped(CropImage.ActivityResult result) {
                        Log.e(TAG,"imageView.setOnClickListener  onImageCropped:-----------  "+ result);
                        uploadImageToServer(result.getUri().getPath(), new ImageUploadListener() {
                            @Override
                            public void onImageUploaded(String sResponse) {
                                imageView.setImageURI(result.getUri());
                                try {
                                    JSONObject imagePath = new JSONObject(sResponse);
                                    String imageUrl = imagePath.getString("path");
                                    storeObjectDetails.put(mapDetails.get("setTag").toString(), imageUrl);
                                    Log.e(TAG,"ImageView storeCustomerDetails:-  "+storeObjectDetails);

                                } catch (JSONException e) {
                                    Log.e("Error "," Error Upload Image in ImageView :  "+ e.getMessage());
                                }

                            }
                        });

                    }
                };

            }
        });*/

        imageView.setOnClickListener(v -> {
            if (CheckNetworkConnection.networkHasConnection(context)){
                textErrorMsg.setVisibility(View.GONE);
                retryView.setVisibility(View.GONE);
                startCamera();
                imageCroppedListener = new ImageCroppedListener() {
                    @Override
                    public void onImageCropped(Intent data) {
                        try {
                            Log.e(TAG,"Imageview After  onImageCropped 1 data>>>>>>>>>>>>>>>>>>  "+data);
                            if (data != null) {
                                if(data.getExtras()!= null && data.getData() == null){
                                    Log.i(TAG,"Print if data has only bitmap :-----------"+data.getData() +"  "+ data.getExtras() );
                                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                                    Uri tempUri = getImageUri(getActivity(), data.getExtras().getParcelable("data"));
                                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                                    tempImageFile = new File(getRealPathFromURI(tempUri));


                                }
                               // Log.e(TAG,"Imageview  onImageCropped Final ImagePath to upload:-  "+ tempImageFile.getAbsolutePath());
                                if(tempImageFile!=null ) {

                                    File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/cached_files/");
                                    if(!folder.exists())
                                        folder.mkdirs();
                                    String finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/"+System.currentTimeMillis()+".JPG";
                                    copyFile(tempImageFile,new File(finalFileName));
                                    mapDetails.put("SetValue",finalFileName);
                                    if(new ModelUtil(context).isCachedAttr(modelName,mapDetails.get("setTag").toString(),context)){
                                        Log.e(TAG,mapDetails.get("setTag").toString()+" is a cached attr");
                                        storeObjectDetails.put(mapDetails.get("setTag").toString(), finalFileName);
                                        if (data.getData() != null) {
                                            Log.i(TAG, "Print After cropped get URI:-----------" + data.getData());
                                            imageView.setImageURI(data.getData());
                                        } else if (data.getExtras() != null) {
                                            Bitmap croppedBitmap = data.getExtras().getParcelable("data");
                                            Log.i(TAG, "Print After cropped get Bitmap:-----------" + croppedBitmap);
                                            imageView.setImageBitmap(croppedBitmap);
                                        }

                                    }else{
                                        Log.e(TAG,mapDetails.get("setTag").toString()+" is not a cached attr");



                                    DaveModels daveModels = new DaveModels(context, false);
                                    daveModels.uploadImageToServer(new DaveAIListener() {
                                        @Override
                                        public void onReceivedResponse(JSONObject jsonObject) {
                                            try {
                                                textErrorMsg.setVisibility(View.GONE);
                                                retryView.setVisibility(View.GONE);
                                                if (data.getData() != null) {
                                                    Log.i(TAG, "Print After cropped get URI:-----------" + data.getData());
                                                    imageView.setImageURI(data.getData());
                                                } else if (data.getExtras() != null) {
                                                    Bitmap croppedBitmap = data.getExtras().getParcelable("data");
                                                    Log.i(TAG, "Print After cropped get Bitmap:-----------" + croppedBitmap);
                                                    imageView.setImageBitmap(croppedBitmap);
                                                }
                                                String imageUrl = jsonObject.getString("path");
                                                mapDetails.put("SetValue",imageUrl);
                                                storeObjectDetails.put(mapDetails.get("setTag").toString(), imageUrl);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
                                                Log.e("Error ", " Error Upload Image in ImageView :  " + e.getMessage());
                                            }
                                        }

                                        @Override
                                        public void onResponseFailure(int responseCode, String responseMsg) {
                                            Toast.makeText(context,responseMsg,Toast.LENGTH_SHORT).show();
                                            try {
                                                textErrorMsg.setVisibility(View.VISIBLE);
                                                retryView.setVisibility(View.VISIBLE);
                                                MultiUtil.setImageUsingGlide(context,tempImageFile.toString(),imageView);
                                                mapDetails.put("SetValue",tempImageFile.getAbsolutePath());
                                                storeObjectDetails.put(mapDetails.get("setTag").toString(), tempImageFile.getAbsolutePath());
                                                Log.i(TAG, " Failure ImageView storeCustomerDetails>>>>>>>:-  " + storeObjectDetails);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                Log.e("Error ", " Error Upload Image in ImageView onResponseFailure :  " + e.getMessage());
                                            }


                                        }
                                    }, "file", "image.png", tempImageFile.getAbsolutePath(), null, null);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Error "," Error Upload Image in ImageView :  "+ e.getMessage());
                        }
                    }
                };
            }else {
                 CheckNetworkConnection.showNetDisabledAlertToUser(context, "To Upload File need Internet Connection");
            }
        });

        retryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String  tempImageFile = mapDetails.get("SetValue").toString();
                    Log.i(TAG, "On Retry ImageView  get tempImageFile  to Upload :-  " + tempImageFile);
                    if(tempImageFile!=null ) {
                        DaveModels daveModels = new DaveModels(context, false);
                        daveModels.uploadImageToServer(new DaveAIListener() {
                            @Override
                            public void onReceivedResponse(JSONObject jsonObject) {
                                try {
                                    textErrorMsg.setVisibility(View.GONE);
                                    retryView.setVisibility(View.GONE);
                                    String imageUrl = jsonObject.getString("path");
                                    storeObjectDetails.put(mapDetails.get("setTag").toString(), imageUrl);
                                   // Log.i(TAG, "Retry Success  ImageView storeCustomerDetails:-  " + storeObjectDetails);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("Error ", " Error Retry Upload Image in ImageView :  " + e.getMessage());
                                }

                            }

                            @Override
                            public void onResponseFailure(int responseCode, String responseMsg) {
                                Toast.makeText(context,responseMsg,Toast.LENGTH_SHORT).show();
                                try {
                                    storeObjectDetails.put(mapDetails.get("setTag").toString(), tempImageFile);
                                    Log.i(TAG, " Retry Failure ImageView  onResponseFailure:-  " + storeObjectDetails);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("Error ", " Error Upload Image in ImageView onResponseFailure :  " + e.getMessage());
                                }


                            }
                        }, "file", "image.png", tempImageFile, null, null);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        //todo for testing
       /* imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                retryView.setVisibility(View.GONE);
                textErrorMsg.setVisibility(View.GONE);
                startCamera();
                imageCroppedListener = new ImageCroppedListener() {
                    @Override
                    public void onImageCropped(Intent data) {
                        try {
                            Log.i(TAG,"Imageview After  onImageCropped data>>>>>>>>>>>>>>>>>>  "+data);
                            if (data != null) {
                                if(data.getExtras()!= null && data.getData() == null){
                                    Log.i(TAG,"Print if data has only bitmap :-----------"+data.getData() +"  "+ data.getExtras() );
                                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                                    Uri tempUri = getImageUri(getActivity(), data.getExtras().getParcelable("data"));
                                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                                    tempImageFile = new File(getRealPathFromURI(tempUri));
                                }
                                Log.e(TAG,"Imageview  onImageCropped Final ImagePath to upload:-  "+ tempImageFile.getAbsolutePath());
                                Toast.makeText(context,"Image upload failed",Toast.LENGTH_SHORT).show();
                                textErrorMsg.setVisibility(View.VISIBLE);
                                retryView.setVisibility(View.VISIBLE);
                                setImageUsingGlide(context,tempImageFile.toString(),imageView);
                                mapDetails.put("SetValue",tempImageFile.getAbsolutePath());
                                storeObjectDetails.put(mapDetails.get("setTag").toString(), tempImageFile.getAbsolutePath());
                                Log.e(TAG, "ImageView storeCustomerDetails onResponseFailure:-  " + storeObjectDetails);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e("Error "," Error Upload Image in ImageView :  "+ e.getMessage());
                        }
                    }
                };

            }
        });*/

    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }


    }
    //create ViewPager (Slide show) with title
    private void createViewPager(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.viewpager_layout, null);
        TextView imageTitle = view.findViewById(R.id.imageTitle);
        ImageButton btnEditImage = view.findViewById(R.id.btnEditImage);
        ViewPager viewPager = view.findViewById(R.id.viewPager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(context, new ArrayList<Object>());
        viewPager.setAdapter(viewPagerAdapter);

        if (mapDetails.containsKey("setTag")) {
            viewPager.setTag(mapDetails.get("setTag").toString());
        }

        if (mapDetails.containsKey("setTitle")) {
            imageTitle.setText(mapDetails.get("setTitle").toString());
        }else
            imageTitle.setVisibility(View.GONE);

        JSONArray imageUploadedList = new JSONArray();
        if (mapDetails.containsKey("SetValue")) {
            Object value = mapDetails.get("SetValue");
            //imageUploadedList = (JSONArray) mapDetails.get("SetValue");
            ArrayList<Object> imageList = ObjectData.convertJsonArrayToArrayObject((JSONArray) value);
            viewPagerAdapter.updateViewPagerList(imageList);
        }else{
            viewPagerAdapter.updateViewPagerListItem(R.drawable.placeholder);
        }
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.invalidate();


        /*btnEditImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // onSelectImageClick(v);
                imageCroppedListener = new ImageCroppedListener() {
                    @Override
                    public void onImageCropped(CropImage.ActivityResult result) {
                        // Log.e(TAG,"ViewPager btnEditImage.setOnClickListener  onImageCropped:-  "+result);
                        uploadImageToServer(result.getUri().getPath(), new ImageUploadListener() {
                            @Override
                            public void onImageUploaded(String sResponse) {
                                viewPagerAdapter.updateViewPagerListItem(result.getUri());
                                viewPagerAdapter.notifyDataSetChanged();
                                try {
                                    JSONObject imagePath = new JSONObject(sResponse);
                                    String imageUrl = imagePath.getString("path");
                                    imageUploadedList.add(imageUrl);
                                    storeObjectDetails.put(mapDetails.get("setTag").toString(), imageUploadedList);

                                    Log.e(TAG,"ViewPager storeCustomerDetails:-  "+storeObjectDetails);

                                } catch (JSONException e) {
                                    Log.e("Error "," Error Upload Image in ImageView :  "+ e.getMessage());
                                }

                            }
                        });
                    }
                };

            }
        });*/
        btnEditImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CheckNetworkConnection.networkHasConnection(context)){
                    startCamera();
                    imageCroppedListener = new ImageCroppedListener() {
                        @Override
                        public void onImageCropped(Intent data) {
                            try {
                                if (data != null) {
                                    if(data.getExtras()!= null && data.getData() == null){
                                        Log.i(TAG,"Print if data has only bitmap :-----------"+data.getData() +"  "+ data.getExtras() );
                                        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                                        Uri tempUri = getImageUri(getActivity(), data.getExtras().getParcelable("data"));
                                        // CALL THIS METHOD TO GET THE ACTUAL PATH
                                        tempImageFile = new File(getRealPathFromURI(tempUri));

                                    }
                                    if(tempImageFile!=null ) {
                                        DaveModels daveModels = new DaveModels(context, false);
                                        daveModels.uploadImageToServer(new DaveAIListener() {
                                            @Override
                                            public void onReceivedResponse(JSONObject jsonObject) {
                                                try {
                                                    if (data.getData() != null) {
                                                        Log.i(TAG, "Print After cropped get URI:-----------" + data.getData());
                                                        viewPagerAdapter.updateViewPagerListItem(data.getData());
                                                        viewPagerAdapter.notifyDataSetChanged();
                                                    } else if (data.getExtras() != null) {
                                                        Bitmap croppedBitmap = data.getExtras().getParcelable("data");
                                                        Log.i(TAG, "Print After cropped get Bitmap:-----------" + croppedBitmap);

                                                        viewPagerAdapter.updateViewPagerListItem(croppedBitmap);
                                                        viewPagerAdapter.notifyDataSetChanged();
                                                    }
                                                    String imageUrl = jsonObject.getString("path");
                                                    imageUploadedList.put(imageUrl);
                                                    storeObjectDetails.put(mapDetails.get("setTag").toString(), imageUploadedList);
                                                    Log.e(TAG, "ViewPAger storeCustomerDetails:-  " + storeObjectDetails);

                                                } catch (Exception e) {
                                                    Log.e("Error ", " Error Upload Image in ImageView :  " + e.getMessage());
                                                }

                                            }

                                            @Override
                                            public void onResponseFailure(int responseCode, String responseMsg) {
                                                Toast.makeText(context,responseMsg,Toast.LENGTH_SHORT).show();
                                                //todo save temp file in object and show retry button

                                            }
                                        }, "file", "image.png", tempImageFile.getAbsolutePath(), null, null);

                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.e("Error "," Error Upload Image in ViewPager  ImageView :  "+ e.getMessage());
                            }

                        }
                    };

                }else {
                    CheckNetworkConnection.showNetDisabledAlertToUser(context, "To Upload need Internet Connection");
                }
            }
        });

        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }

     //Create GridView
    /*private GridView createGridView(Context context, HashMap<String, Object> mapDetails) {


        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.viewpager_layout, null);
        Spinner spinnerName =view.findViewById(R.id.spinner);

        ImageView imageView_name = new ImageView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 8, 0, 8);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        imageView_name.setLayoutParams(layoutParams);
        imageView_name.setMaxHeight(R.dimen._120sdp);
        imageView_name.setMaxWidth(R.dimen._120sdp);
        imageView_name.setImageResource(R.drawable.no_image);
        imageView_name.setScaleType(ImageView.ScaleType.FIT_XY);

        if (mapDetails.containsKey("setTag")) {
            imageView_name.setTag(mapDetails.get("setTag").toString());
        }

        if (mapDetails.containsKey("SetValue")) {
            String imageUrl = "";
            Object value = mapDetails.get("SetValue");


        }
        return imageView_name;
    }


*/
    //create date time view
    private void createDateTimeView(String uiElement, HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack) {

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;

        View view = inflater.inflate(R.layout.edit_text_layout, null);
        TextView viewTitle = view.findViewById(R.id.viewTitle);
        EditText editText = view.findViewById(R.id.editText);
        editText.setBackgroundResource(R.drawable.date_time_background);

        if (mapDetails.containsKey("setTitle")) {
            viewTitle.setText(mapDetails.get("setTitle").toString());
        }

        if (mapDetails.containsKey("setTag")) {
            editText.setTag(mapDetails.get("setTag").toString());
        }
        if (mapDetails.get("setHint") != null) {
            editText.setHint(mapDetails.get("setHint").toString());
        }
        if (mapDetails.containsKey("SetValue")) {
            editText.setText(mapDetails.get("SetValue").toString());
        }

        switch (uiElement) {
            case "date":
                new DateTimePickerDialog(editText, context,DateTimePickerDialog.DATE_DIALOG_ID);
                break;
            case "time":
                new DateTimePickerDialog(editText, context, DateTimePickerDialog.TIME_DIALOG_ID);
                break;
            case "datetime":
                new DateTimePickerDialog(editText, context, DateTimePickerDialog.DATE_TIME_DIALOG_ID);
                break;
        }

        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);
    }

    private GoogleMap mMap;
    //create rating bar with Title
    private void createMapView(HashMap<String, Object> mapDetails,OnCreateDynamicViewCallBack onCreateDynamicViewCallBack){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.google_map_view, null);
        TextView title = view.findViewById(R.id.title);
       // EditText editSearchView = view.findViewById(R.id.editSearchView);
        SearchView searchView = view.findViewById(R.id.searchView);
        FrameLayout frameLayout = view.findViewById(R.id.mapCevent);


        //SupportMapFragment mapFragment = view.findViewById(R.id.map);
        // SupportMapFragment smf = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));

        if (mapDetails.containsKey("setTitle")) {
            title.setText(mapDetails.get("setTitle").toString());
        }

        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mapCevent, mapFragment,"MapView").commit();


        GoogleMap.OnMarkerDragListener markerDragListener = new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker arg0) {
                Log.e(TAG,"Print on onMarkerDragStart:------+----"+ arg0);
            }
            @Override
            public void onMarkerDragEnd(Marker arg0) {
                Log.e(TAG, "onMarkerDragEnd..." + arg0.getPosition());
                //LatLng latLng = arg0.getPosition();
                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.getPosition()));
                try {
                    storeObjectDetails.put(mapDetails.get("setTag").toString(),arg0.getPosition().latitude+","+arg0.getPosition().longitude);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            @Override
            public void onMarkerDrag(Marker arg0) {
                Log.e(TAG,"Print on marker drag:------+----"+ arg0);
            }
        };

        GoogleMap.OnCameraIdleListener onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng latLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(getActivity());

               /* try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0).getAddressLine(0);
                        String country = addressList.get(0).getCountryName();
                        if (!locality.isEmpty() && !country.isEmpty())
                            resutText.setText(locality + "  " + country);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }*/

            }
        };

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                mMap = googleMap;
                int height = frameLayout.getWidth();
                frameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,height));

                Log.e(TAG,"Show marker in map :-------------"+ mapDetails);
                if(mapDetails.containsKey("SetValue")) {
                    String striLoc = mapDetails.get("SetValue").toString();
                    String[] parts = striLoc.split(",");
                    String lat = parts[0];
                    String lon = parts[1];
                    Log.e(TAG, "Show current GeoLocation:-----------------" + lat+"  "+ lon);
                    LatLng latLng = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
                    Log.e(TAG, "Show current latLng:---------" + latLng);
                    mMap.addMarker(new MarkerOptions()
                            .position(latLng))
                            .setDraggable(true);
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
                }
                mMap.setOnMarkerDragListener(markerDragListener);
                //mMap.setOnCameraIdleListener(onCameraIdleListener);



            }
        });

        MarkerOptions markerOptions = new MarkerOptions();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Log.e(TAG,"Print search query for map : "+ query);
                mMap.clear();
                if(query.length()>2) {
                    Geocoder coder = new Geocoder(context);
                    List<Address> address;
                    try {
                        //Get latLng from String
                        address = coder.getFromLocationName(query, 5);

                        //Lets take first possibility from the all possibilities.
                        Address location = address.get(0);

                        Log.e(TAG, "On map ready " + location + "  " + location.getLatitude() + " " + location.getLongitude());
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                        //Put marker on map on that LatLng
                        mMap.addMarker(markerOptions.position(latLng)).setDraggable(true);
                        // .icon(BitmapDescriptorFactory.fromResource(R.drawable.bb)));

                        //Animate and Zoon on that map location
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                        mMap.setOnMarkerDragListener(markerDragListener);
                        storeObjectDetails.put(mapDetails.get("setTag").toString(),location.getLatitude()+","+location.getLongitude());

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e(TAG,"Print onQueryTextChange "+ newText);
                return false;
            }
        });
        setCreateDynamicViewCallBack(view,onCreateDynamicViewCallBack);

    }




    /**************************************************Create View Group Start **************************************************/
    private static LinearLayout createLinearLayout(Context context) {

        LinearLayout l_layout = new LinearLayout(context);
        l_layout.setOrientation(LinearLayout.VERTICAL);
        // set Layout_Width and Layout_Height
        LinearLayout.LayoutParams layoutForOuter = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        l_layout.setLayoutParams(layoutForOuter);

        return l_layout;
    }

    private static RelativeLayout createRelativeLayout(Context context) {

        RelativeLayout r_layout = new RelativeLayout(context);
        // set Layout_Width and Layout_Height
        RelativeLayout.LayoutParams layoutForOuter = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        r_layout.setLayoutParams(layoutForOuter);

        return r_layout;
    }

    private static ScrollView createScrollView(Context context) {

        ScrollView scroll_view = new ScrollView(context);
        // set Layout_Width and Layout_Height
        RelativeLayout.LayoutParams layoutForOuter = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        scroll_view.setLayoutParams(layoutForOuter);

        return scroll_view;
    }

    private static CardView createCardView(Context context) {

        CardView card_view = new CardView(context);
        LinearLayout.LayoutParams layoutForOuter = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        card_view.setLayoutParams(layoutForOuter);
        card_view.setContentPadding(2, 1, 2, 1);
        card_view.setMaxCardElevation(5);
        card_view.setCardElevation(9);

        return card_view;
    }

    /**************************************************Create View Group End**************************************************/
    private ImageCroppedListener imageCroppedListener;
  /*  private interface ImageCroppedListener {
        // void onImageCropped(Uri imageUri,Bitmap imageBitMap,String imagePath);
        void onImageCropped(CropImage.ActivityResult result);


    }*/

    private interface ImageCroppedListener {
        void onImageCropped(Intent data);

    }



 /*   @SuppressLint("NewApi")
    public void onSelectImageClick(View view) {
        if (CropImage.isExplicitCameraPermissionRequired(getActivity())) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
        } else {
            CropImage.startPickImageActivity(getActivity());
           // startCamera();

        }
    }*/

   /* private void startCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE);
        }
    }*/

  /*  @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {

        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //CropImage.startPickImageActivity(this);
                CropImage.startPickImageActivity(getContext(),DynamicForm.this);
                //startCamera();
            } else {
                Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }



    }*/

 /*   @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG,"*********************onActivityResult***************************  "+ resultCode);
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(context, data);
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(context, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }



        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Log.e("Path of","imageCroppedListener.******************  "+ imageCroppedListener);
                if(imageCroppedListener!=null)
                    imageCroppedListener.onImageCropped(result);
                Log.e("Path of"," Cropping successful, Sample:.******************  "+ result.getSampleSize() + "Result Bitmap :- "+result.getBitmap());
                imageCroppedListener = null;
                uploadImageToServer(result.getUri().getPath());
                if(imageViewType.equals("viewPager")){
                    viewPagerAdapter.updateViewPagerListItem(result.getBitmap());
                    viewPagerAdapter.notifyDataSetChanged();
                }else{
                    imageView.setImageURI(result.getUri());
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_SHORT).show();
            }
        }


    }*/

    /**
     * Start crop image activity for the given image.
     */
   /* private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(getContext(), this);
    }*/

    private void startCamera(){
       // Log.i(TAG,"startCamera:-------------------"+checkPermission());
        if (checkPermission()) {
            openCameraIntent();
        } else {
            requestPermission();
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE);
    }



    File tempImageFile = null;
    private void openCameraIntent(){
        try {
            //use standard intent to capture an image
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // File imageFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/picture.jpg");
            tempImageFile = new  MultiUtil(context).createImageFile();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                picUri = FileProvider.getUriForFile(context, new MultiUtil(context).getProviderAuthority(), tempImageFile);
            } else {
                picUri = Uri.fromFile(tempImageFile); // convert path to Uri
            }
            Log.i(TAG,"openCameraIntent:-----temp file path-------"+tempImageFile +" picUri:-"+ picUri);
            takePictureIntent.putExtra( MediaStore.EXTRA_OUTPUT, picUri );
            takePictureIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            takePictureIntent.putExtra("return-data", "true");
            startActivityForResult(takePictureIntent, CAMERA_CAPTURE);

        } catch(ActivityNotFoundException anfe){
            String errorMessage = "Whoops - your device doesn't support capturing images!";
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
           e.printStackTrace();
        }
    }

    //keep track of camera capture intent
    final int CAMERA_CAPTURE = 1;
    //keep track of cropping intent
    final int PIC_CROP = 3;
    private Uri picUri;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        Log.e(TAG,"onRequestPermissionsResult:-------------------"+requestCode);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    // main logic
                    openCameraIntent();
                } else {
                    Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= M) {
                        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermission();
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Log.i(TAG,"onActivityResult*****RESULT_OK***resultCode>>>>>>"+ RESULT_OK +" == "+resultCode +  " requestCode"+  requestCode);
            if (requestCode == CAMERA_CAPTURE) {
                CropImage();

            } else if (requestCode ==PIC_CROP) {
                Log.e("Path of","imageCroppedListener.******************  "+ imageCroppedListener+"  ImageCapturePath:- "+ data.getData() );
               if(imageCroppedListener!=null)
                   imageCroppedListener.onImageCropped(data);
               imageCroppedListener = null;
            }
        }else{
            Log.e(TAG,"onActivityResult****RESULT_CANCELED****resultCode>>>>>>>>>"+ RESULT_CANCELED +" == "+resultCode );
            if(requestCode ==CAMERA_CAPTURE )
                Toast.makeText(context,"Image Captured Failed",Toast.LENGTH_SHORT).show();
            else if (requestCode == PIC_CROP)
                Toast.makeText(context,"Image Cropping Failed"+"",Toast.LENGTH_SHORT).show();
        }
    }

    private void CropImage() {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            if(Build.VERSION.SDK_INT > M){
                context.getApplicationContext().grantUriPermission("com.android.camera", picUri,
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);

                cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
            }
            //indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //cropIntent.setData(picUri);


            //indicate aspect of desired crop
            //cropIntent.putExtra("aspectX", 1);
            //cropIntent.putExtra("aspectY", 1);

            //indicate output X and Y
            //cropIntent.putExtra("outputX", 256);
            //cropIntent.putExtra("outputY", 256);

            // cropIntent.putExtra("scale", true); // boolean
            //cropIntent.putExtra("scaleUpIfNeeded", true); // boolean

            // cropIntent.putExtra("spotlightX", spotlightX); // int
            // cropIntent.putExtra("spotlightY", spotlightY); // int

            //retrieve data on return true - don't return uri |  false - return uri
            cropIntent.putExtra("return-data", true);
            //cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);

            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        catch (ActivityNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Your device is not supporting the crop action", Toast.LENGTH_SHORT).show();

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

   /* private void setImageUsingGlide(Context context,String imageUrl , ImageView imageViewName){
        Glide.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.no_image)
                .override(250, 250)
                .centerCrop()
                .into(imageViewName);

    }*/

    private boolean checkIsRemoteUrlPath(String source){
        //String protocol = null;
        try {
           /* boolean isUrl =  URLUtil.isFileUrl(source);
            Log.i(TAG,">>>>>>>>>>>>isUrl:- "+ isUrl + " Source :--- "+source );

            boolean bb = Patterns.WEB_URL.matcher(source).matches();
            Log.e(TAG,"Patterns.WEB_URL.matcher:----"+bb);*/
            if(HttpUrl.parse(source) != null){
                Log.e(TAG,"Print HttpUrl.parse(source).scheme():-" + HttpUrl.parse(source).scheme());
            }
            return URLUtil.isHttpsUrl(source) || URLUtil.isHttpUrl(source);



           /* final URI uri = new URI(source);
            Log.i(TAG,"Print uri>>>>>>>>>>>>>" +uri.isAbsolute() );

            if( uri.isAbsolute()) {
                //protocol = uri.getScheme();
                Log.e(TAG,"\n <<<<<<<<<<<<<<<<<<<<<<<< if get Uri protocol >>>>>>>>>>>>>" +protocol );
            }
            else {
                final URL url = new URL( source );
                protocol = url.getProtocol();
                Log.e(TAG,"\n<<<<<<<<<<<<<<<<<<<<<<<< else get Url>>>>>>>>>>>>>" +"\n url:-"+url+" prootocol:-"+ protocol);
            }*/

           /* if (uri.getScheme() != null && (uri.getScheme().equals("content") || uri.getScheme().equals("file"))) {
                Log.e(TAG,"if uri.getScheme()>>>>>>>>>>>>>" + uri.getScheme());
            } else {
                Log.e(TAG,"Else >>>>>>>>>>>>>"  );
            }*/

        } catch( final Exception e ) {
            e.printStackTrace();
            Log.e(TAG,"Error >>>>>>>>>>>>>>" +e.getMessage());
            // Could be HTTP, HTTPS?
           /* if( source.startsWith( "//" ) ) {
                throw new IllegalArgumentException( "Relative context: " + source );
            }
            else {
                final File file = new File( source );
                protocol = getProtocol( file );
            }*/
        }

        return false;

    }

    /**
     * Returns the protocol for a given file.
     *
     * @param file Determine the protocol for this file.
     *
     * @return The protocol for the given file.
     */
    private static String getProtocol( final File file ) {
        String result;
        try {
            result = file.toURI().toURL().getProtocol();
        } catch( Exception e ) {
            result = "unknown";
        }
        return result;
    }



}

/*    private void postObjectToServer(String postCustomerDetails)  {
        Log.e(TAG, "Create Or Update  Object post Body >>>>> " + postCustomerDetails);

        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postCustomerDetails);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                if(response!=null && !response.isEmpty()) {
                    if (mListener != null)
                        mListener.onCreateOrUpdateView(response);
                }
                else
                    Toast.makeText(getActivity(),"Something is wrong with the server connection... please try after sometime",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Log.e(TAG," onTaskFailure requestCode :- "+requestCode +" && responseMsg:- "+responseMsg);
                if (requestCode >= 400) {
                    if( responseMsg!= null && !responseMsg.isEmpty())
                        Toast.makeText(getActivity(),responseMsg,Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(),"Something is wrong with the server connection... please try after sometime",Toast.LENGTH_SHORT).show();
                }

                if (mListener != null)
                    mListener.onFailureCreateOrUpdate(responseMsg);

            }
        };
        if (modelName != null && !modelName.isEmpty()) {

            //Log.e(TAG, "<<<<methodName>>>>>>>>>>>>>" + methodName);
            if (methodName.equals("POST")) {
                new APICallAsyncTask(postTaskListener, getActivity(), methodName, body_part,true)
                        .execute(APIRoutes.postObjectAPI(modelName));
            } else if (methodName.equals("UPDATE")) {
                String exitID = "";
                try {
                    JSONObject jsonObject = new JSONObject(objectDetails);
                    exitID = jsonObject.getString(objectId);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("createOrUpdateObject", "Error During Reading ExitingID:---   " + exitID);
                }
                new APICallAsyncTask(postTaskListener, getActivity(), methodName, body_part,true)
                        .execute(APIRoutes.updateOrGetObjectAPI(modelName, exitID));
            }
        }
        else{
            Log.e(TAG, "<<<<Model Name is Null Or Empty>>>>>>>>>>>>>>" + modelName);
        }
    }*/
