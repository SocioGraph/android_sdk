package com.example.admin.daveai.dialogbox;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.admin.daveai.R;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveHelper;
import com.example.admin.daveai.others.DaveModels;
import com.squareup.okhttp.RequestBody;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;


import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;

public class SMSTemplate extends Dialog {


    private final String TAG = getClass().getSimpleName();
    public static final int SEND_SMS_TEMPLATE = 0;
    public static final int SEND_MAIL_TEMPLATE = 1;
    private Context mContext;
    private Spinner selectTemplate;
    private CustomProgressDialog customProgressDialog;
    private LinearLayout parentLayout;
    private JSONObject jsonTemplateObject = new JSONObject();
    private DaveAIPerspective daveAIPerspective;
    private String customerMobileOrEmail = "";
    int sendType = -1;

    public SMSTemplate(Context context, String mobileOrEmail , int sendType){
        super(context);
        this.mContext = context;
        this.customerMobileOrEmail =mobileOrEmail;
        this.sendType=sendType;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sms_template);

        parentLayout = findViewById(R.id.parentLayout);
        selectTemplate = findViewById(R.id.selectTemplate);
        Button btnSend = findViewById(R.id.btnSend);
       // Button btnCancel = findViewById(R.id.btnCancel);
        Button btnSendWithTem = findViewById(R.id.btnSendWithTem);

        customProgressDialog = new CustomProgressDialog(mContext);
        daveAIPerspective= DaveAIPerspective.getInstance();
       // daveAIPerspective= new DaveAIPerspective(mContext);

        /* btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
                dismiss();

            }
        });*/

        getSmsTemplateList();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(sendType==SEND_SMS_TEMPLATE) {
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", customerMobileOrEmail);
                    //smsIntent.putExtra("sms_body","message");
                    mContext.startActivity(smsIntent);

                }else if(sendType == SEND_MAIL_TEMPLATE) {

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{customerMobileOrEmail});
                    i.putExtra(Intent.EXTRA_SUBJECT, "");
                    i.putExtra(Intent.EXTRA_TEXT, "");
                    mContext.startActivity(Intent.createChooser(i, "Email:"));
                }

            }
        });

        btnSendWithTem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = selectTemplate.getSelectedItemPosition();
                if (pos != 0) {
                    sendSmsTemplate(selectTemplate.getSelectedItem().toString());

                }else
                    Toast.makeText(mContext,"Please Select Template",Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getSmsTemplateList(){
        //customProgressDialog.showProgressBar();
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (response != null && !response.isEmpty()) {
                    try {
                        JSONObject mainObject = new JSONObject(response);
                        jsonTemplateObject = new JSONObject(response);
                        ArrayList<String> optionList = new ArrayList<String>();
                        if (mainObject.names() != null) {
                            optionList.add("Select Template");
                            for (int i=0;i<mainObject.names().length();i++){
                                optionList.add(mainObject.names().get(i).toString());
                            }
                        }
                       // Log.e("Print Template","optionList:------   "+optionList);
                        if (!optionList.isEmpty()) {
                            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter<String>
                                    (mContext, android.R.layout.simple_spinner_dropdown_item, optionList);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            selectTemplate.setAdapter(spinnerArrayAdapter);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {

                    Toast.makeText(mContext,"Something is wrong with the server connection...please try after sometime",
                            Toast.LENGTH_SHORT).show();
                    dismiss();
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Toast.makeText(mContext,"Something is wrong with the server connection...please try after sometime",Toast.LENGTH_SHORT).show();
                dismiss();
            }
        };
        new APICallAsyncTask(postTaskListener, mContext, "GET", true).execute(APIRoutes.smsTemplateListAPI());

    }

    private void sendSmsTemplate(String selectedTem){

        //customProgressDialog.showProgressBar();
        //JSONObject post_body = new JSONObject();
        try {
            JSONObject post_body =   jsonTemplateObject.getJSONObject(selectedTem);
            // Get User Singleton
            DaveModels daveModels = new DaveModels(mContext,true);
            JSONObject getUserObject = daveModels.getSingleton(daveAIPerspective.getUser_login_model_name());
            post_body.put("user", getUserObject);

            JSONObject customer_Details = DaveHelper.getInstance().getCustomerDetails();
           // JSONObject customer_Details =  new JSONObject(CustomerInteractions.customerDetails);
            //post_body.put("reciver_ids",customer_Details.getString(Model.customerIdAttrName));
            post_body.put("customer_id",customer_Details.getString(new ModelUtil(mContext).getCustomerIdAttrName()));
            post_body.put("customer", customer_Details);

             Log.e(TAG, "<<<<<<<<<sendSmsTemplate>>>>>>>>>" + post_body);

             if (CheckNetworkConnection.networkHasConnection(mContext)){
                RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, post_body.toString());
                APIResponse postTaskListener = new APIResponse() {
                    @Override
                    public void onTaskCompleted(String response) {
                        dismiss();
                       // Toast.makeText(mContext,response,Toast.LENGTH_SHORT).show();
                        Toast.makeText(mContext,"Template sent successfully...",Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onTaskFailure(int requestCode, String responseMsg) {
                        Toast.makeText(mContext,"Something is wrong with the server connection...please try after sometime",Toast.LENGTH_SHORT).show();

                    }
                };

                if(sendType == SEND_SMS_TEMPLATE)
                    new APICallAsyncTask(postTaskListener,mContext,"POST", body_part,true).execute(APIRoutes.smsSendTemplateAPI());
                else if(sendType == SEND_MAIL_TEMPLATE)
                    new APICallAsyncTask(postTaskListener,mContext,"POST", body_part,true).execute(APIRoutes.mailSendTemplateAPI());
             }else{
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }

        }catch(Exception e){
            Log.e(TAG, "********postInteraction Error*********   " + e.getLocalizedMessage());
        }

    }

}
