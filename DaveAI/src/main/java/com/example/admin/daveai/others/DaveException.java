package com.example.admin.daveai.others;

public abstract class DaveException extends Exception{

    public DaveException(){}
    public DaveException(String s){
        super(s);
    }

    public static class DaveAIListenerException extends DaveException{
        public DaveAIListenerException(String s){
            super(s);
        }
    }

    public static class ModelNotFound extends DaveException{
        public ModelNotFound(String s){
            super(s);
        }
    }

    public static class ObjectIdNotFound extends DaveException{
        public ObjectIdNotFound(String s){
            super(s);
        }
    }



}
