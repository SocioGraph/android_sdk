package com.example.admin.daveai.others;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;


import com.example.admin.daveai.R;


import java.util.HashMap;


public class DaveIconService extends Service  {

    private String TAG = "DaveIconService";
    private WindowManager windowManager;
    private View daveIcon;
    WindowManager.LayoutParams params;
    String dave_function="";
    String customer_id = "";
    String product_id="";


    @Override
    public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        //Inflate the floating view layout we created
        daveIcon = LayoutInflater.from(this).inflate(R.layout.dave_icon,null);
        ImageView myButton = (ImageView) daveIcon.findViewById( R.id.dave_icon );

   /*     new SimpleTooltip.Builder(DaveIconService.this)
                .anchorView(myButton)
                .text("Text")
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(false)
               // .dismissOnInsideTouch(true)
               // .modal(true)
                //.transparentOverlay(false)
               // .arrowDirection(Gravity.CENTER)
                .animated(true)
                .build()
                .show();*/

       /* Tooltip mTooltip = new Tooltip.Builder(daveIcon.findViewById(R.id.imageButton), R.style.Tooltip)
                .setDismissOnClick(true)
                .setGravity(Gravity.BOTTOM)
                .setText("I`m on the bottom of first menu item and showing dynamically on menu item click")
                .show();*/

        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }
        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
               // WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.END;
        windowManager.addView(daveIcon, params);

        //Drag and move floating view using user's touch action.
        daveIcon.findViewById(R.id.dave_icon).setOnTouchListener(new View.OnTouchListener() {

            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if ((Math.abs(initialTouchX - event.getRawX()) < 5) && (Math.abs(initialTouchY - event.getRawY()) < 5)) {
                            HashMap<String, String> dave_intent = new HashMap<>();
                            dave_intent.put("customer_id", customer_id);
                            dave_intent.put("product_id", product_id);
                            dave_intent.put("Dave_function", dave_function);

                            //TODO open Dave Function

                          /*  Intent intent = new Intent(DaveIconService.this, DaveFunction.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("message_from_dave", dave_intent);
                            startActivity(intent);*/

                        } else Log.e(TAG, "you moved the head");

                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(daveIcon, params);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (daveIcon != null)
            windowManager.removeView(daveIcon);
    }


    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras()!=null) {
            HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("message_to_dave");
            if (hashMap != null) {
                customer_id = hashMap.get("customer_id");
                dave_function = hashMap.get("Dave_function");
                product_id = hashMap.get("product_id");
            }
        }
        return START_STICKY; // or whatever your flag
    }



}
