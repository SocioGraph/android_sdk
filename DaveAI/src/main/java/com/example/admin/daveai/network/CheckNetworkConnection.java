package com.example.admin.daveai.network;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

public class CheckNetworkConnection {

    public static boolean networkHasConnection(Context context){

        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()){
            return true;
        }
        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()){
            return true;
        }
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()){
            Log.d("Network", "Connected");
            return true;
        }

       /* NetworkInfo info = Connectivity.getNetworkInfo(context);
        if(info.getType() == ConnectivityManager.TYPE_WIFI){
            // do something
        } else if(info.getType() == ConnectivityManager.TYPE_MOBILE){
            // check NetworkInfo subtype
            if(info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS){
                // Bandwidth between 100 kbps and below
            } else if(info.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE){
                // Bandwidth between 50-100 kbps
            } else if(info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0){
                // Bandwidth between 400-1000 kbps
            } else if(info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A){
                // Bandwidth between 600-1400 kbps
            }

            // Other list of various subtypes you can check for and their bandwidth limits
            // TelephonyManager.NETWORK_TYPE_1xRTT       ~ 50-100 kbps
            // TelephonyManager.NETWORK_TYPE_CDMA        ~ 14-64 kbps
            // TelephonyManager.NETWORK_TYPE_HSDPA       ~ 2-14 Mbps
            // TelephonyManager.NETWORK_TYPE_HSPA        ~ 700-1700 kbps
            // TelephonyManager.NETWORK_TYPE_HSUPA       ~ 1-23 Mbps
            // TelephonyManager.NETWORK_TYPE_UMTS        ~ 400-7000 kbps
            // TelephonyManager.NETWORK_TYPE_UNKNOWN     ~ Unknown

        }*/
        return false;
    }

    public static void showNetDisabledAlertToUser(final Context context){

        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("No Internet Connect")
                .setMessage("Please check your internet connection")
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //dialog.cancel(); dialog.dismiss();
                    }
                })
                .show();
    }


    public static void showNetDisabledAlertToUser(final Context context,String internetMsg){

        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("No Internet Connect")
                .setMessage(internetMsg)
                .setCancelable(false)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //dialog.cancel(); dialog.dismiss();
                    }
                })
                .show();
    }

    public static void methodToToastAPICallError( Context mContext, int responseCode){

        if(responseCode >=400 && responseCode<500){
            Toast.makeText(mContext, "Requested resource not found", Toast.LENGTH_LONG).show();

        }else if(responseCode >= 500 && responseCode < 600){

            Toast.makeText(mContext, "Something went wrong at server end. try after sometime..", Toast.LENGTH_LONG).show();

        }else{
            Toast.makeText(mContext, "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]", Toast.LENGTH_LONG).show();

        }
    }

}
