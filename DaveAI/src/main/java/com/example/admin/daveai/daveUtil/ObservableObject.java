package com.example.admin.daveai.daveUtil;

import android.content.Intent;

import java.util.Observable;


/**
 * Created by Rinki on 12,August,2019
 */
public class ObservableObject extends Observable {

    private static ObservableObject instance = new ObservableObject();

    public static ObservableObject getInstance() {
        return instance;
    }

    private ObservableObject() { }

    public void updateValue(Object data) {
        synchronized (this) {
            setChanged();
            notifyObservers(data);
        }
    }

}

//Use To communicate from broadcast rececier to activity

//step : 1
/*
 in Broadcast receiver class :-
 ObservableObject.getInstance().updateValue(intent);
 */

//step :2
/*
 In activity class :
 i) on create method
       ObservableObject.getInstance().addObserver(this);
 ii)
      @Override
    public void update(Observable observable, Object data) {

        Log.d(TAG,"Print ;activity observer " + data);
        Intent hh = (Intent) data;
        Bundle bundle =  hh.getExtras();
      }
 */

