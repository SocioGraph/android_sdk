package com.example.admin.daveai.daveUtil;

import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.model.Attribute;
import com.example.admin.daveai.model.InteractionStages;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;



public class ModelUtil implements DaveConstants, DatabaseConstants {

    private final static String TAG ="ModelUtil";
    private Context context;
    private DaveSharedPreference sharedPreference;
    private DaveAIPerspective daveAIPerspective;


    public ModelUtil(Context context){
        this.context=context;
        sharedPreference= new DaveSharedPreference(context);
        daveAIPerspective = DaveAIPerspective.getInstance();
    }

    public  void setupModelCacheMetaData(String modelName,String modelIdAttrName){
        Log.i(TAG,"SEt CacheMeta data hot and cold update :---"+modelName+"=="+sharedPreference.getHotUpdateTime()+" "+ sharedPreference.getColdUpdateTime());
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        sharedPreference.getHotUpdateTime(),
                        sharedPreference.getColdUpdateTime(),
                        0,
                        modelIdAttrName,
                        System.currentTimeMillis())
        );


    }

    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,long coldUpdateTimeLimit,long hotUpdateTimeLimit){
        Log.i(TAG,"SEt CacheMeta data hot and cold update :---"+modelName+"=="+hotUpdateTimeLimit+" "+ coldUpdateTimeLimit);
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        hotUpdateTimeLimit,
                        coldUpdateTimeLimit,
                        0,
                        modelIdAttrName,
                        System.currentTimeMillis())
        );


    }

    /**
     *  method to get model object reference based on model jsonObject
     * @param modelResponse pass model json object
     * @return  return model object reference
     */
    public static Model getModelInstance(String modelResponse) {
        try {
            if(modelResponse!= null && !modelResponse.isEmpty()){
                Gson gson = new Gson();
                return gson.fromJson(modelResponse, Model.class);
            }
        } catch (Exception exception){
            exception.printStackTrace();
            Log.e("ModelInstance","Error during getting model:--------"+exception.getMessage());
        }
        return null;
    }

    /**
     * method to get model object reference based on modelName
     * @param context current context
     * @param modelName pass model name
     * @return return model object reference
     */

    public static Model getModelInstance(Context context, String modelName) {
        Model modelInstance;
        try {
           // Log.d(TAG,"Get Model Response from db of model :--------"+modelName);
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject modelJson = databaseManager.getModelData(modelName);
           // Log.e(TAG,"Get Model Response from db:--------"+modelJson);
            if(modelJson!= null && modelJson.length()>0){
                Gson gson = new Gson();
                modelInstance = gson.fromJson(modelJson.toString(), Model.class);
                return modelInstance;
            }
        } catch (Exception exception){
            exception.printStackTrace();
            Log.e("ModelInstance", "Gson Error setting Model respone in Model class*****  " + exception.getMessage());
        }
        return null;
    }

    public Attribute getAttributeInstanceOfModel(String modelName){
        try {
            Model modelInstance = getModelInstance(context, modelName);
            if(modelInstance!=null) {
                for (Attribute s : modelInstance.getAttributes()) {
                    if(s.isId()){
                        // Log.e(TAG, "Get Model Id Attr name :---- " + s.getName() +" get Default:-  "+ s.getDefaultX());
                        return s;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * method to get id of any model based on name
     * @param modelName pass model name
     * @return id of that particular method
     */
    public String getModelIDAttrName(String modelName){
        String modelIdAttrNAme = "";
        try {
            Attribute attribute = getAttributeInstanceOfModel(modelName);
            if(attribute!=null) {
                modelIdAttrNAme = attribute.getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelIdAttrNAme;
    }

    public String getIDAttrNameOfModel(String modelResponse){
        //Log.e(TAG,"getIDAttrNameOfModel>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+modelResponse);
        Model model_instance = getModelInstance(modelResponse);
        if(model_instance!=null) {
            for (Attribute s : model_instance.getAttributes()) {
                if(s.isId()){
                     //Log.e(TAG, "Get Model Id Name+++++++++++ " +s.getName());
                    return s.getName();
                }
            }
        }
        return "";
    }

   /* public String getUserModelName(){
        return DatabaseManager.getInstance(context).getModelNameBasedOnType("login");
    }*/


    public boolean contactExists(Context _activity, String number) {
        if (number != null) {
            Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
            String[] mPhoneNumberProjection = { ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME };
            Cursor cur = _activity.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
            try {
                if (cur.moveToFirst()) {
                    return true;
                }
            } finally {
                if (cur != null)
                    cur.close();
            }
            return false;
        } else {
            return false;
        }
    }
   public void addToContacts(String name, String number){
        Log.e(TAG,"button clicked for contactExists = "+contactExists(context,number));
        if(!contactExists(context,number)) {
            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
            int rawContactInsertIndex = ops.size();

            Log.e(TAG, "Here"+"number = "+number+" name = "+name);
            ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, AccountManager.KEY_ACCOUNT_TYPE)
                    .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, AccountManager.KEY_ACCOUNT_NAME)
                    .build());

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Contacts.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                    .build());

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, number)
                    .build());

            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, "")
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, "")
                    .build());

            //Log.i("Line43", Data.CONTENT_URI.toString()+" - "+rawContactInsertIndex);

            try {
                context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            } catch (RemoteException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (OperationApplicationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
   }

    public String getCustomerModelName(){
        return DatabaseManager.getInstance(context).getModelNameBasedOnType("customer");
    }

    public String getProductModelName(){
        return DatabaseManager.getInstance(context).getModelNameBasedOnType("product");
    }

    public String getInteractionModelName(){
        return DatabaseManager.getInstance(context).getModelNameBasedOnType("interaction");
    }

    public String getCustomerIdAttrName(){
        //DatabaseManager.getInstance(context).getSingletonRow("_customer_id_attr_name");
        return sharedPreference.readString("_customer_id_attr_name");
    }

    public String getProductIdAttrName(){
        //DatabaseManager.getInstance(context).getSingletonRow("_product_id_attr_name");
        return sharedPreference.readString("_product_id_attr_name");
    }

    public String getInteractionIdAttrName(){
        //DatabaseManager.getInstance(context).getSingletonRow("_interaction_id_attr_name");
        return sharedPreference.readString("_interaction_id_attr_name");
    }

    public String getInteractionStageAttrName(){
       // DatabaseManager.getInstance(context).getSingletonRow("_interaction_stage_attr_name");
        return sharedPreference.readString("_interaction_stage_attr_name");
    }

    public String getInteractionCustomerId(){
       // DatabaseManager.getInstance(context).getSingletonRow("_interaction_customerId_attr_name");
        return sharedPreference.readString("_interaction_customerId_attr_name");
    }

    public String getInteractionProductId(){
       // DatabaseManager.getInstance(context).getSingletonRow("_interaction_productId_attr_name");
        return sharedPreference.readString("_interaction_productId_attr_name");
    }

    public ArrayList<String> getInteractionStageNameList(){
        return sharedPreference.getArrayList("_interaction_stage_names");
    }

    public HashMap<String, Object> getInteractionStageButtonList(){
        return sharedPreference.getHashMapObjectType("_stage_button_list");
    }

    public HashMap<String, Object> getInteractionStageTemplateList(){
        return sharedPreference.getHashMapObjectType("_stage_template_list");
    }

    private String getFirstPositiveStageName(){
        return DatabaseManager.getInstance(context).getCustomSingletonRow("_first_positive_stage_name");
    }

    private String getFirstNegativeStageName(){
        return DatabaseManager.getInstance(context).getCustomSingletonRow("_first_negative_stage_name");
    }


    public boolean isCachedAttr(String modelName,String attr,Context context){
        Model model = getModelInstance(context,modelName);
        if(model.checkAttrExistInModel(attr)){
            List<String> cachedAttrs = model.getQuick_views().getCached();
            if(cachedAttrs.contains(attr)){
                return true;
            }
        }else{
            return false;
        }
        return false;
    }

    public Map<String,Object> getModelHierarchy(String modelName){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow(modelName+MODEL_HIERARCHY);
            if(singletonResponse!=null && singletonResponse.length()>0){
                Iterator<String> keys = singletonResponse.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (singletonResponse.get(key) instanceof JSONObject) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<Map<String,Object>>(){}.getType();
                        return gson.fromJson(singletonResponse.get(key).toString(), type);
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG,"Error during getting Customer Hierarchy:-"+e.toString());
        }

        return new HashMap<>() ;

    }


    public Map<String,Object> getCustomerCategoryHierarchy(){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow("customer_hierarchy");
            if(singletonResponse!=null && singletonResponse.length()>0){
                Iterator<String> keys = singletonResponse.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (singletonResponse.get(key) instanceof JSONObject) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<Map<String,Object>>(){}.getType();
                        return gson.fromJson(singletonResponse.get(key).toString(), type);
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG,"Error during getting Customer Hierarchy:-"+e.toString());
        }

        return new HashMap<>() ;

    }

    public Map<String,Object> getProductCategoryHierarchy(){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow("product_hierarchy");
            if(singletonResponse!=null && singletonResponse.length()>0){
                Iterator<String> keys = singletonResponse.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (singletonResponse.get(key) instanceof JSONObject) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<Map<String,Object>>(){}.getType();
                        return gson.fromJson(singletonResponse.get(key).toString(), type);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Product Hierarchy:-"+e.toString());
        }

        return new HashMap<>() ;

    }

    public HashMap<String,ArrayList<String>> getModelFilterAttrList(String modelName){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow(modelName+MODEL_FILTER_LIST);
            // Log.d(TAG,"<<<<<<<<<<<<<<<getCustomerFilterAttrList:--------"+singletonResponse);
            if(singletonResponse!=null && singletonResponse.length()>0){
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String,ArrayList<String>>>(){}.getType();
                return gson.fromJson(singletonResponse.toString(), type);

            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Product Hierarchy:-"+e.toString());
        }
        return new HashMap<>();
    }

    public HashMap<String,ArrayList<String>> getCustomerFilterAttrList(){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow("customerFilterList");
           // Log.d(TAG,"<<<<<<<<<<<<<<<getCustomerFilterAttrList:--------"+singletonResponse);
            if(singletonResponse!=null && singletonResponse.length()>0){
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String,ArrayList<String>>>(){}.getType();
                return gson.fromJson(singletonResponse.toString(), type);

            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Product Hierarchy:-"+e.toString());
        }
        return new HashMap<>();
    }

    public HashMap<String,ArrayList<String>> getProductFilterAttrList(){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject singletonResponse =  databaseManager.getSingletonRow("productFilterList");
            //Log.d(TAG,"<<<<<<<<<<<<<<<getProductFilterAttrList:--------"+singletonResponse);
            if(singletonResponse!=null && singletonResponse.length()>0){
                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String,ArrayList<String>>>(){}.getType();
                return gson.fromJson(singletonResponse.toString(), type);
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Product Hierarchy:-"+e.toString());
        }
        return new HashMap<>();
    }


    public String saveIDAttrNameOfCoreModel(String model_type, String modelResponse){

        Model model_instance = getModelInstance(modelResponse);
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        Log.d(TAG,"saveIDAttrNameOfCoreModel:-------------"+ model_type +" modelName:--"+ " "+model_instance);
        if(model_instance!=null) {
            for (Attribute s : model_instance.getAttributes()) {
                if(s.isId()){
                    if(model_type!=null && !model_type.isEmpty()) {
                        // sharedPreference.writeString("_"+model_type+"_id_attr_name", s.getName());
                        switch (model_type) {
                            case "customer":
                                sharedPreference.writeString("_customer_id_attr_name", s.getName());
                                databaseManager.insertOrUpdateCustomSingleton( "_customer_id_attr_name",
                                        new SingletonTableRowModel("_customer_id_attr_name",s.getName(),
                                                System.currentTimeMillis(),"_customer_id_attr_name"));
                                break;

                            case "product":
                                sharedPreference.writeString("_product_id_attr_name", s.getName());
                                databaseManager.insertOrUpdateCustomSingleton( "_product_id_attr_name",
                                        new SingletonTableRowModel("_product_id_attr_name",s.getName(),
                                                System.currentTimeMillis(),"_product_id_attr_name"));
                                break;

                            case "interaction":
                                sharedPreference.writeString("_interaction_id_attr_name", s.getName());
                                databaseManager.insertOrUpdateCustomSingleton( "_interaction_id_attr_name",
                                        new SingletonTableRowModel("_interaction_id_attr_name",s.getName(),
                                                System.currentTimeMillis(),"_interaction_id_attr_name"));
                                break;
                        }
                    }
                    return s.getName();
                }
            }
        }
        return "";
    }



    /**
     * method to save Interaction Models attributes name as singleton of :- 1) Interaction CustomerId 2) Interaction ProductId 3) Interaction Stage Name
     * @param interactionModel model name of Interaction
     */
    public void saveInteractionModelAttributes(String interactionModel){

        Model model_instance = getModelInstance(context,interactionModel);
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        if(model_instance!=null) {
            for (Attribute s : model_instance.getAttributes()) {
                if(s.getRefers()!=null){
                    if(s.getRefers().equalsIgnoreCase("parents.interaction_stage.name"))
                    {
                        sharedPreference.writeString("_interaction_stage_attr_name",s.getName());
                        databaseManager.insertOrUpdateCustomSingleton( "_interaction_stage_attr_name",
                                new SingletonTableRowModel("_interaction_stage_attr_name",s.getName(),
                                        System.currentTimeMillis(),"interaction_stage_attr_name"));
                    }
                    if(s.getRefers().equalsIgnoreCase("parents."+getCustomerModelName()+"."+getCustomerIdAttrName()))
                    {
                        sharedPreference.writeString("_interaction_customerId_attr_name",s.getName());

                        databaseManager.insertOrUpdateCustomSingleton( "_interaction_customerId_attr_name",
                                new SingletonTableRowModel("_interaction_customerId_attr_name",s.getName(),
                                        System.currentTimeMillis(),"_interaction_customerId_attr_name"));
                    }
                    if(s.getRefers().equalsIgnoreCase("parents."+ getProductModelName()+"."+getProductIdAttrName()))
                    {
                        sharedPreference.writeString("_interaction_productId_attr_name",s.getName());
                        databaseManager.insertOrUpdateCustomSingleton( "_interaction_productId_attr_name",
                                new SingletonTableRowModel("_interaction_productId_attr_name",
                                        s.getName(), System.currentTimeMillis(),"_interaction_productId_attr_name"));
                    }
                }

            }
        }
    }


    public static InteractionStages getStageInstance(Context context ,String stageName) {
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        String stageDetail = databaseManager.getObjectData(INTERACTION_STAGES_TABLE_NAME, stageName).toString();
        Log.e(TAG,"Get Stage Details of Stage >>>>>>>>>>>>>>>>>>>>>>:-------"+stageName+" == "+ stageDetail);
        if (stageDetail != null && !stageDetail.isEmpty()) {
            try {
                Gson gson = new Gson();
                return gson.fromJson(stageDetail, InteractionStages.class);

            } catch (Exception exception) {
                exception.printStackTrace();
                Log.e(TAG, "Error setting Stage Detail in InteractionStages class*****  " + stageName + " Error:-" + exception.getMessage());
            }
        }
        return null;
    }



}
