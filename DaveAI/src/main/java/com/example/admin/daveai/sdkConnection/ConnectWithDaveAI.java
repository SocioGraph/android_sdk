package com.example.admin.daveai.sdkConnection;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.broadcasting.MediaService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.BulkDownloadUtil;
import com.example.admin.daveai.daveUtil.DataConversion;
import com.example.admin.daveai.daveUtil.DaveConnectUtil;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.ProgressLoaderDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static com.example.admin.daveai.others.DaveAIStatic.quantityPredictStatus;


public class ConnectWithDaveAI implements DatabaseConstants , DaveConstants {


    private static final String TAG = "ConnectWithDaveAI";
    private Context mContext;

    private List<String> CORE_MODEL_LIST = Arrays.asList("customer", "product","interaction");

    ArrayList<String> getModelList = new ArrayList<>();
    ArrayList<String> otherPerspectives;
    private Queue<String> tempGetModelList = new LinkedList<>();
    private Queue<String> tempPrefList = new LinkedList<>();

    private Dialog pDialog;
    private DaveSharedPreference sharedPreferences;
    private DaveAIPerspective daveAIPerspective;
    private DaveConnectUtil daveConnectUtil;
    ModelUtil modelUtil;
    DatabaseManager databaseManager ;


    public ConnectWithDaveAI(Context context){
        this.mContext = context;

        sharedPreferences = new DaveSharedPreference(mContext);
        daveAIPerspective=DaveAIPerspective.getInstance();
        modelUtil = new ModelUtil(mContext);
        databaseManager = DatabaseManager.getInstance(mContext);
        APIRoutes.getMetadata(mContext);

    }

    private void showLoader(String loaderMsg){

        ProgressLoaderDialog progressLoaderDialog = new ProgressLoaderDialog(mContext);
        pDialog = progressLoaderDialog.progressDialog();
        if(TextUtils.isEmpty(loaderMsg)) {
            progressLoaderDialog.setMessage("Please wait...");
        }else {
            progressLoaderDialog.setMessage(loaderMsg);
        }
        pDialog.setCancelable(false);
        pDialog.show();

        APIRoutes.getMetadata(mContext);

    }

    public void setURLAndEnterpriseID(String enterpriseId, String baseURL){
        Log.i(TAG,"setURLAndEnterpriseID:- "+enterpriseId +" && baseURL == "+baseURL);
        sharedPreferences.writeString(DaveSharedPreference.EnterpriseId,enterpriseId);
        sharedPreferences.writeString(DaveSharedPreference.BASE_URL,baseURL);
    }

    public interface OnLoginListener {
        void loginSucess(JSONObject userLoginDetails);
        void loginFailed(String userLoginFailed);
    }

    /**
     * method to login
     * @param postBody provide post body for login
     *
     */
    public void login(JSONObject postBody,OnLoginListener onLoginListener ){

        APIResponse postTaskListener  = new APIResponse(){
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    try {
                        JSONObject loginDetails = new JSONObject(response);
                        sharedPreferences.writeString(DaveSharedPreference.USER_LOGIN_DETAILS, loginDetails.toString());
                        sharedPreferences.writeString(DaveSharedPreference.EnterpriseId, loginDetails.optString("enterprise_id"));
                        sharedPreferences.writeString(DaveSharedPreference.Role, loginDetails.optString("role"));
                        sharedPreferences.writeString(DaveSharedPreference.USER_ID, loginDetails.optString("user_id"));
                        sharedPreferences.writeString(DaveSharedPreference.API_KEY, loginDetails.optString("api_key"));
                        sharedPreferences.writeString(DaveSharedPreference.PUSH_TOKEN, loginDetails.optString("push_token"));

                        createTableDuringLogin();
                        saveLoginDetails(loginDetails.optString("role"),loginDetails.optString("user_id"));

                        if(onLoginListener!= null) {
                            onLoginListener.loginSucess(loginDetails);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error User LogIn :-  " + e.getMessage());
                    }
                }
            }

            @Override
            public void onTaskFailure(int responseCode, String responseMsg) {
                if(pDialog!=null)
                    pDialog.dismiss();
                onLoginListener.loginFailed(responseMsg);

            }
        };
        try {

            if(sharedPreferences.checkStringExist(DaveSharedPreference.USER_ID) &&
                    !postBody.getString("user_id").equalsIgnoreCase(sharedPreferences.readString(DaveSharedPreference.USER_ID))){

                //if previous login details are different from current attempt.
                Log.i(TAG,"Match USer Id from db And Current "+postBody.getString("user_id")+" =="+
                        sharedPreferences.readString(DaveSharedPreference.USER_ID));
                deleteLocalDatabase();
                sharedPreferences.removeAll();
            }

            APIRoutes.getMetadata(mContext);
            RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, postBody.toString());
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                new APICallAsyncTask(postTaskListener, mContext, "POST", body_part,
                        true, "First Time Login: This may take a long time").execute(APIRoutes.userLoginAPI());
            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Login with DaveAI:- " + e.getLocalizedMessage());
        }
    }

    private OnDaveConnectedListener onDaveConnectedListener;
    public interface OnDaveConnectedListener {
        void onDaveConnected();
        void onDaveConnectedFailed(String daveConnectFailed);

    }

    /**
     * method to store date in Local Database
     *  other perspective settings.
     *  all core models and other models
     *  interaction stages details
     *  Hierarchy & Filter list  of given models.
     *  objects of given modelList.
     * @param loaderMsg msg want to show during download data.
     * @param daveConnectUtil instance of DaveConnectUtil .From where read data to download.
     * @param callbackClass return msg when coonection got completed.
     */
    public void connectWithDaveAI(String loaderMsg, DaveConnectUtil daveConnectUtil,OnDaveConnectedListener callbackClass){

        this.daveConnectUtil = daveConnectUtil;
        this.onDaveConnectedListener = callbackClass;

        String userId =  sharedPreferences.readString(DaveSharedPreference.USER_ID);
        if(userId!=null){
            try {

                JSONObject jsonObject = new JSONObject();
                if(daveConnectUtil.getPrefSettingList()!= null && daveConnectUtil.getPrefSettingList().size()>0)
                    jsonObject.put("prefs_list", DataConversion.arrayToJSONArray(daveConnectUtil.getPrefSettingList()));

                if(daveConnectUtil.getCoreModelList()!= null && daveConnectUtil.getCoreModelList().size()>0)
                    jsonObject.put("core_models",DataConversion.arrayToJSONArray(daveConnectUtil.getCoreModelList()));

                if(daveConnectUtil.getOtherModelList()!=null && daveConnectUtil.getOtherModelList().size()>0)
                    jsonObject.put("other_models",DataConversion.arrayToJSONArray(daveConnectUtil.getOtherModelList()));

                jsonObject.put("stages_list",daveConnectUtil.isCallInteractionStages());

                BulkDownloadUtil bulkDownloadUtil = daveConnectUtil.getBulkDownloadUtil();
                if(bulkDownloadUtil!=null) {
                    HashMap<String, Object> coreModelObjects = bulkDownloadUtil.getCoreModelObjectsMap();
                    if(coreModelObjects!=null && coreModelObjects.size()>0)
                        jsonObject.put("core_model_objects_map",DataConversion.mapObjectToJSONObject(coreModelObjects) );

                    HashMap<String, Object> modelObjects = bulkDownloadUtil.getModelObjectsMap();
                    if(modelObjects!=null && modelObjects.size()>0)
                        jsonObject.put("model_objects_map",DataConversion.mapObjectToJSONObject(modelObjects) );

                    HashMap<String, ArrayList<String>> hierarchyMap = bulkDownloadUtil.getModelHierarchyMap();
                    if(hierarchyMap!=null && hierarchyMap.size()>0)
                        jsonObject.put("model_hierarchy_map", DataConversion.mapListToJSONObject(hierarchyMap));

                    HashMap<String, ArrayList<String>> filtersMap = bulkDownloadUtil.getModelFiltersMap();
                    if(filtersMap!=null && filtersMap.size()>0)
                        jsonObject.put("model_filter_map", DataConversion.mapListToJSONObject(filtersMap));
                }

                Log.d(TAG,"Print data cached list :----------- "+ jsonObject);

                sharedPreferences.writeString("_cached_data_list", jsonObject.toString());

                ArrayList<String> mediaCachedModel = daveConnectUtil.getMediaCachingModels();
                if(mediaCachedModel!=null && mediaCachedModel.size()>0){
                    sharedPreferences.saveArrayList("_media_cached_models", mediaCachedModel);
                }

                APIRoutes.getMetadata(mContext);
                connect(loaderMsg);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * method to check update for store data during login time.
     * it will check update for:
     * 1) Default preference, 2) user Model, 3) user Details , 4) preference List 4) core Models 5) otherModels
     * 6) interaction_satage 7) Customer Hierarchy and filterList 8) Product Hierarchy and filterList
     * 9) Invoice model
     * @param appContext context
     */
    public  void initSDK(final Context appContext) {

        try {

            String defaultPrefName = sharedPreferences.readString(DEFAULT_PERSPECTIVE_NAME);
            if(defaultPrefName!=null){
                checkUpdateForPerspectives(defaultPrefName,false, APIQueuePriority.ONE.getValue());
            }

            String cachedData = sharedPreferences.readString("_cached_data_list");
            JSONObject objCachedData = new JSONObject(cachedData);

            Log.d(TAG,"Print data list which had store during connect() "+ objCachedData );

            if(objCachedData.has("prefs_list") && !objCachedData.isNull("prefs_list") &&  objCachedData.getJSONArray("prefs_list").length()>0){
                JSONArray prefList = objCachedData.getJSONArray("prefs_list");
                for(int i =0;i< prefList.length();i++){
                    checkUpdateForPerspectives(prefList.getString(i),false, APIQueuePriority.ONE.getValue());
                }
            }

            if(objCachedData.getBoolean("stages_list")){
                checkUpdateForStageObjects(false,APIQueuePriority.ONE.getValue());
            }

            if(objCachedData.has("core_models") && !objCachedData.isNull("core_models") && objCachedData.getJSONArray("core_models").length()>0){
                JSONArray coreModelList = objCachedData.getJSONArray("core_models");
                for(int i =0;i< coreModelList.length();i++){
                    String modelName = databaseManager.getModelNameBasedOnType(coreModelList.getString(i));
                    //Log.e(TAG,"Print Check  core Model for update :-----------  "+ modelName);
                    checkUpdateForModel(modelName,false,APIQueuePriority.ONE.getValue());
                }
            }

            if(objCachedData.has("other_models") && !objCachedData.isNull("other_models")&& objCachedData.getJSONArray("other_models").length()>0){
               // Log.e(TAG,"Print data other_models type"+  objCachedData.get("other_models"));
                JSONArray otherModelList = objCachedData.getJSONArray("other_models");
                for(int i =0;i< otherModelList.length();i++){
                    checkUpdateForModel(otherModelList.getString(i),false,APIQueuePriority.ONE.getValue());

                }
            }

            if(objCachedData.has("model_hierarchy_map") && !objCachedData.isNull("model_hierarchy_map")&& objCachedData.getJSONObject("model_hierarchy_map").length()>0){
                JSONObject jsonObject = objCachedData.getJSONObject("model_hierarchy_map");
                Iterator<String> keys = jsonObject.keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    if(CORE_MODEL_LIST.contains(key))
                        key = databaseManager.getModelNameBasedOnType(key);
                    checkUpdateForHierarchy(key,jsonObject.getJSONArray(key),false,APIQueuePriority.ONE.getValue());
                }

            }

            if(objCachedData.has("model_filter_map") && !objCachedData.isNull("model_filter_map")&& objCachedData.getJSONObject("model_filter_map").length()>0){
                JSONObject jsonObject = objCachedData.getJSONObject("model_filter_map");
                Iterator<String> keys = jsonObject.keys();
                while(keys.hasNext()) {
                    String key = keys.next();
                    if(CORE_MODEL_LIST.contains(key))
                        key = databaseManager.getModelNameBasedOnType(key);
                    //Log.i(TAG,"Print data filters key "+  key +" value " +jsonObject.get(key) );
                    checkUpdateForFilters(key,jsonObject.getJSONArray(key),false,APIQueuePriority.ONE.getValue());
                }
            }

            new DaveService().startBackgroundService(appContext);
            new MediaService().startBackgroundService(appContext);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    OnSyncDataListener onSyncDataListener = null;
    public interface OnSyncDataListener {
        void onDataSynced(boolean isSynced, String daveMsg);
        void onDataSyncFailed(boolean isSynced, String daveMsg);
    }
    public void syncBulkData(String loaderMsg,OnSyncDataListener onSyncDataListener){
        this.onSyncDataListener = onSyncDataListener;
        connect(loaderMsg);
    }


/*  enum ConnectStatus{

        CONNECT_SUCCESS (2),  //different models in the same table
        STARTED (1),  //different models in the same table
        CONNECT_FAILED (-1)  //different models in the same table
        ;

        private final int levelCode;

        private ConnectStatus(int levelCode) {
            this.levelCode = levelCode;
        }

        public int getValue() {
            return levelCode;
        }
    }*/

    //method to call models
    private void connect(String loaderMSG) {

        tempPrefList.clear();
        if(daveConnectUtil!=null ){
            ArrayList <String> prfList = daveConnectUtil.getPrefSettingList();
            if(prfList!=null && prfList.size()>0){
                tempPrefList.addAll(prfList);
            }
        }else if(otherPerspectives!=null && otherPerspectives.size()>0)
            tempPrefList.addAll(otherPerspectives);

        if(loaderMSG!= null && !loaderMSG.isEmpty())
            showLoader(loaderMSG);

        Log.i(TAG,"***********************isReadyForNewConnect() == ");
        getModelList();
        Log.i(TAG,"*****************Print Other perspective List***************"+ tempPrefList);
        if(tempPrefList!= null && tempPrefList.size()>0) {
            getOtherMobilePerspectives(tempPrefList.remove());

        }else {
            callGetModelAPI();
        }

    }

    private void getOtherMobilePerspectives(String prefName) {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                try {
                    modelUtil.setupModelCacheMetaData(prefName, "perspective_settings");
                    databaseManager.insertOrUpdateSingleton(
                            prefName,
                            new SingletonTableRowModel(prefName, response, System.currentTimeMillis(), "perspective_settings", "")
                    );

                    if(tempPrefList.size()>0){
                        getOtherMobilePerspectives(tempPrefList.remove());
                    }else {
                        callGetModelAPI();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                   Log.e(TAG,"Error get other perspective:----------"+ e.getMessage());

                }
            }

            @Override
            public void onTaskFailure(int responseCode, String errorMsg) {
                Log.e(TAG, " Get Other Pref ON ERROR" + responseCode +" "+ errorMsg);
                setFailedListener(responseCode+": "+errorMsg);
                //todo check connect status
               // sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());

            }
        };
        new APICallAsyncTask(postTaskListener, mContext, "GET", false)
                .execute(APIRoutes.getPerspectiveAPI(prefName), APIRequestMethod.GET.name());
    }

    private void getModelList() {

        tempGetModelList.clear();
        getModelList.clear();
        Log.i(TAG,"Print  before ModelList :- "+ getModelList +" tempGetModelList: "+ tempGetModelList);
        if(daveConnectUtil != null) {
            getModelList = new ArrayList<>();
            ArrayList<String> coreModelList = daveConnectUtil.getCoreModelList();
            if(coreModelList!= null && coreModelList.size()>0)
                getModelList.addAll(coreModelList);

            ArrayList<String> otherModelList = daveConnectUtil.getOtherModelList();
            if(otherModelList!= null && otherModelList.size()>0)
                getModelList.addAll(otherModelList);

            String invoiceModel = daveAIPerspective.getInvoice_model_name();
            if(invoiceModel != null && !getModelList.contains(invoiceModel))
                getModelList.add(invoiceModel);

            tempGetModelList= new LinkedList<>();
            tempGetModelList.addAll(getModelList);
        }
        Log.i(TAG,"Print After ModelList :- "+ getModelList +" tempGetModelList: "+ tempGetModelList);

    }

    private void callGetModelAPI() {
        if(tempGetModelList.size()>0){
            getModels(tempGetModelList.remove());
        }else {
          /*  try {
                new ModelUtil(mContext).saveInteractionModelAttributes(new ModelUtil(mContext).getInteractionModelName());
            } catch (Exception e) {
                e.printStackTrace();
            }*/
            //todo check connect status
            //sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());
            if(daveConnectUtil!=null && daveConnectUtil.isCallInteractionStages()){
                getInteractionStagesList();
            }else {
                callBulkDownload();
            }

        }

    }
    private void getModels(String model){
        if(CORE_MODEL_LIST.contains(model)){
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        JSONArray modelJArray = new JSONArray(response);
                        JSONObject modelResponse = modelJArray.getJSONObject(0);
                        String modelName = modelResponse.getString("name");
                        String modelType = modelResponse.getString("model_type");
                        String modelIDAttrName = new ModelUtil(mContext).saveIDAttrNameOfCoreModel(modelType, modelResponse.toString());
                        Log.i(TAG, "getCoreModel model name modelIDAttrName>>>>" + modelName + " ModelType:-- " + modelType + " modelIDAttrName:--" + modelIDAttrName);
                        if (!modelIDAttrName.isEmpty()) {
                            modelUtil.setupModelCacheMetaData(modelName, modelIDAttrName);
                        }
                        if (modelJArray.length() > 0) {
                            DatabaseManager.getInstance(mContext).insertORUpdateModelData(modelName,
                                    new ModelTableRowModel(modelType, modelName, modelResponse.toString(), System.currentTimeMillis(), ""));


                        }
                        databaseManager.createTable(modelName, DatabaseConstants.CacheTableType.OBJECTS);

                        getInteractionModelAttr(model);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    callGetModelAPI();

                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG,"ON ERROR");
                   //todo check connect status
                   // sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());
                    setFailedListener(responseCode +": "+errorMsg);

                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.getCoreModel(model), APIRequestMethod.GET.name());

        }else{
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        JSONObject modelResponse = new JSONObject(response);
                        String modelName = modelResponse.getString("name");

                        String modelType = modelResponse.getString("model_type");
                        String modelIDAttrName = new ModelUtil(mContext).getIDAttrNameOfModel(modelResponse.toString());
                        Log.e(TAG, "get model name && modelIDAttrName>>>>" + modelName + " ModelType:-- " + modelType + " modelIDAttrName:--" + modelIDAttrName);
                        if (!modelIDAttrName.isEmpty()) {
                            modelUtil.setupModelCacheMetaData(modelName, modelIDAttrName);
                        }
                        DatabaseManager.getInstance(mContext).insertORUpdateModelData(modelName,
                                new ModelTableRowModel(modelType, modelName,
                                        modelResponse.toString(),
                                        System.currentTimeMillis(), ""));

                        getInteractionModelAttr(model);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    callGetModelAPI();
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG,"ON ERROR");

                    //todo check connect status
                    //sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());
                    setFailedListener(responseCode +": "+errorMsg);
                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.modelAPI(model));
        }
    }

    private void getInteractionModelAttr(String modelNAme){

        String interactionModel = new ModelUtil(mContext).getInteractionModelName();
        if(interactionModel!=null && interactionModel.equalsIgnoreCase(modelNAme)){
            try {
                new ModelUtil(mContext).saveInteractionModelAttributes(new ModelUtil(mContext).getInteractionModelName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getInteractionStagesList() {
        try {
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {

                    databaseManager.createTable(INTERACTION_STAGES_TABLE_NAME, CacheTableType.OBJECTS);
                    modelUtil.setupModelCacheMetaData(INTERACTION_STAGES_TABLE_NAME, INTERACTION_STAGES_TABLE_NAME);
                    saveInteractionStagesDetails(response);
                    callBulkDownload();
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    setFailedListener(responseCode +": "+errorMsg);

                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.interactionStagesAPI());

        }catch (Exception e){
            e.printStackTrace();
            setFailedListener("Error on get Interaction Stages");

        }
    }


    private void callBulkDownload(){

        BulkDownload bulkDownload = new BulkDownload(mContext);
        BulkDownloadUtil bulkDownloadUtil = daveConnectUtil.getBulkDownloadUtil();

        Log.e(TAG,"Print bulk download instance "+bulkDownloadUtil);

        if(bulkDownloadUtil != null){
            JSONObject bulkPostBody = bulkDownload.createBulkDownloadPostBody(bulkDownloadUtil);
            if(bulkPostBody!=null && bulkPostBody.length()>0) {

                bulkDownload.downloadBulkData(bulkPostBody, bulkDownloadUtil.getBulkDownloadTime() * 60 * 1000, new BulkDownload.OnDownloadBulkDataListener() {
                    @Override
                    public void onDownloadBulkDataCompleted(JSONObject bulkDataResponse) {
                        bulkDownload.saveBulkDataInLocalDb(bulkDataResponse, sharedPreferences.getColdUpdateTime(),
                                sharedPreferences.getHotUpdateTime(), new BulkDownload.OnSaveDataListener() {
                                    @Override
                                    public void onSaveBulkData(boolean isDataSaved) {
                                        Log.i(TAG, "Print is bulkData saved in db:--- " + isDataSaved);
                                        if (isDataSaved) {
                                            setSuccessListener();
                                        }

                                    }
                                });

                    }

                    @Override
                    public void onDownLoadBulkDataFailed(int errorCode, String errorMessage) {
                         setFailedListener(errorCode +" : "+ errorMessage);
                         //todo check connect status

                    }
                });
            }else {
                Log.e(TAG,"BulkDownload body is null or empty :- "+bulkPostBody);
                setSuccessListener();
            }
        }else {
            setSuccessListener();
        }

    }

    private void setSuccessListener() {

        if(pDialog!=null&&pDialog.isShowing()){
            pDialog.cancel();
        }

        if(onDaveConnectedListener!=null){
            onDaveConnectedListener.onDaveConnected();
        }

        if(onSyncDataListener!=null){
            onSyncDataListener.onDataSynced(true,"data sync completed.");
        }
    }

    private void setFailedListener(String failedMSg) {

        if(pDialog!=null&&pDialog.isShowing()){
            pDialog.cancel();
        }

        if(onDaveConnectedListener!=null){
            onDaveConnectedListener.onDaveConnectedFailed(failedMSg);
        }

        if(onSyncDataListener!=null){
            onSyncDataListener.onDataSynced(false,failedMSg);
        }

    }


    // method to create table
    private void createTableDuringLogin() {

        databaseManager.createTable(META_TABLE_NAME, CacheTableType.META_DATA);
        databaseManager.createTable(SINGLETON_TABLE_NAME, CacheTableType.SINGLETON);
        databaseManager.createTable(MODEL_TABLE_NAME,CacheTableType.MODEL);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.POST_QUEUE);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.MEDIA_QUEUE_TABLE);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.MEDIA_TABLE);

        modelUtil.setupModelCacheMetaData(SINGLETON_TABLE_NAME, SINGLETON_TABLE_NAME);
        databaseManager.createTable(INTERACTIONS_TABLE_NAME, CacheTableType.INTERACTIONS);

        Log.i(TAG,"Set up all default table:-----");
    }

    //method to delete table
    private void deleteLocalDatabase() {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

        ModelUtil modelUtil = new ModelUtil(mContext);
        if(databaseManager.isTableExists(modelUtil.getCustomerModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getCustomerModelName());

        if(databaseManager.isTableExists(modelUtil.getProductModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getProductModelName());

        if(databaseManager.isTableExists(modelUtil.getInteractionModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getInteractionModelName());

        if(databaseManager.isTableExists(daveAIPerspective.getInvoice_model_name()))
            databaseManager.deleteAllRowsInTable(daveAIPerspective.getInvoice_model_name());

        databaseManager.deleteAllRowsInTable(META_TABLE_NAME);
        if(databaseManager.isTableExists(INTERACTIONS_TABLE_NAME))
            databaseManager.deleteAllRowsInTable(INTERACTIONS_TABLE_NAME);

        if(databaseManager.isTableExists(INTERACTION_STAGES_TABLE_NAME))
            databaseManager.deleteAllRowsInTable(INTERACTION_STAGES_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(POST_QUEUE_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(MODEL_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(SINGLETON_TABLE_NAME);

    }

    //save interaction_stage objects details
    public void saveInteractionStagesDetails(String response){

        //collect and keep all the stage names,
        // all new_interaction_attributes,
        // all quantity attributes and
        // all tagged product attributes during the init itself...

        ArrayList<String> interactionStageList = new ArrayList<>();
        HashMap<String,Object> shareButtonListMap = new HashMap<>();
        HashMap<String,Object> shareTemplateListMap = new HashMap<>();
        JSONObject shareButtonDetails = new JSONObject();
        JSONObject shareTemplateDetails = new JSONObject();
        if(response!=null) {
            try {
                boolean isFirstPositiveStage = true;
                boolean isFirstNegativeStage = true;
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                JSONObject data = new JSONObject(response);
                Iterator<String> keys = data.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray data_array = data.getJSONArray(key);

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject get_data = data_array.getJSONObject(i);

                            DatabaseManager.getInstance(mContext).insertOrUpdateObjectData(
                                    INTERACTION_STAGES_TABLE_NAME,
                                    new ObjectsTableRowModel(
                                            get_data.getString("name"),
                                            get_data.toString(),
                                            System.currentTimeMillis()
                                    )
                            );

                            if(get_data.getBoolean("display")) {
                                String stageName = get_data.getString("name");
                                interactionStageList.add(stageName);
                                double defaultScoreValue = get_data.getDouble("default_positivity_score");

                                //Log.e(TAG,"Testing saveInteractionStagesDetails:--------"+stageName +" scoreVAlue:--- "+ defaultScoreValue);
                                if(isFirstPositiveStage && defaultScoreValue > 0){
                                    isFirstPositiveStage = false;
                                    databaseManager.insertOrUpdateCustomSingleton( "_first_positive_stage_name",
                                            new SingletonTableRowModel("_first_positive_stage_name",
                                                    stageName,
                                                    System.currentTimeMillis(),"firstPositiveStageName"));
                                }
                                if(isFirstNegativeStage && defaultScoreValue < 0){
                                    isFirstNegativeStage = false;
                                    databaseManager.insertOrUpdateCustomSingleton( "_first_negative_stage_name",
                                            new SingletonTableRowModel("_first_negative_stage_name",
                                                    stageName,
                                                    System.currentTimeMillis(),"firstNegativeStageName"));
                                }
                            }

                            if(get_data.has("quantity_attributes")&& !get_data.isNull("quantity_attributes")){
                                quantityPredictStatus = true;
                            }
                            if(get_data.has("share_button_name")&& !get_data.get("share_button_name").equals("__NULL__")){
                                shareButtonListMap.put(get_data.getString("name"),get_data.getString("share_button_name"));
                                shareButtonDetails.put(get_data.getString("name"),get_data.getString("share_button_name"));
                            }
                            if(get_data.has("share_button_template")&& !get_data.isNull("share_button_template")){
                                shareTemplateListMap.put(get_data.getString("name"),get_data.getString("share_button_template"));
                                shareTemplateDetails.put(get_data.getString("name"),get_data.getString("share_button_template"));
                            }

                            // Log.e(TAG,"get Interaction Stages name and object details:-"+get_data.getString("name")+" "+get_data);


                        }
                        // Log.e(TAG, " Print shareButtonListMap+++++++++++" + shareButtonListMap);
                    }
                }


                sharedPreferences.saveArrayList("_interaction_stage_names", interactionStageList);
                sharedPreferences.saveHashMapObjectType("_stage_button_list", shareButtonListMap);
                sharedPreferences.saveHashMapObjectType("_stage_template_list", shareTemplateListMap);

                // save stageNAme list , stageButtonList, stageTemplateList  in Singeleton table
                databaseManager.insertOrUpdateCustomSingleton( "_interaction_stage_names",
                        new SingletonTableRowModel("_interaction_stage_names",interactionStageList.toString(),System.currentTimeMillis(),"stageName"));

                databaseManager.insertOrUpdateCustomSingleton( "_stage_button_list",
                        new SingletonTableRowModel("_stage_button_list",shareButtonListMap.toString(),System.currentTimeMillis(),"stage_button_list"));

                databaseManager.insertOrUpdateCustomSingleton( "_stage_template_list",
                        new SingletonTableRowModel("_stage_template_list",shareTemplateListMap.toString(),System.currentTimeMillis(),"stage_template_list"));

                Log.i(TAG,"<<<<<<<<<<<<<Get Frist Positive Stage NAme "+  databaseManager.getCustomSingletonRow("_first_positive_stage_name"));
                Log.i(TAG,"<<<<<<<<<<<<Get Frist negative Stage NAme "+  databaseManager.getCustomSingletonRow("_first_negative_stage_name"));



            }
            catch (Exception exception){
                exception.printStackTrace();
                Log.e(TAG, "Error in Interaction_stages Objects+++++++++++" + exception.getMessage());
            }
        }

    }



//**********************************************update sdk method***************************************************/


    /**
     * method to save user details as Singleton and user Model in Database during Login time
     * @param modelName user Login model name
     * @param objectId user id
     */
    private void saveLoginDetails(final String modelName, final String objectId){

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        boolean isUserDetailsFresh = true;
        int priorityLevel = APIQueuePriority.LOGIN.getValue();
        CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
        if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
            CacheRefreshStatus refreshStatus = databaseManager.updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
            switch (refreshStatus) {
                case COLD_UPDATE_REQUIRED:
                    isUserDetailsFresh = true;
                    break;
                case HOT_UPDATE_REQUIRED:
                    isUserDetailsFresh = true;
                    priorityLevel = APIQueuePriority.ONE.getValue();
                    break;
                case NOT_REQUIRED:
                    isUserDetailsFresh = false;
                    break;
            }
            Log.i(TAG, "<<<<<<<<<<< Login Details refreshStatus:-" + refreshStatus +" isUserDetailsFresh:-"+ isUserDetailsFresh);
        }
        if(isUserDetailsFresh) {

            if(databaseManager.isTableExists(POST_QUEUE_TABLE_NAME))
                databaseManager.createTable(POST_QUEUE_TABLE_NAME,CacheTableType.POST_QUEUE);

            //add user object details
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.updateOrGetObjectAPI(modelName, objectId), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", APIQueueStatus.PENDING.name(),
                            priorityLevel, CacheTableType.SINGLETON.getValue()
                    )
            );

            //add user model
          /*  databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.modelAPI(modelName), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", APIQueueStatus.PENDING.name(),
                            priorityLevel,CacheTableType.MODEL.getValue()
                    )
            );
*/
            new DaveService().startBackgroundService(mContext);
        }

    }

    /**
     * method to check update for user details.if update require it will add in API queue
     * @param modelName user model name
     * @param objectId user object id
     * @param isFresh isfresh
     * @param priorityLevel priorityLevel
     */
    private void checkUpdateForUserDetails(final String modelName, String objectId, boolean isFresh, int priorityLevel){

        if(!isFresh) {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName, CacheTableType.MODEL);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<getUserSingleton " + modelName + " && refreshStatus:- " + refreshStatus+" isFresh:-"+isFresh);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.updateOrGetObjectAPI(modelName, objectId), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", APIQueueStatus.PENDING.name(),
                            priorityLevel, CacheTableType.SINGLETON.getValue()
                    )
            );
        }

    }

    /**
     * method to check update for perspective .if update require it will add in API queue
     * @param prefName perspective name
     * @param priorityLevel priorityLevel
     */
    private void checkUpdateForPerspectives(String prefName, boolean isFresh, int priorityLevel){
        try {
            if(!isFresh) {
                CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(prefName);
                if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {

                    CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case NOT_REQUIRED:
                            isFresh = false;
                            break;
                    }
                    Log.e(TAG, "readPerspectivePreference check meta update:- " + cacheModelPOJO.modelName +" status:- "+ refreshStatus);
                }
            }
            //mobile_application
            if (isFresh) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.getPerspectiveAPI(prefName), APIRequestMethod.GET.name(),
                                "", prefName,
                                "perspective_settings", APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.SINGLETON.getValue()
                        )
                );
            }

        }catch (Exception e){
            Log.e(TAG, " Error During readPerspectivePreference " + e.getMessage());
        }
    }

    /**
     * method to check update for interaction_stage objects .if update require it will add in API Queue
     * @param isFresh isfresh
     * @param priorityLevel priorityLevel
     */
    private void checkUpdateForStageObjects(boolean isFresh, int priorityLevel){
        try {
            if (!isFresh){
                CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(INTERACTION_STAGES_TABLE_NAME);
                if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                    CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.OBJECTS);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case NOT_REQUIRED:
                            isFresh = false;
                            break;
                    }
                    Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<getInteractionList refreshStatus:- " + refreshStatus);
                }
            }
            if(isFresh) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.interactionStagesAPI(), APIRequestMethod.GET.name(),
                                "", INTERACTION_STAGES_TABLE_NAME,
                                INTERACTION_STAGES_TABLE_NAME, APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.OBJECTS.getValue()
                        )
                );
            }
        }catch (Exception e){
            Log.e(TAG, " Error During getInteractionList " + e.getMessage());
        }
    }

    /**
     * method to check update for model.if update require it will add in API queue
     * @param modelName modelName
     * @param priorityLevel priorityLevel
     */
    private void checkUpdateForModel(final String modelName, boolean isFresh, int priorityLevel){
        if(!isFresh) {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName, CacheTableType.MODEL);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<getModel " + modelName + " && refreshStatus:- " + refreshStatus);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.modelAPI(modelName), APIRequestMethod.GET.name(),
                            "", modelName,
                            modelName, APIQueueStatus.PENDING.name(),
                            priorityLevel,CacheTableType.MODEL.getValue()
                    )
            );
        }

    }

    private void checkUpdateForHierarchy(String modelName, JSONArray hierarchyList, boolean isFresh,int priorityLevel ){

        if (!isFresh){
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName + MODEL_HIERARCHY);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<getCategoryHierarchyList refreshStatus:- " + refreshStatus);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            StringBuilder categoryParam = new StringBuilder();
            if(hierarchyList != null){
                for (int i=0;i<hierarchyList.length();i++){
                    try {
                        categoryParam.append(hierarchyList.get(i).toString());
                        if((i+1)< hierarchyList.length())
                            categoryParam.append("/");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.pivotAttributeAPI(modelName,categoryParam.toString()), APIRequestMethod.GET.name(),
                                "", modelName + MODEL_HIERARCHY,
                                modelName + MODEL_HIERARCHY, APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.SINGLETON.getValue()
                        )
                );

            }
        }


    }

    //filter feature for type: [name, uid, phone number, email] to directly type instead of pivot
    private boolean checkFilterAttrType(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }
    private void checkUpdateForFilters(String modelName, JSONArray filterList, boolean isFresh,int priorityLevel) {
        try {
            Model modelInstance = ModelUtil.getModelInstance(mContext, modelName);
           // Log.e(TAG,"checkUpdateForFilters :- "+modelName +"  "+ modelInstance +"  "+  filterList);
            if (modelInstance != null && filterList != null) {
                for(int i= 0;i<filterList.length();i++){
                    String attrName = filterList.get(i).toString();
                    String attrType = modelInstance.getAttributeType(attrName);
                    isFresh =false;
                   // Log.e(TAG,"checkUpdateForFilters  attrNAMe:- "+attrName +" AttrType:-  "+ attrType);
                    if(!checkFilterAttrType(attrType)) {

                        //Log.e(TAG, " Call FILTER_TYPE_LIST Method  :--- " + attrName + " attrType:" + attrType);
                        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                        if (attrType.equals("price") || attrType.equals("discount")) {
                            CacheModelPOJO cacheMin = DatabaseManager.getInstance(mContext).getMetaData(modelName + "_min_" + attrName);
                            if (cacheMin != null && !cacheMin.modelName.isEmpty()) {
                                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheMin.modelName, CacheTableType.SINGLETON);
                                switch (refreshStatus) {
                                    case COLD_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case HOT_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case NOT_REQUIRED:
                                        isFresh = false;
                                        break;
                                }
                                Log.i(TAG, "<<<<<<getFilterAttr min  name " + cacheMin.modelName + " status: "+ refreshStatus);
                            }
                            if(isFresh) {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.priceRangeAPI("min", attrName), APIRequestMethod.GET.name(),
                                                "", modelName + "_min_" + attrName,

                                                "pivot_min_" + attrName, APIQueueStatus.PENDING.name(),
                                                priorityLevel, CacheTableType.SINGLETON.getValue()
                                        )
                                );

                            }
                            isFresh = false;
                            CacheModelPOJO cacheMax = DatabaseManager.getInstance(mContext).getMetaData(modelName + "_max_" + attrName);
                            if (cacheMax != null && !cacheMax.modelName.isEmpty()) {
                                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheMax.modelName, CacheTableType.SINGLETON);
                                switch (refreshStatus) {
                                    case COLD_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case HOT_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case NOT_REQUIRED:
                                        isFresh = false;
                                        break;
                                }
                                Log.i(TAG, "<<<<<<getFilterAttr max name " + cacheMax.modelName + " status: "+ refreshStatus);
                            }
                            if(isFresh) {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(

                                                APIRoutes.priceRangeAPI("max", attrName), APIRequestMethod.GET.name(),
                                                "", modelName + "_max_" + attrName,

                                                "pivot_max_" + attrName, APIQueueStatus.PENDING.name(),
                                                priorityLevel, CacheTableType.SINGLETON.getValue()
                                        )
                                );
                            }


                        } else {

                            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName + "_" + attrName);
                            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                                switch (refreshStatus) {
                                    case COLD_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case HOT_UPDATE_REQUIRED:
                                        isFresh = true;
                                        break;
                                    case NOT_REQUIRED:
                                        isFresh = false;
                                        break;
                                }
                                Log.i(TAG, "<<<<<<getFilterAttr name " + cacheModelPOJO.modelName + " status: "+ refreshStatus);
                            }
                            if(isFresh) {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.pivotAttributeAPI(modelName, attrName), APIRequestMethod.GET.name(),
                                                "", modelName + "_" + attrName,
                                                "pivot_" + attrName, APIQueueStatus.PENDING.name(),
                                                priorityLevel, CacheTableType.SINGLETON.getValue()
                                        )
                                );
                            }

                        }
                    }

                }


            }else {
                Log.e(TAG," modelInstance or Filterlist is null:------"+modelInstance +" filterList:---- "+filterList);
            }

        }catch (Exception e){
            e.printStackTrace();
        }



    }







}





