package com.example.admin.daveai.others;

import android.content.Context;
import android.util.Log;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;

import org.json.JSONException;
import org.json.JSONObject;

public class PerspectiveParent {

    private static final String TAG = "PerspectiveParent";
    public JSONObject prefResponse = new JSONObject();

    public boolean checkPreferenceExist(Context context,String perspectiveName){
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        if(databaseManager.isTableExists(DatabaseConstants.SINGLETON_TABLE_NAME)) {
            prefResponse = databaseManager.getSingletonRow(perspectiveName);
            Log.e(TAG,"savePerspectivePreferenceValue:----prefResponse--------"+prefResponse);
            if (prefResponse != null && prefResponse.length() > 0) {
                Log.e(TAG, "Preference with name "+perspectiveName+" found in cache");
                return true;
            }else{
                Log.e(TAG, "Preference with name "+perspectiveName+" not found in cache");
                return false;
            }
        }else{
            Log.e(TAG, "Singleton Table not found in local database");
            return false;
        }
    }

    public void setPreferenceJSONResponse(String jsonPerspective) {
        try {
            this.prefResponse = new JSONObject(jsonPerspective);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setPreferenceJSONResponse(JSONObject jsonPerspective) {
        this.prefResponse = jsonPerspective;
    }



    public boolean setPreferenceJSONResponse(Context context,String perspectiveName){
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);

        if(databaseManager.isTableExists(DatabaseConstants.SINGLETON_TABLE_NAME)) {
            prefResponse = databaseManager.getSingletonRow(perspectiveName);
            Log.e(TAG,"savePerspectivePreferenceValue:----prefResponse--------"+prefResponse);
            if (prefResponse == null || prefResponse.length() > 0) {
                Log.e(TAG, "Preference with name "+perspectiveName+" found in cache");
                return true;
            }else{
                Log.e(TAG, "Preference with name "+perspectiveName+" not found in cache");
                return false;
            }
        }else{
            Log.e(TAG, "Singleton Table not found in local database");
            return false;
        }
    }

   /* protected String getStringFromPreference(String key){
        try {
            if (prefResponse.has(key) && !prefResponse.isNull(key)) {
                return prefResponse.getString(key);
            }else{
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }*/

   /**
   * gets the
   * */

    public <T> T getFromPreference(String key,Class<T> returnClass,Object returnObject){
        try {
            if(prefResponse == null || prefResponse.length() == 0){
                return returnClass.cast(returnObject);
            }
            if (prefResponse.has(key) && !prefResponse.isNull(key)) {
                //Log.e(TAG,"FOUND "+key+"  ==  "+prefResponse.get(key).getClass() +" value :- "+returnClass.cast(prefResponse.get(key)));
                return returnClass.cast(prefResponse.get(key));
            }else{
                return returnClass.cast(returnObject);
            }
        }catch (Exception e){
            e.printStackTrace();
            return returnClass.cast(returnObject);
        }
    }

}
