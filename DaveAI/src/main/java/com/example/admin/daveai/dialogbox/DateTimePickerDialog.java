package com.example.admin.daveai.dialogbox;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


public class DateTimePickerDialog implements View.OnFocusChangeListener,View.OnClickListener,
        TimePickerDialog.OnTimeSetListener ,DatePickerDialog.OnDateSetListener{

    private  final String TAG = getClass().getSimpleName();
    public static final int DATE_DIALOG_ID = 0;
    public static final int TIME_DIALOG_ID = 1;
    public static final int DATE_TIME_DIALOG_ID = 2;
    private EditText editText;
    private Context context;
    private Calendar myCalendar;
    private int dialogType=0;
    private String updatedDate="";


    public DateTimePickerDialog(EditText editText, Context context,int dialogType){
        this.editText = editText;
        this.context=context;
        this.dialogType=dialogType;
        this.editText.setOnClickListener(this);
        this.editText.setOnFocusChangeListener(this);
        this.myCalendar = Calendar.getInstance();

    }


    @Override
    public void onClick(View v) {
        if(dialogType==DATE_DIALOG_ID ||dialogType==DATE_TIME_DIALOG_ID) {
            showDateDialog();
        }
        if(dialogType==TIME_DIALOG_ID) {
            showTimeDialog();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(hasFocus){
            if(dialogType==DATE_DIALOG_ID ||dialogType==DATE_TIME_DIALOG_ID) {
                showDateDialog();
            }
            if(dialogType==TIME_DIALOG_ID) {
                showTimeDialog();
            }

        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        /*  String hour;
        String min;
        if(hourOfDay<10) {
            hour = "0"+ hourOfDay;
        }else{
            hour=String.valueOf(hourOfDay);
        }
        if(String.valueOf(minute).length()<2){
            min = "0" + minute;
        }else{
            min= String.valueOf(minute);
        }

        if(!updatedDate.isEmpty() && updatedDate!=null&& dialogType==DATE_TIME_DIALOG_ID){
            updateDateTimeInView(updatedDate+" "+ hour + ":" + min);
        }else {
            updateDateTimeInView(hour + ":" + min);
        }*/

        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        myCalendar.set(Calendar.MINUTE, minute);

        if( dialogType==DATE_TIME_DIALOG_ID){
            java.text.SimpleDateFormat sdformat = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm");
            updateDateTimeInView(sdformat.format(myCalendar.getTime()));

        }else if(dialogType==TIME_DIALOG_ID) {
            java.text.SimpleDateFormat sdTimeFormat = new java.text.SimpleDateFormat("HH:mm");
            updateDateTimeInView(sdTimeFormat.format(myCalendar.getTime()));
        }




    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        // SimpleDateFormat sdformat = new SimpleDateFormat(""dd-mm-yyyy", Locale.US);
        // myCalendar.set(Calendar.MONTH, month+1);

        java.text.SimpleDateFormat sdformat = new java.text.SimpleDateFormat("yyyy-MM-dd");

        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        updatedDate = sdformat.format(myCalendar.getTime());
        if(dialogType==DATE_DIALOG_ID){
            updateDateTimeInView(updatedDate);
        }else if(dialogType==DATE_TIME_DIALOG_ID)
            showTimeDialog();

    }

    private void showDateDialog(){

        int mYear = myCalendar.get(Calendar.YEAR);
        int mMonth = myCalendar.get(Calendar.MONTH);
        int mDay = myCalendar.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(context, this, mYear, mMonth, mDay).show();
    }

    private void showTimeDialog(){
        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        new TimePickerDialog(context, this, hour, minute, true).show();
        /* new TimePickerDialog(context,this, hour, minute,
                    DateFormat.is24HourFormat(context)).show();*/

    }

   /* private void updateDateTimeInView(String date, String time){
        Log.i(TAG,"updateDateTimeInView DAte:-"+date +" Time:-"+time);
        if(!date.isEmpty() && !time.isEmpty())
            this.editText.setText(date +" "+time);
        else if(date.isEmpty())
            this.editText.setText(time);
        else if(time.isEmpty())
            this.editText.setText(date);

    }*/

    private void updateDateTimeInView(String dateTime){
        Log.i(TAG,"<<<<<<<<<<updateDateTimeInView DAteTime>>>>>>>>>>>>>>>>>>>:-"+dateTime );
        if(dateTime!=null && !dateTime.isEmpty())
            this.editText.setText(dateTime);
    }



}
