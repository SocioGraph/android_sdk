package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.VideoviewItem;

import me.drakeet.multitype.ItemViewBinder;

public class VideoItemViewBinder extends ItemViewBinder<VideoviewItem, VideoItemViewBinder.VideoHolder>

{
    private Context context;


    public VideoItemViewBinder(Context context) {
        this.context = context;

    }

    @Override
    protected @NonNull
    VideoItemViewBinder.VideoHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = LayoutInflater.from(context).inflate(R.layout.videoview_layout, parent, false);
        return new VideoItemViewBinder.VideoHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull VideoHolder holder, @NonNull VideoviewItem videoviewItem) {
        Uri uri = Uri.parse(videoviewItem.getStrVideoUrl());
        holder.showVideoView.setVideoURI(uri);

        holder.showVideoView.start();
        holder.showVideoView.setVideoPath(videoviewItem.getStrVideoUrl());
        MediaController mediaController = new MediaController(context);
        mediaController.setAnchorView(holder.showVideoView);
        holder.showVideoView.setMediaController(mediaController);

        holder.showVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

            }
        });
        holder.showVideoView.start();

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull VideoHolder holder) {
        holder.itemView.clearAnimation();
    }


    static class VideoHolder extends RecyclerView.ViewHolder {

        VideoView showVideoView;

        VideoHolder(@NonNull View itemView) {
            super(itemView);


            this.showVideoView = itemView.findViewById(R.id.showVideoView);

        }
    }
}
