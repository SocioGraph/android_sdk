package com.example.admin.daveai.database;

import android.content.Context;
import android.util.Log;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.InteractionStages;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveModels;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by rinki on 24/10/18.
 */

public class InteractionsHelper implements DatabaseConstants, DaveConstants {

    private final String TAG = getClass().getSimpleName();
    private Context mContext;

    private ModelUtil modelUtil;
    private DaveAIPerspective daveAIPerspective;
    private DatabaseManager databaseManager;
    private Model interactionModelInstance;


    //private int objectCount = 0;
    private int totalObjectCount = 0;
    private String preTaggedProductAtrr ;
    private String nxtTaggedProductAtrr ;

    public InteractionsHelper(Context context) {
        this.mContext = context;

        modelUtil = new ModelUtil(mContext);
        daveAIPerspective = DaveAIPerspective.getInstance();
        databaseManager = DatabaseManager.getInstance(mContext);
        interactionModelInstance  = Model.getModelInstance(mContext, modelUtil.getInteractionModelName());
    }


    /**
     * method to add interactions response in database.
     * @param response provide interactions response (get interactions objects)
     */
    public void addObjectsInInteractionsTable(String response) {
        try {
            List<JSONObject> interactionData = ObjectData.getJSONObjectDataList(response);
            if (interactionData != null && interactionData.size() > 0) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                CacheModelPOJO modelMetaData = databaseManager.getMetaData(INTERACTIONS_TABLE_NAME);
                for (int i = 0; i < interactionData.size(); i++) {
                    databaseManager.insertOrUpdateObjectData(
                            INTERACTIONS_TABLE_NAME,
                            new ObjectsTableRowModel(
                                    interactionData.get(i).getString(modelMetaData.model_id_name),
                                    interactionData.get(i).toString(),
                                    System.currentTimeMillis()
                            )
                    );
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public interface OnInteractionsAdded {
        void onInteractionAdded();
        void onInteractionAddedFailed();
    }

    /**
     * method to convert interaction objects to interactions objects and save in database
     * @param response provide String value of interaction response(get interaction objects)
     */

    public void addObjectsAsInteractions(String response ,OnInteractionsAdded onInteractionsAdded){
        try {
            addObjectsAsInteractions(new JSONObject(response),onInteractionsAdded);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * method to convert interaction objects to interactions objects and save in database
     * @param response provide JSONObject value of interaction response(get interaction objects)
     */
    public void addObjectsAsInteractions(JSONObject response,OnInteractionsAdded onInteractionsAdded){
        try {
            modelUtil.setupModelCacheMetaData(INTERACTIONS_TABLE_NAME,modelUtil.getInteractionIdAttrName(),daveAIPerspective.getHot_update_time_limit()
            ,daveAIPerspective.getCold_update_time_limit());
            //List<JSONObject> interactionData = ObjectData.getJSONObjectDataList(response);
            JSONArray interactionData = response.getJSONArray("data");
            if (interactionData != null && interactionData.length() > 0) {
                Log.e(TAG,"Print interactionobjects Count:----------"+interactionData.length());
                //addObjectInInteractionsTable(0,interactionData, onInteractionsAdded);
                addObjectsAsInteractions(interactionData, onInteractionsAdded);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addObjectsAsInteractions(JSONArray interactionList ,OnInteractionsAdded onInteractionsAdded){
        try {
            modelUtil.setupModelCacheMetaData(INTERACTIONS_TABLE_NAME,modelUtil.getInteractionIdAttrName(),daveAIPerspective.getHot_update_time_limit(),
                    daveAIPerspective.getCold_update_time_limit());
            List<JSONObject> interactionData = ObjectData.getJSONObjectDataList(interactionList);
            //addObjectInInteractionsTable(0,interactionData,onInteractionsAdded);

            totalObjectCount =0;
            for(int i=0; i< interactionData.size();i++){
                JSONObject interactionObject = interactionData.get(i);
                Log.e(TAG,"addObjectInInteractionsTable:----------"+i+" total count"+ totalObjectCount);
                createInteractionsBody(interactionObject, new OnInteractionsHelper() {
                    @Override
                    public void onCreatedCustomJSONObject(JSONObject interactionsObject) {
                        try {
                            Log.e(TAG,"+++++++++++++++++++++++++++++++++++Print Interactions Final Object:- "+ interactionsObject);
                            databaseManager.insertOrUpdateObjectData(
                                    INTERACTIONS_TABLE_NAME,
                                    new ObjectsTableRowModel(interactionObject.getString(modelUtil.getInteractionIdAttrName()),
                                            interactionsObject.toString(),
                                            System.currentTimeMillis()
                                    )
                            );
                            totalObjectCount ++;
                            Log.e(TAG,"Print totalObjectCount +++++++++++++ "+ totalObjectCount + "  "+interactionData.size() );
                            if(totalObjectCount == interactionData.size()){
                                if(onInteractionsAdded!=null)
                                    onInteractionsAdded.onInteractionAdded();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e(TAG, "Error productOrderQtyListMap*****Error:-" + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailedCreation(String errorMsg) {
                        totalObjectCount ++;
                        Log.e(TAG,"Print totalObjectCount  on onFailedCreation+++++++++++++ "+ totalObjectCount + "  "+interactionData.size() );
                        if(totalObjectCount == interactionData.size()){
                            if(onInteractionsAdded!=null)
                                onInteractionsAdded.onInteractionAdded();
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addObjectInInteractionsTable(int currentCount, List<JSONObject> interactionList,OnInteractionsAdded onInteractionsAdded) {
        try {
            Log.e(TAG,"addObjectInInteractionsTable:----------"+currentCount+" <"+ interactionList);

            if(currentCount < interactionList.size()){
                JSONObject interactionObject = interactionList.get(currentCount);
                createInteractionsBody(interactionObject, new OnInteractionsHelper() {
                    @Override
                    public void onCreatedCustomJSONObject(JSONObject interactionsObject) {
                        try {
                            Log.e(TAG,"++++++++++++++++++++++++Print Interactions Final Object:----------"+currentCount +"  "+ interactionsObject);
                            databaseManager.insertOrUpdateObjectData(
                                    INTERACTIONS_TABLE_NAME,
                                    new ObjectsTableRowModel(interactionObject.getString(modelUtil.getInteractionIdAttrName()),
                                            interactionsObject.toString(),
                                            System.currentTimeMillis()
                                    )
                            );
                            int tempCount = currentCount;
                            tempCount++;
                            addObjectInInteractionsTable(tempCount,interactionList,onInteractionsAdded);

                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e(TAG, "Error productOrderQtyListMap*****Error:-" + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailedCreation(String errorMsg) {

                    }
                });

            }else if(currentCount == interactionList.size()){
                if(onInteractionsAdded!=null)
                    onInteractionsAdded.onInteractionAdded();
            }else {
                if(onInteractionsAdded!=null)
                    onInteractionsAdded.onInteractionAddedFailed();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error addObjectInInteractionsTable:-" + e.getMessage());
        }

    }


    public void addObjectInInteractionsTable( JSONObject interactionObject) {
        try {
            addObjectInInteractionsTable(interactionObject.getString(new ModelUtil(mContext).getInteractionIdAttrName()),interactionObject);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error addObjectInInteractionsTable:------" + e.getMessage());
        }
    }


    /**
     * method to convert interaction object to Interactions Objects
     * @param interactionId provide interaction Id
     * @param interactionObject provide Interaction object
     */
    public void addObjectInInteractionsTable(String interactionId, JSONObject interactionObject) {
        try {
            createInteractionsBody(interactionObject, new OnInteractionsHelper() {

                @Override
                public void onCreatedCustomJSONObject(JSONObject interactionsObject) {
                    Log.e(TAG,"+++++++++++++++++++++++++++++++++++Print Interactions Final Object:----------"+ interactionsObject);
                    databaseManager.insertOrUpdateObjectData(
                            INTERACTIONS_TABLE_NAME,
                            new ObjectsTableRowModel(interactionId, interactionsObject.toString(),System.currentTimeMillis())
                    );


                }

                @Override
                public void onFailedCreation(String errorMsg) {
//                    Toast.makeText(mContext,errorMsg,Toast.LENGTH_SHORT).show();

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error addObjectInInteractionsTable:------" + e.getMessage());
        }
    }


    private interface OnInteractionsHelper {
        void onCreatedCustomJSONObject(JSONObject interactionsObject);
        void onFailedCreation(String errorMsg);
    }
    private void createInteractionsBody(JSONObject interactionBody ,OnInteractionsHelper onInteractionsHelper) {

         preTaggedProductAtrr = "";
         nxtTaggedProductAtrr = "";
        Log.e(TAG," +++++++++++++++++++++++++++++++++++Print InteractionBody :------"+interactionBody);
        JSONObject interactionsObject = new JSONObject();
        try {
            if (interactionBody.has(modelUtil.getInteractionIdAttrName()))
                interactionsObject.put(modelUtil.getInteractionIdAttrName(), interactionBody.getString(modelUtil.getInteractionIdAttrName())); //String

            if (interactionBody.has(modelUtil.getInteractionCustomerId()))
                interactionsObject.put("customer_id", interactionBody.getString(modelUtil.getInteractionCustomerId())); //String
            else if (interactionBody.has("customer_id"))
                interactionsObject.put("customer_id", interactionBody.getString("customer_id")); //String

            String productId = "";
            if (interactionBody.has(modelUtil.getProductIdAttrName())) {
                productId = interactionBody.getString(modelUtil.getProductIdAttrName());
                interactionsObject.put("product_id", interactionBody.getString(modelUtil.getProductIdAttrName())); //String
            } else if (interactionBody.has("product_id")) {
                productId = interactionBody.getString("product_id");
                interactionsObject.put("product_id", interactionBody.getString("product_id")); //String
            }


            if (interactionBody.has("recommended") && !interactionBody.isNull("recommended"))
                interactionsObject.put("recommended", interactionBody.get("recommended")); //boolean
            else
                interactionsObject.put("recommended", false); //boolean


            InteractionStages cInstance = null;
            String previous_stage = null;
            String next_stage = null;
            List<String> qtyAttr = null;


            if (interactionBody.has(modelUtil.getInteractionStageAttrName())) {
                interactionsObject.put("interaction_stage", interactionBody.getString(modelUtil.getInteractionStageAttrName())); //String
                cInstance = getStageInstance(interactionBody.getString(modelUtil.getInteractionStageAttrName()));

            } /*else if (interactionBody.has("interaction_stage")) {
                interactionsObject.put("interaction_stage", interactionBody.getString("interaction_stage")); //String
                cInstance = getStageInstance(interactionBody.getString("interaction_stage"));
            }*/


            if(cInstance != null) {
                // add customer and product attr from interaction stage objects
                List<String> customerAttributes = cInstance.getCustomer_attributes();
                List<String> productAttributes = cInstance.getProduct_attributes();
                if (customerAttributes != null && !customerAttributes.isEmpty()) {
                    for (int i = 0; i < customerAttributes.size(); i++) {
                        String key = customerAttributes.get(i);
                        if (interactionBody.has(key) && !interactionBody.isNull(key))
                            interactionsObject.put(key, interactionBody.getString(key));
                    }

                }

                if (productAttributes != null && !productAttributes.isEmpty()) {
                    for (int i = 0; i < productAttributes.size(); i++) {
                        String key = productAttributes.get(i);
                        if (interactionBody.has(key) && !interactionBody.isNull(key))
                            interactionsObject.put(key, interactionBody.getString(key));
                    }

                }

                // stage.get('interaction_time_attribute')
                if (interactionBody.has(cInstance.getInteraction_time_attribute()) && !interactionBody.isNull(cInstance.getInteraction_time_attribute())) {
                    interactionsObject.put("interaction_timestamp", interactionBody.getString(cInstance.getInteraction_time_attribute()));//String
                }

                // stage.get('tagged_products_attribute')
                if (interactionBody.has(cInstance.getTagged_products_attribute()) && !interactionBody.isNull(cInstance.getTagged_products_attribute())) {
                    interactionsObject.put("tagged_products", interactionBody.get(cInstance.getTagged_products_attribute()));//String
                } else {
                    interactionsObject.put("tagged_products", JSONObject.NULL);//String
                }

                //  stage.get('interaction_group_attribute')
                if (interactionBody.has(cInstance.getInteraction_group_attribute()) && !interactionBody.isNull(cInstance.getInteraction_group_attribute()))
                    interactionsObject.put("interaction_group", interactionBody.get(cInstance.getInteraction_group_attribute())); //String
                else {
                    interactionsObject.put("interaction_group", JSONObject.NULL);//String
                }

                // set(st.interaction_number_attributes + stage.get('quantity_attributes',[]))
                // Model interactionModel = Model.getModelInstance(mContext, modelUtil.getInteractionModelName());

                assert interactionModelInstance != null;
                List<String> interactionNumAttr = interactionModelInstance.getModelAttributes(ATTR_NAME_UI_ELEMENT, ATTR_TYPE_NUMBER);
                JSONObject createQtyJson = new JSONObject();
                if (interactionNumAttr != null && interactionNumAttr.size() > 0) {
                    for (int i = 0; i < interactionNumAttr.size(); i++) {
                        String attr = interactionNumAttr.get(i);
                        if (interactionBody.has(attr))
                            createQtyJson.put(attr, interactionBody.get(attr));
                    }
                }
                //List<String> qtyAttr = cInstance.getQuantity_attributes();
                qtyAttr = cInstance.getQuantity_attributes();
                if (qtyAttr != null && qtyAttr.size() > 0) {
                    for (int i = 0; i < qtyAttr.size(); i++) {
                        String attr = qtyAttr.get(i);
                        if (interactionBody.has(attr))
                            createQtyJson.put(attr, interactionBody.get(attr));
                    }
                }
                interactionsObject.put("quantity_attributes", createQtyJson); //JSONObject

                //_interaction_attributes from perspective
                ArrayList interactionAttr = daveAIPerspective.getInteraction_stage_attr_list();
                JSONObject inAttJson = new JSONObject();
                if (interactionAttr != null && interactionAttr.size() > 0) {
                    for (int j = 0; j < interactionAttr.size(); j++) {
                        String key = interactionAttr.get(j).toString();
                        if (interactionBody.has(key))
                            inAttJson.put(key, interactionBody.get(key));
                    }
                }
                interactionsObject.put("_interaction_attributes", inAttJson);//JSONObject

                previous_stage = cInstance.getPrevious_stage();
                next_stage = cInstance.getNext_stage();

                cInstance = null;
            }

             Log.i(TAG, "Print interaction Previous Stage:------------" + previous_stage + " NextStage:-" + next_stage + " istagesInstance:-" + cInstance);



             if(previous_stage!=null && !previous_stage.equalsIgnoreCase("__NULL__")) {
                //set previous stage
                interactionsObject.put("previous_stage", previous_stage); //String

                InteractionStages pInstance = getStageInstance(previous_stage);

                assert pInstance != null;

                //todo :- how to get it
                interactionsObject.put("previous_istage", ""); //String

                //interaction['details'].get(ps.get('tagged_products_attribute'))
                 preTaggedProductAtrr =  pInstance.getTagged_products_attribute();
                 String previousTagAttr =  pInstance.getTagged_products_attribute();
                if(previousTagAttr!=null && interactionBody.has(previousTagAttr) && !interactionBody.isNull(previousTagAttr) ){
                    interactionsObject.put("previous_stage_tagged_products",interactionBody.get(previousTagAttr)); //JSONArray
                } else {
                    interactionsObject.put("previous_stage_tagged_products", JSONObject.NULL); //JSONArray
                }

                //  ps.get('quantity_attributes',[])
                 if (pInstance.getQuantity_attributes() != null) {
                    interactionsObject.put("previous_stage_quantity_attributes", new JSONArray(pInstance.getQuantity_attributes())); //JSONArray
                } else {
                    interactionsObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ps.get('new_interaction_attributes', [])
                if (pInstance.getNew_interaction_attributes() != null) {
                    interactionsObject.put("previous_stage_new_attributes", new JSONArray(pInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    interactionsObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
                }

                 pInstance = null;

            }else{
                interactionsObject.put("previous_stage", "__NULL__"); //String
                interactionsObject.put("previous_stage_tagged_products", JSONObject.NULL); //JSONArray
                interactionsObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                interactionsObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
            }


            if(next_stage!=null && !next_stage.equalsIgnoreCase("__NULL__")) {
                // set next stage
                interactionsObject.put("next_stage", next_stage); //String

                InteractionStages nInstance = getStageInstance(next_stage);
                assert nInstance != null;
                //todo :- how to get it
                interactionsObject.put("next_istage", ""); //String

                // list(set(interaction['quantity_attributes'].keys()) - set(interaction['next_stage_quantity_attributes']))
                List<String> nsQtrAttr = nInstance.getQuantity_attributes();
                if (qtyAttr != null && qtyAttr.size() > 0) {
                    if (nsQtrAttr != null && nsQtrAttr.size() > 0)
                        qtyAttr.removeAll(nsQtrAttr);
                    interactionsObject.put("new_quantity_attributes", new JSONArray(qtyAttr)); //JSONArray
                } else
                    interactionsObject.put("new_quantity_attributes", new JSONArray()); //JSONArray


                // interaction['details'].get(ns.get('tagged_products_attribute'))
                nxtTaggedProductAtrr= nInstance.getTagged_products_attribute();
                String nTagAttr = nInstance.getTagged_products_attribute();
                if ( nTagAttr!=null && interactionBody.has(nTagAttr) && !interactionBody.isNull(nTagAttr)) {
                    interactionsObject.put("next_stage_tagged_products", interactionBody.get(nTagAttr)); //JSONArray
                } else {
                    interactionsObject.put("next_stage_tagged_products", new JSONArray()); //JSONArray
                }

                //  ns.get('quantity_attributes',[])
                //if (interactionBody.has("next_stage_quantity_attributes") && !interactionBody.isNull("next_stage_quantity_attributes")) {
                if (nInstance.getQuantity_attributes() != null) {
                    interactionsObject.put("next_stage_quantity_attributes", new JSONArray(nInstance.getQuantity_attributes())); //JSONArray
                } else {
                    interactionsObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ns.get('new_interaction_attributes', [])
                if (nInstance.getNew_interaction_attributes() != null) {
                    interactionsObject.put("next_stage_new_attributes", new JSONArray(nInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    interactionsObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
                }

               nInstance =null;
            }else{
                interactionsObject.put("next_stage", "__NULL__"); //String
                interactionsObject.put("new_quantity_attributes", new JSONArray()); //JSONArray
                interactionsObject.put("next_stage_tagged_products", new JSONArray()); //JSONArray
                interactionsObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                interactionsObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
            }

            // add all other custom attr
            interactionsObject = copySourceToTargetJSONObject(interactionBody,interactionsObject);

            if (productId != null && !productId.isEmpty()) {
                getProductDetails(productId,interactionsObject,onInteractionsHelper);
            }
            //Log.e(TAG, " Print Final interactions Format Size 22:------" + interactionsObject.length() + " " + interactionsObject);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during Converting object to InteractionsObject:--"+e.getMessage());
            if(onInteractionsHelper!=null)
                onInteractionsHelper.onFailedCreation(e.getMessage());
        }

    }

    private InteractionStages getStageInstance(String stageName) {

        InteractionStages iStagesInstance = null;
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        String stageDetail = databaseManager.getObjectData(INTERACTION_STAGES_TABLE_NAME, stageName).toString();
        Log.i(TAG,"Get Stage Details of Stage >>>>>>>>>>>>>>>>>>>>>>:-------"+stageName+" == "+ stageDetail);
        if (stageDetail != null && !stageDetail.isEmpty()) {
            try {
                Gson gson = new Gson();
                //iStagesInstance = gson.fromJson(stageDetail, InteractionStages.class);
               return gson.fromJson(stageDetail, InteractionStages.class);
            }
            catch (StackOverflowError exception){
                exception.printStackTrace();
            }
            catch (Exception exception) {
                exception.printStackTrace();
                Log.e(TAG, "Error setting Stage Detail in InteractionStages class*****  " + stageName + " Error:-" + exception.getMessage());
            }
        }
        return null;
    }

    private void getProductDetails(String productID,JSONObject interactionsObject,OnInteractionsHelper onInteractionsHelper){

        //Log.e(TAG, " <<<<<<<<<<<<<<<<<<<Before calling getObject product Details with Id>>>>>>>>>>>:------"+productID);
        DaveAIListener daveAIListener = new DaveAIListener() {
            @Override
            public void onReceivedResponse(JSONObject response) {
                try {
                    //Log.e(TAG, "<<<<<<<<<<<<<<On Response product Details with Id>>>>>>>>>>>"+productID+" Details:-" + response);

                    interactionsObject.put("details",response);
                    if(preTaggedProductAtrr!=null && !preTaggedProductAtrr.isEmpty() &&
                            response.has(preTaggedProductAtrr) && !response.isNull(preTaggedProductAtrr)){
                        interactionsObject.put("previous_stage_tagged_products",response.get(preTaggedProductAtrr)); //JSONArray

                    }

                    if(nxtTaggedProductAtrr!=null && !nxtTaggedProductAtrr.isEmpty() &&
                            response.has(nxtTaggedProductAtrr) && !response.isNull(nxtTaggedProductAtrr)){
                        Log.e(TAG, "On Response product Details  add nestStage tag attr >>>>>>>>>>> " + response.get(nxtTaggedProductAtrr));
                        interactionsObject.put("next_stage_tagged_products", response.get(nxtTaggedProductAtrr)); //JSONArray
                    }
                    if(onInteractionsHelper!=null) {
                        onInteractionsHelper.onCreatedCustomJSONObject(interactionsObject);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG, "Error during  getting Product Id +++++++++++" + e.getMessage());
                }

            }

            @Override
            public void onResponseFailure(int requestCode, String responseMsg) {
               // Log.e(TAG, "<<<<<<<<<<<<<<onResponseFailure>>>>>>>>>>>"+productID);
                try {
                    interactionsObject.put(ERROR_MSG_ATTRIBUTE,responseMsg);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(onInteractionsHelper!=null) {
                    onInteractionsHelper.onCreatedCustomJSONObject(interactionsObject);
                }
            }
        };
        try {
            if(productID!=null && !productID.isEmpty()) {
                DaveModels daveModels = new DaveModels(mContext, true);
                Log.e(TAG,"calling daveModel.getObject() for :---------------- "+modelUtil.getProductModelName()+"/"+productID);
                daveModels.getObject(modelUtil.getProductModelName(), productID, daveAIListener, false, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"error during getting product details:--"+e.getMessage());
            if(onInteractionsHelper!=null)
                onInteractionsHelper.onFailedCreation(e.getMessage());
        }
    }

    private   JSONObject copySourceToTargetJSONObject(JSONObject source,JSONObject target) {

        Iterator<?> keys = source.keys();
        while(keys.hasNext() ) {
            String key = (String)keys.next();
            try {
                if(!target.has(key))
                    target.put(key,source.get(key));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG,"copyAllOriginalToFinalJSONObject  :--------  "+source +"\n Final Jsonobject:-"+ target);

        return target;
    }



}


