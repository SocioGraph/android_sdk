package com.example.admin.daveai.others;

import org.json.JSONObject;
public class DaveHelper {

    private JSONObject customerDetails = new JSONObject();
    private int objectPageSize = 50;

    // static variable single_instance of type Singleton
    private static DaveHelper singleInstance = null;

    // private constructor restricted to this class itself
    private DaveHelper() {}

    // static method to create instance of Singleton class
    public static DaveHelper getInstance() {
        if (singleInstance == null)
            singleInstance = new DaveHelper();
        return singleInstance;
    }

    public JSONObject getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(JSONObject customerDetails) {
        this.customerDetails = customerDetails;
    }

    public int getObjectPageSize() {
        return objectPageSize;
    }

    public void setObjectPageSize(int objectPageSize) {
        this.objectPageSize = objectPageSize;
    }
}
