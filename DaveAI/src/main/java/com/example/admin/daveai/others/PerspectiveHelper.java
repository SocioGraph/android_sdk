package com.example.admin.daveai.others;

import android.content.Context;
import android.util.Log;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;

import org.json.JSONObject;


public class PerspectiveHelper {

    private static final String TAG = "PerspectiveHelper";

    public void getPerspective(Context mContext, String perspectiveName, DaveAIListener daveAIListener){

        boolean isFresh = false;
        boolean isFreshen = true;

        APIRoutes.getMetadata(mContext);

        CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(perspectiveName);
        if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
            DatabaseConstants.CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, DatabaseConstants.CacheTableType.SINGLETON);
            switch (refreshStatus){
                case COLD_UPDATE_REQUIRED:
                    isFreshen = true;

                    break;
                case HOT_UPDATE_REQUIRED:
                    isFresh = true;
                    isFreshen = true;

                    break;
                case NOT_REQUIRED:
                    isFreshen = false;
                    //isFresh=false;
                    break;
            }
            Log.e(TAG, "readPerspectivePreference check meta update:- " + cacheModelPOJO.modelName +" status:- "+ refreshStatus);
        }

        if(!isFresh){
            JSONObject jsonResponse = DatabaseManager.getInstance(mContext).getSingletonRow(perspectiveName);
            Log.e(TAG,"Get Perspective from Local database-------:- "+perspectiveName +" jsonobject "+jsonResponse);
            if(jsonResponse == null||jsonResponse.length()==0) {
                isFresh = true;

            }else{
                if(daveAIListener!=null ) {
                    daveAIListener.onReceivedResponse(jsonResponse);
                }
            }
        }

        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            new APICallAsyncTask(new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        setupModelCacheMetaData(perspectiveName, perspectiveName, mContext);
                        databaseManager.insertOrUpdateSingleton(
                                perspectiveName,
                                new SingletonTableRowModel(perspectiveName, response, System.currentTimeMillis(), "perspective_settings", "")
                        );
                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(new JSONObject(response));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        daveAIListener.onResponseFailure(0,"Failure in Parsing Perspective JSON");
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    if(daveAIListener!=null){
                        daveAIListener.onResponseFailure(responseCode,errorMsg);
                    }
                }
            }, mContext, "GET", false).execute(APIRoutes.getPerspectiveAPI(perspectiveName), DaveConstants.APIRequestMethod.GET.name());



        }else if(isFreshen ){
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.getPerspectiveAPI(perspectiveName), DaveConstants.APIRequestMethod.GET.name(),
                            "", perspectiveName,
                            "perspective_settings", DatabaseConstants.APIQueueStatus.PENDING.name(),
                            DatabaseConstants.APIQueuePriority.ONE.getValue(), DatabaseConstants.CacheTableType.SINGLETON.getValue()
                    )
            );
        }

    }

    public void updatePerspective(Context mContext, String perspectiveName, DaveAIListener daveAIListener){

        boolean isFresh = false;
        boolean isFreshen = true;

        APIRoutes.getMetadata(mContext);

        CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(perspectiveName);
        if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
            DatabaseConstants.CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, DatabaseConstants.CacheTableType.SINGLETON);
            switch (refreshStatus){
                case COLD_UPDATE_REQUIRED:
                    isFreshen = true;

                    break;
                case HOT_UPDATE_REQUIRED:
                    isFresh = true;
                    isFreshen = true;

                    break;
                case NOT_REQUIRED:
                    isFreshen = false;
                    //isFresh=false;
                    break;
            }
            Log.e(TAG, "update PerspectivePreference check meta update:- " + cacheModelPOJO.modelName +" status:- "+ refreshStatus);
        }
        if(isFresh || isFreshen) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            new APICallAsyncTask(new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        setupModelCacheMetaData(perspectiveName, perspectiveName, mContext);
                        databaseManager.insertOrUpdateSingleton(
                                perspectiveName,
                                new SingletonTableRowModel(perspectiveName, response, System.currentTimeMillis(), "perspective_settings", "")
                        );
                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(new JSONObject(response));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        daveAIListener.onResponseFailure(0,"Failure in Parsing Perspective JSON");
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    if(daveAIListener!=null){
                        daveAIListener.onResponseFailure(responseCode,errorMsg);
                    }
                }
            }, mContext, "GET", false).execute(APIRoutes.getPerspectiveAPI(perspectiveName), DaveConstants.APIRequestMethod.GET.name());



        }
    }



    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,Context context){
        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        daveAIPerspective.getHot_update_time_limit(),
                        daveAIPerspective.getCold_update_time_limit(),
                        0, modelIdAttrName,System.currentTimeMillis())
        );


    }
}
