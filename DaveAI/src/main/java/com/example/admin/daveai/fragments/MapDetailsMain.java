package com.example.admin.daveai.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



public class MapDetailsMain extends SupportMapFragment implements OnMapReadyCallback {

    public double latt = 0.0;
    public double longg = 0.0;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        latt = getArguments().getDouble("LAT");
        longg = getArguments().getDouble("LOG");
        Log.e("MapDetailsMain", "onCreate: map"+ latt +" lon: "+ longg);

        this.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        Log.e("MapDetailsMain", "onMapReady1: "+ googleMap);
      /*  if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }*/
       // googleMap.setMyLocationEnabled(true);
        Log.e("MapDetailsMain", "onMapReady"+ latt +" lon: "+ longg);
        LatLng user = new LatLng(latt, longg);
       /* googleMap.getUiSettings().setScrollGesturesEnabled(false);

        googleMap.addMarker(new MarkerOptions().position(user));
*/
        googleMap.addMarker(new MarkerOptions()
                .position(user)
                .title("LinkedIn")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(user));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user, 10));

    }

}
