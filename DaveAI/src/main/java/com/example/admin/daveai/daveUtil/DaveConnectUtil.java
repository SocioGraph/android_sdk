package com.example.admin.daveai.daveUtil;

import java.util.ArrayList;


public class DaveConnectUtil {

    //by default call  user object
    //  mobile perspective,interactionsStages,
    // customer hierarchy & filters ,product hierarchy & filters


    private ArrayList<String> coreModelList ;
    private ArrayList<String> otherModelList;
    private ArrayList<String> prefSettingList;
    private boolean callInteractionStages = false;
    private ArrayList<String> mediaCachingModels;
    private BulkDownloadUtil bulkDownloadUtil;





    public ArrayList<String> getCoreModelList() {
        return coreModelList;
    }

    public void setCoreModelList(ArrayList<String> coreModelList) {
        this.coreModelList = coreModelList;
    }

    public ArrayList<String> getOtherModelList() {
        return otherModelList;
    }

    public void setOtherModelList(ArrayList<String> otherModelList) {
        this.otherModelList = otherModelList;
    }

    public ArrayList<String> getPrefSettingList() {
        return prefSettingList;
    }

    public void setPrefSettingList(ArrayList<String> prefSettingList) {
        this.prefSettingList = prefSettingList;
    }

    public boolean isCallInteractionStages() {
        return callInteractionStages;
    }

    public void setCallInteractionStages(boolean callInteractionStages) {
        this.callInteractionStages = callInteractionStages;
    }

    public ArrayList<String> getMediaCachingModels() {
        return mediaCachingModels;
    }

    public void setMediaCachingModels(ArrayList<String> mediaCachingModels) {
        this.mediaCachingModels = mediaCachingModels;
    }

    public BulkDownloadUtil getBulkDownloadUtil() {
        return bulkDownloadUtil;
    }

    public void setBulkDownloadUtil(BulkDownloadUtil bulkDownloadUtil) {
        this.bulkDownloadUtil = bulkDownloadUtil;
    }


}
