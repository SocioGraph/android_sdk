package com.example.admin.daveai.others;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;

import com.example.admin.daveai.broadcasting.S3UploadService;
import com.example.admin.daveai.daveUtil.AmazonS3UploadUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class DaveLogger {

    private static final String TAG = "DaveLogger";
    Context mContext;
    private String android_id = "";
    private String s3Bucket = "";
    private String s3Path = "";
    private ArrayList<String> avoidStringList = new ArrayList<>();
    /**
     * @param localDirName sdCard Folders ex: "/dave/logcat" will save logs in '[sdcard]/dave/logcat'
     * @param timePerFile interval for log posting.
     * @param s3Path folder path in s3 '[Folder]/[SubFolder]/[SubSubFolder]'
     * @param s3Bucket s3 bucket name ex: general-iamdave
     * @param avoidStringList log lines with these phrases will not be saved
     * */
    protected void writeLogCat(Context context, String localDirName, long timePerFile, String s3Path, String s3Bucket, ArrayList<String> avoidStringList)
    {

        mContext = context;

        this.s3Path = s3Path;
        this.s3Bucket = s3Bucket;
        this.avoidStringList = avoidStringList;
        android_id = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
                if(t!=null && t.isAlive()) {
                    t.interrupt();
                }
                log(localDirName);

            }
        }, 0, timePerFile);



    }

    Thread t;
    private void log(String localDirName){
        t = new Thread(new Runnable() {
            @Override
            public void run() {
                try
                {
                    //Create txt file in SD Card
                    File sdCard = Environment.getExternalStorageDirectory();
                    File dir = new File(sdCard.getAbsolutePath()+localDirName +File.separator + "Log File");
                    Date c2 = Calendar.getInstance().getTime();
                    SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
                    String fileTimeFormatter = df2.format(c2);
                    if(!dir.exists())
                    {
                        dir.mkdirs();
                    }
                    StringBuilder avoidStrings = new StringBuilder();
                    for(int x = 0 ; x < avoidStringList.size() ; x++){
                        avoidStrings.append(avoidStringList.get(x));
                        if(x<(avoidStringList.size()-1)) {
                            avoidStrings.append("|");
                        }

                    }


                    String avoidRegex = "";
                    if(avoidStringList.size()>0)
                        avoidRegex = "-e ^(?!.*("+avoidStrings.toString()+"*)).*$";
                    String fileName = "log__"+fileTimeFormatter+".txt";
                    File myFile = new File(dir, fileName);
//                    Process clearLogProcess = Runtime.getRuntime().exec("logcat -c");
                    Process process = Runtime.getRuntime().exec(new String[]{"logcat","-f",myFile.getAbsolutePath(),"*:I"});
                    try {
                        process.waitFor();
                    }catch (Exception e){
                        Log.e(TAG,"EXCEPTION = "+e.getMessage());
//                        e.printStackTrace();
                        process.destroy();
                    }
                    /*BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(process.getInputStream()));

                    String line = "";

                    FileOutputStream fileOutputStream = new FileOutputStream(myFile);
                    OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);

                    Log.e(TAG,"file created and writing to = "+myFile.getAbsolutePath());

                    while ((line = bufferedReader.readLine()) != null) {
                        boolean writeToFile = true;
                        *//*for(int i = 0 ; i < avoidStringList.size() ; i++) {
                            if (line.contains(avoidStringList.get(i))) {
                                writeToFile = false;
                            }
                        }
                        if(writeToFile) {*//*
                            writer.append(line + "\n");
//                        }
                    }*/

//                    Process clearLogProcess = Runtime.getRuntime().exec("logcat -c");
/*
                    process.destroy();
                    writer.flush();
                    writer.close();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    bufferedReader.close();*/
                    Date c = Calendar.getInstance().getTime();
                    System.out.println("Current time => " + c);
                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                    String formattedDate = df.format(c);

                    uploadFile(dir+File.separator,s3Path+"/"+formattedDate+"/"+android_id+"/",fileName, s3Bucket);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

    }
    private static String COMMAND = "logcat";


    private void uploadFile(String filePath, String s3Path, String fileName, String s3Bucket){
        AmazonS3UploadUtil amazonS3UploadUtil = new AmazonS3UploadUtil();

        amazonS3UploadUtil.addToUploadQueue(mContext, filePath+fileName, s3Path, s3Bucket, fileName);
        Intent msgIntent = new Intent(mContext, S3UploadService.class);
        mContext.startService(msgIntent);

    }
}
