package com.example.admin.daveai.dialogbox;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import com.example.admin.daveai.R;


public class CustomProgressDialog {

    private Context context;

    public CustomProgressDialog(Context context){
        this.context = context.getApplicationContext();

    }

    /**
     * @return  progressBar view
     */
    public ProgressBar showProgressBar(){
        Log.i("ProgressBar","Show ProgressBar in View Group :------ ");
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.custom_progress_bar, null);
        return view.findViewById(R.id.progressBar);
    }

    /**
     *
     * @param viewGroup  provide View where added ProgressBar
     */
    public void hideProgressBar(ViewGroup viewGroup){
        Log.i("hideProgressBar","GetViewGroup Name:------ "+viewGroup);
        if(viewGroup!=null && viewGroup.findViewById(R.id.progressBar)!=null) {
            Log.e("ProgressBar","Remove ProgressBar from view :------ ");
            ProgressBar pb = viewGroup.findViewById(R.id.progressBar);
            pb.setVisibility(View.GONE);
            viewGroup.removeView(pb);
        }

    }

}
