package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.MapItem;
import com.example.admin.daveai.fragments.MapDetailsMain;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import me.drakeet.multitype.ItemViewBinder;

public class MapItemViewBinder extends ItemViewBinder<MapItem, MapItemViewBinder.MapHolder> {
    public Context context;
    GoogleMap gMap;


    public MapItemViewBinder(Context context) {
        this.context = context;

    }

    @NonNull
    @Override
    protected MapItemViewBinder.MapHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new MapItemViewBinder.MapHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.map_view_layout, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull MapItemViewBinder.MapHolder holder, @NonNull MapItem mapItem) {


       /* MapDetailsMain mapFr = new MapDetailsMain();
        Bundle b = new Bundle();
        b.putDouble("LAT", mapItem.getLatitude());
        b.putDouble("LOG", mapItem.getLongitude());

        mapFr.setArguments(b);
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mapCevent, mapFr).commit();
*/
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mapCevent, mapFragment).commit();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                int height = holder.frameLayout.getWidth();
                holder.frameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,height));
                LatLng latLng = new LatLng(mapItem.getLatitude(), mapItem.getLongitude());

                googleMap.addMarker(new MarkerOptions().position(latLng));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
            }
        });



    }


    public class MapHolder extends RecyclerView.ViewHolder {

        private FrameLayout frameLayout;
        public MapHolder(View itemView) {
            super(itemView);

            frameLayout  = itemView.findViewById(R.id.mapCevent);

        }
    }
}

