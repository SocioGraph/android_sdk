package com.example.admin.daveai.broadcasting;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.DaveCachedUtil;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;

import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveModels;
import com.google.gson.Gson;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.admin.daveai.others.DaveApplication.runOnUiThread;

public class PushNotifications implements OneSignal.NotificationOpenedHandler , OneSignal.NotificationReceivedHandler ,
        DaveConstants, DatabaseConstants {


    private final String TAG = getClass().getSimpleName();
    private Context context;
    private DatabaseManager databaseManager;

    private PushNotificationsListener mListener;
    public interface PushNotificationsListener {
        void onNotificationReceived(OSNotification notification,JSONObject notificationData);
        void onNotificationOpened(OSNotificationOpenResult result, JSONObject notificationData);
    }

    public PushNotifications(Context context,PushNotificationsListener pushNotificationsListener){
        this.context = context.getApplicationContext();
        this.mListener = pushNotificationsListener;
        databaseManager= DatabaseManager.getInstance(context);

    }

    public void startPushNotification(){

        Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<startPushNotification:-------------- ");


        // Logging set to help debug issues, remove before releasing your app.
        //OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.WARN);

        //MyNotificationOpenedHandler : This will be called when a notification is tapped on.
        //MyNotificationReceivedHandler : This will be called when a notification is received while your app is running.
        OneSignal.startInit(context)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                // .autoPromptLocation(true)
                //.setNotificationOpenedHandler(new MyNotificationOpenedHandler(context))
                //.setNotificationReceivedHandler( new MyNotificationReceivedHandler(context))
                .setNotificationOpenedHandler(PushNotifications.this)
                .setNotificationReceivedHandler(PushNotifications.this)
                .init();
        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);



    }

    /**
     *   _action = "sync" ,"post", "update","delete","open"
     *   _page = "model", "perspective","media", "open page name"
     *   _model = <modelName> Name of Model
     *   _id = <id> = perspective Id , objectId
     *    _instance={} //object details , queryParams in case of getObjects
     *    _order_d=<orderId> //action "open
     */



    //fired when your app is in focus or in the background.
    @Override
    public void notificationReceived(OSNotification notification) {

        int notificationId = notification.androidNotificationId;
        String notificationID = notification.payload.notificationID;
        JSONObject data = notification.payload.additionalData;

        String title = notification.payload.title;
        String body = notification.payload.body;
        String smallIcon = notification.payload.smallIcon;
        String largeIcon = notification.payload.largeIcon;
        String bigPicture = notification.payload.bigPicture;
        String smallIconAccentColor = notification.payload.smallIconAccentColor;
        String sound = notification.payload.sound;
        String ledColor = notification.payload.ledColor;
        int lockScreenVisibility = notification.payload.lockScreenVisibility;
        String groupKey = notification.payload.groupKey;
        String groupMessage = notification.payload.groupMessage;
        String fromProjectNumber = notification.payload.fromProjectNumber;
        String rawPayload = notification.payload.rawPayload;


        Log.e(TAG," notificationReceived Print notification id:----------------"+ notificationId);
        //String defaultEnterpriseId = MultiUtil.getManifestMetadata(context, MANIFEST_ENTERPRISE_ID);
        if (data != null) {

             if(mListener != null)
                    mListener.onNotificationReceived(notification, data);

            String actionName = data.optString("_action",null);
            if(actionName!= null && !actionName.isEmpty() && !actionName.equalsIgnoreCase("open")){

                removeNotification(notificationId);  //remove Notification
                Log.d(TAG,"<<<<<<<notificationReceived Print Notification  ActionNAme>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ actionName);
                if(actionName.equalsIgnoreCase("post") ) {
                    postNotificationData(data);
                }
                else if(actionName.equalsIgnoreCase("update")) {
                    updateNotificationData(data);
                }
                else if(actionName.equalsIgnoreCase("delete")) {
                    deleteNotificationData(data);
                }
                else if(actionName.equalsIgnoreCase("sync")) {
                    syncNotificationData(data);
                }

            }else {
                Log.e(TAG," notificationReceived Don't do any thing.  ActionName:  " + actionName);
            }
        }
    }

    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {

        //OSNotificationAction.ActionType actionType = result.action.type;

        JSONObject additionalData = result.notification.payload.additionalData; // notification_data

      /*  String defaultEnterpriseId = MultiUtil.getManifestMetadata(context, DaveConstants.MANIFEST_ENTERPRISE_ID);
        if (additionalData != null) {
            String enterpriseId = additionalData.optString("enterprise_id", null);
            Log.e(TAG," notificationOpened Print  enterpriseId "+ enterpriseId +"  defaultEnterpriseId: "+defaultEnterpriseId );
            if (enterpriseId != null && enterpriseId.equalsIgnoreCase(defaultEnterpriseId)) {
                if(mListener!=null){
                    mListener.onNotificationOpened(result,additionalData);
                }
            }else {
                Log.e(TAG,"Error notificationOpened enterpriseId is not matched EnterpriseId : "+enterpriseId +" defaultEnterpriseId: "+ defaultEnterpriseId);
            }
        }else {
            Log.e(TAG,"Error notificationOpened notificationData is null  ");
        }*/

        if (additionalData != null) {
            if(mListener != null)
                mListener.onNotificationOpened(result, additionalData);
        }else {
            Log.e(TAG,"Error notificationOpened notificationData is null  ");
        }

    }



    private void removeNotification(int notificationId){

        OneSignal.clearOneSignalNotifications();
        OneSignal.cancelNotification(notificationId);
    }

    private void postNotificationData(JSONObject notificationData){

        try  {
            String modelName = notificationData.optString("_model",null);
            JSONObject postBody = notificationData.optJSONObject("_instance");
            String idAttrName = new ModelUtil(context).getModelIDAttrName(modelName);

           // Log.i(TAG,"postNotificationData idAttrName >>>>>>>>>>>>>>  "+ idAttrName +" postBody:-  "+ postBody);
            if(idAttrName !=null && postBody != null && postBody.has(idAttrName)) {
                String objectId = postBody.optString(idAttrName,null);
                if(modelName != null && objectId != null ) {
                    Log.e(TAG,"postNotificationData  add in table >>>>>>>>>>>>>>  "+ idAttrName +" objectID:-  "+ objectId);
                    databaseManager.insertOrUpdateObjectData(
                            modelName,
                            new ObjectsTableRowModel(
                                    objectId, //id
                                    postBody.toString(), //data
                                    System.currentTimeMillis() // current time
                            ));
                }
                else{
                    Log.e(TAG,"Error postNotificationData:------modelNAme   " + modelName +"  && postBody   "+ postBody);
                }
            }
            else{
                Log.e(TAG,"Error postNotificationData   ModelName:  " + modelName + "  idAttrName:  "+ idAttrName+ "  && postBody   "+ postBody);
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error postNotificationData:-----  " +e.getMessage());
        }
    }

    private void updateNotificationData( JSONObject notificationData){

        try {
            String modelName = notificationData.optString("_model",null);
            String objectId = notificationData.optString("_id",null);
            JSONObject updatedBody = notificationData.getJSONObject("_instance");

            if(modelName!=null && objectId!=null && updatedBody!= null) {

                //Log.i(TAG,"updateNotificationData add in table>>>>>>>>>>>>>>  "+ modelName +" objectID:-  "+ objectId +" updatedBOdy: "+ updatedBody);
               /* databaseManager.insertOrUpdateObjectData(
                        modelName,
                        new ObjectsTableRowModel(
                                objectId, //id
                                updatedBody.toString(), //data
                                System.currentTimeMillis() // current time
                        ));*/

                databaseManager.updateDataCached(
                        modelName,
                        objectId,
                        updatedBody, //data
                        "");

            }else{
                Log.e(TAG,"Error updateNotificationData:- Print -----modelNAme:   " + modelName +"  && ObjectID:   "+ objectId +"  updatedBody: "+updatedBody );
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error updateNotificationData:-----  " +e.getMessage());
        }
    }

    /**
     * method to delete object of particular table from local DataBase
     * @param notificationData notification data
     */
    private void deleteNotificationData(JSONObject notificationData){

        String modelName = notificationData.optString("_model",null);
        String objectId = notificationData.optString("_id",null);
        if(modelName != null && objectId != null) {
            //Log.i(TAG,"deleteNotificationData >>>>>>>>>>>>>>  "+ modelName +" objectID:-  "+ objectId );
            ArrayList<String> attrs1 = new ArrayList<>();
            attrs1.add(OBJECT_ID);
            databaseManager.deleteRow(
                    modelName, //model Name
                    databaseManager.createWhereClause(attrs1), new String[]{String.valueOf(objectId)}
            );
        }else{
            Log.e(TAG,"Error deleteNotificationData:------modelNAme   " + modelName +"  && ObjectID   "+ objectId);
        }

    }

    /**
     * method to sync notification Data
     * _page = "model", "perspective","media"
     * _model = <modelName> when page is "model"
     * _id = <id>  when page is "perspective"
     * _instance={}
     * _order_d=<orderId>
     * @param notificationData notification data
     */
    private void syncNotificationData(JSONObject notificationData){

        String page = notificationData.optString("_page", null);
        if(page!= null) {
            switch (page) {

                case "model":
                    String modelName = notificationData.optString("_model", null);
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.modelAPI(modelName), DaveConstants.APIRequestMethod.GET.name(),
                                    "", modelName,
                                    modelName, DatabaseConstants.APIQueueStatus.PENDING.name(),
                                    APIQueuePriority.TWO.getValue(), CacheTableType.MODEL.getValue()
                            )
                    );

                    break;

                case "perspective":
                    Log.e(TAG,"notification data = "+notificationData);
                    String prefName = notificationData.optString("_id", null);
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.getPerspectiveAPI(prefName), APIRequestMethod.GET.name(),
                                    "", prefName,
                                    "perspective_settings", DatabaseConstants.APIQueueStatus.PENDING.name(),
                                    APIQueuePriority.TWO.getValue(), CacheTableType.SINGLETON.getValue()
                            )
                    );
                    new DaveService().startBackgroundService(context);
                    break;

               /* case "objects":
                    try {
                        String objModelName = notificationData.optString("_model", null);
                        addDataToCachedTables(objModelName, notificationData.getJSONArray("_data"));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;*/

                default:
                    Log.e(TAG, "**************No page matched***************");
                    break;

            }
        }


    }

    private void addDataToCachedTables( String modelName, JSONArray dataJar){
        try{

            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            List<String> modelMediaCache = new ArrayList<>();
            Model mod = new Model(context);
            try {
                JSONObject modelJsn = databaseManager.getModelData(modelName);
                Gson gson = new Gson();
                mod = gson.fromJson(modelJsn.toString(),Model.class);
                try {
                    modelMediaCache = mod.getQuick_views().getCached();
                }catch (Exception e){
                    e.printStackTrace();

                    modelMediaCache = new ArrayList<>();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            Log.i("getObjects", " Total COunt of Objects:-" + dataJar.length());
            CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
            DaveCachedUtil daveCachedUtil = new DaveCachedUtil(context);
            for (int i = 0; i < dataJar.length(); i++) {


                HashMap<String,Object> params = new HashMap<>();
                params.put(OBJECT_ID,dataJar.getJSONObject(i).getString(modelMetaData.model_id_name));

                Cursor c = databaseManager.getDataFromTable(modelName,params,new HashMap<>(),"",0,"");
                JSONObject finalDataCached = dataJar.getJSONObject(i);
                DatabaseManager.getInstance(context).insertOrUpdateObjectData(
                        modelName,
                        new ObjectsTableRowModel(
                                dataJar.getJSONObject(i).getString(modelMetaData.model_id_name),
                                dataJar.getJSONObject(i).toString(),
                                System.currentTimeMillis()
                        ));


                for (int x = 0; x < modelMediaCache.size(); x++) {
                    daveCachedUtil.addToMediaQueue(modelMediaCache.get(x), mod, finalDataCached, modelName, modelMetaData);
                }

            }
            Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
            if(cur.getCount()>0){
                Intent msgIntent = new Intent(context, MediaService.class);
                context.startService(msgIntent);
            }
            databaseManager.alertOnMediaQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
                @Override
                public void isEmpty() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                DaveModels dm_final_media = new DaveModels(context, true);

                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put("data", dataJar);

                                JSONObject finalJSON = new JSONObject();
                                finalJSON.put("media",daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonObject1).getJSONArray("data"));
                                //writeToFile("media_file", finalJSON.toString());
                                SendBroadcast.sendCustomBroadcast(context,"com.daveai.kioskecommerce.media_file","data_synced",finalJSON.toString());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            },2000);

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(String filename, String data){
        try {
            Log.e("LOGGER","WRITING DATA TO FILE = "+filename);
            Log.e("LOGGER","WRITING DATA = "+data);
            Writer output = null;
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/dave/cached_files/" + filename + ".json");
            if(file.exists()){
                file.delete();
            }
            output = new BufferedWriter(new FileWriter(file));
            output.write(data);
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


}
