package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TextItem;

import me.drakeet.multitype.ItemViewBinder;


public class TextItemViewBinder extends ItemViewBinder<TextItem, TextItemViewBinder.TextHolder> {


    @Override
    protected @NonNull
    TextHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_text, parent, false);
        return new TextHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull TextHolder holder, @NonNull TextItem textItem) {

        holder.key_title.setText(textItem.getText_key());
        holder.key_value.setText(textItem.getText_value());

    }

    @Override
    public void onViewDetachedFromWindow(@NonNull TextHolder holder) {
        holder.itemView.clearAnimation();
    }


    static class TextHolder extends RecyclerView.ViewHolder {

        private TextView key_title, key_value;

        TextHolder(@NonNull View itemView) {
            super(itemView);
            this.key_title = itemView.findViewById(R.id.key_title);
            this.key_value = itemView.findViewById(R.id.key_value);
        }
    }
}
