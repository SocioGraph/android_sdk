package com.example.admin.daveai.network;


public interface APIResponse {
   void onTaskCompleted(String response);
   void onTaskFailure( int responseCode, String errorMsg);
}


