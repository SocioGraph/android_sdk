package com.example.admin.daveai.daveUtil;

import java.util.Arrays;
import java.util.List;

public interface DaveConstants {

    //Manifest META DATA  Key NAme
    String MANIFEST_BASE_URL = "BASE_URL";
    String MANIFEST_ENTERPRISE_ID = "X-I2CE-ENTERPRISE-ID";
    String MANIFEST_APP_ID = "APPLICATION_ID";
    String MANIFEST_PROVIDER_AUTHORITY = "PROVIDER_AUTHORITY";

    String DEFAULT_PERSPECTIVE_NAME = "_default_perspective_name";

    List<String> CORE_MODEL_LIST = Arrays.asList("customer", "product","interaction");

    //perspective variable name
   // String DEFAULT_PREF_NAME = "mobile_application";
    String USER_LOGIN_MODEL_NAME= "user_login_model_name";
    String USER_LOGIN_ID_ATTRIBUTE = "user_login_id_attr_name";
    String USER_PROFILE_ATTRIBUTE_LIST = "user_profile_details_list";
    String RECOMMENDATION_BUTTON = "recommendations_btn_name";
    String menu_title_for_primary_orders = "menu_title_for_primary_orders";
    String menu_title_for_incentives = "menu_title_for_incentives";
    String menu_title_for_reports = "menu_title_for_reports";
    String RECOMMENDATION_COUNT = "no_of_recommendations";
    String SWIPE_COUNT_TO_NEXT_RECOMMENDATION="count_after_next_recommendations";
    String SCAN_BUTTON_NAME ="scan_btn_name";
    String SCAN_CODE_ATTRIBUTE_NAME ="scan_code_attribute_name";
    String CUSTOMER_CARD_IMAGE ="customer_card_image";
    String CUSTOMER_CARD_HEADER ="customer_card_header";
    String CUSTOMER_CARD_SUB_HEADER ="customer_card_sub_header";
    String CUSTOMER_PROFILE_TITLE ="customer_profile_title";
    String CUSTOMER_DETAILS_VIEW_LIST ="customer_details_view";
    String CUSTOMER_SIGN_UP_LIST ="customer_sign_up_list";
    String CUSTOMER_HIERARCHY_FIRST ="customer_attr_hierarchy_first";
    String CUSTOMER_VARIABLE_ATTRIBUTE_TITLE ="customer_variable_attr_title";
    String CUSTOMER_VARIABLE_ATTRIBUTE_LIST ="customer_variable_attr";
    String CUSTOMER_FILTER_LIST ="customer_filter_attr";
    String CUSTOMER_HIERARCHY_LIST ="customer_category_hierarchy";
    String CUSTOMER_EMAIL_ATTRIBUTE ="customer_email_attr";
    String CUSTOMER_MOBILE_ATTRIBUTE ="customer_mobile_attr";

    String ENABLE_MANAGE_PRODUCT="enable_manage_product";
    String PRODUCT_CARD_IMAGE="product_card_image_view";
    String PRODUCT_CARD_HEADER="product_card_header";
    String PRODUCT_CARD_SUB_HEADER="product_card_sub_header";
    String PRODUCT_CARD_RIGHT_ATTRIBUTE="product_card_right_attr";
    String PRODUCT_EXPAND_VIEW_TITLE="product_expand_view_title";
    String PRODUCT_EXPAND_VIEW_LIST="product_expand_view";
    String PRODUCT_HIERARCHY_TITLE="product_category_hierarchy_title";
    String PRODUCT_HIERARCHY_LIST="product_category_hierarchy";
    String PRODUCT_FILTER_LIST="product_filter_attr";
    String INTERACTION_SUMMATION_LIST="summation_attributes";
    String INTERACTION_STAGE_QTY="interaction_qty";
    String INTERACTION_STAGE_ATTRIBUTE_LIST="interaction_stage_attr_list";
    String PREDICATED_QTY_ATTRIBUTE_NAME="predicated_qty_attr";
    String INVOICE_MODEL_NAME ="invoice_model_name";
    String INVOICE_ID_ATTRIBUTE_NAME ="invoice_id_attr_name";
    String INVOICE_CLOSE_ATTRIBUTE_NAME ="invoice_close_attr_name";
    String INVOICE_CHECKOUT_ATTRIBUTE_LIST ="invoice_checkout_attr_list";
    String FORM_VIEW_BUTTON_NAME ="form_view_button_name";
    String FORM_VIEW_ATTRIBUTE_LIST ="form_view_attribute_list";
    String MENU_TITLE_FOR_SIGNUP ="menu_title_for_sign_up";
    String MENU_TITLE_FOR_CUSTOMER_LIST ="menu_title_for_customer_list";
    String MENU_TITLE_FOR_PRODUCT_LIST ="menu_title_for_product_list";
    String MENU_TITLE_FOR_ORDER_LIST ="menu_title_for_order_list";
    String ENABLE_SET_DEFAULT_QUANTITY="enable_set_default_quantity";
    String MANAGE_PRODUCT_FROM_CUSTOMER_PROFILE ="manage_product_from_customer_profile";
    String ADD_PRODUCT_TITLE="add_product_title";
    String COLD_UPDATE_TIME_LIMIT = "cold_update_time_limit";
    String HOT_UPDATE_TIME_LIMIT = "hot_update_time_limit";
    String SEARCH_BY_IMAGE_ATTRIBUTE = "search_by_image_attr";
    String SEARCH_BY_IMAGE_BUTTON_NAME = "search_by_image_button_name";
    String SHARE_IMAGE_ATTRIBUTES = "share_image_attributes";




    String ATTR_NAME_UI_ELEMENT = "ui_element";
    String ATTR_TYPE_NUMBER = "number";
    String MODEL_FILTER_LIST = "FilterList";
    String MODEL_PIVOT_REQUEST_TYPE = "Pivot";
    String MODEL_HIERARCHY = "_hierarchy";

    String ERROR_MSG_ATTRIBUTE = "__error__";
    String ADD_TYPE_VIEW = "add_type_view";


    String IS_CONVERT_INTERACTIONS = "_convert_into_interactions";




    enum APIRequestMethod{

        GET("GET"),
        POST("POST"),
        UPDATE("UPDATE"),
        DELETE("DELETE")
        ;

        private String requestMethod;
        /**
         * @param requestMethod
         */
        APIRequestMethod(String requestMethod) {
            this.requestMethod = requestMethod;
        }

        public String getMethod() {
            return requestMethod;
        }

    }
}
