package com.example.admin.daveai.database.models;

import com.example.admin.daveai.database.DatabaseConstants;

/**
 * Created by soham on 24/9/18.
 */

public class APIQueueTableRowModel {
    public int postId;
    public String apiURL = "";
    public String apiMethod = "";
    public String apiBody = "";
    public String updateTableName= "";
    public String updateTableRowId= "";
    public String status = "";
    public int priority = 0 ;
    public int cacheTableType = 0 ;
    public String onError = "";

    //insert
    public APIQueueTableRowModel(String apiURL, String apiMethod, String apiBody, String updateTableName, String updateTableRowId,
                                 String status,int priority ,int cacheTableType) {
        this.apiURL = apiURL;
        this.apiMethod = apiMethod;
        this.apiBody = apiBody;
        this.updateTableName = updateTableName;
        this.updateTableRowId = updateTableRowId;
        this.status = status;
        this.priority = priority;
        this.cacheTableType = cacheTableType;

    }


    // update
    public APIQueueTableRowModel(int postId, String apiURL, String apiMethod, String apiBody, String updateTableName,
                                 String updateTableRowId, String status, int priority, int cacheTableType, String onError) {
        this.postId = postId;
        this.apiURL = apiURL;
        this.apiMethod = apiMethod;
        this.apiBody = apiBody;
        this.updateTableName = updateTableName;
        this.updateTableRowId = updateTableRowId;
        this.status = status;
        this.priority = priority;
        this.cacheTableType = cacheTableType;
        this.onError = onError;
    }


}
