package com.example.admin.daveai.expandViewRecycler.ViewItem;

import java.util.ArrayList;


public class ViewPagerItem {

    private String ketTitle;
    private ArrayList<String> imageList;

    public ViewPagerItem(  String ketTitle,ArrayList<String> imageList) {
        this.imageList = imageList;
        this.ketTitle=ketTitle;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public String getImageTitle() {
        return ketTitle;
    }
}
