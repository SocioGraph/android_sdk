package com.example.admin.daveai.daveUtil;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.example.admin.daveai.broadcasting.S3UploadService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.S3UploadFileModel;

import java.io.File;

import static com.example.admin.daveai.database.DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME;

public class AmazonS3UploadUtil {
    private static String TAG = "AmazonS3Upload";
    private static String ACCESS_KEY = "AKIAJ3YZKUJYJW2TRRCQ";
    private static String SECRET_KEY = "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";

    public interface AmazonS3UploadListener{
        public void uploadSuccess();
        public void uploadFailed(int id, Exception ex);
    }

    public void upload(Context context, File fileToBeUploaded, String uploadFileName, String uploadFilePath, String uploadBucketName, AmazonS3UploadListener amazonS3UploadListener){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);

        Log.e(TAG,"1");

        AmazonS3 s3 = new AmazonS3Client(awsCreds);
        TransferUtility transferUtility = new TransferUtility(s3, context);
        final TransferObserver observer = transferUtility.upload(
                uploadBucketName,  //this is the bucket name on S3
                uploadFilePath+uploadFileName, //this is the path and name
                fileToBeUploaded, //path to the file locally
                CannedAccessControlList.PublicRead //to make the file public
        );
        Log.e(TAG,"2");

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (state.equals(TransferState.COMPLETED)) {
                    //Success
                    Log.e(TAG,"UPLOAD SUCCESS!!!!!!!!!!!!!");
                    deleteFile(fileToBeUploaded);
                    if(amazonS3UploadListener!=null){
                        amazonS3UploadListener.uploadSuccess();
                    }
                } else if (state.equals(TransferState.FAILED)) {
                    //Failed
                    Log.e(TAG,"UPLOAD FAILED!!!!!!!!!!!!!");
                    deleteFile(fileToBeUploaded);
                    if(amazonS3UploadListener!=null){
                        amazonS3UploadListener.uploadFailed(id,new Exception("Error During Upload"));
                    }
                }

            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e(TAG,"error = "+id+"  "+ex.getMessage());
                ex.printStackTrace();
                if(amazonS3UploadListener!=null){
                    amazonS3UploadListener.uploadFailed( id,  ex);
                }
            }
        });
    }


    private void deleteFile(File file){
        file.delete();
    }

    public void addToUploadQueue(Context context,String localPath,String uploadFolderPath,String uploadBucket,String uploadFileName){
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);

        Log.e(TAG,"insert s3 upload queue called");
        databaseManager.insertS3UploadQueueData(new S3UploadFileModel(localPath,uploadFileName,uploadFolderPath,uploadBucket));

    }
}

