package com.example.admin.daveai.others;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.broadcasting.MediaService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.InteractionsHelper;
import com.example.admin.daveai.database.RecommendationHelper;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.RecommendationsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.DaveCachedUtil;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Attribute;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import static android.net.Uri.encode;
import static com.example.admin.daveai.database.DatabaseConstants.APIQueueStatus.PENDING;
import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;
import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_PNG;
import static com.example.admin.daveai.network.APIRoutes.getMetadata;
import static com.example.admin.daveai.others.DaveAIHelper.currentOrderId;
import static com.example.admin.daveai.others.DaveAIHelper.interactionAttr;
import static com.example.admin.daveai.others.DaveAIStatic.base_url;


public class DaveModels implements DatabaseConstants, DaveConstants {

    private final String TAG = "DaveModels";
    private DaveSharedPreference sharedPreferences;
    private boolean isCached =true;
    private boolean isFresh = false;
    private boolean isFreshen = true;
    private Context mContext;
    private JSONObject jsonResponse = null;
    private DaveAIPerspective daveAIPerspective;
    DatabaseManager databaseManager;
    Model modelInstance;

    /**
     *
     * @param mContext :- context of current class .if is null throw null point exception
     * @param isCached :- if its true ,stored data in Database.
     *                  By Default  its true.
     */
    public DaveModels(Context mContext,boolean isCached){
        if(mContext!=null) {
            this.mContext = mContext;
            this.isCached = isCached;

            APIRoutes.getMetadata(mContext);
            sharedPreferences = new DaveSharedPreference(mContext);
            daveAIPerspective = DaveAIPerspective.getInstance();
            databaseManager = DatabaseManager.getInstance(mContext);
            modelInstance = new Model(mContext);

        }else
            throw new NullPointerException("Context is null");
    }

    /**
     *
     * @return :-   JSONObject(Response) of application perspective Settings
     */
    public JSONObject getAppPerspectiveSettings(){
        if(sharedPreferences.checkStringExist("perspective_settings")) {
            String getPreference = sharedPreferences.readString("perspective_settings");
            return convertStringTOJSONObject(getPreference);
        }
        return null;
    }



    /**
     * method for save Singleton OBject  and update with server..Exception:- don't call for login
     * @param modelName provide model name
     * @param postBody provide post body .
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.
     */
    //add in queue table
    public void postSingleton(final String modelName, JSONObject postBody, DaveAIListener daveAIListener){
        try {
            String objectID = "";
            Model model= new Model(mContext);
            Attribute attribute = model.getModelIdAttributeInstance(modelName);
           // Attribute attribute = getAttrNameOfIDFromModel(modelName);

            if(attribute!=null) {
                if(postBody.has(attribute.getName())){
                    objectID = postBody.getString(attribute.getName());
                }else {
                    //Generate UUID and add uuid in post body
                   // objectID = generateUUID(attribute.getDefaultX(), postBody);
                    objectID = createNewObjectId(modelName, postBody);
                    postBody.put(attribute.getName(),objectID);
                }
            }
            Log.e(TAG," 1.)Singleton post object details Model NAme :-  "+modelName +" 2)uuid:- " +objectID+ " 3)postbody:-" +postBody);
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            if(!databaseManager.isTableExists(SINGLETON_TABLE_NAME)) {
                databaseManager.createTable(SINGLETON_TABLE_NAME, CacheTableType.SINGLETON);
            }

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(MODEL_NAME,modelName);
            Cursor cursor = databaseManager.getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);
            if(cursor.getCount() == 0) {
                //Log.e(TAG," Object  does not exist in Signleton :-  "+cursor.getCount());
                // insert data in Singleton table
                databaseManager.insertOrUpdateSingleton(
                        modelName,
                        new SingletonTableRowModel(modelName,postBody.toString(),System.currentTimeMillis(),"")
                );

                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(postBody);
                }

                // create post Queue table if not exist
                createApiQueueTAble();

                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                //insert data in postQueue table
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.postObjectAPI(modelName), DaveConstants.APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                objectID, PENDING.name(), 1,CacheTableType.SINGLETON.getValue()

                        )
                );

                //Start service to post data
               // startBackgroundService();
                new DaveService().startBackgroundService(mContext);

            }else if(cursor.getCount()> 0) {
                Log.e(TAG," Object  exists in Signleton :-  "+cursor.getCount());
                // insert data in Singleton table
                databaseManager.insertOrUpdateSingleton(
                        modelName,
                        new SingletonTableRowModel(modelName,postBody.toString(),System.currentTimeMillis(),"")
                );
                Log.e(TAG,"object in singleton table = "+databaseManager.getSingletonRow(modelName));
                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(postBody);
                }

                // create post Queue table if not exist
                createApiQueueTAble();

                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                //insert data in postQueue table
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.postObjectAPI(modelName), DaveConstants.APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                objectID, PENDING.name(), 1,CacheTableType.SINGLETON.getValue()

                        )
                );

                //Start service to post data
               // startBackgroundService();
                new DaveService().startBackgroundService(mContext);

//                Toast.makeText(mContext, "Already exist..", Toast.LENGTH_SHORT).show();

            }else {
                Log.e(TAG," else Alternate Check object exist in table :-  "+cursor.getCount());
            }
            cursor.close();
        } catch (Exception e) {

            Log.e(TAG," Error During Post Object :-  "+e.getMessage());
        }
    }

    //add in queue table
    public void updateSingleton(final String modelName,String requestType, JSONObject postBody ,DaveAIListener daveAIListener){
        try {
            String objectID = "";
            Model model= new Model(mContext);
            Attribute attribute = model.getModelIdAttributeInstance(modelName);
            if(attribute!=null) {
                objectID = postBody.getString(attribute.getName());
            }

            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(MODEL_NAME,modelName);
            Cursor cursor = databaseManager.getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);

            if(cursor.getCount() == 1) {
               JSONObject sJsonObject = databaseManager.insertOrUpdateSingleton(
                        modelName,
                        new SingletonTableRowModel(modelName,postBody.toString(),System.currentTimeMillis(),requestType)
               );
               if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(sJsonObject);
               }

                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);
                queueHashMap.put(API_METHOD, APIRequestMethod.GET.name());

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                //insert data in postQueue table
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.updateOrGetObjectAPI(modelName,objectID),APIRequestMethod.UPDATE.name(), postBody.toString(),
                                modelName, requestType, PENDING.name(),
                                APIQueuePriority.ONE.getValue(),CacheTableType.SINGLETON.getValue())
                );
                //start background
                new DaveService().startBackgroundService(mContext);

            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG," Error During Update Singleton :-  "+e.getMessage());
        }

    }


    /**
     * call method to update Singleton object and by default it will store in Database.
     * @param daveAIListener provide  DaveAI callback Listener to get  response.
     * @param modelName provide model name of singleton
     * @param loader if its true show ProgressLoaderDialog during API server call.
     *                Default value is false.
     * @param objectId provide objectId to update data
     *  @param isFreshen by Default  its true.
     *                   if it's true it will make sever call and update data Base
     *                   if it's false just update only local database with new jsonObject.
     *  @param jsonObject if it has already .it updates in previous object.
     */
    public void updateSingleton(final String modelName, JSONObject jsonObject,String objectId, boolean loader,
                               boolean isFreshen, DaveAIListener daveAIListener){

        this.isFreshen = isFreshen;
        if(isFreshen) {

            RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, jsonObject.toString());
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    if (response != null && !response.isEmpty()) {
                        sharedPreferences.writeString("_object_" + modelName + "_singleton", response);
                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                        }
                    }
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                    if (daveAIListener != null) {
                        daveAIListener.onResponseFailure(requestCode, responseMsg);
                    }

                }
            };
            if (CheckNetworkConnection.networkHasConnection(mContext))
                if (objectId != null && !objectId.isEmpty())
                    new APICallAsyncTask(postTaskListener, mContext, "UPDATE", body_part, loader)
                            .execute(APIRoutes.updateOrGetObjectAPI(modelName, objectId));
                else
                    new APICallAsyncTask(postTaskListener, mContext, "POST", body_part, loader)
                            .execute(APIRoutes.postObjectAPI(modelName));

            else
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
        }else {
            if(jsonObject!= null) {
                if (sharedPreferences.checkStringExist("_object_" + modelName + "_singleton")) {
                    try {
                        JSONObject getResult = new JSONObject(sharedPreferences.readString("_object_" + modelName + "_singleton"));
                        Iterator<?> keys = jsonObject.keys();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            getResult.put(key, jsonObject.get(key));

                        }
                        sharedPreferences.writeString("_object_" + modelName + "_singleton", getResult.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else
                    sharedPreferences.writeString("_object_" + modelName + "_singleton", jsonObject.toString());
            }
        }

    }

    /**
     *  call method to get singleton object of model from local database
     * @param modelName provide model name .if its null return null point exception.
     * @return JSONObject of response
     */
    public JSONObject getSingleton(String modelName){
        if(modelName==null || modelName.isEmpty())
            throw  new NullPointerException("Model Name is null .. must be provide model name");
        else {
            return databaseManager.getSingletonRow(modelName);
        }
    }

    public JSONObject getSingleton(final String modelName,String requestType, DaveAIListener daveAIListener, boolean isFresh,
                                   boolean isFreshen, boolean loader) throws DaveException {
        return getSingleton(modelName,null,requestType,daveAIListener,isFresh,isFreshen,loader);
    }


    public JSONObject getSingleton(final String modelName,String objectId,String requestType, DaveAIListener daveAIListener, boolean isFresh,
                                   boolean isFreshen, boolean loader) throws DaveException {

        jsonResponse = new JSONObject();
        if(objectId == null || objectId.isEmpty()) {
            Model model = new Model(mContext);
            String idAttrName = model.getModelIDAttrName(modelName);
            if (idAttrName != null && !idAttrName.isEmpty()) {
                try {
                    objectId = databaseManager.getSingletonRow(modelName).getString(idAttrName);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide Model Name");
        else if(objectId == null || objectId.isEmpty()) {
            throw new DaveException.ModelNotFound("objectId is null or Empty. must be provide objectId");
        }
        else  {
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
            if(cacheModelPOJO!=null && !cacheModelPOJO.modelName.isEmpty()){
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName,CacheTableType.SINGLETON);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;
                        loader = true;
                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
            }
            this.isFresh=isFresh;
            this.isFreshen=isFreshen;
            if(isCached && !this.isFresh){
                jsonResponse = databaseManager.getSingletonRow(modelName);
                if(jsonResponse==null||jsonResponse.length()==0) {
                    this.isFresh = true;

                }else{
                    Log.i(TAG,"Get Singleton of Model from Local Database-------:- "+modelName +" isFresh == "+this.isFresh+ " isFreshen:- "+this.isFreshen+" \n jsonobject "+jsonResponse);
                    if(daveAIListener!=null ) {
                        daveAIListener.onReceivedResponse(jsonResponse);
                    }
                }
            }
            if(daveAIListener== null && jsonResponse ==null) {
                loader = true;
            }
            if(!isCached || this.isFresh || this.isFreshen) {
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            //save in local database
                            if (isCached && (DaveModels.this.isFreshen || DaveModels.this.isFresh)) {
                                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                databaseManager.insertMetaData(
                                        new CacheModelPOJO(modelName,
                                                daveAIPerspective.getHot_update_time_limit(),
                                                daveAIPerspective.getCold_update_time_limit(),
                                                0, modelName,System.currentTimeMillis())
                                );
                                databaseManager.insertOrUpdateSingleton(
                                        modelName,
                                        new SingletonTableRowModel(modelName, response, System.currentTimeMillis(),requestType)
                                );
                            }
                            if (DaveModels.this.isFresh) {
                                 Log.e(TAG, " Get Singleton from server isFresh is true for model " + modelName +" Response:-"+ response);
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (daveAIListener != null) {
                                daveAIListener.onResponseFailure(requestCode, responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext)) {
                        new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                                .execute(APIRoutes.updateOrGetObjectAPI(modelName, encode(objectId)));
                    }else
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext, "Local data not found.Need Internet Connection");

                }else if(this.isFreshen){
                    //insert data in postQueue table
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.updateOrGetObjectAPI(modelName, encode(objectId)), APIRequestMethod.GET.name(),
                                    "", modelName,
                                    requestType, PENDING.name(),
                                    APIQueuePriority.THREE.getValue(), CacheTableType.SINGLETON.getValue()
                            )
                    );
                    // startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }
        return jsonResponse;
    }

    /**
     * method to delete singleton from local database
     * @param modelName provide model name
     */
   /* public void deleteSingleton(String modelName){
        //TODO:- replace share preference with local Db
        if(sharedPreferences.checkStringExist("_object_" + modelName + "_singleton")){
            sharedPreferences.removeKey("_object_" + modelName + "_singleton");
        }
    }*/

    public JSONObject  getModel(String modelName) throws DaveException {
        return getModel(modelName,null,this.isFresh, this.isFresh, false);
    }


    public JSONObject getModel(final String modelName,DaveAIListener daveAIListener) throws DaveException {
        return getModel(modelName,daveAIListener, this.isFresh,this.isFreshen,false);
    }

    public JSONObject getModel(final String modelName, DaveAIListener daveAIListener, boolean isFresh) throws DaveException {
        return getModel(modelName,daveAIListener, isFresh,this.isFreshen,false);
    }

    public JSONObject getModel(final String modelName, DaveAIListener daveAIListener, boolean isFresh, boolean isFreshen) throws DaveException {
        return getModel(modelName,daveAIListener, isFresh,isFreshen,false);
    }

    /**
     *
     * @param modelName  provide model name. if its null throw ModelNot Found  exception
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.if its null it will create own callback.
     * @param isFresh  always return API Server call response.By Default value is false.
     * @param isFreshen  if its true update data in Data base.By Default its true.
     * @param loader  if its true show ProgressLoaderDialog during API server call.
     *                Default value is false.
     * @return  return JSONObject response
     * @throws DaveException  throw ModelNotFound if model name is null or empty.
     */

    public JSONObject getModel(final String modelName, DaveAIListener daveAIListener, boolean isFresh,
                               boolean isFreshen, boolean loader) throws DaveException {

        jsonResponse = new JSONObject();
        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide model name");
        else {
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
            if(cacheModelPOJO!=null && !cacheModelPOJO.modelName.isEmpty()){
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName,CacheTableType.MODEL);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;
                        loader = true;
                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
               // Log.i(TAG," get MOdel Update Status :-"+refreshStatus);
            }
            this.isFresh=isFresh;
            this.isFreshen=isFreshen;
            Log.e(TAG,"getModel>>>>>>>>>>>>>>"+modelName+" isFresh:-"+this.isFresh +" isFreshen:--"+ this.isFreshen);
            if(isCached && !isFresh){
                jsonResponse = databaseManager.getModelData(modelName);
                if(jsonResponse == null||jsonResponse.length()==0) {
                    this.isFresh = true;

                }else{
                    if(daveAIListener!=null ) {
                        daveAIListener.onReceivedResponse(jsonResponse);
                    }
                }

            }
            if(daveAIListener== null && jsonResponse ==null)
                loader = true;

            if(!isCached || isFresh || isFreshen) {
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            if (isCached && DaveModels.this.isFreshen) {
                                //Log.e(TAG, "onModelLoaded 1:-----------------   " + modelName);
                                ModelUtil modelUtil = new ModelUtil(mContext);
                                String modelIdAttr = modelUtil.getIDAttrNameOfModel(response);
                                if(!modelIdAttr.isEmpty()) {
                                    setupModelCacheMetaData(modelName, modelIdAttr, mContext);
                                }
                                databaseManager.insertORUpdateModelData(
                                        modelName,
                                        new ModelTableRowModel(modelName, modelName,response,System.currentTimeMillis())
                                );
                            }
                            if (DaveModels.this.isFresh) {
                                Log.e(TAG," GetModel isFresh is true for model "+ modelName);
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);

                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            Log.e("MODEL_CACHING","cache update failed for getModels("+modelName+")");
                            if(daveAIListener!=null) {
                                daveAIListener.onResponseFailure(requestCode,responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext))
                        new APICallAsyncTask(postTaskListener, mContext, "GET", loader).execute(APIRoutes.modelAPI(modelName));
                    else
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext);

                }else if(this.isFreshen && !this.isFresh){
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.modelAPI(modelName), DaveConstants.APIRequestMethod.GET.name(), "", modelName,
                                   "", PENDING.name(), APIQueuePriority.THREE.getValue(),CacheTableType.MODEL.getValue()
                            )
                    );
                    new DaveService().startBackgroundService(mContext);
                }

            }
        }
        return jsonResponse;
    }



    /**
     *
     * @param modelNames  Post Body with Object{} and pivots{} with counts for number of objects requested per model/pivot
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.if its null it will create own callback.
     * @return  return JSONObject response
     * @throws DaveException  throw ModelNotFound if model name is null or empty.
     */

    public void postBulkRequest(final JSONObject modelNames, DaveAIListener daveAIListener) throws DaveException {

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                if (daveAIListener != null)
                {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if(daveAIListener!=null) {
                    daveAIListener.onResponseFailure(requestCode,responseMsg);
                }

            }
        };
        if (CheckNetworkConnection.networkHasConnection(mContext)) {
            RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, modelNames.toString());
            new APICallAsyncTask(postTaskListener, mContext, "POST", body_part, false).execute(APIRoutes.bulkDownloadAPI());
        }
        else {
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
        }



    }

    private void saveFilterAttrData(String response , String updateTableName, String updateTableRowId){
        if(response!= null) {
            try {
                Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<saveFilterAttrData"+response);
                JSONObject pivotResponse = new JSONObject(response).getJSONObject("data");
                if(updateTableRowId.startsWith("pivot_min_")){
                    String[] parts = updateTableRowId.split("pivot_min_");
                    Log.e(TAG,"Print pivot attr  pivot_min_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                    double d = pivotResponse.getDouble(parts[1]);
                    int price = (int) d;
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(price);
                    checkModelNameOfPivot(parts[1], jsonArray,updateTableName);

                }else if(updateTableRowId.startsWith("pivot_max_")){
                    String[] parts = updateTableRowId.split("pivot_max_");
                    Log.d(TAG,"Print pivot attr  pivot_max_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                    double d = pivotResponse.getDouble(parts[1]);
                    int price = (int) d;
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(price);
                    checkModelNameOfPivot(parts[1], jsonArray,updateTableName);


                }else if(updateTableRowId.startsWith("pivot_")){
                    String[] parts = updateTableRowId.split("pivot_");
                    Log.d(TAG,"Print pivot attr  pivot_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                    checkModelNameOfPivot(parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse),updateTableName);
                }


            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Error in Customer Save Filter Attr+++++++++++ " + e.getMessage());
            }


        }

    }

    private void checkModelNameOfPivot(String key , JSONArray value , String updateTableName){

        String customerModelName = new ModelUtil(mContext).getCustomerModelName();
        String productModelName = new ModelUtil(mContext).getProductModelName();
        if(updateTableName.startsWith(productModelName+"_")){
            saveProductFilterAttr(key, value);

        }else if(updateTableName.startsWith(customerModelName+"_")){
            saveCustomerFilterAttr(key, value);
        }

    }

    public void processBulkData(String bulkData, JSONObject requestedModels, File file){
        try{
            //Setting the state of db in sharedpref
            DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
            sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA,true);

            Log.e(TAG,"response from bulk = "+bulkData);

            JSONObject bulkJSON = new JSONObject(bulkData);
            if(bulkJSON.has("pivots")){

                //todo remove custom filterlist from Singleton table
                //productFilterList, customerFilterList
                databaseManager.deleteSingletonRow("productFilterList");
                databaseManager.deleteSingletonRow("customerFilterList");
                new ConnectDaveAI(mContext).getFilterAttributePivots(daveAIPerspective.getProduct_filter_attr(), databaseManager.getModelNameBasedOnType("product"));
                new ConnectDaveAI(mContext).getFilterAttributePivots(daveAIPerspective.getCustomer_filter_attr(), databaseManager.getModelNameBasedOnType("customer"));

                JSONObject objectsJSON = bulkJSON.getJSONObject("pivots");
                Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<I GOT PIVOTS"+objectsJSON);
                Iterator<String> iterator = objectsJSON.keys();
                while(iterator.hasNext()) {
                    String key = iterator.next();
                    Log.e(TAG,"pivot key:---"+key.split("___").toString() );

                    String tableName = key.split("___")[0];
                    String tableRowIdName = key.split("___")[1];
                    setupModelCacheMetaData(tableName, tableRowIdName, mContext);


                    databaseManager.insertOrUpdateSingleton(
                            tableName,
                            new SingletonTableRowModel(tableName, objectsJSON.getString(key),System.currentTimeMillis(),tableRowIdName,"")
                    );
                    saveFilterAttrData(objectsJSON.getString(key),tableName,tableRowIdName);
                }
            }

            if(bulkJSON.has("objects")){
               JSONObject objectsJSON = bulkJSON.getJSONObject("objects");
               Iterator<String> iterator = objectsJSON.keys();
               Cursor c = databaseManager.openDatabase().rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
               String[] columns = c.getColumnNames();
               /*for(int m = 0 ; m < columns.length ; m ++) {
                   Log.e("LOGGER", "column names = " +columns[m]);
               }*/
               if (c.moveToFirst()) {
                   while ( !c.isAfterLast() ) {
                       if(c.getString(0).startsWith("old___")) {
                           Log.e(TAG, "Table Name=> " + c.getString(0));
                           databaseManager.dropTable(c.getString(0));
//                           databaseManager.openDatabase().execSQL("DROP TABLE "+c.getString(0));
                       }
                       c.moveToNext();
                   }
               }
               c.close();

               ArrayList<String> renamedTables = new ArrayList<>();
               //renaming old tables
               while(iterator.hasNext()) {
                   String key = iterator.next();
                   Log.e(TAG,"objects key:---"+key );
                    if(databaseManager.isTableExists(key)) {
                        databaseManager.renameTable(key, "old___" + key);
                        renamedTables.add("old___"+key);
                    }
                    databaseManager.createTable(key,CacheTableType.OBJECTS);
                    JSONArray objsJar = objectsJSON.getJSONArray(key);

                   CacheModelPOJO modelMetaData = databaseManager.getMetaData(key);
                    for(int j = 0 ; j < objsJar.length() ; j++) {

                        databaseManager.insertObjectsData(key, new ObjectsTableRowModel(
                                objsJar.getJSONObject(j).getString(modelMetaData.model_id_name),
                                objsJar.getJSONObject(j).toString(),
                                System.currentTimeMillis()));

                    }

                    Thread media_caching_thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject checkCacheJSON = new JSONObject();
                                checkCacheJSON.put("data", objsJar);
                                new DaveCachedUtil(mContext).checkForMediaCaching(key, checkCacheJSON.toString());

                                Log.e(TAG, "media queue size = " + databaseManager.checkAPIQueueCount(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    media_caching_thread.start();

                    //convert interaction object in interactions object
                    if(key.equalsIgnoreCase(new ModelUtil(mContext).getInteractionModelName())){
                       Log.e(TAG," if Model NAme is  interaction  save as Interactions object Print updated Table name:-------------"+key);
                       long now = System.currentTimeMillis();
                       setupModelCacheMetaData(INTERACTIONS_TABLE_NAME,modelInstance.getModelIDAttrName(key),mContext);
                       InteractionsHelper interactionsHelper = new InteractionsHelper(mContext);
                       interactionsHelper.addObjectsAsInteractions(objsJar,null);
                        Log.e(TAG+"TIMER~~"," if Model NAme is  interaction  save as Interactions object Print updated Table name:-------------"+(System.currentTimeMillis()-now));

                    }
                   /* Log.e(TAG,"<<<<<<<<<<<<<<bulk download Calling getObjects("+key+")");

                    new DaveModels(mContext,true).getObjects(key, new HashMap<>(), new DaveAIListener() {
                        @Override
                        public void onReceivedResponse(JSONObject response) {

                        }

                        @Override
                        public void onResponseFailure(int responseCode, String responseMsg) {

                        }
                    },false,false);
*/


               }

               /*Cursor c1 = databaseManager.openDatabase().rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

               if (c1.moveToFirst()) {
                   while ( !c1.isAfterLast() ) {
                       Log.e(TAG, "Table Name=> "+c1.getString(0));
                       c1.moveToNext();
                   }
               }
               c1.close();*/
               databaseManager.dropTables(renamedTables);

               startBackgroundMediaCacheService();
               sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA,false);
               file.delete();
/*

              Log.e("TESTING MODEL UTILS","getProductModelName() == "+ new ModelUtil(mContext).getProductModelName());
              Log.e("TESTING MODEL UTILS","getProductIdAttrName() == "+ new ModelUtil(mContext).getProductIdAttrName());
              Log.e("TESTING MODEL UTILS","getProductCategoryHierarchy() == "+ new ModelUtil(mContext).getProductCategoryHierarchy().toString());
              Log.e("TESTING MODEL UTILS","getProductFilterAttrList() == "+ new ModelUtil(mContext).getProductFilterAttrList());
              Log.e("TESTING MODEL UTILS","getCustomerModelName() == "+ new ModelUtil(mContext).getCustomerModelName());
              Log.e("TESTING MODEL UTILS","getCustomerIdAttrName() == "+ new ModelUtil(mContext).getCustomerIdAttrName());
              Log.e("TESTING MODEL UTILS","getCustomerCategoryHierarchy() == "+ new ModelUtil(mContext).getCustomerCategoryHierarchy());
              Log.e("TESTING MODEL UTILS","getCustomerFilterAttrList() == "+ new ModelUtil(mContext).getCustomerFilterAttrList());
              Log.e("TESTING MODEL UTILS","getInteractionModelName() == "+ new ModelUtil(mContext).getInteractionModelName());
              Log.e("TESTING MODEL UTILS","getInteractionIdAttrName() == "+ new ModelUtil(mContext).getInteractionIdAttrName());
              Log.e("TESTING MODEL UTILS","getInteractionStageAttrName() == "+ new ModelUtil(mContext).getInteractionStageAttrName());*/

            }

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        }
    }


    private void checkModelNameOfPivot(String key , JSONArray value ,String updateTableName ,String updateTableRowId){

        String customerModelName = new ModelUtil(mContext).getCustomerModelName();
        String productModelName = new ModelUtil(mContext).getProductModelName();
        if(updateTableName.startsWith(productModelName+"_")){
            saveProductFilterAttr(key, value);

        }else if(updateTableName.startsWith(customerModelName+"_")){
            saveCustomerFilterAttr(key, value);
        }

    }

    public void saveProductFilterAttr(String key , JSONArray value ){
        try {

            ConnectDaveAI connectDaveAI = new ConnectDaveAI(mContext);
            connectDaveAI.setupModelCacheMetaData("productFilterList", "productFilterList", mContext);
            JSONObject productFil = databaseManager.getSingletonRow("productFilterList");
            Log.i(TAG," saveProductFilterAttr before update saveProductFilterAttr:------------"+value);
            if(productFil != null && productFil.length() >0 ){
                if(productFil.has(key)) {
                    JSONArray priceRange = productFil.getJSONArray(key);
                    for(int i = 0; i<value.length();i++)
                        priceRange.put(value.get(i));
                    productFil.put(key, priceRange);
                }else {
                    productFil.put(key,value);
                }

            }else{
                productFil = new JSONObject();
                productFil.put(key,value);
            }
            Log.i(TAG,"saveProductFilterAttr After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    "productFilterList",
                    new SingletonTableRowModel("productFilterList",productFil.toString(),
                            System.currentTimeMillis(),"productPivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void saveCustomerFilterAttr(String key , JSONArray value ){
        try {

            ConnectDaveAI connectDaveAI = new ConnectDaveAI(mContext);
            connectDaveAI.setupModelCacheMetaData("customerFilterList", "customerFilterList", mContext);
            JSONObject productFil = databaseManager.getSingletonRow("customerFilterList");
            Log.i(TAG," saveProductFilterAttr before update sproductFil:------------"+productFil);
            if(productFil != null && productFil.length() >0 ){
                if(productFil.has(key)) {
                    JSONArray priceRange = productFil.getJSONArray(key);
                    for(int i = 0; i<value.length();i++)
                        priceRange.put(value.get(i));
                    productFil.put(key, priceRange);
                }else {
                    productFil.put(key,value);
                }

            }else{
                productFil = new JSONObject();
                productFil.put(key,value);
            }
            Log.i(TAG," saveProductFilterAttr After update sproductFil:------------"+productFil.length() + productFil);
            databaseManager.insertOrUpdateSingleton(
                    "customerFilterList",
                    new SingletonTableRowModel("customerFilterList",productFil.toString(),
                            System.currentTimeMillis(),"customerPivot")
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void getBulkData(String id,JSONObject requestedModels, DaveAIListener daveAIListener, long maxWaitTime, long bulkStartTime){
        Timer timer = new Timer();
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                timer.cancel();
                if (daveAIListener != null)
                {
                    //timer.cancel();
                    try {
                        JSONObject responseJSON = new JSONObject(response);
                        if (responseJSON.has("error")) {
                            daveAIListener.onResponseFailure(-1, responseJSON.getString("error"));
                        }else {
                            try {
                                File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/");
                                if (!folder.exists())
                                    folder.mkdirs();
                                String finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/"
                                        + "bulkData.txt";
                                File file = new File(finalFileName);
                                FileOutputStream os = new FileOutputStream(file);
                                String data = response;
                                os.write(data.getBytes());
                                os.close();
                                long now = System.currentTimeMillis();
                                Handler handler = new Handler(){
                                    @Override
                                    public void handleMessage(Message msg) {
                                        super.handleMessage(msg);

                                        daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                                    }
                                };
                                Thread t = new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();

                                        processBulkData(response, requestedModels, file);
                                        handler.sendEmptyMessage(0);

                                    }
                                };
                                t.start();
                            } catch (Exception e) {
                                e.printStackTrace();
                                daveAIListener.onResponseFailure(-1, "error copying new data");

                            }
                        }
                    }catch (Exception e){
                        daveAIListener.onResponseFailure(-1, "Error processing bulk data download. Please try again.");
                        e.printStackTrace();
                        Log.e(TAG,"Exception during bulk download:------- "+ e.getMessage());
                    }
                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
               //IGNORE ERROR and Wait for successful response
                if(requestCode!=404) {
                    daveAIListener.onResponseFailure(requestCode, responseMsg);
                }
            }
        };
        Handler mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if (CheckNetworkConnection.networkHasConnection(mContext)) {
                    new APICallAsyncTask(postTaskListener, mContext, "GET", false)
                            .execute(APIRoutes.getBulkDownloadAPI(id));
                }
                else {
                    CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
                }
            }
        };
        Handler mHandler2 = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if(daveAIListener!=null){
                    daveAIListener.onResponseFailure(408,"Response Time exceeded maxWaitTime of "+maxWaitTime);
                }
            }
        };
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if((System.currentTimeMillis()-bulkStartTime) < maxWaitTime) {
                    mHandler.sendMessage(new Message());
                }else{
                    timer.cancel();
                    mHandler2.sendMessage(new Message());
                }
            }
        }, 0, 10*1000);

    }


    /**
     * method to delete model from local database
     * @param modelName provide model name
     */
    public void deleteModel(String modelName){
        if(sharedPreferences.checkStringExist("_model_" + modelName)){
            sharedPreferences.removeKey("_model_" + modelName);
        }
    }

    public JSONObject  getObject(final String modelName,String objectId) throws DaveException {
        return getObject(modelName,objectId,null, this.isFresh,this.isFreshen,false);
    }

    public JSONObject getObject(final String modelName,String objectId, DaveAIListener daveAIListener) throws DaveException {

        return getObject(modelName,objectId,daveAIListener, this.isFresh,this.isFreshen,false);
    }

    public JSONObject getObject(final String modelName, String objectId, DaveAIListener daveAIListener,
                                 boolean isFresh) throws DaveException {
        return getObject(modelName,objectId,daveAIListener, isFresh,this.isFreshen,false);
    }

    public JSONObject getObject(final String modelName, String objectId, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen) throws DaveException {
        return getObject(modelName,objectId,daveAIListener, isFresh,isFreshen,false);
    }

    /**
     *
     * @param modelName provide model name. if its null throw ModelNot Found  exception
     * @param objectId  provide ObjectId. if its null throw ObjectId Found  exception
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.if its null it will creatye own callback.
     * @param isFresh  always return API Server call response.By Default value is false.
     * @param isFreshen  if its true update data in Data base.By Default its true.
     * @param loader  if its true show ProgressLoaderDialog during API server call.
     *                Default value is false.
     * @return  return JSONObject response
     * @throws DaveException throw ModelNotFound if model name is null or empty.
     */
    public JSONObject getObject(final String modelName,String objectId, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen, boolean loader) throws DaveException {

        DaveCachedUtil daveCachedUtil = new DaveCachedUtil(mContext);
        jsonResponse = new JSONObject();
       // Log.d(TAG,"getObject 1 :- "+modelName + " objectId"+objectId);
        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide Model Name");
        else if(objectId==null || objectId.isEmpty())
            throw new DaveException.ObjectIdNotFound("ObjectId is null or Empty. must be provide ObjectId");
        else {
            if(!databaseManager.isTableExists(modelName)) {
                databaseManager.createTable(modelName, DatabaseConstants.CacheTableType.OBJECTS);
            }
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
            if(cacheModelPOJO!=null && !cacheModelPOJO.modelName.isEmpty()){
                HashMap<String, Object> queryParams = new HashMap<>();
                queryParams.put(OBJECT_ID, objectId);
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName,CacheTableType.OBJECTS,(HashMap<String, Object>) queryParams.clone(),new HashMap<String, Object>(),0,0);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;

                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;

                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
                Log.i(TAG,"<<<<<<<<<<<<< Check get object()  Update Status>>>>>>>>>>>>>>>>>>>>>>>>>"+refreshStatus);
            }
            this.isFresh=isFresh;
            this.isFreshen=isFreshen;
            Log.e(TAG," get object() Update Status isFresh :-"+this.isFresh+" isfreshen:-"+this.isFreshen +" iscached:-"+isCached+" model = "+modelName+" object id = "+objectId);
            if(isCached && !this.isFresh){
                jsonResponse = databaseManager.getObjectData(modelName,objectId);
                Log.e(TAG,"Get Object of Model from Local database-------:- "+modelName +" isFresh == "+this.isFresh+ " isFreshen:- "+this.isFreshen+" jsonobject "+jsonResponse);
                if(jsonResponse == null||jsonResponse.length()==0) {
                    this.isFresh = true;

                }else{
                    if(daveAIListener!=null ) {
                        daveAIListener.onReceivedResponse(jsonResponse);
                    }
                }
            }
            if(daveAIListener== null && jsonResponse ==null) {
                loader = true;

            }
            if(!isCached || this.isFresh || this.isFreshen) {

                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            //Log.e(TAG, "flow = getObject().onTaskCOmpleted for " + modelName + "/" + objectId);
                            if (isCached && (DaveModels.this.isFreshen || DaveModels.this.isFresh)) {
                                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                databaseManager.insertOrUpdateObjectData(
                                        modelName,
                                        new ObjectsTableRowModel(objectId, response, System.currentTimeMillis())
                                );
                                JSONArray cachedAttrs = new JSONArray();
                                List<String> modelMediaCache = new ArrayList<>();
                                CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                                Model mod = new Model(mContext);
                                try {
                                    JSONObject modelJsn = databaseManager.getModelData(modelName);
                                    JSONObject responseJSON = new JSONObject(response);
                                    Gson gson = new Gson();
                                    mod = gson.fromJson(modelJsn.toString(), Model.class);
                                    modelMediaCache = mod.getQuick_views().getCached();
                                    if(modelMediaCache.size()>0) {
                                        for (int x = 0; x < modelMediaCache.size(); x++) {
                                            daveCachedUtil.addToMediaQueue(modelMediaCache.get(x), mod, responseJSON, modelName, modelMetaData);
                                        }
                                    }
                                    Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
                                    if (cur.getCount() > 0) {
                                        startBackgroundMediaCacheService();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                            if (DaveModels.this.isFresh) {
                               Log.e(TAG, " GetObject From server isFresh is true for model " + modelName+" Response:- "+response);
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (daveAIListener != null) {
                                daveAIListener.onResponseFailure(requestCode, responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext))
                        new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                                //.execute(APIRoutes.updateOrGetObjectAPI(modelName, objectId));
                                .execute(APIRoutes.updateOrGetObjectAPI(modelName, encode(objectId)));
                    else
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext, "Local data not found.Need Internet Connection");

                }else if(this.isFreshen && !this.isFresh){
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.updateOrGetObjectAPI(modelName,encode(objectId)), DaveConstants.APIRequestMethod.GET.name(), "", modelName,
                                    objectId, PENDING.name(), APIQueuePriority.TWO.getValue(),CacheTableType.OBJECTS.getValue()
                            )
                    );
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }

        Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
        if (cur.getCount() > 0) {
            startBackgroundMediaCacheService();
        }
        return jsonResponse;
    }

   /*
    public void deleteObject(String modelName,String objectId){
        if(sharedPreferences.checkStringExist("_object_" + modelName + "_" + objectId)){
            sharedPreferences.removeKey("_object_" + modelName + "_" + objectId);
        }
    }*/


    public JSONObject  getObjects(String modelName) throws DaveException {
        return getObjects(modelName,new HashMap<>(),null,false, false, false);
    }

    public JSONObject  getObjects(final String modelName,HashMap<String,Object> queryParams) throws DaveException {
        return getObjects(modelName,queryParams,null, this.isFresh,this.isFreshen,false);
    }

    public JSONObject getObjects(final String modelName,HashMap<String,Object> queryParams,DaveAIListener daveAIListener) throws DaveException {

        return getObjects(modelName,queryParams,daveAIListener, this.isFresh,this.isFreshen,false);
    }

    public JSONObject getObjects(final String modelName, HashMap<String,Object> queryParams, DaveAIListener daveAIListener,
                                 boolean isFresh) throws DaveException {
        return getObjects(modelName,queryParams,daveAIListener, isFresh,this.isFreshen,false);
    }

    public JSONObject getObjects(final String modelName, HashMap<String,Object> queryParams, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen) throws DaveException {
        return getObjects(modelName,queryParams,daveAIListener, isFresh,isFreshen,false);
    }

    public JSONObject setReferedAttributes(JSONObject object, String modelName){
        Model model = ModelUtil.getModelInstance(mContext,modelName);
        HashMap<String,String> parentIdMap = new HashMap<>();
        HashMap<String,JSONObject> parentObjectMap = new HashMap<>();
        List<String> parents = model.getParents();
        try {
            for (int i = 0; i < parents.size(); i++) {
                if (databaseManager.getMetaData(parents.get(i)) != null) {
                    Log.e(TAG, "checking for model = " + parents.get(i) + "  attr = " + databaseManager.getMetaData(parents.get(i)).model_id_name + "  result = " + object.has(databaseManager.getMetaData(parents.get(i)).model_id_name));
                    if (object.has(databaseManager.getMetaData(parents.get(i)).model_id_name)) {
                        Log.e(TAG, "parent of " + modelName + " has parent " + "with id attr = " + databaseManager.getMetaData(parents.get(i)).model_id_name);
                        parentIdMap.put(parents.get(i), databaseManager.getMetaData(parents.get(i)).model_id_name);
                        try {
                            parentObjectMap.put(parents.get(i), databaseManager.getObjectData(parents.get(i), object.getString(databaseManager.getMetaData(parents.get(i)).model_id_name)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
//                Log.e(TAG,"model is null for = "+parents.get(i));
                }
            }

            Log.e(TAG, "parentidmap ==== " + parentObjectMap.toString());

            for (int j = 0; j < model.getAttributes().size(); j++) {
                Attribute attr = model.getAttributes().get(j);
//            Log.e(TAG,"REFERS for attr "+attr.getName()+"   =   "+attr.getRefers());
                if (attr.getRefers() != null && !TextUtils.isEmpty(attr.getRefers())) {
                    Log.e(TAG, "obj.has attr = " + !object.has(attr.getName()) + "  parentIdMap.contains key = " + parentIdMap.containsKey(attr.getRefers().split("\\.")[1]));
                    if (!object.has(attr.getName()) && parentIdMap.containsKey(attr.getRefers().split("\\.")[1]) && attr.getRefers().split("\\.").length == 3) {
                        Log.e(TAG, "will try to get attr from refers table = " + attr.getRefers().split("\\.")[1] + "  " + attr.getName());
                        String modName = attr.getRefers().split("\\.")[1];
                        String attrName = attr.getRefers().split("\\.")[2];
                        try {
                            String value = parentObjectMap.get(modName).getString(attrName);
                            Log.e(TAG, "found val for " + attr.getName() + " from model = " + modName + "  == " + value);
                            object.put(attr.getName(), value);
                        } catch (Exception e) {
                            Log.d(TAG,e.getMessage());
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return object;

    }


    private JSONObject mergeTwoJsonObjs(JSONObject jobj1, JSONObject jobj2){
        if(jobj1!=null&&jobj2!=null){
            Iterator<String> iter = jobj2.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    if(!jobj1.has(key)){
                        jobj1.put(key,jobj2.get(key));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return jobj1;
    }


    /**
     *
     * @param modelName  provide model name. if its null throw ModelNot Found  exception
     * @param queryParams  additional QueryParam as HashMap  based on that its filter response data
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.if its null it will create own callback.
     * @param isFresh  always return API Server call response.By Default value is false.
     * @param isFreshen if its true update data in Data base.By Default its true.
     * @param loader   if its true show ProgressLoaderDialog during API server call.
     *                Default value is false.
     * @return  return JSONObject response
     * @throws DaveException  throw ModelNotFound if model name is null or empty.
     */
    public JSONObject getObjects(final String modelName,HashMap<String,Object> queryParams, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen, boolean loader) throws DaveException {

        DaveCachedUtil daveCachedUtil = new DaveCachedUtil(mContext);
        jsonResponse = new JSONObject();
        if(queryParams!= null)
        for (String Getkey : queryParams.keySet()) {
            if(queryParams.get(Getkey)== null)
                queryParams.remove(Getkey);
        }
        this.isFresh=isFresh;
        this.isFreshen=isFreshen;
        HashMap<String,Object> cachedQueryParams = new HashMap<String,Object>(queryParams);
        HashMap<String,Object> regularQueryParams = new HashMap<String,Object>();

        HashMap<String,Object> cleanCachedQueryParams = getCachedQueryParams(cachedQueryParams);
        String keyword = getKeyword(queryParams);
        if(!TextUtils.isEmpty(keyword)){
            regularQueryParams.put(DATA_CACHED+TILDE,keyword);
        }

        Log.e(TAG,"Print cleanCachedQueryParams:-- "+ cleanCachedQueryParams);


//        Log.e(TAG,"Get Objects of Model from Local database-------:- "+modelName +" isFresh == "+this.isFresh+ " isFreshen:- "+this.isFreshen+" jsonobject "+jsonResponse);


        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must provide model name");
        else  {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            if(!databaseManager.isTableExists(modelName)) {
                databaseManager.createTable(modelName, DatabaseConstants.CacheTableType.OBJECTS);
            }
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
            if(!databaseManager.isTableExists(modelName)){
                databaseManager.createTable(modelName, CacheTableType.OBJECTS);

            }
            if(cacheModelPOJO!=null && cacheModelPOJO.modelName != null && !cacheModelPOJO.modelName.isEmpty()){
                CacheModelHelper.CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName,CacheTableType.OBJECTS,(HashMap<String,Object>)regularQueryParams.clone(),cleanCachedQueryParams,getLimit(queryParams),getPageNumber(queryParams));
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;
                        //loader = true;
                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
               Log.e(TAG, "Get Objects update Status>>>>> " +refreshStatus );

            }

            this.isFresh = this.isFresh || isFresh;
            this.isFreshen = this.isFreshen || isFreshen;


            if(isCached&&!isFresh){
                try{
                    Log.e(TAG," 2 regularQueryParams = "+regularQueryParams.toString());

                    jsonResponse = DatabaseManager.getInstance(mContext).getAllObjectsData(modelName,regularQueryParams,cleanCachedQueryParams,getLimit(queryParams),getPageNumber(queryParams));

                    if(jsonResponse.getJSONArray("data").length()==0){
                        Log.e(TAG,"getObjects for "+modelName+" local data length 0");
                        this.isFresh = true;
                    }else{
                        if(daveAIListener!=null&&!this.isFresh) {
                            Log.e(TAG,"getObjects for "+modelName+" sending cached data through callback");
                            daveAIListener.onReceivedResponse(daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonResponse));
                        }
                    }
                    if(jsonResponse.getJSONArray("data").length()< getLimit(queryParams) && (getCachedQueryParams(queryParams).size()>0||queryParams.containsKey("_keyword")||queryParams.containsKey("_last_updated")||queryParams.containsKey("_sort_by")||queryParams.containsKey("_sort_reverse"))){
                        this.isFreshen=true;
                    }
                    //when network is not present, not applicable for isfresh = false
                    /*if (!CheckNetworkConnection.networkHasConnection(mContext)||jsonResponse.getJSONArray("data").length()!=0) {
                        if(daveAIListener!=null) {
                            daveAIListener.onReceivedResponse(replaceWithLocalFileCachePath(modelName,jsonResponse));

                        }
                    }*/
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG," Error getObjects From Local Db:-"+this.isFresh);
                }


            }
            if(daveAIListener== null && jsonResponse == null) {
                loader = true;
            }

            if(!isCached || this.isFresh || this.isFreshen) {
               final HashMap<String,Object> freshQueryParams = new HashMap<String,Object>(queryParams);

                if(queryParams == null){
                    queryParams = new HashMap<String,Object>();
                }

                if(!queryParams.containsKey("_page_size")){
                    queryParams.put("_page_size",DaveHelper.getInstance().getObjectPageSize());
                }
               // long lastUpdated = 0;
                //queryParams.put("_last_updated",(databaseManager.getLastupdated(modelName,CacheTableType.OBJECTS)/1000));
                Log.e(TAG,"getObjects  After Reutn response update>>>>>Model Name:-"+modelName+" QueryParam"+queryParams+" isFresh:-"+this.isFresh+" isFreshen:-"+this.isFreshen);
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            try{
                                jsonResponse = convertStringTOJSONObject(response);

                                if (isCached && (DaveModels.this.isFresh || DaveModels.this.isFreshen)) {

                                    JSONArray cachedAttrs = new JSONArray();
                                    List<String> modelMediaCache = new ArrayList<>();
                                    Model mod = new Model(mContext);
                                    try {
                                        JSONObject modelJsn = databaseManager.getModelData(modelName);
                                        Gson gson = new Gson();
                                        mod = gson.fromJson(modelJsn.toString(),Model.class);
                                        try {
                                            cachedAttrs = modelJsn.getJSONObject("quick_views").getJSONArray("cached");
                                            modelMediaCache = mod.getQuick_views().getCached();
                                        }catch (Exception e){
                                            e.printStackTrace();
                                            cachedAttrs = new JSONArray();
                                            modelMediaCache = new ArrayList<>();
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                    JSONObject respJson = new JSONObject(response);
                                    if (respJson.has("data")) {
                                        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                        JSONArray dataJar = respJson.getJSONArray("data");
                                        Log.i("getObjects", " Total COunt of Objects:-" + dataJar.length());
                                        CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                                        for (int i = 0; i < dataJar.length(); i++) {


                                            HashMap<String,Object> params = new HashMap<>();
                                            params.put(OBJECT_ID,dataJar.getJSONObject(i).getString(modelMetaData.model_id_name));

                                            Cursor c = databaseManager.getDataFromTable(modelName,params,new HashMap<>(),"",0,"");
                                            JSONObject finalDataCached = dataJar.getJSONObject(i);
                                            if(c.getCount() == 1){
                                                if(c.getColumnIndex(ERROR_MESSAGE)!=-1){
                                                    c.moveToFirst();
                                                    if(!TextUtils.isEmpty(c.getString(c.getColumnIndex(ERROR_MESSAGE)))){
                                                       // JSONObject jobj1 = new JSONObject(databaseManager.getObjectData(modelName, dataJar.getJSONObject(i).getString(modelMetaData.model_id_name)).getString(DATA_CACHED));
                                                        JSONObject jobj1 = databaseManager.getObjectData(modelName, dataJar.getJSONObject(i).getString(modelMetaData.model_id_name));

                                                        finalDataCached = mergeTwoJsonObjs(jobj1,dataJar.getJSONObject(i));

                                                        DatabaseManager.getInstance(mContext).insertOrUpdateObjectData(
                                                                modelName,
                                                                new ObjectsTableRowModel(
                                                                        dataJar.getJSONObject(i).getString(modelMetaData.model_id_name),
                                                                        finalDataCached.toString(),
                                                                        System.currentTimeMillis()
                                                                ));


                                                    }else {
                                                        DatabaseManager.getInstance(mContext).insertOrUpdateObjectData(
                                                                modelName,
                                                                new ObjectsTableRowModel(
                                                                        dataJar.getJSONObject(i).getString(modelMetaData.model_id_name),
                                                                        dataJar.getJSONObject(i).toString(),
                                                                        System.currentTimeMillis()
                                                                ));
                                                    }
                                                }
                                            }else{
                                                DatabaseManager.getInstance(mContext).insertOrUpdateObjectData(
                                                        modelName,
                                                        new ObjectsTableRowModel(
                                                                dataJar.getJSONObject(i).getString(modelMetaData.model_id_name),
                                                                dataJar.getJSONObject(i).toString(),
                                                                System.currentTimeMillis()
                                                        ));

                                            }

                                            long now = System.currentTimeMillis();
                                            Log.e(TAG+"TIMER!!","addtomediaqueue started");

                                            for (int x = 0; x < modelMediaCache.size(); x++) {
                                                daveCachedUtil.addToMediaQueue(modelMediaCache.get(x), mod, finalDataCached, modelName, modelMetaData);
                                                Log.e(TAG+"TIMER!!","times");
                                            }
                                            Log.e(TAG+"TIMER!!","addtomediaqueue ended"+(System.currentTimeMillis()-now));

                                        }
                                        Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
                                        if(cur.getCount()>0){
                                            startBackgroundMediaCacheService();
                                        }
                                        if (DaveModels.this.isFresh) {
                                            Log.e(TAG, " GetObjects From server isFresh is true for model " + modelName+" Response:- "+response);
                                            if (daveAIListener != null)
                                                daveAIListener.onReceivedResponse(daveCachedUtil.replaceWithLocalFileCachePath(modelName,convertStringTOJSONObject(response)));
                                        }
                                    }
                                }

                            }catch (Exception e){
                                e.printStackTrace();
                            }


                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            Log.e(TAG, "cache update failed for getObjects(" + modelName + ")");

                            if (daveAIListener != null) {
                                daveAIListener.onReceivedResponse(daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonResponse));
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext)) {
                        /* new APICallAsyncTask(postTaskListener, mContext, "GET", loader,"Syncing with the server, \nThis may take some time.")
                                .execute(APIRoutes.getObjectsAPI(modelName, queryParams));*/
                        new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                                .execute(APIRoutes.getObjectsAPI(modelName, queryParams));
                    }else {
                       // CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
                        //when network is not present, not applicable for isfresh = false
                        try {
                                if(daveAIListener!=null) {
                                    daveAIListener.onReceivedResponse(daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonResponse));
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else if(this.isFreshen){
                    String status = PENDING.name();
                    if(cleanCachedQueryParams.size() == 0){
                        //dividing by 1000 as python timestamp in server is in Seconds
                        queryParams.put("_last_updated",(databaseManager.getModelMasterTimestamp(modelName)/1000));
                        status = APIQueueStatus.LAST_UPDATE.name();

                    }
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.getObjectsAPI(modelName,queryParams), DaveConstants.APIRequestMethod.GET.name(), "", modelName,
                                    "", status, APIQueuePriority.THREE.getValue(),CacheTableType.OBJECTS.getValue()

                            )
                    );
                    //startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }
        Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
        if(cur.getCount()>0){
            startBackgroundMediaCacheService();
        }
        return daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonResponse);
    }

    //query param of objects without any action
    private HashMap<String,Object> getCachedQueryParams(HashMap<String,Object> queryParams){
        HashMap<String,Object> retParams = new HashMap<>();

        Iterator iterator = queryParams.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();

            Log.e(TAG,"getCachedQueryParams key:---"+key );
            if(!key.startsWith("_")){
                retParams.put(key,entry.getValue());
            }
            /*if(key.startsWith("_")) {
                retParams.remove(key);

            }*/
            /*else if(entry.getValue().toString().startsWith("~")){
                retParams.put(key,entry.getValue().toString().replaceFirst("~",""));
            }*/
        }

        return retParams;
    }


    public int checkPaging(String response){
        try{
            JSONObject jsonObject = new JSONObject(response);
            int page_size = 0;
            int total_number = 1000;
            int page_number = 0;
            boolean is_last = false;

            if(jsonObject.has("page_size")){
                page_size = jsonObject.getInt("page_size");
            }else{
                return -1;
            }
           /* if(jsonObject.has("total_number")){
                total_number = jsonObject.getInt("total_number");
            }else{
                return -1;
            }*/
            if(jsonObject.has("page_number")){
                page_number = jsonObject.getInt("page_number");
            }else{
                return -1;
            }

            if(jsonObject.has("is_last")){
                is_last = jsonObject.getBoolean("is_last");
            }
            Log.e(TAG,"checkPaging:-----------"+page_number +" isLast:--------"+is_last);
            int objectsTotalRead = page_number * page_size;
            if(objectsTotalRead < total_number && !is_last){
                page_number++;
                return page_number;

            }else{
                return -1;
            }
        }catch (Exception e){
            e.printStackTrace();
        }



        return -1;
    }

    private String getKeyword(HashMap<String,Object> queryParams){
        Iterator iterator = queryParams.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            if(key.equals("_keyword")){
                return entry.getValue().toString();
            }

        }
        return "";
    }
    private long getLimit(HashMap<String,Object> queryParams){
        try {
            Iterator iterator = queryParams.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
                String key = entry.getKey();
                if (key.equals("_page_size")) {
                    return ((Number) entry.getValue()).longValue();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }
    private long getPageNumber(HashMap<String,Object> queryParams){
        Iterator iterator = queryParams.entrySet().iterator();
        while(iterator.hasNext()) {
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>) iterator.next();
            String key = entry.getKey();
            if(key.equals("_page_number")){
                return  ((Number) entry.getValue()).longValue();
            }

        }
        return 0;
    }




    public JSONObject replaceServerPathWithLocalFilePath(String modelName, JSONObject objectDetails){

        try {
            Model modelInstance = ModelUtil.getModelInstance(mContext,modelName);
            List<String>  mediaCachedList = modelInstance.getQuick_views().getCached();

            //Log.e(TAG,"replaceServerPathWithLocalFilePath -- mediaCachedList "+mediaCachedList+" for model = "+modelName);

            if(mediaCachedList!= null && mediaCachedList.size()>0)
            for(int x = 0 ; x < mediaCachedList.size(); x++ )
            {
                String attName = mediaCachedList.get(x);
               // Log.e(TAG,"replaceServerPathWithLocalFilePath -- forLoop  "+x+" attrName = "+attName);
                if(objectDetails.has(attName) && !objectDetails.isNull(attName))
                {
                    DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                    CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                    if (modelInstance.getAttributeType(attName).equals("image") || modelInstance.getAttributeType(attName).equals("video") || modelInstance.getAttributeType(attName).equals("file") || modelInstance.getAttributeType(attName).equals("apk") || modelInstance.getAttributeType(attName).equals("url"))
                    {
                        HashMap<String, Object> param = new HashMap<>();
                        param.put(URL, objectDetails.getString(attName));
                        Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                        if (imagedata.getCount() > 0) {
                            imagedata.moveToFirst();
                            String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                            objectDetails.put(attName, localPath);

                            //Log.d(TAG,"replaceServerPathWithLocalFilePath -- attrName  "+attName+"  " +objectDetails);

                        } else if (objectDetails.getString(attName).contains("http")) {
                            new DaveCachedUtil(mContext).addToMediaQueue(attName, modelInstance, objectDetails, modelName, modelMetaData);
                        }


                    } else if (modelInstance.getAttributeType(mediaCachedList.get(x)).equals("list")) {

                        JSONArray listUrls = objectDetails.getJSONArray(attName);
                        for (int a = 0; a < listUrls.length(); a++) {
                            HashMap<String, Object> param = new HashMap<>();
                            param.put(URL, listUrls.getString(a));
                            Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                            if (imagedata.getCount() > 0) {
                                imagedata.moveToFirst();
                                String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                listUrls.put(a, localPath);

                            } else if (objectDetails.getString(mediaCachedList.get(x)).contains("http")) {
                                new DaveCachedUtil(mContext).addToMediaQueue(attName, modelInstance, objectDetails, modelName, modelMetaData);
                            }
                        }
                        objectDetails.put(attName, listUrls);

                    } else if (modelInstance.getAttributeType(mediaCachedList.get(x)).equals("media_map") || modelInstance.getAttributeType(mediaCachedList.get(x)).equals("nested_object")) {
                        //todo implement for media_map 0r nested_object
                        JSONObject json = new JSONObject();
                        json = objectDetails.getJSONObject(mediaCachedList.get(x));
                        Iterator<String> iter = json.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();

                        }
                    }
                }
            }
            return objectDetails;

        } catch (Exception e) {
            e.printStackTrace();
        }


        return objectDetails;

    }







    /**
     * method to delete objects of any model from local database
     * @param modelName  provide model name
     */
    public void deleteObjects(String modelName){
        if(sharedPreferences.checkStringExist("_objects_" + modelName)){
            sharedPreferences.removeKey("_objects_" + modelName);
        }
    }

    public JSONObject getInteractions(final String customerId,String stageName,DaveAIListener daveAIListener, boolean isFresh, boolean isFreshen, boolean loader) throws DaveException {

        jsonResponse = new JSONObject();
        if(customerId == null || customerId.isEmpty())
            throw new DaveException.ObjectIdNotFound("CustomerID is null or Empty. must be provide CustomerID");
        else {
            try {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

                String cOrderId = currentOrderId.get(customerId);
                //String cOrderId = interactionAttr.getString(customerId);
                HashMap<String, Object> localQueryParams = new HashMap<>();
                localQueryParams.put("customer_id", customerId);
                localQueryParams.put("interaction_stage", stageName);

                if (cOrderId != null && !cOrderId.isEmpty()) {
                    localQueryParams.put("interaction_group", cOrderId);
                }else {
                    cOrderId = DaveAIHelper.preOrderId.get(customerId);
                    localQueryParams.put("interaction_group", cOrderId);
                }

                CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(INTERACTIONS_TABLE_NAME);
                if (!cacheModelPOJO.modelName.isEmpty()) {
                    CacheModelHelper.CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(INTERACTIONS_TABLE_NAME, CacheTableType.INTERACTIONS,new HashMap<>(),localQueryParams,0,0);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFreshen = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            //isFresh = true;
                            isFreshen = true;
                            break;
                        case NOT_REQUIRED:
                            isFreshen = false;
                            break;
                    }
                    Log.e(TAG, "<<<<<<<<<<<<<<<<<<<<Get Interactions  Check Update Status>>>>> " +refreshStatus);
                }
                this.isFresh = isFresh;
                this.isFreshen = isFreshen;

                //get from local interactions table
                if (isCached) {

                    jsonResponse = databaseManager.getInteractionsData(stageName, localQueryParams);
                    Log.e(TAG, "Get Interactions From local db>>>>> QueryParam" + localQueryParams + " isFresh:-" + this.isFresh + " isFreshen:-" + this.isFreshen +
                            "\n jsonResponse:- " + jsonResponse);
                    if (jsonResponse != null && jsonResponse.has(stageName) && !jsonResponse.isNull(stageName) && jsonResponse.getJSONArray(stageName).length() > 0) {
                       // this.isFresh = false;
                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(jsonResponse);
                        }
                    } else {
                        this.isFresh = true;
                    }
                }
                if (daveAIListener == null && jsonResponse.getJSONArray(stageName).length() == 0)
                    loader = true;

                Log.e(TAG,"getInteractions  isFresh:-"+this.isFresh+" isFreshen:-"+this.isFreshen+ " loader:---- "+loader);
                if (!isCached || this.isFresh || this.isFreshen) {
                    ModelUtil modelUtil = new ModelUtil(mContext);
                    HashMap<String, Object> queryParams = new HashMap<>();
                    queryParams.put(modelUtil.getCustomerIdAttrName(), customerId);
                    queryParams.put(modelUtil.getInteractionStageAttrName(), stageName);

                    if (cOrderId != null && !cOrderId.isEmpty()) {
                        queryParams.put(daveAIPerspective.getInvoice_id_attr_name(), cOrderId);
                    }
                    if (this.isFresh) {
                        APIResponse postTaskListener = new APIResponse() {
                            @Override
                            public void onTaskCompleted(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.has("data") && !jsonObject.isNull("data") && jsonObject.getJSONArray("data").length()>0) {
                                        JSONArray dataJar = jsonObject.getJSONArray("data");

                                        for (int i = 0; i < dataJar.length(); i++) {
                                            databaseManager.insertOrUpdateObjectData(
                                                    modelUtil.getInteractionModelName(),
                                                    new ObjectsTableRowModel(
                                                            dataJar.getJSONObject(i).getString(cacheModelPOJO.model_id_name),
                                                            dataJar.getJSONObject(i).toString(),
                                                             System.currentTimeMillis()
                                                    ));
                                        }

                                        InteractionsHelper interactionsHelper = new InteractionsHelper(mContext);
                                        interactionsHelper.addObjectsAsInteractions(response, new InteractionsHelper.OnInteractionsAdded(){
                                            @Override
                                            public void onInteractionAdded() {

                                                Log.e(TAG,"Print getInteractions queryParam"+ queryParams +" localQueryParams:"+ localQueryParams);
                                                jsonResponse = databaseManager.getInteractionsData(stageName, localQueryParams);
                                                if (daveAIListener != null)
                                                    daveAIListener.onReceivedResponse(jsonResponse);


                                            }

                                            @Override
                                            public void onInteractionAddedFailed() {
                                                /*if (daveAIListener != null)
                                                    daveAIListener.onReceivedResponse(jsonObject);
*/
                                            }
                                        });
                                    }else {
                                        if (daveAIListener != null)
                                            daveAIListener.onReceivedResponse(jsonResponse);

                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onTaskFailure(int requestCode, String responseMsg) {
                                if (daveAIListener != null) {
                                    daveAIListener.onResponseFailure(requestCode, responseMsg);
                                }

                            }
                        };
                        if (CheckNetworkConnection.networkHasConnection(mContext)) {
                            new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                                    .execute(APIRoutes.getObjectsAPI(modelUtil.getInteractionModelName(),queryParams));

                        } else {
                            CheckNetworkConnection.showNetDisabledAlertToUser(mContext,"no Local Data found.need Internet Connection.");
                            if (daveAIListener != null) {
                                daveAIListener.onReceivedResponse(jsonResponse);
                            }
                        }

                    }else if (this.isFreshen) {

                        databaseManager.insertAPIQueueData(
                                new APIQueueTableRowModel(
                                        APIRoutes.getObjectsAPI(modelUtil.getInteractionModelName(),queryParams),
                                        DaveConstants.APIRequestMethod.GET.name(),
                                        "", modelUtil.getInteractionModelName(),
                                        "", PENDING.name(),
                                        APIQueuePriority.TWO.getValue(),
                                        CacheTableType.OBJECTS.getValue()
                                )
                        );
                        //startBackgroundService();
                        new DaveService().startBackgroundService(mContext);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonResponse;
    }




    /*public JSONObject getInteractions(final String customerId,String stageName, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen, boolean loader) throws DaveException {

        jsonResponse = new JSONObject();
        if(customerId == null || customerId.isEmpty())
            throw new DaveException.ObjectIdNotFound("CustomerID is null or Empty. must be provide CustomerID");
        else  {
            try {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(INTERACTIONS_TABLE_NAME);
                CacheModelHelper.CacheRefreshStatus refreshStatus = CacheModelHelper.CacheRefreshStatus.NOT_REQUIRED;
                if (!cacheModelPOJO.modelName.isEmpty()) {
                    refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(INTERACTIONS_TABLE_NAME, CacheTableType.INTERACTIONS);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFreshen = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            isFresh = true;
                            isFreshen = true;
                            loader = true;
                            break;
                        case NOT_REQUIRED:
                            isFreshen = false;
                            break;
                    }
                    Log.e(TAG, "Get Interactions update Status>>>>> " +refreshStatus );
                }
                this.isFresh = isFresh;
                this.isFreshen = isFreshen;

                HashMap<String, Object> queryParams = new HashMap<>();
                queryParams.put("customer_id", customerId);
                queryParams.put("interaction_stage", stageName);
                String cOrderId = currentOrderId.get(customerId);
                if (cOrderId != null && !cOrderId.isEmpty()) {
                    queryParams.put("interaction_group", cOrderId);
                }
                if (isCached) {
                    jsonResponse = DatabaseManager.getInstance(mContext).getInteractionsData(stageName, queryParams);
                    Log.e(TAG, "Get Interactions From local db>>>>> QueryParam" + queryParams + " isFresh:-" + this.isFresh + " isFreshen:-" + this.isFreshen+
                            "\n jsonResponse:- "+jsonResponse);
                    if (jsonResponse == null && jsonResponse.has(stageName) && !jsonResponse.isNull(stageName) && jsonResponse.getString(stageName).length()>0) {
                        this.isFresh = true;
                    } else {
                        this.isFreshen = true;
                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(jsonResponse);
                        }
                    }
                }
                if (daveAIListener == null && jsonResponse == null)
                    loader = true;
                // Log.e(TAG,"getObjects  After Reutn response update>>>>>Model Name:-"+modelName+" QueryParam"+queryParams+" isFresh:-"+this.isFresh+" isFreshen:-"+this.isFreshen);
            }catch (Exception e){
                e.printStackTrace();
            }
            if(!isCached || this.isFresh || this.isFreshen) {
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            try {
                            if (isCached && DaveModels.this.isFreshen) {
                                InteractionsHelper interactionsHelper = new InteractionsHelper(mContext);
                                interactionsHelper.addObjectsInInteractionsTable(response);
                            }
                            if (DaveModels.this.isFresh && jsonResponse.getString(stageName).length() == 0) {
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (daveAIListener != null) {
                                daveAIListener.onResponseFailure(requestCode, responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext)) {
                        new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                                .execute(APIRoutes.interactionsAPI(mContext,customerId, stageName));
                    } else {
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
                    }
                }else if(this.isFreshen){
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.interactionsAPI(mContext,customerId, stageName),
                                    DaveConstants.APIRequestMethod.GET.name(),
                                    "", INTERACTIONS_TABLE_NAME,
                                    "", DatabaseConstants.APIQueueStatus.PENDING.name(),
                                    1,
                                    CacheTableType.INTERACTIONS.getValue()
                            )
                    );
                    //startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }
        return jsonResponse;
    }*/

    /**
     *
     * method to get Recommendations offline
     * get product details from product table
     * get interaction objects from interaction table (find based on customerId and ProductId (list)
     * if productId doesn't exist in interaction objects set next stage is first positive stage and previous stage is first negative stage...
     * create recommendation json body.
     *
     * @param customerId provide customerId to get recommendation
     * @param productId provide product to get Similar product
     * @param queryParams queryParam to filter objects
     * @param daveAIListener provide listener to get response
     * @throws DaveException throw error
     */
    public void getRecommendationAndSimilar(String customerId, String productId,String orderId, HashMap<String,Object> queryParams, DaveAIListener daveAIListener) throws DaveException{
        try{
            DaveCachedUtil daveCachedUtil = new DaveCachedUtil(mContext);
            ModelUtil modelUtil = new ModelUtil(mContext);
            HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
            if(defaultParams!=null && !defaultParams.isEmpty()){
                for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                    //Log.e(TAG,"Print query param attr "+ entry.getKey() +" Value:- "+ entry.getValue() +" ValueType:-"+entry.getValue().getClass() );
                    queryParams.put(entry.getKey(), entry.getValue());
                }
            }
            Log.e(TAG,"getRecommendationAndSimilar offline CustomerId:------"+customerId +" productId:- "+productId+" queryParams:-"+queryParams );
            //regularQuryPAram with search keyword
            HashMap<String,Object> regularQueryParams = new HashMap<String,Object>();
            String keyword = getKeyword(queryParams);
            if(!TextUtils.isEmpty(keyword)){
                regularQueryParams.put(DATA_CACHED+TILDE,keyword);
            }

            JSONObject objData =  DatabaseManager.getInstance(mContext).getAllObjectsData(
                    new ModelUtil(mContext).getProductModelName(),
                    regularQueryParams,getCachedQueryParams(queryParams),getLimit(queryParams),getPageNumber(queryParams));

            Log.e(TAG,"Print productResponse Objects>>>>>>>>>>>>>>> " +objData);

            //media caching of product List
            JSONObject productResponse = daveCachedUtil.replaceWithLocalFileCachePath(modelUtil.getProductModelName(),objData);
            JSONArray productsList = productResponse.getJSONArray("data");

            if(!productResponse.isNull("data") && productsList.length()>0)
            {
                JSONArray interactionData = new JSONArray();
                ArrayList<String> productIdList = new ArrayList<>();

                if(customerId!=null && !customerId.isEmpty()) {

                    for (int i = 0; i < productsList.length(); i++) {
                        productIdList.add(productsList.getJSONObject(i).getString(modelUtil.getProductIdAttrName()));
                    }

                    HashMap<String, Object> interactionQueryParam = new HashMap<>();
                    interactionQueryParam.put(modelUtil.getCustomerIdAttrName(), customerId);
                    interactionQueryParam.put(modelUtil.getProductIdAttrName(), productIdList);
                    if(orderId!=null && !orderId.isEmpty())
                        interactionQueryParam.put(daveAIPerspective.getInvoice_id_attr_name(), orderId);

                    JSONObject interactionResponse = DatabaseManager.getInstance(mContext).getAllObjectsData(
                            modelUtil.getInteractionModelName(),
                            new HashMap<>(), getCachedQueryParams(interactionQueryParam), getLimit(queryParams), getPageNumber(queryParams));

                    interactionData = interactionResponse.getJSONArray("data");
                }
                //Log.e(TAG,"Print Interaction Objects>>>>>>>>>>>>>>> " +interactionData.length());

                RecommendationHelper recommendationUtil = new RecommendationHelper(mContext,customerId,productId);
                recommendationUtil.getRecommendationsObjects(productsList, interactionData, new RecommendationHelper.OnRecommendationsHelperListener()
                {
                    @Override
                    public void onSuccess(JSONObject recommendationsObjects) {
                        if(daveAIListener!=null)
                            daveAIListener.onReceivedResponse(recommendationsObjects);
                    }

                    @Override
                    public void onFailed() {

                    }
                });

            }else {
                Log.e(TAG,"No product Found.........................");
                if(daveAIListener!=null)
                    daveAIListener.onReceivedResponse(new JSONObject());
            }


        }catch (Exception e){
            e.printStackTrace();
        }



    }


    public JSONObject getRecommendations(final String categoryId, HashMap<String,String> queryParams, DaveAIListener daveAIListener,
                                 boolean isFresh, boolean isFreshen, boolean loader) throws DaveException {

        this.isFresh=isFresh;
        this.isFreshen=isFreshen;
        DaveCachedUtil daveCachedUtil = new DaveCachedUtil(mContext);
        Log.e("LOGGER","GET RECOMMENDATION CALLED FOR "+categoryId);
        if(categoryId == null || categoryId.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide model name");
        else  {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(DatabaseManager.getInstance(mContext).generateTableName(categoryId, DatabaseConstants.CacheTableType.RECOMMENDATION));
            CacheModelHelper.CacheRefreshStatus refreshStatus =  CacheModelHelper.CacheRefreshStatus.NOT_REQUIRED;
            if(!cacheModelPOJO.modelName.isEmpty()){
                Log.e("LOGGER","cachemodelpojo model name = "+cacheModelPOJO.modelName);
                refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(categoryId,CacheTableType.RECOMMENDATION);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        Log.e("MODEL_CACHE_STATUS","COLD_UPDATE_REQUIRED");
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        Log.e("MODEL_CACHE_STATUS","HOT_UPDATE_REQUIRED");
                        isFresh = true;
                        isFreshen = true;
                        loader = true;
                        break;
                    case NOT_REQUIRED:
                        Log.e("MODEL_CACHE_STATUS","NOT_REQUIRED");
                        break;
                }
            }

            this.isFresh=this.isFresh||isFresh;
            this.isFreshen=this.isFreshen||isFreshen;

            Log.e("LOGGER","RECOMMENDATTION isfresh = "+this.isFresh+"  isfreshen = "+this.isFreshen);
            if(isCached && !isFresh){
                jsonResponse = DatabaseManager.getInstance(mContext).getRecommendationData(categoryId);
                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(jsonResponse);
                }
            }
            if(daveAIListener== null && jsonResponse ==null)
                loader = true;
            if(!isCached || isFresh || isFreshen) {
                Log.e("MODEL_CACHING","isFreshen is true for getObjects("+categoryId+")");

                APIResponse postTaskListener = new APIResponse() {
                    @Override
                    public void onTaskCompleted(String response) {
                        if (isCached && DaveModels.this.isFreshen) {

                            try {

                                Log.e("LOGGER","recommendation response ----------------- "+response);
                                JSONObject respJson = new JSONObject(response);
                                if(respJson.has("recommendations")){
                                    JSONArray dataJar = respJson.getJSONArray("recommendations");
                                    CacheModelPOJO modelMetaData = DatabaseManager.getInstance(mContext).getMetaData(DatabaseManager.getInstance(mContext).generateTableName(categoryId, DatabaseConstants.CacheTableType.RECOMMENDATION));
                                    for(int i = 0 ; i < dataJar.length() ; i++) {
                                        Log.e("LOGGER", "recommendation row - " + dataJar.getJSONObject(i).toString());
                                        DatabaseManager.getInstance(mContext).insertRecommendationsData(
                                                categoryId,
                                                new RecommendationsTableRowModel(
                                                        dataJar.getJSONObject(i).getString(modelMetaData.model_id_name),
                                                        dataJar.getJSONObject(i).toString(),
                                                        System.currentTimeMillis()
                                                )
                                        );

                                        JSONArray cachedAttrs = new JSONArray();
                                        List<String> modelMediaCache = new ArrayList<>();

                                        String modelName = databaseManager.generateTableName(categoryId,DatabaseConstants.CacheTableType.RECOMMENDATION);
                                        Model mod = new Model(mContext);
                                        try {
                                            JSONObject modelJsn = databaseManager.getModelData(new ModelUtil(mContext).getProductModelName());
                                            JSONObject responseJSON = new JSONObject(response);
                                            Gson gson = new Gson();
                                            mod = gson.fromJson(modelJsn.toString(), Model.class);
                                            cachedAttrs = modelJsn.getJSONObject("quick_views").getJSONArray("cached");
                                            modelMediaCache = mod.getQuick_views().getCached();
                                            Log.e("LOGGER", "ADDING TO MEDIA QUEUE");
                                            for (int x = 0; x < modelMediaCache.size(); x++) {
                                                daveCachedUtil.addToMediaQueue(modelMediaCache.get(x),mod,dataJar.getJSONObject(i),modelName,modelMetaData);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }


                            if (DaveModels.this.isFresh) {
                                Log.e("MODEL_CACHING","isFresh is true for getObjects("+"reco_"+categoryId+")");
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);
                        }else {
                            if(daveAIListener!=null)
                                daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            jsonResponse = convertStringTOJSONObject(response);
                        }
                    }

                    @Override
                    public void onTaskFailure(int requestCode, String responseMsg) {
                        Log.e("MODEL_CACHING","cache update failed for getObjects("+"reco_"+categoryId+")");

                        if(daveAIListener!=null) {
                            daveAIListener.onResponseFailure(requestCode,responseMsg);
                        }

                    }
                };
                if (CheckNetworkConnection.networkHasConnection(mContext)) {

                    RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, "");
                    new APICallAsyncTask(postTaskListener, mContext, "GET", body_part, false).
                            execute(new String[]{addParamsToURL(APIRoutes.recommendationsAPI("base_customer"),
                                    daveAIPerspective.getNo_of_recommendations(), categoryId)});


                            /* mContext, "GET", loader)
                            .execute(APIRoutes.getObjectsAPI(modelName, queryParams));*/
                }
                else
                    CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }
        }
        return jsonResponse;
    }

    String categoryId = "";
    public void cacheRecommendations(DaveAIListener daveAIListener, String categoryId, String userId, int cacheCount){
        try {
//                Model model = new Model(getApplicationContext());
            this.categoryId = categoryId;
            RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, "");
            if(CheckNetworkConnection.networkHasConnection(mContext)) {
                (new APICallAsyncTask(new APIResponse() {
                    @Override
                    public void onTaskCompleted(String s) {
                        try {
                            sharedPreferences.writeString("reco_"+categoryId,s);

                            daveAIListener.onReceivedResponse(new JSONObject(s));
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onTaskFailure(int i, String s) {
                        daveAIListener.onResponseFailure(i,s);
                    }
                }, mContext, "GET", body_part, false)).execute(new String[]{addParamsToURL(APIRoutes.recommendationsAPI("base_customer"), cacheCount, categoryId)});
            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private String addParamsToURL(String url, int limitNumber,String category) {

        HttpUrl.Builder urlBuilder = HttpUrl.parse(url).newBuilder();

        if (limitNumber != 0)
            urlBuilder.addQueryParameter("_limit", String.valueOf(limitNumber));
        urlBuilder.addQueryParameter("_sort_by", "trending");
        urlBuilder.addQueryParameter("_fresh", "true");



        HashMap<String,Object> defaultParams=  daveAIPerspective.getDefault_recommendation_params();
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        if(category!=null && !category.isEmpty()) {
            urlBuilder.addQueryParameter("category_id", category);
        }

        return urlBuilder.build().toString();

    }

    /**
     *
     * @param modelName provide model name
     * @param requestType provide attr name
     * @param queryParams queryParam to filter based on it.
     * @param daveAIListener listener to get response
     * @param isFresh make server call
     * @param isFreshen get data from localDb.
     * @return pivot result
     * @throws DaveException
     */
    public JSONObject getPivot(final String modelName,String requestType,HashMap<String,String> queryParams, DaveAIListener daveAIListener,
                               boolean isFresh, boolean isFreshen) throws DaveException {

        jsonResponse = new JSONObject();
        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide Model Name");
        else  if(requestType == null || requestType.isEmpty())
            throw new DaveException.ModelNotFound("requestType is null or Empty. must be provide requestType");

        else  {
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName+"_"+requestType);
            if(cacheModelPOJO!=null && !cacheModelPOJO.modelName.isEmpty()){
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName,CacheTableType.SINGLETON);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;
                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
            }
            this.isFresh=isFresh;
            this.isFreshen=isFreshen;
            if(isCached && !this.isFresh){
                jsonResponse = databaseManager.getSingletonRow(modelName+"_"+requestType);
                if(jsonResponse==null||jsonResponse.length()==0) {
                    this.isFresh = true;

                }else{
                    Log.i(TAG,"Get Pivot of Model from Local Database-------:- "+modelName+"_"+requestType +" isFresh == "+this.isFresh+ " isFreshen:- "+this.isFreshen+" \n jsonobject "+jsonResponse);
                    if(daveAIListener!=null ) {
                        daveAIListener.onReceivedResponse(jsonResponse);
                    }
                }
            }
            if(!isCached || this.isFresh || this.isFreshen) {
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            //save in local database
                            if (isCached && (DaveModels.this.isFreshen || DaveModels.this.isFresh)) {
                                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                //add meta data
                                databaseManager.insertMetaData(
                                        new CacheModelPOJO(modelName+"_"+requestType,
                                                daveAIPerspective.getHot_update_time_limit(),
                                                daveAIPerspective.getCold_update_time_limit(),
                                                0, "pivot_"+requestType,System.currentTimeMillis())
                                );
                                //add date as singleton
                                databaseManager.insertOrUpdateSingleton(
                                        modelName+"_"+requestType,
                                        new SingletonTableRowModel(modelName+"_"+requestType, response,
                                                System.currentTimeMillis(),"pivot_"+requestType)
                                );
                            }
                            if (DaveModels.this.isFresh) {
                                Log.e(TAG, " Get Pivot from server isFresh is true for model " + modelName +" Response:-"+ response);
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (daveAIListener != null) {
                                daveAIListener.onResponseFailure(requestCode, responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext)) {
                        new APICallAsyncTask(postTaskListener, mContext, "GET")
                                .execute(APIRoutes.getPivotAPI(modelName, queryParams));
                    }else
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext, "Local data not found.Need Internet Connection");

                }else if(this.isFreshen){
                    //insert data in postQueue table
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.getPivotAPI(modelName, queryParams), APIRequestMethod.GET.name(),
                                    "", modelName+"_"+requestType,
                                    "pivot_"+requestType, PENDING.name(),
                                    APIQueuePriority.THREE.getValue(), CacheTableType.SINGLETON.getValue()
                            )
                    );
                    // startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }
        return jsonResponse;
    }

    private JSONObject convertStringTOJSONObject(String passString) {
        try {
            return new JSONObject(passString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
      return null;
    }

    public void postObject(final String modelName, JSONObject postBody, DaveAIListener daveAIListener){
        postObject(modelName, postBody, daveAIListener,false);
    }
    /**
     * method for save  object in local database and sync with server:-
     * Step :- create UUID from ObjectDetails if postBody doesn't have id
     * Step :- save details in specify  database table ... if table  not exist create table and save it
     * Step :-  delete everything from the post queue table and then Add in post Queue table
     * Step :- Start service to sync with remote server
     * @param modelName model name
     * @param postBody  details want to post
     */
    public void postObject(final String modelName, JSONObject postBody, DaveAIListener daveAIListener,boolean attemptHotPost){
        Log.e(TAG,"asdf post called");

        try {
            Log.e(TAG," <<<<<<postObject>>>>>> before create iD 1.)post object details Model NAme :-  "+modelName + " 2)postbody:-" +postBody);
            String objectID = "";
            Model model= new Model(mContext);
            Attribute attribute = model.getModelIdAttributeInstance(modelName);
            Log.e(TAG,"<<<<<<<<<<<<<<<<<PostObject Attribute:---------"+attribute);
            if(attribute!=null) {
                if(postBody.has(attribute.getName())){
                    objectID = postBody.getString(attribute.getName());
                }else {
                    objectID = createNewObjectId(modelName, postBody);
                    postBody.put(attribute.getName(),objectID);

                }
            }
            Log.e(TAG,"<<<<<<<<<<postObject  After check ObjectID 1.) Model NAme>>>>>>>>> :-" +modelName +" 2)uuid:-" +objectID+ "\n 3)postbody:-" +postBody);
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            if(!databaseManager.isTableExists(modelName)) {
                databaseManager.createTable(modelName, DatabaseConstants.CacheTableType.OBJECTS);
            }

            Cursor cursor = isObjectInObjectsTable(modelName,objectID);
            if(cursor.getCount() == 0) {
                Log.e(TAG," Object  does not exist in table :-  "+ modelName +" ObjectCount:-"+ cursor.getCount());

                // create post Queue table if not exist
                createApiQueueTAble();

                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                if(attemptHotPost) {

                    Model postModel = new Model(mContext);
                    String finalObjectID = objectID;
                    postModel.postObject(modelName, postBody, false, new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            try {

                                Log.e(TAG,"Live call 1");
                                postBody.put(ERROR_MSG_ATTRIBUTE, ERROR_MESSAGE_TEXT);
                                // insert data in object table
                                databaseManager.insertOrUpdateObjectData(
                                        modelName,
                                        new ObjectsTableRowModel(finalObjectID, setReferedAttributes(postBody, modelName).toString(), System.currentTimeMillis(), DatabaseConstants.ERROR_MESSAGE_TEXT)
                                );

                                if (modelName.equals(new ModelUtil(mContext).getInteractionModelName())) {
                                    InteractionsHelper interactionPostHelper = new InteractionsHelper(mContext);
                                    interactionPostHelper.addObjectInInteractionsTable(finalObjectID, postBody);
                                }
                                postDataHandlingForPostObjects(response,modelName,APIQueuePriority.ONE.getValue(),"");

                                if (daveAIListener != null) {
                                    daveAIListener.onReceivedResponse(postBody);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onTaskFailure(int responseCode, String errorMsg) {
                            try{
                            databaseManager.insertAPIQueueData(
                                    new APIQueueTableRowModel(
                                            APIRoutes.postObjectAPI(modelName), APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                            finalObjectID, PENDING.name(), APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                                    )
                            );

                            postBody.put(ERROR_MSG_ATTRIBUTE, ERROR_MESSAGE_TEXT);
                            // insert data in object table
                            databaseManager.insertOrUpdateObjectData(
                                    modelName,
                                    new ObjectsTableRowModel(finalObjectID, setReferedAttributes(postBody, modelName).toString(), System.currentTimeMillis(), DatabaseConstants.ERROR_MESSAGE_TEXT)
                            );

                            if (modelName.equals(new ModelUtil(mContext).getInteractionModelName())) {
                                InteractionsHelper interactionPostHelper = new InteractionsHelper(mContext);
                                interactionPostHelper.addObjectInInteractionsTable(finalObjectID, postBody);
                            }

                            if (daveAIListener != null) {
                                daveAIListener.onReceivedResponse(postBody);
                            }

                            //Start service to post data
                            new DaveService().startBackgroundService(mContext);

                            }catch (Exception e){
                                e.printStackTrace();
                                Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }else {
                    //insert data in postQueue table
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.postObjectAPI(modelName), APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                    objectID, PENDING.name(), APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                            )
                    );

                    postBody.put(ERROR_MSG_ATTRIBUTE, ERROR_MESSAGE_TEXT);
                    // insert data in object table
                    databaseManager.insertOrUpdateObjectData(
                            modelName,
                            new ObjectsTableRowModel(objectID, setReferedAttributes(postBody, modelName).toString(), System.currentTimeMillis(), DatabaseConstants.ERROR_MESSAGE_TEXT)
                    );

                    if (modelName.equals(new ModelUtil(mContext).getInteractionModelName())) {
                        InteractionsHelper interactionPostHelper = new InteractionsHelper(mContext);
                        interactionPostHelper.addObjectInInteractionsTable(objectID, postBody);
                    }

                    if (daveAIListener != null) {
                        daveAIListener.onReceivedResponse(postBody);
                    }

                    //Start service to post data
                    new DaveService().startBackgroundService(mContext);
                }
            }else if(cursor.getCount()> 0) {
                Log.e(TAG," Object   exist in table :-  "+ modelName +" ObjectCount:-"+ cursor.getCount());
                if(modelName.equals(new ModelUtil(mContext).getInteractionModelName())){

                   JSONObject updatedObj =  databaseManager.updateDataCached(modelName,objectID,postBody,DatabaseConstants.ERROR_MESSAGE_TEXT);

                    InteractionsHelper interactionPostHelper = new InteractionsHelper(mContext);
                    interactionPostHelper.addObjectInInteractionsTable(objectID,updatedObj);


                    // create post Queue table if not exist
                    createApiQueueTAble();

                    HashMap<String, Object> queueHashMap = new HashMap<>();
                    queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                    queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);

                    deleteObjectExistInQueueTable(modelName,queueHashMap);

                    if(attemptHotPost){
                        Model postModel = new Model(mContext);
                        String finalObjectID = objectID;
                        postModel.postObject(modelName, postBody, false, new APIResponse() {
                            @Override
                            public void onTaskCompleted(String response) {
                                Log.e(TAG,"Live call 2");

                                postDataHandlingForPostObjects(response,modelName,APIQueuePriority.ONE.getValue(),"");
                                if (daveAIListener != null) {
                                    daveAIListener.onReceivedResponse(postBody);
                                }
                            }

                            @Override
                            public void onTaskFailure(int responseCode, String errorMsg) {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.postObjectAPI(modelName), DaveConstants.APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                                finalObjectID, PENDING.name(), APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                                        )
                                );

                                if (daveAIListener != null) {
                                    daveAIListener.onReceivedResponse(postBody);
                                }
                                //Start service to post data
                                //startBackgroundService();
                                new DaveService().startBackgroundService(mContext);
                            }
                        });
                    }else {
                        //insert data in postQueue table
                        databaseManager.insertAPIQueueData(
                                new APIQueueTableRowModel(
                                        APIRoutes.postObjectAPI(modelName), DaveConstants.APIRequestMethod.POST.name(), postBody.toString(), modelName,
                                        objectID, PENDING.name(), APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                                )
                        );

                        if (daveAIListener != null) {
                            daveAIListener.onReceivedResponse(postBody);
                        }
                        //Start service to post data
                        //startBackgroundService();
                        new DaveService().startBackgroundService(mContext);
                    }

                }else {
                    //Toast.makeText(mContext, "Already exist..", Toast.LENGTH_SHORT).show();
                    //todo patch object
                    Log.e(TAG," Object exist in table do patch :-  "+ modelName +" ObjectCount:-"+ cursor.getCount() +" "+ postBody);
                    patchObject(modelName,objectID,postBody,daveAIListener);
                }
                cursor.close();
            }/*else {
                Log.e(TAG," else Alternate Check object exist in table :-  "+cursor.getCount());
            }*/

        } catch (Exception e) {
            Log.e(TAG," Error During Post Object :-  "+e.getMessage());
        }

    }



    private void postDataHandlingForPostObjects(String response,String updateTableName,int priority, String errorMsg) {
        try {
            ConnectDaveAI connectDaveAI = new ConnectDaveAI(mContext);

            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("data")) {
                Log.i(TAG, "****update OBJECTS table***" + updateTableName + " && Object Details:-" + response);
                if (updateTableName.equalsIgnoreCase(INTERACTION_STAGES_TABLE_NAME)) {
                    connectDaveAI.saveInteractionStagesDetails(response);

                } else {
                    // do pagination of objects only during login
                    //objectsPagination(response);
                    if (priority == APIQueuePriority.LOGIN.getValue()) {
                        int intNextPage = checkPaging(response);
                        Log.i(TAG, "<<<<<<<<<Print check Paging FOr Get Objects:------------------" + updateTableName + " PageNumber>>> " + intNextPage);
                        if (intNextPage != -1) {
                            HashMap<String, Object> pagingParam = new HashMap<String, Object>();
                            pagingParam.put("_page_number", intNextPage);
                            pagingParam.put("_page_size", DaveHelper.getInstance().getObjectPageSize());
                            databaseManager.insertAPIQueueData(
                                    new APIQueueTableRowModel(
                                            APIRoutes.getObjectsAPI(updateTableName, pagingParam), DaveConstants.APIRequestMethod.GET.name(),
                                            "", updateTableName, "", PENDING.name(),
                                            APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue()
                                    )
                            );

                        }
                    }

                    CacheModelPOJO modelMetaData = databaseManager.getMetaData(updateTableName);
                    modelMetaData.master_timestamp = System.currentTimeMillis();
                    databaseManager.insertMetaData(modelMetaData);
                    List<JSONObject> getList = ObjectData.getJSONObjectDataList(response);
                    if (getList != null) {
                        for (int i = 0; i < getList.size(); i++) {

                            HashMap<String, Object> params = new HashMap<>();
                            params.put(OBJECT_ID, getList.get(i).getString(modelMetaData.model_id_name));

                            Cursor c = databaseManager.getDataFromTable(updateTableName, params, new HashMap<>(), "", 0, "");
                            if (c.getCount() == 1) {
                                if (c.getColumnIndex(ERROR_MESSAGE) != -1) {
                                    c.moveToFirst();
                                    if (!TextUtils.isEmpty(c.getString(c.getColumnIndex(ERROR_MESSAGE)))) {

                                        JSONObject jobj1 = databaseManager.getObjectData(updateTableName, getList.get(i).getString(modelMetaData.model_id_name));

                                        JSONObject finalDataCached = mergeTwoJsonObjs(jobj1, getList.get(i));

                                        databaseManager.insertOrUpdateObjectData(
                                                updateTableName,
                                                new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                        finalDataCached.toString(),
                                                        System.currentTimeMillis(), errorMsg)

                                        );
                                    } else {
                                        databaseManager.insertOrUpdateObjectData(
                                                updateTableName,
                                                new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                        getList.get(i).toString(),
                                                        System.currentTimeMillis(), errorMsg)

                                        );
                                    }
                                }
                            } else {

                                databaseManager.insertOrUpdateObjectData(
                                        updateTableName,
                                        new ObjectsTableRowModel(getList.get(i).getString(modelMetaData.model_id_name),
                                                getList.get(i).toString(),
                                                System.currentTimeMillis(), errorMsg)

                                );
                            }

                        }
                    }
                    ModelUtil modelUtil = new ModelUtil(mContext);
                    if (updateTableName.equalsIgnoreCase(modelUtil.getInteractionModelName())) {
                        Log.e(TAG, " Get interaction model NAme:--------Print updated Table name:-------------" + updateTableName);
                        connectDaveAI.setupModelCacheMetaData(INTERACTIONS_TABLE_NAME, modelInstance.getModelIDAttrName(updateTableName), mContext);
                        InteractionsHelper interactionsHelper = new InteractionsHelper(mContext);
                        interactionsHelper.addObjectsAsInteractions(jsonObject, null);

                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void patchObject(final String modelName, JSONObject updateBody ,DaveAIListener daveAIListener){

        patchObject(false,modelName, updateBody, daveAIListener);
    }

    /**
     * method for update  object in local database and sync with server:-
     * Step :- update  object details in specify model database
     * Step :-  delete all gets request from the queue and add patch in queue table
     * Step :- Start service to sync with remote server
     * @param modelName model name
     * @param updateBody  details want to post
     */
    public void patchObject(boolean attemptHotUPdate, final String modelName, JSONObject updateBody ,DaveAIListener daveAIListener){
        try {
            String objectID = "";
            Model model= new Model(mContext);
            Attribute attribute = model.getModelIdAttributeInstance(modelName);
            if(attribute!=null) {
                objectID = updateBody.getString(attribute.getName());
            }
            patchObject(modelName,objectID,updateBody,daveAIListener,attemptHotUPdate);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG," Error During Patch Object :-  "+e.getMessage());
        }

    }

    public void patchObject(String modelName,String objectID, JSONObject updateBody ,DaveAIListener daveAIListener){

        patchObject(modelName,objectID, updateBody ,daveAIListener,false);
    }
    /**
     * method for update  object in local database and sync with server:-
     * Step :- update  object details in specify model database
     * Step :-  delete all gets request from the queue and add patch in queue table
     * Step :- Start service to sync with remote server
     * @param modelName model name
     * @param updateBody  details want to post
     */
    public void patchObject(final String modelName,String objectID, JSONObject updateBody ,DaveAIListener daveAIListener,boolean attemptHotUpdate){
        Log.e(TAG,"asdf patch called");
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            Cursor cursor = isObjectInObjectsTable(modelName,objectID);
            if(cursor.getCount() == 1) {
                JSONObject updatedDetails =  databaseManager.updateDataCached(modelName,objectID,updateBody,DatabaseConstants.ERROR_MESSAGE_TEXT);

                Log.e(TAG, "***Patch object update details *************" + updatedDetails);
                if(modelName.equals(new ModelUtil(mContext).getInteractionModelName())){
                    InteractionsHelper interactionPostHelper = new InteractionsHelper(mContext);
                    interactionPostHelper.addObjectInInteractionsTable(objectID,updatedDetails);
                }

                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);
                queueHashMap.put(API_METHOD, APIRequestMethod.GET.name());

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                HashMap<String, Object> searchQuery = new HashMap<>();
                searchQuery.put(UPDATE_TABLE_NAME,modelName);
                searchQuery.put(UPDATE_TABLE_ROW_ID,objectID);
                searchQuery.put(API_METHOD, APIRequestMethod.UPDATE.name());
                Cursor postCursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME,searchQuery);
                JSONObject obj = new JSONObject();
                Log.e(TAG, "***************search params for other updates *************" + modelName +
                        " queueId:---" + objectID+
                        " method:---" + APIRequestMethod.UPDATE.name());
                if(postCursor.getCount()> 0) {
                    if (postCursor.moveToFirst()) {
                        do {
                            try {
                                int queueId = postCursor.getInt(postCursor.getColumnIndex(POST_ID));
                                Log.e(TAG, "***************found other update entries *************" + modelName + " queueId:---" + postCursor.getString(postCursor.getColumnIndex(POST_ID))+
                                        " method:---" + postCursor.getString(postCursor.getColumnIndex(API_METHOD))+
                                        " row_id:---" + postCursor.getString(postCursor.getColumnIndex(UPDATE_TABLE_ROW_ID))+
                                        " table name:---" + postCursor.getString(postCursor.getColumnIndex(UPDATE_TABLE_NAME)));
                                JSONObject obj2 = new JSONObject(postCursor.getString(postCursor.getColumnIndex(API_BODY)));
                                Iterator<String> keys = obj2.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    obj.put(key, obj2.optString(key));
                                }
                                ArrayList<String> attrs1 = new ArrayList<>();
                                attrs1.add(POST_ID);
                                databaseManager.deleteRow(
                                        POST_QUEUE_TABLE_NAME,
                                        databaseManager.createWhereClause(attrs1),
                                        new String[]{String.valueOf(queueId)}
                                );
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        } while (postCursor.moveToNext());
                    }
                    postCursor.close();
                }

                Iterator<String> keys = updateBody.keys();
                while( keys.hasNext() ) {
                    String key =  keys.next();
                    obj.put(key, updateBody.get(key));
                }
                Log.e(TAG," Final Patch JSON Object = "+obj);
                Log.e(TAG," attemptHotUpdate = "+attemptHotUpdate);
                Log.e(TAG," CheckNetworkConnection.networkHasConnection(mContext) = "+CheckNetworkConnection.networkHasConnection(mContext));

                if (attemptHotUpdate && CheckNetworkConnection.networkHasConnection(mContext)) {

                    Model mod = new Model(mContext);
                    mod.updateObject(modelName, objectID, obj, false, new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            if (response != null && response.length() > 0) {
                                if (daveAIListener != null) {
                                    Log.e(TAG, "After hot update model name:--" + modelName + " Object Detail:-" + updatedDetails);
                                    daveAIListener.onReceivedResponse(updatedDetails);
                                    JSONObject updatedDetails =  databaseManager.updateDataCached(modelName,objectID,updateBody, "");

                                }
                            } else {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.updateOrGetObjectAPI(modelName, encode(objectID)), APIRequestMethod.UPDATE.name(), obj.toString(),
                                                modelName, objectID, PENDING.name(),
                                                APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue())
                                );

                                if (daveAIListener != null) {
                                    Log.e(TAG, "After update locally  3  model name:--" + modelName + " Object Detail:-" + updatedDetails);
                                    daveAIListener.onReceivedResponse(updatedDetails);
                                }
                                //start background
                                // startBackgroundService();
                                new DaveService().startBackgroundService(mContext);
                            }
                        }

                        @Override
                        public void onTaskFailure(int responseCode, String errorMsg) {
                            //insert data in postQueue table
                            if(responseCode < 500) {
                                databaseManager.insertAPIQueueData(
                                        new APIQueueTableRowModel(
                                                APIRoutes.updateOrGetObjectAPI(modelName, encode(objectID)), APIRequestMethod.UPDATE.name(), obj.toString(),
                                                modelName, objectID, PENDING.name(),
                                                APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue())
                                );

                                if (daveAIListener != null) {
                                    Log.e(TAG, "After update locally 2  model name:--" + modelName + " Object Detail:-" + updatedDetails);
                                    daveAIListener.onReceivedResponse(updatedDetails);
                                }
                                new DaveService().startBackgroundService(mContext);
                            }else{
                                if(errorMsg.contains("does not have any objects on ids")){
                                    // 2.) delete row from api queue table and add post request in queue


                                    JSONObject jsonObject = databaseManager.getObjectData(modelName,objectID);
                                    databaseManager.updateDataCached(modelName, objectID, jsonObject, ERROR_MESSAGE_TEXT);

                                    jsonObject.remove(ERROR_MSG_ATTRIBUTE);

                                    Log.e(TAG,"Sync error for Patch. Posting Object Again with body = "+DaveAIStatic.base_url +"/object/"+modelName);
                                    databaseManager.insertAPIQueueData(
                                            new APIQueueTableRowModel( DaveAIStatic.base_url +"/object/"+modelName,
                                                    APIRequestMethod.POST.getMethod(),
                                                    jsonObject.toString(),
                                                    modelName, objectID, PENDING.toString(),
                                                    APIQueuePriority.ONE.getValue(),
                                                    CacheTableType.OBJECTS.getValue())
                                    );
                                    daveAIListener.onReceivedResponse(jsonObject);

                                }else {
                                    Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
                                    JSONObject updatedDetails = databaseManager.updateDataCached(modelName, objectID, updateBody, errorMsg);

                                    if (daveAIListener != null) {
                                        daveAIListener.onResponseFailure(responseCode, errorMsg);
                                    }
                                }
                            }
                        }
                    });
                }else{
                    //insert data in postQueue table
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.updateOrGetObjectAPI(modelName, encode(objectID)), APIRequestMethod.UPDATE.name(), obj.toString(),
                                    modelName, objectID, PENDING.name(),
                                    APIQueuePriority.ONE.getValue(), CacheTableType.OBJECTS.getValue())
                    );

                    if (daveAIListener != null) {
                        Log.e(TAG, "After update locally 1  model name:--" + modelName + " Object Detail:-" + updatedDetails);
                        daveAIListener.onReceivedResponse(updatedDetails);
                    }
                    //start background
                    // startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }


            }else{
                Log.e(TAG,"model id is not exist in table :-----------"+modelName);
            }
            cursor.close();



        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG," Error During Patch Object :-  "+e.getMessage());
        }

    }

    /**
     * method for iUpdate  object in local database and sync with server:-
     * Step :- update  object details in specify model database
     * Step :-  delete all gets request from the queue and add patch in queue table
     * Step :- Start service to sync with remote server
     * @param modelName model name
     * @param postBody  details want to post
     */
    public void iupdateObject(final String modelName, String objectID, JSONObject postBody ,DaveAIListener daveAIListener){
        try {

            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            Cursor cursor = isObjectInObjectsTable(modelName,objectID);
            if(cursor.getCount() == 1) {

               /* databaseManager.updateObjectsData(
                        modelName,
                        new ObjectsTableRowModel(objectID, postBody.toString(), System.currentTimeMillis()),
                        objectID
                );*/

                Log.e(TAG, "Update  model>>>>> " +modelName);


                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);
                queueHashMap.put(API_METHOD, APIRequestMethod.GET.name());

//                deleteObjectExistInQueueTable(modelName,queueHashMap);

                //insert data in postQueue table
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.iUpdateObjectAPI(modelName,objectID),APIRequestMethod.UPDATE.name(), postBody.toString(),
                                modelName, objectID, PENDING.name(),APIQueuePriority.ONE.getValue(),CacheTableType.OBJECTS.getValue())
                );

                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(postBody);
                }

                //start background
                //startBackgroundService();
                new DaveService().startBackgroundService(mContext);

            }
            cursor.close();

        } catch (Exception e) {
            Log.e(TAG," Error During Patch Object :-  "+e.getMessage());
        }

    }

    /**
     * method for delete  object from local database and server:-
     * Step :- check object details in specify model database and delete it.
     * Step :- delete everything from the post queue table and then Add in post Queue table
     * Step :- Start service to sync with remote server
     * @param objectID object id

     */
    public void deleteObject(final String modelName,final String objectID,DaveAIListener daveAIListener){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

            Cursor cursor = isObjectInObjectsTable(modelName,objectID);
            if(cursor.getCount() == 1) {
                // delete data from objects table
                ArrayList<String> attrs = new ArrayList<>();
                attrs.add(OBJECT_ID);
                databaseManager.deleteRow(
                        modelName,
                        databaseManager.createWhereClause(attrs),
                        new String[]{String.valueOf(objectID)}
                );

                // delete everything from the post queue table
                HashMap<String, Object> queueHashMap = new HashMap<>();
                queueHashMap.put(UPDATE_TABLE_NAME,modelName);
                queueHashMap.put(UPDATE_TABLE_ROW_ID,objectID);

                deleteObjectExistInQueueTable(modelName,queueHashMap);

                //insert data in postQueue table
                APIRoutes apiRoutes = new APIRoutes();
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                apiRoutes.deleteObjectAPI(modelName,encode(objectID),null), DaveConstants.APIRequestMethod.DELETE.getMethod(),
                                "", modelName, objectID, PENDING.name(),APIQueuePriority.ONE.getValue(),
                                CacheTableType.OBJECTS.getValue())
                );

                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(databaseManager.getObjectData(modelName,objectID));
                }

                //start Service
                //startBackgroundService();
                new DaveService().startBackgroundService(mContext);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteObjectFromLocalDB(final String modelName,final String objectID,DaveAIListener daveAIListener){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

            Cursor cursor = isObjectInObjectsTable(modelName,objectID);
            if(cursor.getCount() == 1) {
                // delete data from objects table
                ArrayList<String> attrs = new ArrayList<>();
                attrs.add(OBJECT_ID);
                databaseManager.deleteRow(
                        modelName,
                        databaseManager.createWhereClause(attrs),
                        new String[]{String.valueOf(objectID)}
                );

                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(databaseManager.getObjectData(modelName,objectID));
                }

            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // create APi Queue table if not exist
    private void createApiQueueTAble(){
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        if(!databaseManager.isTableExists(POST_QUEUE_TABLE_NAME)) {
            Log.e(TAG, "**************** create postQueueTable table*************");
            databaseManager.createTable(POST_QUEUE_TABLE_NAME, DatabaseConstants.CacheTableType.POST_QUEUE);
        }
    }

    //Check object Details in particular table
    private Cursor isObjectInObjectsTable(String modelName, String objectID){
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(OBJECT_ID,objectID);
        Cursor cursor = databaseManager.getDataFromTableWhere(modelName,hashMap);
        Log.e(TAG,"Check object exist in table  tableName:-  "+ modelName +" TotalCount:-"+ cursor.getCount());

        return cursor;
    }

    // delete object from API Queue Table
    private void deleteObjectExistInQueueTable(String modelName , HashMap<String, Object> searchQuery){

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        Cursor postCursor = databaseManager.getDataFromTableWhere(POST_QUEUE_TABLE_NAME,searchQuery);
        if(postCursor.getCount()> 0) {
            if (postCursor.moveToFirst()) {
                do {
                    int  queueId = postCursor.getInt(postCursor.getColumnIndex(POST_ID));
                    Log.e(TAG, "***************deleteObjectExistInQueueTable *************"+modelName+" queueId:---"+ queueId);
                    ArrayList<String> attrs1 = new ArrayList<>();
                    attrs1.add(POST_ID);
                    databaseManager.deleteRow(
                            POST_QUEUE_TABLE_NAME,
                            databaseManager.createWhereClause(attrs1),
                            new String[]{String.valueOf(queueId)}
                    );
                } while (postCursor.moveToNext());
            }
            postCursor.close();
        }
    }
    private  void startBackgroundMediaCacheService(){
        //Start service to post data
        Intent msgIntent = new Intent(mContext,MediaService.class);
        mContext.startService(msgIntent);
    }

    public String createNewObjectId(String modelName,JSONObject objectDetail){

       String objectID = "";
       try {
           Attribute attribute = modelInstance.getModelIdAttributeInstance(modelName);
           if(attribute!=null)
               if(objectDetail.has(attribute.getName())){
                   objectID = objectDetail.getString(attribute.getName());
               }else {
                   if(attribute.getDefaultX() instanceof LinkedTreeMap) {
                       LinkedTreeMap gd = (LinkedTreeMap) attribute.getDefaultX();
                       //todo check func ,default is present
                       String funcName = gd.get("function").toString();
                       ArrayList args = new ArrayList();
                       if(gd.containsKey("args")) {
                           args = (ArrayList) gd.get("args");
                       }
                       Log.e(TAG, "Get Model Id func Name  to create UUID:---- " + funcName + "  Args :- "+args);
                       StringBuilder nameToCreateId = new StringBuilder();
                       for(int i =0 ;i< args.size();i++) {
                           String agrsName = args.get(i).toString();
                           if(objectDetail.has(agrsName) && !objectDetail.isNull(agrsName)) {
                               String value = objectDetail.getString(agrsName).toLowerCase();
                               if(value.contains(" ")) {
                                   value = value.replaceAll("( +)","_");
                               }
                               nameToCreateId.append(value.trim());
                           }
                           else {
                               // if args is not in object then get default value for agrs
                               Object attrDefault = modelInstance.getAttrDefaultValues(modelName,agrsName);
                               Log.e(TAG, "Get attr NAme:---- " + agrsName+"  agrs default:-"+attrDefault);
                               if( attrDefault != null && attrDefault instanceof LinkedTreeMap) {
                                   LinkedTreeMap attributeDefaultX = (LinkedTreeMap) attrDefault;
                                   ArrayList attrArgs = (ArrayList) gd.get("args");
                                   if(attributeDefaultX.containsKey("function")){
                                       String attrFunc = attributeDefaultX.get("function").toString();
                                       if(attrFunc.equalsIgnoreCase("today")){
                                           nameToCreateId.append(methodToday());
                                           Log.e(TAG,"Get method Today:---- "+ methodToday());

                                       }
                                   }
                               }
                           }

                       }
                       objectID = generateUUIDBasedOnFunc(funcName,nameToCreateId.toString(),objectDetail);

                   }

               }

       }catch (Exception e){
           e.printStackTrace();
       }
       return objectID;
   }

   private String generateUUIDBasedOnFunc(String funcName, String stringToCreateID ,JSONObject objectDetails){
        String help_string = "";
        String uuid = "";
        try {
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            //java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(cal.getTime());

            java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("HH");
            String formattedHour =dateFormat.format(date);
            Log.e(TAG, "<<<<<<<<<<<<<<<<<generateUUIDBasedOnFunc>>>>>>>>>>>>>" +funcName+"nameToCreateId>>>>>"+ stringToCreateID);
            switch (funcName) {
                case "make_uuid":
                    help_string = "Takes nameToCreateId and creates a uuid.";
                    uuid = methodGenerateUuid(stringToCreateID);
                    break;

                case "make_uuid3":
                    help_string = "Takes nameToCreateId and creates a uuid3.";
                    uuid = methodGenerateUuid3(stringToCreateID);
                    break;

                case "make_uuid_timestamp":
                    help_string = "Takes nameToCreateId and creates a uuid along with timestamp.";
                    long unixTime = System.currentTimeMillis() / 1000L;
                    // float unixTime = System.currentTimeMillis() / 1000L;
                    // Timestamp ts = new Timestamp(new Date().getTime());

                    uuid = methodGenerateUuid(unixTime + stringToCreateID);
                    break;

                case "make_uuid_date":
                    // '2018-09-11'
                    help_string = "Takes nameToCreateId and creates a uuid along with date.";
                    uuid = methodGenerateUuid3(formattedDate + stringToCreateID);
                    break;

                case "make_uuid_date_hour":
                    // date.today(),datetime.now().hour hour:- 24
                    help_string = "Takes nameToCreateId and creates a uuid along with date and current hour";
                    uuid = methodGenerateUuid3(formattedDate + formattedHour + stringToCreateID);
                    break;

                case "name_to_id":
                    help_string = "Takes All Attr value and creates a uuid along with date and current hour";
                    // ['/', '%', '+', ' ', '-' , '\"', "\'", '(', ')', '?', '&', '.', ',', '\', '!'] replace with _
                    List<String> SPECIAL_CHAR = Arrays.asList("/", "%", "+",  " ",  "-" , "\"","|", "(",")", "?","&",".","/","!");

                    //todo take only args and replace space with underscore
                   /* StringBuilder stringBuilder = new StringBuilder();
                    Iterator<String> keys = objectDetails.keys();
                    while(keys.hasNext()) {
                        String key = keys.next();
                        String value = objectDetails.getString(key).toLowerCase().trim();
                        if(value.contains(" "))
                            value = value.replaceAll("( +)","_");

                        stringBuilder.append(value);
                    }
                    uuid = stringBuilder.toString().trim();*/
                    for(int i =0; i<SPECIAL_CHAR.size();i++)
                        if(stringToCreateID.contains(SPECIAL_CHAR.get(i))){
                        stringToCreateID = stringToCreateID.replace(SPECIAL_CHAR.get(i),"_");
                        }
                    uuid = stringToCreateID;
                   // uuid = encode(stringToCreateID);
                    break;

            }
            Log.e(TAG, "<<<<<<<<<<<<<<<<<<<<<Print  generated UUID  && Help String>>>>>>>>>" + uuid +"  &&:--"+help_string);
            //objectDetails.put(s.getName(),uuid);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return uuid;
   }



    /**
     * Generates a GUID using a seed.  This will generate the same GUID usign the same seed.
     * @param seed The seed to use for generating the GUID
     * @return A string representation of the GUID
     */
    private String methodGenerateUuid(String seed) {
        Log.e(TAG, "Create uuid  methodGenerateUuid  from Given Value:---- " + seed.toLowerCase());
        UUID uuid;
       /* try {
            uuid = UUID.nameUUIDFromBytes(seed.toLowerCase().getBytes("UTF-8"));
           // String base64encodedString = Base64.getEncoder().encodeToString(seed.toLowerCase().getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(String.format("UnsupportedEncodingException: %f", e.getMessage()));
        }*/
        //return uuid.toString();

              /* String encodedString = Base64.getEncoder().encodeToString(seed.toLowerCase().getBytes());
        String encodedUrl = Base64.getUrlEncoder().encodeToString(seed.toLowerCase().getBytes());
*/
        byte[] encodeValue = Base64.encode(seed.toLowerCase().getBytes(), Base64.DEFAULT);
        String string = new String(encodeValue);
        string = string.replace(System.getProperty("line.separator"), "");
        if(string.contains("="))
           string= string.replace('=', '_');
            //string.replace("=","_");


        //return new String(encodeValue);
        return string;



        //return UUID.nameUUIDFromBytes(seed.toLowerCase().getBytes()).toString();

    }

    private String methodGenerateUuid3(String name){
        String namespace= "6ba7b810-9dad-11d1-80b4-00c04fd430c8";
        String source = (namespace + name).toLowerCase();
        Log.e(TAG, "create Uuid3 from Given Value :---- " + source);
        return UUID.nameUUIDFromBytes(source.getBytes()).toString();
    }

    private String methodToday(){
        Calendar cal = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return df.format(cal.getTime());
    }

    private String methodNow(){
        //format  yyyy-mm-dd hh:MM:ss.zzzzzz+hh:mm    (last is timezone)
        Calendar cal = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-mm-dd hh:MM:ss.zzzzzz+hh:mm");
        return df.format(cal.getTime());
    }

    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,Context context){
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        daveAIPerspective.getHot_update_time_limit(),
                        daveAIPerspective.getCold_update_time_limit(),
                        0, modelIdAttrName,System.currentTimeMillis())
        );

    }

    /**
     *
     * @param uniqueId   provide uniqueId to get data from database
     *                    Note:- DaveAi provided the uniqueId during saving data in database. Use that id .
     */
    private JSONObject getDataFromDataBase(String uniqueId){

        if (sharedPreferences.checkStringExist(uniqueId)){
            String getResponse = sharedPreferences.readString(uniqueId);
            try {
                if(getResponse!=null && !getResponse.isEmpty())
                    return new JSONObject(getResponse);

            }catch (JSONException e){
                Log.e(TAG,"Error During Getting Data From DB:---  "+e.getMessage());
            }

        }
        return null;
    }


//********************************************************direct server call method need internet connection*******************************************

    /**
     *method to post object on server
     * @param modelName
     * @param postBody
     * @param loader
     * @param daveAIListener
     */
    public void postObject(final String modelName,JSONObject postBody, boolean loader, DaveAIListener daveAIListener){
        updateObject(modelName,postBody,"",loader,true,daveAIListener);

    }

    //method to post object on server
    public void postObject(final String modelName, boolean isFreshen,JSONObject postBody, DaveAIListener daveAIListener){
        patchObject(modelName,"",postBody,isFreshen,daveAIListener);
    }

    /**
     *method to update object with server
     * @param modelName provide model name
     * @param jsonObject provide jsonObject body
     * @param loader provide loader value if its true...add loader during making server call
     * @param objectId provide object id which body has to update
     * @param isFreshen if its true then make server call
     * @param daveAIListener provide callback instance to get result
     */
    public void updateObject(final String modelName,JSONObject jsonObject,String objectId, boolean loader , boolean isFreshen,
                             DaveAIListener daveAIListener){
        this.isFreshen = isFreshen;
        if(isFreshen) {
            RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, jsonObject.toString());
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    if (daveAIListener != null) {
                        daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                    }
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                    if (daveAIListener != null) {
                        daveAIListener.onResponseFailure(requestCode, responseMsg);
                    }
                }
            };
            if (CheckNetworkConnection.networkHasConnection(mContext))
                if (objectId != null && !objectId.isEmpty())
                    new APICallAsyncTask(postTaskListener, mContext, "UPDATE", body_part, loader)
                            .execute(APIRoutes.updateOrGetObjectAPI(modelName, objectId));
                else
                    new APICallAsyncTask(postTaskListener, mContext, "POST", body_part, loader)
                            .execute(APIRoutes.postObjectAPI(modelName));
            else
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
        }else {

            if(jsonObject!= null) {
                if (sharedPreferences.checkStringExist("_object_" + modelName + "_" + objectId)) {
                    try {
                        JSONObject getResult = new JSONObject(sharedPreferences.readString("_object_" + modelName + "_" + objectId));
                        Iterator<?> keys = jsonObject.keys();
                        while (keys.hasNext()) {
                            String key = (String) keys.next();
                            getResult.put(key, jsonObject.get(key));

                        }
                        sharedPreferences.writeString("_object_" + modelName + "_" + objectId, getResult.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else
                    sharedPreferences.writeString("_object_" + modelName + "_" + objectId, jsonObject.toString());
            }
        }

    }

    public void patchObject(final String modelName,String objectId, JSONObject jsonObject, boolean isFreshen, DaveAIListener daveAIListener){

        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, jsonObject.toString());
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                //save in local database
                if (isCached && isFreshen) {
                    DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                  /*  if(!databaseManager.isTableExists(modelName)) {
                        databaseManager.createTable(modelName, DatabaseConstants.CacheTableType.OBJECTS);
                    }
                    if(!modelIdAttr.isEmpty()) {
                        setupModelCacheMetaData(modelName, modelIdAttr, mContext);
                    }*/
                    if(databaseManager.isTableExists(modelName)){
                        databaseManager.insertMetaData(
                                new CacheModelPOJO(modelName,
                                        daveAIPerspective.getHot_update_time_limit(),
                                        daveAIPerspective.getCold_update_time_limit(),
                                        0, modelName,System.currentTimeMillis())
                        );

                        databaseManager.insertOrUpdateObjectData(
                                modelName,
                                new ObjectsTableRowModel(objectId, response, System.currentTimeMillis())
                        );
                    }
                }
                if (daveAIListener != null) {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if (daveAIListener != null) {
                    daveAIListener.onResponseFailure(requestCode, responseMsg);
                }
            }
        };
        if (CheckNetworkConnection.networkHasConnection(mContext))
            if (objectId != null && !objectId.isEmpty())
                new APICallAsyncTask(postTaskListener, mContext, "UPDATE", body_part)
                        .execute(APIRoutes.updateOrGetObjectAPI(modelName, objectId));
            else
                new APICallAsyncTask(postTaskListener, mContext, "POST", body_part)
                        .execute(APIRoutes.postObjectAPI(modelName));
        else
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext);


    }

    public void deleteMethod(final String modelName, String objectId,HashMap<String,String> queryParam,
                             boolean loader,DaveAIListener daveAIListener){

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if(daveAIListener!=null) {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if(daveAIListener!=null) {
                    daveAIListener.onResponseFailure(requestCode,responseMsg);
                }
            }
        };
        if (CheckNetworkConnection.networkHasConnection(mContext)) {
            APIRoutes apiRoutes = new APIRoutes();
            new APICallAsyncTask(postTaskListener, mContext, "DELETE", loader)
                    .execute(apiRoutes.deleteObjectAPI(modelName,objectId,queryParam));
        }
        else
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
    }

    public void getPivot(DaveAIListener daveAIListener,final String modelName, HashMap<String,String> queryParams, boolean loader){

        if(daveAIListener ==null)
            throw new NullPointerException("DaveAIListener is null. must be provide DaveAIListener");
        else if(modelName == null || modelName.isEmpty())
            throw new NullPointerException("Model Name is null or Empty. must be provide model name");
        else {
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                    daveAIListener.onResponseFailure(requestCode,responseMsg);

                }
            };
            if (CheckNetworkConnection.networkHasConnection(mContext))
                new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                        .execute(APIRoutes.getPivotAPI(modelName, queryParams));
            else
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);

        }
    }

    /**
     *
     * @param modelName provide model name
     * @param attrName provide attr name
     * @param daveAIListener listener to get response
     * @param isFresh make server call
     * @param isFreshen get data from localDb.
     * @return
     * @throws DaveException
     */
    public JSONObject getPivot(final String modelName,String attrName, DaveAIListener daveAIListener,
                               boolean isFresh, boolean isFreshen) throws DaveException {

        jsonResponse = new JSONObject();
        if(modelName == null || modelName.isEmpty())
            throw new DaveException.ModelNotFound("Model Name is null or Empty. must be provide Model Name");
        else  if(attrName == null || attrName.isEmpty())
            throw new DaveException.ModelNotFound("requestType is null or Empty. must be provide requestType");

        else  {
            CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName+"_"+attrName);
            if(cacheModelPOJO!=null && !cacheModelPOJO.modelName.isEmpty()){
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName,CacheTableType.SINGLETON);
                switch (refreshStatus){
                    case COLD_UPDATE_REQUIRED:
                        isFreshen = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        isFreshen = true;
                        break;
                    case NOT_REQUIRED:
                        isFreshen = false;
                        break;
                }
            }
            this.isFresh=isFresh;
            this.isFreshen=isFreshen;
            if(isCached && !this.isFresh){
                jsonResponse = databaseManager.getSingletonRow(modelName+"_"+attrName);
                if(jsonResponse==null||jsonResponse.length()==0) {
                    this.isFresh = true;

                }else{
                    Log.i(TAG,"Get Pivot of Model from Local Database-------:- "+modelName+"_"+attrName +" isFresh == "+this.isFresh+ " isFreshen:- "+this.isFreshen+" \n jsonobject "+jsonResponse);
                    if(daveAIListener!=null ) {
                        daveAIListener.onReceivedResponse(jsonResponse);
                    }
                }
            }
            if(!isCached || this.isFresh || this.isFreshen) {
                if(this.isFresh) {
                    APIResponse postTaskListener = new APIResponse() {
                        @Override
                        public void onTaskCompleted(String response) {
                            //save in local database
                            if (isCached && (DaveModels.this.isFreshen || DaveModels.this.isFresh)) {
                                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                                //add meta data
                                databaseManager.insertMetaData(
                                        new CacheModelPOJO(modelName+"_"+attrName,
                                                daveAIPerspective.getHot_update_time_limit(),
                                                daveAIPerspective.getCold_update_time_limit(),
                                                0, "pivot_"+attrName,System.currentTimeMillis())
                                );
                                //add date as singleton
                                databaseManager.insertOrUpdateSingleton(
                                        modelName+"_"+attrName,
                                        new SingletonTableRowModel(modelName+"_"+attrName, response,
                                                System.currentTimeMillis(),"pivot_"+attrName)
                                );
                            }
                            if (DaveModels.this.isFresh) {
                                Log.e(TAG, " Get Pivot from server isFresh is true for model " + modelName +" Response:-"+ response);
                                if (daveAIListener != null)
                                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                            }
                            jsonResponse = convertStringTOJSONObject(response);
                        }

                        @Override
                        public void onTaskFailure(int requestCode, String responseMsg) {
                            if (daveAIListener != null) {
                                daveAIListener.onResponseFailure(requestCode, responseMsg);
                            }

                        }
                    };
                    if (CheckNetworkConnection.networkHasConnection(mContext)) {
                        new APICallAsyncTask(postTaskListener, mContext, "GET")
                                .execute(APIRoutes.pivotAttributeAPI(modelName, attrName));
                    }else
                        CheckNetworkConnection.showNetDisabledAlertToUser(mContext, "Local data not found.Need Internet Connection");

                }else if(this.isFreshen){
                    //insert data in postQueue table
                    databaseManager.insertAPIQueueData(
                            new APIQueueTableRowModel(
                                    APIRoutes.pivotAttributeAPI(modelName, attrName), APIRequestMethod.GET.name(),
                                    "", modelName+"_"+attrName,
                                    "pivot_"+attrName, PENDING.name(),
                                    APIQueuePriority.ONE.getValue(), CacheTableType.SINGLETON.getValue()
                            )
                    );
                    // startBackgroundService();
                    new DaveService().startBackgroundService(mContext);
                }
            }
        }
        return jsonResponse;
    }


    public void getOptions(DaveAIListener daveAIListener,final String optionUrl,HashMap<String,Object> queryParam, boolean loader){
        Log.i(TAG,"getOptions:-----optionUrl-"+optionUrl+" queryParam:-"+queryParam);
        if(daveAIListener ==null)
            throw new NullPointerException("DaveAIListener is null. must be provide DaveAIListener");
        else if(optionUrl == null || optionUrl.isEmpty())
            throw new NullPointerException("optionUrl is null or Empty. must be provide optionUrl");
        else {
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                    daveAIListener.onResponseFailure(requestCode,responseMsg);

                }
            };
            if (CheckNetworkConnection.networkHasConnection(mContext))
                new APICallAsyncTask(postTaskListener, mContext, "GET", loader)
                        .execute(new APIRoutes(mContext).optionsAPI(optionUrl,queryParam));
            else
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);

        }
    }

    public void shareCatalogue(DaveAIListener daveAIListener,final String customerId,JSONObject postBody,boolean loader){
        if(postBody ==null)
            throw new NullPointerException("postBody is null. must be provide shareCatalogue post body");
        else if(customerId == null || customerId.isEmpty())
            throw new NullPointerException("Customer Id is null or Empty. must be provide Customer Id");
        else {
            RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    if (daveAIListener != null) {
                        daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                    }
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                    if(daveAIListener!=null) {
                        daveAIListener.onResponseFailure(requestCode,responseMsg);
                    }

                }
            };
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                new APICallAsyncTask(postTaskListener, mContext, "POST", body_part, loader).execute(APIRoutes.catalogueAPI(customerId));
            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }
        }
    }

    public void uploadImageToServer(DaveAIListener daveAIListener, String name, String fileName, String filePath,
                                      HashMap<String,Object> postBody,String loaderMsg) {
        uploadImageToServer(daveAIListener,name,fileName,filePath,postBody,loaderMsg,true);
    }

    public void uploadImageToServer(DaveAIListener daveAIListener, String name, String fileName, String filePath,
                                    HashMap<String,Object> postBody,String loaderMsg,boolean showLoader) {

        Log.i(TAG,"uploadImageToServer File Name : :------"+fileName +"  && filePath:-------   "+ filePath);

        MultipartBuilder builderNew = new MultipartBuilder();
        builderNew.type(MultipartBuilder.FORM);
        builderNew.addFormDataPart(name,fileName, RequestBody.create(MEDIA_TYPE_PNG, new File(filePath)));
        if(postBody!=null && !postBody.isEmpty()) {
            for (Map.Entry<String, Object> entry : postBody.entrySet()) {
                Log.i(TAG,"uploadImageToServer Post Body : Key :------"+entry.getKey()+"  &&  Value :-------   "+ entry.getValue().toString());
                builderNew.addFormDataPart(entry.getKey(), entry.getValue().toString());
            }
        }
        RequestBody requestBody = builderNew.build();
        if (CheckNetworkConnection.networkHasConnection(mContext)){
          /*  final ProgressDialog progress = new ProgressDialog(mContext);
            if(loaderMsg ==null || loaderMsg.isEmpty())
                progress.setMessage("File Uploading...");
            else
                progress.setMessage(loaderMsg);

            progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progress.setIndeterminate(true);
            progress.setProgress(0);
            progress.show();*/
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                   /* progress.setProgress(100);
                    progress.dismiss();*/
                    if(response!=null && !response.isEmpty()){
                        try {
                            JSONObject imagePath = new JSONObject(response);
                            //String imageUrl = imagePath.getString("path");
                            // uploadedImageUrl =  base_url+"/"+imagePath.getString("path");
                            if(daveAIListener!=null)
                                daveAIListener.onReceivedResponse(imagePath);
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("Error "," Error Upload Image :  "+ e.getMessage());
                        }
                    }
                    else
                        Toast.makeText(mContext,"Document is not Uploaded",Toast.LENGTH_LONG).show();
                }

                @Override
                public void onTaskFailure(int requestCode, String responseMsg) {
                   // progress.dismiss();
                    if(daveAIListener!=null) {
                        daveAIListener.onResponseFailure(requestCode,responseMsg);
                    }

                }
            };
            new APICallAsyncTask(postTaskListener,mContext,"POST",requestBody,showLoader,"File Uploading...").execute(APIRoutes.uploadFileAPI());
        }
        else{
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext,"To Upload Image need Internet Connection");
        }
    }

    public void logout(boolean loader, DaveAIListener daveAIListener){

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (daveAIListener != null) {
                    daveAIListener.onReceivedResponse(convertStringTOJSONObject(response));
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if (daveAIListener != null) {
                    daveAIListener.onResponseFailure(requestCode, responseMsg);
                }
            }
        };
        if (CheckNetworkConnection.networkHasConnection(mContext))
            new APICallAsyncTask(postTaskListener, mContext, "GET",loader).execute(new APIRoutes(mContext).logoutAPI().toString());
        else {
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext,"Need Internet connection to logout.");
        }

    }


    public void getAppConfigurationDetails(String userId, String apiKey ,APIResponse postTaskListener){

        if(base_url==null)
            getMetadata(mContext);

        DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);

        sharedPreference.writeString(DaveSharedPreference.USER_ID,userId);
        sharedPreference.writeString(DaveSharedPreference.API_KEY, apiKey);
        if (CheckNetworkConnection.networkHasConnection(mContext)) {
            System.out.println("*****************Get APp Config Details:-------------"+APIRoutes.appConfigureAPI());
            new APICallAsyncTask(postTaskListener, mContext,"GET",true,"Checking for App Update...",10L).execute(APIRoutes.appConfigureAPI());
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
        }
    }


}


    /*  private void methodToCreateUUID3(String name){

        String namespace= "6ba7b810-9dad-11d1-80b4-00c04fd430c8";
        String source = namespace + name;
        byte[] bytes = new byte[0];
        try {
            bytes = source.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        UUID uuid = UUID.nameUUIDFromBytes(bytes);

    }*/

     /*  UUID uuid;
        try {
            uuid= UUID.nameUUIDFromBytes(seed.getBytes());
            //uuid = UUID.nameUUIDFromBytes(seed.getBytes("UTF-8"));
            return uuid.toString();
        } catch (Exception e) {
            throw new RuntimeException(String.format("UnsupportedEncodingException: %f", e.getMessage()));
        }
        return uuid.toString();*/



  /*  @NonNull
    public String postMultiPartToServer(@NonNull String url,
                                        @Nullable ArrayMap<String, String> formField,
                                        @Nullable ArrayMap<String, File> filePart)
            throws Exception {
        okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder().url(url);
        if (formField != null || filePart != null) {
            okhttp3.MultipartBody.Builder multipartBodyBuilder = new okhttp3.MultipartBody.Builder();
            multipartBodyBuilder.setType(okhttp3.MultipartBody.FORM);
            if (formField != null) {
                for (Map.Entry<String, String> entry : formField.entrySet()) {
                    multipartBodyBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                }
            }
            if (filePart != null) {
                for (Map.Entry<String, File> entry : filePart.entrySet()) {
                    File file = entry.getValue();
                    multipartBodyBuilder.addFormDataPart(
                            entry.getKey(),
                            file.getName(),
                            okhttp3.RequestBody.create(getMediaType(file.toURI()), file)
                    );
                }
            }
            requestBuilder.post(multipartBodyBuilder.build());
        }
        okhttp3.Request request = requestBuilder.build();
        okhttp3.Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException(response.message());
        }
        return response.body().string();
    }

    private okhttp3.MediaType getMediaType(URI uri1) {
        Uri uri = Uri.parse(uri1.toString());
        String mimeType;
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return okhttp3.MediaType.parse(mimeType);
    }*/



