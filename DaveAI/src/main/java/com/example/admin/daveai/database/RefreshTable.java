package com.example.admin.daveai.database;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.admin.daveai.broadcasting.SendBroadcast;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.daveUtil.DaveCachedUtil;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.network.DaveRoutes;
import com.example.admin.daveai.others.CacheModelPOJO;

import com.example.admin.daveai.others.DaveSharedPreference;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.admin.daveai.others.DaveApplication.runOnUiThread;


/**
 * Created by Rinki on 08,August,2019
 */
public class RefreshTable implements DatabaseConstants{

    //todo insert current data and delete previous data based on timeStamp
    private final static String TAG = "RefreshTable";
    private Context context;
    private OnRefreshTableListener listener = null;



    public interface OnRefreshTableListener {
        public void onRefreshTable(JSONObject jsonObject);
    }

    public RefreshTable(Context context){
        this.context = context;
    }

    public void callRefreshTableAPI(String modelName, HashMap<String,Object> queryParams){
        callRefreshTableAPI(modelName,queryParams,null);

    }

    public void callRefreshTableAPI(String modelName, HashMap<String,Object> queryParams,OnRefreshTableListener onRefreshTableListener){

        DaveSharedPreference sharedPreferences = new DaveSharedPreference(context);
        Log.e(TAG,"Print call RefreshData :------------ "+ modelName +" queryParams :- "+ queryParams);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                refreshTable(modelName,response,onRefreshTableListener);
                /*Log.e(TAG,"Data got refresh in table **********");
                if(onRefreshTableListener!=null){
                    try {
                        onRefreshTableListener.onRefreshTable(new JSONObject(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //todo send broadcast refreshed table
                SendBroadcast sendBroadcast = new SendBroadcast(context);
                sendBroadcast.sendBroadcast(SendBroadcast.REFRESHED_TABLE_ACTION,
                        SendBroadcast.REFRESHED_TABLE_MSG,new HashMap<>());*/
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                Log.e(TAG, "refresh table failed  (" + modelName + ")");


            }
        };
        if (CheckNetworkConnection.networkHasConnection(context)) {

            new APICallAsyncTask(postTaskListener, context, "GET", false)
                    .execute(DaveRoutes.getObjectsAPI(context,modelName, queryParams));
        }

    }

    public void refreshTable(String modelName, String response,OnRefreshTableListener onRefreshTableListener){
        try{
            JSONObject respJson = new JSONObject(response);
            if (respJson.has("data")) {
                JSONArray dataJar = respJson.getJSONArray("data");
                refreshTable(modelName,dataJar,onRefreshTableListener);
            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    /**
     * Method to refresh data in given table.
     * Its insert new data and delete previous from table based on timeStamp(time before adding new data).
     * After caching media send broadcast message and callbacks(if not null)
     * @param modelName pass model name
     * @param data pass JSONArray data
     * @param onRefreshTableListener listener
     */

    public void refreshTable(String modelName, JSONArray data,OnRefreshTableListener onRefreshTableListener){
        try{
            Log.i("getObjects", " Total COunt of Objects:-" + data.length());
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);

            long beforeInsertMillis = System.currentTimeMillis(); // before insert new data
            CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
            //insert new data
            for (int i = 0; i < data.length(); i++) {
                databaseManager.insertOrUpdateObjectData(
                        modelName,
                        new ObjectsTableRowModel(
                                data.getJSONObject(i).getString(modelMetaData.model_id_name),
                                data.getJSONObject(i).toString(),
                                System.currentTimeMillis()
                        ));

            }
            //media caching
            DaveCachedUtil daveCachedUtil = new DaveCachedUtil(context);
            daveCachedUtil.checkForMediaCaching(modelName,data);

            //delete previous data
            //JSONArray jsonArray = databaseManager.getObjectsData(modelName,beforeInsertMillis);
            Cursor cursor = databaseManager.getDataFromTable(modelName);
            if(cursor!=null) {
                Log.e(TAG," before delete TOTAL COUNT = "+cursor.getCount());
                if (cursor.moveToFirst()) {
                    do {
                        long lastSync =  cursor.getLong(cursor.getColumnIndex(LAST_SYNCED));
                        if(lastSync < beforeInsertMillis) {
                            ArrayList<String> attributes = new ArrayList<>();
                            attributes.add(LAST_SYNCED);
                            databaseManager.deleteRow(
                                    modelName,
                                    databaseManager.createWhereClause(attributes),
                                    new String[]{String.valueOf(lastSync)}
                            );
                        }

                    } while (cursor.moveToNext());
                }
                Log.e(TAG,"AFTER DELETE  TotalCount:-"+cursor.getCount());
                cursor.close();

            }

            databaseManager.alertOnMediaQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
                @Override
                public void isEmpty() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put("data", data);

                                JSONObject finalJSON = new JSONObject();
                                finalJSON.put("media",daveCachedUtil.replaceWithLocalFileCachePath(modelName,jsonObject1).getJSONArray("data"));
                                Log.e(TAG,"Data got refresh in table **********");
                                if(onRefreshTableListener!=null){
                                    onRefreshTableListener.onRefreshTable(finalJSON);
                                }
                                //todo send broadcast refreshed table
                                SendBroadcast.sendCustomBroadcast(context,
                                        SendBroadcast.REFRESHED_TABLE_ACTION,
                                        SendBroadcast.REFRESHED_TABLE_MSG,
                                        finalJSON.toString()
                                );

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            },2000);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
