package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.ViewPagerItem;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import me.drakeet.multitype.ItemViewBinder;


public class ViewPagerItemViewBinder extends ItemViewBinder<ViewPagerItem, ViewPagerItemViewBinder.ViewPagerHolder> {
    private int currentimageindex = 0;
    private Context context;


    public ViewPagerItemViewBinder(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    protected ViewPagerHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_view_pager, parent, false);
        return new ViewPagerHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull final ViewPagerHolder holder, @NonNull final ViewPagerItem item) {

        holder.imageTitle.setText(item.getImageTitle());
        final ArrayList<String> imgList = item.getImageList();

        currentimageindex = 0;
        final Handler mHandler = new Handler();
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
                AnimateandSlideShow(holder.getAdapterPosition(), imgList, holder);
            }
        };

        int delay = 1000;
        int period = 3000;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                mHandler.post(mUpdateResults);
            }
        }, delay, period);
    }

    private void AnimateandSlideShow(int adapterPosition, ArrayList<String> item, ViewPagerHolder holder) {
       // Log.e("imgvie", "AnimateandSlideShow: " + item.size() + ".." + item.get(currentimageindex)+"******==");
       /* Glide.with(context)
                .load(item.get(currentimageindex))
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(holder.slidingimage);*/

        try {
            if (context != null) {
                Log.e("Image Loading"," Loading image from = "+item.get(currentimageindex));
                Glide.with(context)
                        .load(item.get(currentimageindex))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.slidingimage);
            }
        }catch (Exception e){
            Log.e("ViewPager","error during load image using Glide"+e.getMessage());
        }

        if (item.size() - 1 == currentimageindex) {
            currentimageindex = 0;
        } else {
            currentimageindex++;
        }
//        Animation rotateimage = AnimationUtils.loadAnimation(R.anim.fade_in);
//        holder.slidingimage.startAnimation(rotateimage);
    }

    public class ViewPagerHolder extends RecyclerView.ViewHolder {
        private ImageView slidingimage;
        private TextView imageTitle;

        public ViewPagerHolder(View itemView) {
            super(itemView);
            slidingimage = itemView.findViewById(R.id.slideImage);
            imageTitle= itemView.findViewById(R.id.imageTitle);

        }
    }
}
