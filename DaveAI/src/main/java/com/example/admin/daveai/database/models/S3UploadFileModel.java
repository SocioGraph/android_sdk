package com.example.admin.daveai.database.models;

public class S3UploadFileModel {
    String local_path = new String();
    String upload_file_name = new String();
    String upload_folder_path = new String();
    String upload_bucket_name = new String();

    public S3UploadFileModel(String local_path, String upload_file_name, String upload_folder_path, String upload_bucket_name) {
        this.local_path = local_path;
        this.upload_file_name = upload_file_name;
        this.upload_folder_path = upload_folder_path;
        this.upload_bucket_name = upload_bucket_name;
    }

    public String getLocal_path() {
        return local_path;
    }

    public void setLocal_path(String local_path) {
        this.local_path = local_path;
    }

    public String getUpload_file_name() {
        return upload_file_name;
    }

    public void setUpload_file_name(String upload_file_name) {
        this.upload_file_name = upload_file_name;
    }

    public String getUpload_folder_path() {
        return upload_folder_path;
    }

    public void setUpload_folder_path(String upload_folder_path) {
        this.upload_folder_path = upload_folder_path;
    }

    public String getUpload_bucket_name() {
        return upload_bucket_name;
    }

    public void setUpload_bucket_name(String upload_bucket_name) {
        this.upload_bucket_name = upload_bucket_name;
    }
}
