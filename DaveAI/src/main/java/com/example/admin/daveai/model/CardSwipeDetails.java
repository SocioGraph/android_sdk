package com.example.admin.daveai.model;


import org.json.JSONArray;
import org.json.JSONObject;

public class CardSwipeDetails {

    private JSONObject cardItemDetails;
    private JSONObject productDetails;
    private JSONObject quantityAttrs;
    private JSONArray nextStageQuantityAttrs = new JSONArray();
    private String previousStage = null;
    private String interactionStage = null;
    private String nextStage = null;
    private int swipeDirection = 0;
    private JSONObject quantityPredictions= new JSONObject();
    private JSONArray nextStageTaggedProduct = new JSONArray();
    private JSONArray nextStageNewAttributes = new JSONArray();


    public JSONObject getCardItemDetails() {
        return cardItemDetails;
    }

    public void setCardItemDetails(JSONObject cardItemDetails) {
        this.cardItemDetails = cardItemDetails;
    }

    public JSONObject getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(JSONObject productDetails) {
        this.productDetails = productDetails;
    }

    public JSONObject getQuantityAttrs() {
        return quantityAttrs;
    }

    public void setQuantityAttrs(JSONObject quantityAttrs) {
        this.quantityAttrs = quantityAttrs;
    }

    public JSONArray getNextStageQuantityAttrs() {
        return nextStageQuantityAttrs;
    }

    public void setNextStageQuantityAttrs(JSONArray nextStageQuantityAttrs) {
        this.nextStageQuantityAttrs = nextStageQuantityAttrs;
    }

    public String getPreviousStage() {
        return previousStage;
    }

    public void setPreviousStage(String previousStage) {
        this.previousStage = previousStage;
    }

    public String getInteractionStage() {
        return interactionStage;
    }

    public void setInteractionStage(String interactionStage) {
        this.interactionStage = interactionStage;
    }

    public String getNextStage() {
        return nextStage;
    }

    public void setNextStage(String nextStage) {
        this.nextStage = nextStage;
    }

    public int getSwipeDirection() {
        return swipeDirection;
    }

    public void setSwipeDirection(int swipeDirection) {
        this.swipeDirection = swipeDirection;
    }

    public JSONObject getQuantityPredictions() {
        return quantityPredictions;
    }

    public void setQuantityPredictions(JSONObject quantityPredictions) {
        this.quantityPredictions = quantityPredictions;
    }

    public JSONArray getNextStageTaggedProduct() {
        return nextStageTaggedProduct;
    }

    public void setNextStageTaggedProduct(JSONArray nextStageTaggedProduct) {
        this.nextStageTaggedProduct = nextStageTaggedProduct;
    }

    public JSONArray getNextStageNewAttributes() {
        return nextStageNewAttributes;
    }

    public void setNextStageNewAttributes(JSONArray nextStageNewAttributes) {
        this.nextStageNewAttributes = nextStageNewAttributes;
    }


}

