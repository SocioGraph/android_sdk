package com.example.admin.daveai.network;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.dialogbox.ProgressLoaderDialog;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONObject;
import okio.Buffer;




public class APICallAsyncTask extends AsyncTask<String, Void, String>  implements DaveConstants {

           // private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
           // RequestBody body_part = RequestBody.create(JSON, body);

            private static final String TAG = "APICallAsyncTask";
           // private ProgressDialog pDialog;
            @SuppressLint("StaticFieldLeak")
            private Context context;
            private Dialog dialog;
            private APIResponse apiResponse;
            private String method;
            private Request request;
            private RequestBody requestBody;
            private boolean progressLoader = false;
            private String strEnterpriseId = null;
            private String userId = null;
            private String apiKey = null;
            private String pushToken = null;
            private String loaderMessage = "";
            private Long timeout = 40L;
            private Response response;
            private DaveSharedPreference sharedPreferences ;
            private int requestCount = 1;




    public APICallAsyncTask(APIResponse OnTaskCompleted, Context context, String method){
        this.apiResponse = OnTaskCompleted;
        this.context=context;
        this.method=method;
    }

    public APICallAsyncTask(APIResponse OnTaskCompleted, Context context, String method,boolean progressLoader){
        this.apiResponse = OnTaskCompleted;
        this.context=context;
        this.method=method;
        this.progressLoader=progressLoader;
    }
    public APICallAsyncTask(APIResponse OnTaskCompleted, Context context, String method, boolean progressLoader, Long timeout){
        this.apiResponse = OnTaskCompleted;
        this.context=context;
        this.method=method;
        this.timeout=timeout;
        this.progressLoader=progressLoader;
    }

    public APICallAsyncTask(APIResponse OnTaskCompleted, Context context, String method,boolean progressLoader,String loaderMessage){
        this.apiResponse = OnTaskCompleted;
        this.context=context;
        this.method=method;
        this.progressLoader=progressLoader;
        this.loaderMessage=loaderMessage;
    }

    public APICallAsyncTask(APIResponse OnTaskCompleted, Context context, String method,boolean progressLoader,String loaderMessage,Long timeout){
        this.apiResponse = OnTaskCompleted;
        this.context=context;
        this.method=method;
        this.progressLoader=progressLoader;
        this.loaderMessage=loaderMessage;
        this.timeout=timeout;
    }

    public APICallAsyncTask(APIResponse response, Context context, String method, RequestBody body_part){
        this.apiResponse = response;
        this.context= context;
        this.method = method;
        this.requestBody= body_part;
    }

    public APICallAsyncTask(APIResponse response, Context context, String method, RequestBody body_part,boolean progressLoader){
        this.apiResponse = response;
        this.context= context;
        this.method = method;
        this.requestBody= body_part;
        this.progressLoader =progressLoader;


    }

    public APICallAsyncTask(APIResponse response, Context context, String method, RequestBody body_part, String loaderMessage){
        this.apiResponse = response;
        this.context= context;
        this.method = method;
        this.requestBody= body_part;
        this.loaderMessage=loaderMessage;
    }

    public APICallAsyncTask(APIResponse response, Context context, String method, RequestBody body_part,boolean progressLoader,Long timeout){
        this.apiResponse = response;
        this.context= context;
        this.method = method;
        this.requestBody= body_part;
        this.progressLoader =progressLoader;
        this.timeout=timeout;


    }



    public APICallAsyncTask(APIResponse response, Context context, String method, RequestBody body_part,boolean progressLoader, String loaderMessage){
        this.apiResponse = response;
        this.context= context;
        this.method = method;
        this.requestBody= body_part;
        this.progressLoader =progressLoader;
        this.loaderMessage=loaderMessage;
    }

    @Override
    protected void onPreExecute() {

        if(context!=null)
            sharedPreferences = new DaveSharedPreference(context);

        if(strEnterpriseId == null)
            strEnterpriseId = sharedPreferences.readString(DaveSharedPreference.EnterpriseId);

        if(userId == null)
             userId = sharedPreferences.readString(DaveSharedPreference.USER_ID);
        if(apiKey == null)
             apiKey = sharedPreferences.readString(DaveSharedPreference.API_KEY);

        if(sharedPreferences.checkStringExist(DaveSharedPreference.PUSH_TOKEN) && pushToken == null)
            pushToken = sharedPreferences.readString(DaveSharedPreference.PUSH_TOKEN);

        // Log.e(TAG, " Print UserId And Api Key :-------------------  " + userId +" == "  +apiKey);

        // Runs on the UI thread before doInBackground()
        if(progressLoader && context!= null){
            ProgressLoaderDialog progressLoaderDialog = new ProgressLoaderDialog(context);
            dialog = progressLoaderDialog.progressDialog();
            if(TextUtils.isEmpty(loaderMessage)) {
                progressLoaderDialog.setMessage("Please wait...");
            }else {
                progressLoaderDialog.setMessage(loaderMessage);
            }
            dialog.setCancelable(false);
            dialog.show();
        }
    }

    @Override
    protected String doInBackground(String... urls) {

        try {
            selectRequestMethod(urls);
            // Response response = OkHttpClientHelper.getInstance(timeout).newCall(request).execute();
            if(request!= null)
                response = OkHttpClientHelper.getInstance(timeout).newCall(request).execute();

            return response.body().string();
        }
        catch (Exception e) {
            e.printStackTrace();

            /*if(response == null){
                try {
                   response = OkHttpClientHelper.getInstance(timeout).newCall(request).execute();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }*/

            Log.e(TAG, " doInBackground  Error :-  " +  e.getMessage() + "   Msg== " + e.toString());
            return e.getMessage();
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        // Runs on UI thread after publishProgress(Progress...) is invoked
        // from doInBackground()
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            boolean activityFinishing = false;
            if(context instanceof Activity){
                if(((Activity)context).isFinishing()){
                    activityFinishing = true;
                }
            }

            if(!activityFinishing && dialog != null && dialog.isShowing())
                dialog.dismiss();

            if(request.body()!= null) {
                final Buffer buffer = new Buffer();
                requestBody.writeTo(buffer);
                Log.e(TAG, "APICallAsyncTask Print Post Body>>>>>>>>>>" + buffer.readUtf8());
            }

            Log.i(TAG, "******onPostExecute ApiCallResponse******** \n1.Requested Url:--  " + request.url() +
                    "\n 2.Request Method:-- " + request.method() + " \n 3.Request Header:------ \n" + request.headers()+ " 4.Response :------"+ result);
            if(response != null) {
                if (response.isSuccessful()) {
                    if (result != null)
                        apiResponse.onTaskCompleted(result);
                }else {
                     Log.e(TAG," APIResponse  not  Successful response code:---------- "+ response.code());
                    // error case
                    int responseCode = response.code();
                    if(responseCode == 400){
                        //Error Syncing with Server: 400: <error message>
                        String printError = new JSONObject(result).getString("error");
                        apiResponse.onTaskFailure(responseCode,"Error Syncing with Server: "+responseCode+": "+ printError);

                    }else if( responseCode == 401 || responseCode == 403){
                        //401, 403 It should be "Error Authenticating : 401: <Error message>"
                        String printError = new JSONObject(result).getString("error");
                        apiResponse.onTaskFailure(responseCode,"Error Authenticating : "+responseCode+": "+ printError);


                    }else if(responseCode == 404){
                        apiResponse.onTaskFailure(responseCode, "404: Requested resource not found");

                    }
                    else if(responseCode ==500){
                        apiResponse.onTaskFailure(responseCode, "500: Problem connecting with server. Some features may not work as expected!");
                    }
                    else if(responseCode ==502){
                        apiResponse.onTaskFailure(responseCode, "502: Problem connecting with server. Some features may not work as expected!");

                    }
                    else if(responseCode ==504){
                        apiResponse.onTaskFailure(responseCode, "504: Problem connecting with server. Some features may not work as expected!");

                    }else{
                        apiResponse.onTaskFailure(responseCode, responseCode+": Problem connecting with server. Some features may not work as expected!");

                    }
                }
            }else {
                //todo retry if failed && timeout or response is null
                /*if(requestCount <= 3){
                    requestCount++;
                    response = OkHttpClientHelper.getInstance(timeout).newCall(request).execute();
                }*/

                if(result.equalsIgnoreCase("timeout")){
                    //result= "Local network error/internet unavailable, please try again later.";
                    apiResponse.onTaskFailure(10, "TimedOut: Problem connecting with server. Some features may not work as expected!");

                }else
                    apiResponse.onTaskFailure(-1, result);
            }
        }catch (Exception e) {
            Log.e(TAG, " Error onPostExecute ***************:-  " + e.toString());
             e.printStackTrace();
        }

    }

    private void selectRequestMethod(String... urls) {

        Request.Builder builder = new Request.Builder();
        builder.url(urls[0]);
        builder.header("Content-Type", "application/json");
        builder.addHeader("X-I2CE-ENTERPRISE-ID", strEnterpriseId);
        if(pushToken!=null && !pushToken.isEmpty())
            builder.addHeader("X-I2CE-PUSH-TOKEN", pushToken);
        if(userId!=null && !userId.isEmpty())
            builder.addHeader("X-I2CE-USER-ID", userId);
        if(apiKey!=null && !apiKey.isEmpty())
            builder.addHeader("X-I2CE-API-KEY", apiKey);

        if(method.equals(APIRequestMethod.POST.getMethod())){
            if (requestBody != null) {
                builder.post(requestBody);
                request = builder.build();
            } else {
                Log.e(TAG, "Error during Post Request  postBody is null.. ");
            }

        }else if(method.equals(APIRequestMethod.UPDATE.getMethod())){
            if(requestBody!=null) {
                builder.patch(requestBody);
                request = builder.build();
            }else {
                Log.e(TAG,"Error during Update Request  postBody is null.. ");
            }
        }
        else if(method.equals(APIRequestMethod.GET.getMethod()) || method.equals(APIRequestMethod.DELETE.getMethod())){
            request = builder.build();
        }


    }


}


 /* switch (response.code()) {
    case 400:
        String printError = new JSONObject(result).getString("error");
        apiResponse.onTaskFailure(response.code(), printError);
        break;

    case 404:
        apiResponse.onTaskFailure(response.code(), "Requested resource not found");
        break;
    case 500:
       // apiResponse.onTaskFailure(response.code(), "server broken");
        apiResponse.onTaskFailure(response.code(), "500: Problem connecting with server. Some features may not work as expected!");

        break;
    case 502:
        // apiResponse.onTaskFailure(response.code(), "Something went wrong at server end...try after sometime..");
         apiResponse.onTaskFailure(response.code(), "502: Problem connecting with server. Some features may not work as expected!");
        break;
    case 504:
        // apiResponse.onTaskFailure(response.code(), "Something went wrong at server end...try after sometime..");
         apiResponse.onTaskFailure(response.code(), "504: Problem connecting with server. Some features may not work as expected!");
         break;
    default:
        //apiResponse.onTaskFailure(response.code(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]");
        apiResponse.onTaskFailure(response.code(), "Problem connecting with server. Some features may not work as expected!");
        break;
}*/
  /*  private void executeCall(Request request, final ResponseListener listener) {
        mOKHttpClient.newCall(request)
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        postFailure(listener, (String) call.request().tag(),e.toString());
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {
                        if(response.isSuccessful()) {
                            String responseString = response.body().string();
                            postSuccess(listener, (String)call.request().tag(), responseString);
                        }
                        else {
                            postFailure(listener, (String)call.request().tag(),response.code()+","+response.message());
                        }
                    }
                });
    }*/

   /*  client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    mMessage = e.toString();
                    Log.e(TAG, mMessage); // no need inside run()
                    // postFailure(listener, (String) call.request().tag(),e.toString());
                    Log.e(TAG, " APICall Error:-  " + e.getMessage() +"  Request Tag==  "+ request.tag() +  "  Msg== "+ e.toString());
                }

                @Override
                public void onResponse(Response response) throws IOException {
                    mMessage = response.toString();
                    Log.i(TAG, mMessage); // no need inside run()
                    if (!response.isSuccessful()) {
                        Log.e(TAG, " APICall Error:-  " + "  ResponseCode==  "+ response.code() +  "  Msg== "+ response.toString());
                        //throw new IOException("Unexpected code " + response);
                       // postFailure(listener, (String)call.request().tag(),response.code()+","+response.message());
                    } else {
                        // do something wih the result
                         String responseString = response.body().string();
                         //postSuccess(listener, (String)call.request().tag(), responseString);
                    }
                }
            });*/



