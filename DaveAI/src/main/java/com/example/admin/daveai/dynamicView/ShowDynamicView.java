package com.example.admin.daveai.dynamicView;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.R;
import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.SMSTemplate;
import com.example.admin.daveai.expandViewRecycler.ViewItem.AudioItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.CallItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.GridviewImageItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.ImageItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.MailItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.MapItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.NumberItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TagTextItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TextItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.UrlItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.VideoviewItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.ViewPagerItem;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.AudioItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.CallItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.GridAdapter;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.GridImageItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.ImageItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.MailItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.MapItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.NumberItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.TextItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.TextTagItemBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.UrlItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.VideoItemViewBinder;
import com.example.admin.daveai.expandViewRecycler.ViewItemBinder.ViewPagerItemViewBinder;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.fragments.MapDetailsMain;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.drakeet.multitype.MultiTypeAdapter;

import static com.example.admin.daveai.daveUtil.DaveConstants.MANIFEST_BASE_URL;
import static com.example.admin.daveai.daveUtil.DaveConstants.MANIFEST_ENTERPRISE_ID;

public class ShowDynamicView {

    private final String TAG = getClass().getSimpleName();
    private Context context;

    public ShowDynamicView(Context context){
        this.context=context;
    }

    public void showDetailsInView(ViewGroup parentLayout, String modelName, List formSequenceList, String objectDetails) {

        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        JSONObject modelResponse = databaseManager.getModelData(modelName);
        if(modelResponse== null || modelResponse.length() == 0) {
            DaveModels daveModels = new DaveModels(context, true);
            try {
                daveModels.getModel(modelName, new DaveAIListener() {
                    @Override
                    public void onReceivedResponse(JSONObject response) {
                        try {
                            Model modelInstance =  Model.getModelInstance(response.toString());
                            JSONObject jsonObjectDetails = new JSONObject(objectDetails);
                            expandMultiView(context, (RecyclerView) parentLayout, formSequenceList, modelInstance, jsonObjectDetails);

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error During Show OBject Details :>>>>>>>>>>>>>>" + e.getLocalizedMessage());
                        }

                    }

                    @Override
                    public void onResponseFailure(int requestCode, String responseMsg) {

                    }
                });
            } catch (DaveException e) {
                e.printStackTrace();
            }

        }
        else{
            try {
                Model modelInstance = Model.getModelInstance(context, modelName);
                JSONObject jsonObjectDetails = new JSONObject(objectDetails);
                // Model modelInstance =  Model.getModelInstance(modelResponse.toString());
                expandMultiView(context, (RecyclerView) parentLayout, formSequenceList, modelInstance, jsonObjectDetails);

            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Error During Show OBject Details :>>>>>>>>>>>>>>" + e.getLocalizedMessage());
            }
        }

    }


    public void expandMultiView(Context context, RecyclerView listExpandRecycler, List dataSequenceList,
                                Model modelInstance, JSONObject objectDetails) {

        this.context = context;
        Log.e(TAG,"expandMultiView objectDetails:--------"+objectDetails);
       /* List<String> TEXT_VIEW_OPTION = Arrays.asList("text", "textarea", "password", "email",
                "number", "tel", "typeahead", "select", "number","datetime");*/
        List<String> TEXT_VIEW_OPTION = Arrays.asList("text", "textarea", "password",
                "number", "typeahead", "select", "number","datetime","date","time");
        List<String> TAG_VIEW_OPTION = Arrays.asList("tags", "typeahead-tags");

        ArrayList<Object> items = new ArrayList<>();
        MultiTypeAdapter adapter = new MultiTypeAdapter();
        adapter.register(TextItem.class, new TextItemViewBinder());
        adapter.register(ImageItem.class, new ImageItemViewBinder(context));
        adapter.register(NumberItem.class, new NumberItemViewBinder());
        adapter.register(ViewPagerItem.class, new ViewPagerItemViewBinder(context));
        adapter.register(VideoviewItem.class, new VideoItemViewBinder(context));
        adapter.register(AudioItem.class, new AudioItemViewBinder(context));
        adapter.register(MapItem.class, new MapItemViewBinder(context));
        adapter.register(TagTextItem.class, new TextTagItemBinder());
        adapter.register(CallItem.class, new CallItemViewBinder(context));
        adapter.register(MailItem.class, new MailItemViewBinder(context));
        adapter.register(GridviewImageItem.class, new GridImageItemViewBinder(context));
        adapter.register(UrlItem.class, new UrlItemViewBinder(context));
        listExpandRecycler.setAdapter(adapter);
        ArrayList<String> strImage = new ArrayList<>();

        try {
            for (int i = 0; i < dataSequenceList.size(); i++) {
                String key = dataSequenceList.get(i).toString();
                String ui_element = modelInstance.getAttributeUIElement(key);

                StringBuilder stringBuilder = new StringBuilder();
                if (ui_element != null && objectDetails.has(key)  && !objectDetails.isNull(key) ) {

                    if (TEXT_VIEW_OPTION.contains(ui_element)) {
                        items.add(new TextItem(modelInstance.getAttributeTitle(key), objectDetails.getString(key)));

                    }
                    else if (TAG_VIEW_OPTION.contains(ui_element)) {

                        JSONArray getDescription = objectDetails.getJSONArray(key);
                        StringBuilder builder = new StringBuilder();
                        for (int j = 0; j < getDescription.length(); j++) {
                            builder.append(getDescription.get(j).toString());
                            // builder.append(" *");
                            builder.append("\n");
                        }
                        //Log.e(TAG,"getDescription 2:-------------"+builder);
                        //items.add(new TagTextItem(modelObject.getAttributeTitle(key), builder.toString()));
                        items.add(new TagTextItem(modelInstance.getAttributeTitle(key), getDescription));
                    }
                    else if (ui_element.equalsIgnoreCase("image")) {

                        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();

                        if(daveAIPerspective.getProduct_card_header()!=null && objectDetails.has(daveAIPerspective.getProduct_card_header()) &&
                                !objectDetails.isNull(daveAIPerspective.getProduct_card_header())) {
                            stringBuilder.append(daveAIPerspective.getProduct_card_header().replaceAll("-"," ") +": "+objectDetails.getString(daveAIPerspective.getProduct_card_header()) +",\n");
                        }
                        if(daveAIPerspective.getProduct_card_sub_header()!=null && objectDetails.has(daveAIPerspective.getProduct_card_sub_header()) &&
                                !objectDetails.isNull(daveAIPerspective.getProduct_card_sub_header())) {
                            stringBuilder.append(daveAIPerspective.getProduct_card_sub_header().replaceAll("-"," ") +": "+objectDetails.getString(daveAIPerspective.getProduct_card_sub_header()) + "\n");
                        }
                        if(daveAIPerspective.getShare_image_attributes()!=null && daveAIPerspective.getShare_image_attributes().size()>0){

                            ArrayList<String> attrs = daveAIPerspective.getShare_image_attributes();
                            for(String attr : attrs){
                                if(objectDetails.has(attr)){
                                    try {
                                        stringBuilder.append(attr + " : " + objectDetails.get(attr) + "\n");
                                    }catch (Exception e){
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }else{
                            try {
                                String viewUrl = MultiUtil.getManifestMetadata(context, MANIFEST_BASE_URL) + "/view-product/" + MultiUtil.getManifestMetadata(context, MANIFEST_ENTERPRISE_ID) + "/" + objectDetails.getString(modelInstance.getModelIDAttrName(new ModelUtil(context).getProductModelName()));
                                stringBuilder.append("\n View Details:" + Uri.parse(viewUrl));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        items.add(new ImageItem(objectDetails.getString(key),stringBuilder.toString()));

                    }
                    else if (ui_element.equalsIgnoreCase("thumbnails")) {

                        JSONArray gridJsonList = objectDetails.getJSONArray(key);
                        if(gridJsonList!=null && gridJsonList.length()>0){
                            items.add(new GridviewImageItem(gridJsonList));
                        }
                    }
                    else if (ui_element.equalsIgnoreCase("slideshow")) {
                        if (objectDetails.has(key)) {
                            try {
                                JSONArray itemArray = objectDetails.getJSONArray(key);
                                for (int i1 = 0; i1 < itemArray.length(); i1++) {
                                    strImage.add(itemArray.getString(i1));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        items.add(new ViewPagerItem(modelInstance.getAttributeTitle(key),strImage));
                    }
                    else if (ui_element.equalsIgnoreCase("geolocation")) {
                        String striLoc = objectDetails.getString(key);
                        Log.e(TAG,"Show GeoLocation:-----------------"+ striLoc);
                        String[] parts = striLoc.split(",");
                        String lat = parts[0];
                        String lon = parts[1];
                        items.add(new MapItem(Double.parseDouble(lat), Double.parseDouble(lon)));
                    }
                    else if (ui_element.equalsIgnoreCase("video")) {
                        items.add(new VideoviewItem(objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("audio")) {
                        items.add(new AudioItem(objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("tel")) {
                        items.add(new CallItem(modelInstance.getAttributeTitle(key), objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("email")) {
                        items.add(new MailItem(modelInstance.getAttributeTitle(key), objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("url")) {
                        items.add(new UrlItem(modelInstance.getAttributeTitle(key), objectDetails.getString(key)));
                    }
                }/*else{
                    Log.e(TAG,"Error during showing view ViewNAme:-"+key +"  UIElement:-"+ ui_element+" isExist:-"+objectDetails.has(key));
                }*/
            }

            adapter.setItems(items);
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void showDetailsInMultiView(LinearLayout linearLayout, List detailSequenceList, Model modelInstance, JSONObject objectDetails) {
        try {
            linearLayout.removeAllViews();
            List<String> TEXT_VIEW_OPTION = Arrays.asList("text", "textarea", "password", "number", "typeahead", "select", "number","datetime","date","time");
            List<String> TAG_VIEW_OPTION = Arrays.asList("tags", "typeahead-tags");

            for (int i = 0; i < detailSequenceList.size(); i++) {

                String key = detailSequenceList.get(i).toString();
                String ui_element = modelInstance.getAttributeUIElement(key);

                if (ui_element != null && objectDetails.has(key)  && !objectDetails.isNull(key)) {
                    //Log.i(TAG, "*****showDetailsInMultiView AttrName : " + key + "   **********UIElement: " + ui_element+"  ******** value: "+objectDetails.get(key));
                    if (TEXT_VIEW_OPTION.contains(ui_element)) {
                        linearLayout.addView(showInTextView(modelInstance.getAttributeTitle(key),objectDetails.getString(key)));
                    }
                    else if (TAG_VIEW_OPTION.contains(ui_element)) {
                        JSONArray getDescription = objectDetails.getJSONArray(key);
                        linearLayout.addView(showInTagTextView(modelInstance.getAttributeTitle(key),getDescription));
                    }
                    else if (ui_element.equalsIgnoreCase("image")) {
                        linearLayout.addView(showInImageView(objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("thumbnails")) { //image objects
                        JSONArray gridJsonList = objectDetails.getJSONArray(key);
                        if(gridJsonList!=null && gridJsonList.length()>0){
                            linearLayout.addView(showGridView(gridJsonList));
                        }
                    }
                    else if (ui_element.equalsIgnoreCase("slideshow")) { //imageList
                        ArrayList<String> imageList = new ArrayList<>();
                        if (objectDetails.has(key)) {
                            try {
                                JSONArray itemArray = objectDetails.getJSONArray(key);
                                for (int i1 = 0; i1 < itemArray.length(); i1++) {
                                    imageList.add(itemArray.getString(i1));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        linearLayout.addView(showViewPagerView(modelInstance.getAttributeTitle(key),imageList));
                    }
                    else if (ui_element.equalsIgnoreCase("geolocation")) {
                        String striLoc = objectDetails.getString(key);
                        String[] parts = striLoc.split(",");
                        String lat = parts[0];
                        String lon = parts[1];
                        linearLayout.addView(showMapView(Double.parseDouble(lat), Double.parseDouble(lon)));
                    }
                    else if (ui_element.equalsIgnoreCase("video")) {
                        linearLayout.addView(showVideoView(objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("audio")) {

                        linearLayout.addView(showAudioView(objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("tel")) {
                        linearLayout.addView(showPhoneCallView(modelInstance.getAttributeTitle(key),objectDetails.getString(key)));
                    }
                    else if (ui_element.equalsIgnoreCase("email")) {
                        linearLayout.addView(showEmailView(modelInstance.getAttributeTitle(key),objectDetails.getString(key)));
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG,"Error during show details view:----------"+e.getMessage());
        }
    }

    private View showInTextView(String keyName , String keyValue){

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.item_text, null);
        TextView keyTitle=  view.findViewById(R.id.key_title);
        TextView textValue = view.findViewById(R.id.key_value);

        if (keyName!=null && !keyName.isEmpty()) {
            keyTitle.setText(keyName);
        }

        if (keyValue!=null && !keyValue.isEmpty()) {
            textValue.setText(keyValue);
        }
        return view;
    }

    private View showInTagTextView(String keyName ,JSONArray jsonArray){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.item_tag_text, null);
        TextView keyTitle=  view.findViewById(R.id.key_title);
        TextView textValue = view.findViewById(R.id.key_value);

        if (keyName!=null && !keyName.isEmpty()) {
            keyTitle.setText(keyName);
        }
        textValue.setText("");
        try {
            for (int j = 0; j < jsonArray.length(); j++){
                textValue.append(jsonArray.getString(j) + "\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return view;
    }

    private View showInImageView(String imageUrl ){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.item_image, null);
        ImageView imageView =  view.findViewById(R.id.image);
        ImageView shareImage =  view.findViewById(R.id.shareImage);
        ImageView zoomImage =  view.findViewById(R.id.zoomImage);

        if (imageUrl!= null && !imageUrl.isEmpty()) {
            //MultiUtil.setImageUsingGlide(context,imageUrl,imageView);
            Glide.with(context)
                    .load(imageUrl)
                    .asBitmap()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.no_image)
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageView);
        }

        shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiUtil crudUtil = new MultiUtil(context);
                Uri bitmap = crudUtil.getLocalBitmapUri(context,imageView);
                Log.e(TAG,"print bitmap in image :-------"+bitmap);
                if(bitmap!=null)
                    crudUtil.shareImage(context,bitmap);
            }
        });

        zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle b=new Bundle();
                b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.IMAGE.toString());
                b.putString(Visualization.IMAGE_URL,imageUrl);

                Intent intent = new Intent(context, Visualization.class);
                intent.putExtras(b);
                context.startActivity(intent);

            }
        });

        return view;
    }

    private View showGridView(JSONArray gridJsonList){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.image_grid_layout, null);

        RecyclerView gridRecycleview = view.findViewById(R.id.gridRecycleview);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(context, 3);
        gridRecycleview.setLayoutManager(mLayoutManager);

        GridAdapter alertsAdapter = new GridAdapter(context, gridJsonList);
        gridRecycleview.setAdapter(alertsAdapter);

        return view;
    }

    private View showViewPagerView(String ketTitle,ArrayList<String> imageList){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.item_view_pager, null);

        TextView imageTitle = view.findViewById(R.id.imageTitle);
        ImageView slidingImage = view.findViewById(R.id.slideImage);
        imageTitle.setText(ketTitle);
        int currentImageIndex = 0;
        final Handler mHandler = new Handler();
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
                try {
                    if (context != null) {
                        Glide.with(context)
                                .load(imageList.get(currentImageIndex))
                                .placeholder(R.drawable.no_image)
                                .error(R.drawable.no_image)
                                .into(slidingImage);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG,"error during load image using Glide in ViewPager(Slideshow):--- "+e.getMessage());
                }

                /*if (imageList.size() - 1 == currentImageIndex) {
                    currentImageIndex = 0;
                } else {
                    currentImageIndex++;
                }*/
            }
        };

        int delay = 1000;
        int period = 3000;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                mHandler.post(mUpdateResults);
            }
        }, delay, period);

        return view;
    }

    private View showMapView(double latitude, double longitude){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.map_view_layout, null);
        FrameLayout frameLayout = view.findViewById(R.id.mapCevent);

        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.mapCevent, mapFragment).commit();
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                int height = frameLayout.getWidth();
                frameLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,height));
                LatLng latLng = new LatLng(latitude, longitude);

                googleMap.addMarker(new MarkerOptions().position(latLng));
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
            }
        });
        return view;

    }

    private View showVideoView(String strVideoUrl){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.videoview_layout, null);

        VideoView showVideoView = view.findViewById(R.id.showVideoView);

        Uri uri = Uri.parse(strVideoUrl);
        showVideoView.setVideoURI(uri);

        showVideoView.start();
        showVideoView.setVideoPath(strVideoUrl);
        MediaController mediaController = new MediaController(context);
        mediaController.setAnchorView(showVideoView);
        showVideoView.setMediaController(mediaController);
        showVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

            }
        });
        showVideoView.start();

        return view;

    }


    private View showAudioView(String strAudioUrl){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.audio_layout, null);

        ImageView audioBtn = view.findViewById(R.id.audioBtn);
        ImageView pauseBtn = view.findViewById(R.id.pauseBtn);

        MediaPlayer mediaplayer = new MediaPlayer();
        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        audioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mediaplayer.setDataSource(strAudioUrl);
                    mediaplayer.prepare();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mediaplayer.start();
            }
        });

        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaplayer.pause();
            }
        });

        return view;

    }


    private View showPhoneCallView(String title ,String value){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.call_item_text, null);

        TextView key_title = view.findViewById(R.id.key_title);
        TextView key_value = view.findViewById(R.id.key_value);
        ImageButton btnSendSMS = view.findViewById(R.id.btnSendSMS);

        key_title.setText(title);
        key_value.setText(value);
        key_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ClikingText", "onClick: ");
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + value));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.CALL_PHONE},
                            1);
                    return;
                }
                context.startActivity(intent);
            }
        });

        btnSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("btnSendSMS", "onClick: ");
                //TODO inplement call back func to open SMS Template
                SMSTemplate cdd = new SMSTemplate(context,value,SMSTemplate.SEND_SMS_TEMPLATE);
                cdd.show();


            }
        });
        return view;

    }

    private View showEmailView(String title ,String value){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View  view = inflater.inflate(R.layout.mail_item_text, null);

        TextView mail_title = view.findViewById(R.id.mail_title);
        TextView mail_value = view.findViewById(R.id.mail_value);

        mail_title.setText(title);
        mail_value.setText(value);
        mail_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick mail view: ");
                //TODO inplement call back func to open SMS Template
                SMSTemplate cdd = new SMSTemplate(context,value,SMSTemplate.SEND_MAIL_TEMPLATE);
                cdd.show();

             /*   Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{mailItem.getText_value()});
                i.putExtra(Intent.EXTRA_SUBJECT, "");
                i.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(i, "Email:"));*/

            }
        });

        return view;

    }
}
