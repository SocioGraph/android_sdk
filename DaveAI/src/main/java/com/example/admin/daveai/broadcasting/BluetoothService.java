package com.example.admin.daveai.broadcasting;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.MultiUtil;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Set;
import java.util.UUID;



public class BluetoothService implements DaveConstants {

    public static final String TAG = "BluetoothService";

    public static final int REQUEST_ENABLE_BT = 1;

    private Context context;
    String bluetoothMessage  = "";
    private Set<BluetoothDevice> set_pairedDevices;
    private BluetoothAdapter bluetoothAdapter;

    private int mState;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private String fileName = null;


    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED  =  3; // now connected to a remote device

    public static final int NO_SOCKET_FOUND = 4;
    public static final int CONNECTION_FAILED = 5;
    public static final int MESSAGE_READ = 6;
    public static final int MESSAGE_WRITE = 7;



     private static  UUID MY_UUID ;
    //public static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

   // public static final UUID MY_UUID = UUID.fromString("00001105-0000-1000-8000-00805f9b34fb");
    //private static final UUID MY_UUID = UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    //UUID MY_UUID = UUID.fromString("33bb1246-1472-11e5-b60b-1697f925ec7b");
    //private static final UUID MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");


    public BluetoothService(Context context){
        this.context = context;
        mState = STATE_NONE;

        String appId = "com.dave."+ MultiUtil.getManifestMetadata(context,MANIFEST_ENTERPRISE_ID);
        MY_UUID = UUID.nameUUIDFromBytes(appId.getBytes());

        Log.e(TAG,"*********************Print appId :--  "+appId +"  UUID: "+ MY_UUID);


    }

   /* private BluetoothService(Context context, String appId){
        this.context = context;
        mState = STATE_NONE;

        UUID uuid = UUID.nameUUIDFromBytes(appId.getBytes());
        Log.e(TAG,"Print UUID:--  "+uuid);


    }*/

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg_type) {
            super.handleMessage(msg_type);

            switch (msg_type.what){

                case STATE_CONNECTING:
                    Log.d(TAG,"HandlerMessage****************Connecting-------");
                    break;

                case STATE_CONNECTED:
                    Log.d(TAG,"HandlerMessage****************Connected-------");
                    break;


                case NO_SOCKET_FOUND:
                    Log.d(TAG,"HandlerMessage****************No socket found-------");
                    break;

                case CONNECTION_FAILED:
                    Log.d(TAG,"HandlerMessage****************Connection Failed : " +msg_type.obj.toString());
                    break;

                case MESSAGE_WRITE:

                    Log.d(TAG,"HAndler MESSAGE_WRITE:------------");
                    if(msg_type.obj!=null){
                        ConnectedThread connectedThread = new ConnectedThread((BluetoothSocket)msg_type.obj);
                       connectedThread.write(bluetoothMessage.getBytes());
                        /*try {
                           byte[] b = bluetoothMessage.getBytes("UTF-8");
                            connectedThread.write(b);
                        } catch (UnsupportedEncodingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }*/

                    }


                    break;

                case MESSAGE_READ:

                    Log.d(TAG,"*******************MESSAGE_READ in bytes:-----------"+ msg_type.obj);
                    if(msg_type.obj != null){
                        byte[] readbuf = (byte[])msg_type.obj;
                        String string_recieved = new String(readbuf);
                        //String string_recieved = msg_type.obj.toString();


                       /* byte[] writeBuf = (byte[]) msg_type.obj;
                        int begin = (int)msg_type.arg1;
                        int end = (int)msg_type.arg2;

                        String writeMessage = new String(writeBuf);
                        writeMessage = writeMessage.substring(begin, end);*/

                        Log.e(TAG,"*******************MESSAGE_READ in bytes:-----------"+ string_recieved);

                        //do some task based on recieved string
                        if(onAcceptMessageListener!=null){
                            onAcceptMessageListener.onMessageAccepted(string_recieved);
                        }

                    }

                    break;
            }
        }
    };

    private OnAcceptMessageListener onAcceptMessageListener;
    public interface OnAcceptMessageListener {
        void onMessageAccepted(String message);

    }

    public void registerAcceptMessageCallback(OnAcceptMessageListener callbackClass){
        this.onAcceptMessageListener = callbackClass;
    }

    /**
     *  start AcceptThread to begin a session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void startAcceptConnection() {
        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to listen on a BluetoothServerSocket
        if(checkBlueToothState()) {
            if (mAcceptThread == null) {
                mAcceptThread = new AcceptThread();
                mAcceptThread.start();
            }
           /* AcceptThread acceptThread = new AcceptThread();
            acceptThread.start();*/
        }

    }


    private synchronized void startAcceptConnection(OnAcceptMessageListener onAcceptMessageListener) {

        this.onAcceptMessageListener = onAcceptMessageListener;
        Log.e(TAG,"startAcceptConnection  getCureent State:------------"+ mState);
        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to listen on a BluetoothServerSocket
        if(checkBlueToothState()) {
            if (mAcceptThread == null) {
                mAcceptThread = new AcceptThread();
                mAcceptThread.start();
            }
        }

    }

    public void sendMessgaeToPairedDevice(String bluetoothMessage) {
        this.bluetoothMessage = bluetoothMessage;
        if(checkBlueToothState()){

            set_pairedDevices = bluetoothAdapter.getBondedDevices();
            Log.e(TAG,"sendMessgaeToPairedDevice  Print paired device:------------"+ set_pairedDevices);
            if (set_pairedDevices.size() > 0) {
                for (BluetoothDevice device : set_pairedDevices) {
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    Log.e(TAG,"get Pair Device name:- "+device.getName() +" Device Address:- "+device.getAddress());
                    connect(device);
                }
            }
        }

    }


    //connect with all paired Device
    public void connectWithPairedDevice(){

        if(checkBlueToothState()){
            Log.e(TAG,"connectWithPairedDevice  getCureent State:------------"+ mState);
            set_pairedDevices = bluetoothAdapter.getBondedDevices();
            if (set_pairedDevices.size() > 0) {
                for (BluetoothDevice device : set_pairedDevices) {
                    String deviceName = device.getName();
                    String deviceHardwareAddress = device.getAddress(); // MAC address
                    Log.e(TAG,"get Pair Device name:- "+device.getName() +" Device Address:- "+device.getAddress());
                    //todo connect all device
                    connect(device);
                }
            }
        }

    }

    /**
     * Check device has Bluetooth and that it is turned on.
     */
    private boolean checkBlueToothState()
    {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device doesn't support Bluetooth
            Toast.makeText(context,"Your Device doesn't support bluetooth. you can play as Single player",Toast.LENGTH_SHORT).show();
            Log.e(TAG,"<<<<<<<<Your Device doesn't support bluetooth. you can play as Single player>>>>>");
        }
        else {
            if (bluetoothAdapter.isEnabled()) {
                Log.e(TAG,"**************Bluetooth is  enabled*****************");
                return true;
            }else {
                /* Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
               */
                Log.e(TAG,"**************Bluetooth is not enable***************** :)");
                Toast.makeText(context,"Please enable Bluetooth",Toast.LENGTH_LONG).show();
            }

        }
        return false;
    }

    /**
     * Set the current state of the chat connection
     *
     * @param state An integer defining the current connection state
     */
    private synchronized void setState(int state) {
        mState = state;
        // Give the new state to the Handler so the UI Activity can update
        //mHandler.obtainMessage(BluetoothChat.MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Return the current connection state.
     */
    public synchronized int getState() {
        return mState;
    }


    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);

       /* ConnectThread connectThread = new ConnectThread(device);
        connectThread.start();*/
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that has been connected
     */
    private synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {

        // Cancel the connect thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        // Cancel the accept thread because we only want to connect to one device
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();
        setState(STATE_CONNECTED);
        Log.e(TAG,"ENd connected******************* "+ mAcceptThread);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
   /* public void write(byte[] out) {
        // Create temporary object
        ConnectedThread r;
        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if (mState != STATE_CONNECTED) return;
            r = mConnectedThread;
        }
        // Perform the write unsynchronized
        r.write(out);
    }*/



   /**
     * Stop all threads
     */
    public synchronized void stop() {

        if (mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }
        if (mAcceptThread != null) {
            mAcceptThread.cancel();
            mAcceptThread = null;
        }
        setState(STATE_NONE);
    }


    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
        setState(STATE_LISTEN);
        // Send a failure message back to the Activity
      /*  Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothChat.TOAST, "Unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);*/
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private void connectionLost() {
        setState(STATE_LISTEN);
        // Send a failure message back to the Activity
        /*Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothChat.TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);*/
    }





    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    public class AcceptThread extends Thread {

        private final BluetoothServerSocket serverSocket;

        public AcceptThread() {
            BluetoothServerSocket tmp = null;
            try {
                // MY_UUID is the app's UUID string, also used by the client code
               // tmp = bluetoothAdapter.listenUsingRfcommWithServiceRecord("NAME",MY_UUID);
                tmp = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord("NAME",MY_UUID);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e){
                e.printStackTrace();

            }
            serverSocket = tmp;
        }

        public void run() {
            setName("AcceptThread");
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned
            while (true) {
                try {
                    Log.e(TAG,"AcceptThread *************AcceptThread running**************** "+ serverSocket);
                    socket = serverSocket.accept();

                } catch (IOException e) {
                    e.printStackTrace();

                    break;

                }catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG,"AcceptThread Exception :---------------"+e.getMessage());
                    break;
                }
                Log.e(TAG,"AcceptThread***********connection was accepted**************** "+ socket);
                // If a connection was accepted
                if (socket != null) {
                    // Do work to manage the connection (in a separate thread)
                    mHandler.obtainMessage(STATE_CONNECTED).sendToTarget();
                    // Start the Connected thread to perform transmissions
                   //// ConnectedThread connectedThread = new ConnectedThread(socket);
                   // connectedThread.start();

                    connected(socket,socket.getRemoteDevice());
                    // Reset the AcceptThread because we're done
                    synchronized (BluetoothService.this) {
                        mAcceptThread = null;
                    }

                    try {
                        serverSocket.close();
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }



                }

            }


        }

        public void cancel() {
            try {
                serverSocket.close();
                Log.d(TAG,"*****************Accept Thread is closed*******************");
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG,"Error closing Accept Thread +++++++++++++++++++  " + e.getMessage());
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket,
            // because mmSocket is final
            BluetoothSocket tmp = null;
            mmDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code

                ParcelUuid[] uuids = device.getUuids();
                //UUID MY_UUID = uuids[1].getUuid();
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
                //tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);

            }
            catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG,"ConnectThread Error Connect thread socket not created ************ "+e.getMessage());
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it will slow down the connection
            bluetoothAdapter.cancelDiscovery();
            try {
                // Connect the device through the socket. This will block
                // until it succeeds or throws an exception
                mHandler.obtainMessage(STATE_CONNECTING).sendToTarget();
                Log.e(TAG,"***************Start connect with Device>>>>>>>>>>>>>>>>>>>> " +mmDevice.getName());
                mmSocket.connect();
                Log.e(TAG,"***************ConnectThread Connected Device>>>>>>>>>>>>>>>>>>>> " +mmDevice.getName());
            }
            catch (IOException connectException) {
                connectException.printStackTrace();
                connectionFailed();
                // Unable to connect; close the socket and get out
                try {
                    mmSocket.close();
                    Log.e(TAG," ConnectThread Cannot connect closing connection of device************* "+mmDevice.getName() +" "+ connectException.getMessage());
                } catch (IOException closeException) {
                    closeException.printStackTrace();
                    Log.e(TAG," ConnectThread Socket not closed for device************* "+ mmDevice.getName());
                }
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothService.this) {
                mConnectThread = null;
            }

            // Do work to manage the connection (in a separate thread)
             mHandler.obtainMessage(MESSAGE_WRITE,mmSocket).sendToTarget();
            // Start the connected thread
            //connected(mmSocket, mmDevice);


        }

        /** Will cancel an in-progress connection, and close the socket */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();

            } catch (IOException e) {
                 e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
           // byte[] buffer = new byte[16*1024];
           // byte[] buffer = new byte[0xFFFF]; // buffer store for the stream
            int bytes=0; // bytes returned from read()
            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {

                    Log.e(TAG,"Print buffer length :-------------------"+ buffer.length);
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    String read = new String(buffer, 0, bytes);

                    String receivedMsg = new String(buffer);
                    Log.e(TAG,"Read data from accept message:------------ " + bytes +" "+ buffer.length+" msg:- "+  receivedMsg );

                    // Send the obtained bytes to the UI activity
                     //mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                    // mHandler.obtainMessage(MESSAGE_READ ,receivedMsg).sendToTarget();
                     mHandler.obtainMessage(MESSAGE_READ, bytes, -1, read.getBytes()).sendToTarget();

                     cancel();

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "IOException reading Input Stream. " + e.getMessage() );
                    break;
                }

            }


            /*int begin = 0;
            int bytes = 0;
            while (true) {
                try {
                    bytes += mmInStream.read(buffer, bytes, buffer.length - bytes);
                    Log.e(TAG,"Print bytes************** "+ bytes);
                    for(int i = begin; i < bytes; i++) {
                        Log.e(TAG,"Print********************* "+ buffer[i] +"  "+ "#".getBytes()[0]);
                        if(buffer[i] == "#".getBytes()[0]) {
                            mHandler.obtainMessage(MESSAGE_READ, begin, i, buffer).sendToTarget();

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }*/




        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {

            String text = new String(bytes, Charset.defaultCharset());
            try {
                mmOutStream.write(bytes);

                Log.e(TAG, "<<<<<<<<<<<<<Data Send to remote Service :  " + bytes.length + "  "+ text);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG, " Error Couldn't send data to the other device " + e.getMessage());
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
                Log.e(TAG,"Close Connected thread************************");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveFileInDaveAIFolder(String sBody) {

    /*    SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd");
        Date now = new Date();
        String sFileName = formatter.format(now) + ".txt"; //like 2016_01_12.txt

        if(fileName==null || fileName.isEmpty())
            fileName = System.currentTimeMillis()+".txt";*/

        try {
            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/daveAI/received_files/");
            if (!folder.exists())
                folder.mkdirs();

            String finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/daveAI/received_files/" + fileName;
            File gpxfile = new File(finalFileName);

            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();


        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}
