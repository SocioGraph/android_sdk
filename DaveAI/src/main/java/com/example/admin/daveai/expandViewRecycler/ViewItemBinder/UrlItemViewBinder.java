/*
 * Copyright 2016 drakeet. https://github.com/drakeet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.TextItem;
import com.example.admin.daveai.expandViewRecycler.ViewItem.UrlItem;

import me.drakeet.multitype.ItemViewBinder;

public class UrlItemViewBinder extends ItemViewBinder<UrlItem, UrlItemViewBinder.UrlHolder> {
    private Context context;

    public UrlItemViewBinder(Context context) {
        this.context = context;
    }

    @Override
    protected @NonNull
    UrlHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.url_view_layout, parent, false);
        return new UrlHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull UrlHolder holder, @NonNull UrlItem urlData) {

        holder.key_value.setText(urlData.getText_value());
        holder.key_title.setText(urlData.getText_key());
    }

    static class UrlHolder extends RecyclerView.ViewHolder {

        private TextView key_title, key_value;


        UrlHolder(View itemView) {
            super(itemView);

            this.key_title = itemView.findViewById(R.id.key_title);
            this.key_value = itemView.findViewById(R.id.key_value);
        }
    }
}
