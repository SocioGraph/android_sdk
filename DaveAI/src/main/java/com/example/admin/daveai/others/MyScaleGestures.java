package com.example.admin.daveai.others;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;


/**
 * Created by Rinki on 05,July,2019
 */
public class MyScaleGestures  extends android.support.v7.widget.AppCompatImageView
        implements View.OnTouchListener , ScaleGestureDetector.OnScaleGestureListener {


    private static final String TAG = "MyScaleGestures";
    private View view;
    private ScaleGestureDetector scaleGestureDetector;
    private float scaleFactor = 1;

    public MyScaleGestures(Context context) {
        super(context);
        scaleGestureDetector = new ScaleGestureDetector(context, this);
        //Log.d(TAG,"gestureScale***********:- " +scaleGestureDetector);
    }

  /*  @Override
    public boolean onTouchEvent(MotionEvent event) {

        int action = event.getActionMasked();
        Log.e(TAG, "Action ***************"+String.valueOf(action));
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }*/

    /* @Override
    public boolean performClick() {
        // Calls the super implementation, which generates an AccessibilityEvent
        // and calls the onClick() listener on the view, if any
        super.performClick();

        // Handle the action for the custom click here

        return true;
    }*/

    /**
     *  this method will be invoked when the listener detect pinch zoom gesture.
     * @param view view
     * @param event event
     * @return return boolean value
     */
    @Override
    public boolean onTouch(View view, MotionEvent event) {
       // Log.d(TAG,"onTouch******onTouch************:- " +view);

        this.view = view;
        view.performClick();
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {

        scaleFactor *= detector.getScaleFactor();

        Log.e(TAG,"onScale before ******************:- "+ scaleFactor +""+ detector.getScaleFactor() );
        scaleFactor = (scaleFactor < 1 ? 1 : scaleFactor); // prevent our view from becoming too small //
        scaleFactor = ((float)((int)(scaleFactor * 100))) / 100; // Change precision to help with jitter when user just rests their fingers
        Log.e(TAG,"onScale After  ******************:- "+ scaleFactor  );

        view.setScaleX(scaleFactor);
        view.setScaleY(scaleFactor);

       /* float scaleFactor = detector.getScaleFactor();
        scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
        matrix.setScale(scaleFactor, scaleFactor);
        imageView.setImageMatrix(matrix);*/
        return true;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        Log.e(TAG,"onScaleBegin******************:- " +detector);

        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
        Log.e(TAG,"onScaleEnd******************:- " +view);

    }





}



