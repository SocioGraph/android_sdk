package com.example.admin.daveai.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by daveai on 21/3/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper implements DatabaseConstants{

    private static final int DATABASE_VERSION = 2;


    private  final String TAG = getClass().getSimpleName();

    private static DatabaseHelper mDatabaseInstance = null;
    private Context mContext;

    public static DatabaseHelper newInstance(Context context){
        if (mDatabaseInstance == null){
            mDatabaseInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return mDatabaseInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG,"DatabaseHelper OnCreate() called");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG,"DatabaseHelper OnUpgrade() called for old ver = "+oldVersion+"  new ver = "+newVersion);
        if (newVersion > oldVersion) {
            db.execSQL("ALTER TABLE "+META_TABLE_NAME+" ADD COLUMN "+MASTER_TIMESTAMP+" BIGINT DEFAULT 0");
        }

        //        db.execSQL("some sql statement to do something");
    }

}
