package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.AudioItem;

import java.io.IOException;

import me.drakeet.multitype.ItemViewBinder;


public class AudioItemViewBinder extends ItemViewBinder<AudioItem, AudioItemViewBinder.AudioHolder>

{
    MediaPlayer mediaplayer;
    private Context context;

    public AudioItemViewBinder(Context context) {
        this.context = context;

    }

    @Override
    protected @NonNull
    AudioItemViewBinder.AudioHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = LayoutInflater.from(context).inflate(R.layout.audio_layout, parent, false);
        return new AudioItemViewBinder.AudioHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull AudioItemViewBinder.AudioHolder holder, @NonNull final AudioItem audioItem) {
        mediaplayer = new MediaPlayer();
        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        holder.audioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    mediaplayer.setDataSource(audioItem.getStrVideoUrl());
                    mediaplayer.prepare();
                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                mediaplayer.start();
            }
        });

        holder.pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaplayer.pause();
            }
        });


    }

    @Override
    public void onViewDetachedFromWindow(@NonNull AudioItemViewBinder.AudioHolder holder) {
        holder.itemView.clearAnimation();
    }


    static class AudioHolder extends RecyclerView.ViewHolder {

        ImageView audioBtn, pauseBtn;

        AudioHolder(@NonNull View itemView) {
            super(itemView);


            this.audioBtn = itemView.findViewById(R.id.audioBtn);
            this.pauseBtn = itemView.findViewById(R.id.pauseBtn);

        }
    }
}

