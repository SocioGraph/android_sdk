package com.example.admin.daveai.database;

import android.content.Context;
import android.util.Log;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.InteractionStages;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.DaveAIPerspective;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecommendationHelper implements DatabaseConstants, DaveConstants {

    private final static String TAG = "RecommendationHelper";
    private final String SORT_BY_POSITIVE = "first_positive_stage";
    private final String SORT_BY_NEGATIVE = "first_negative_stage";

    private Context mContext;
    private ModelUtil modelUtil;
    private DaveAIPerspective daveAIPerspective;

    private JSONObject finalRecommObjects = new JSONObject();
    private JSONArray  jsonArrayRecommendations = new JSONArray();
    private String defaultNextStage = "";
    private String defaultPreviousStage = "";

    private String strCustomerId = "";
    private String strProductId = "";
    private int objectCount = 0;
    private int totalObjectCount = 0;
    private DatabaseManager databaseManager;


    public RecommendationHelper(Context context, String customerId, String productId) {
        this.mContext = context;
        this.strCustomerId = customerId;
        this.strProductId = productId;

        modelUtil = new ModelUtil(mContext);
        daveAIPerspective = DaveAIPerspective.getInstance();
        databaseManager = DatabaseManager.getInstance(context);

        //defaultPreviousStage = getFirstNegativeStage();
        //defaultNextStage = getFirstPositiveStage();
        defaultPreviousStage = databaseManager.getCustomSingletonRow("_first_negative_stage_name");
        defaultNextStage = databaseManager.getCustomSingletonRow("_first_positive_stage_name");


        Log.e(TAG,"Print default previous stage: "+ defaultPreviousStage +"  defaultNextStage : "+defaultNextStage);

    }

    public interface OnRecommendationsHelperListener {
        void onSuccess(JSONObject recommendationsObjects);
        void onFailed();

    }

    private interface OnJSONObjectCreated {
        void onCreateJSONObject(JSONObject recommendationsObject);
        void onFailedCreation(String errorMsg);
    }

    public void getRecommendationsObjects(JSONArray productsList , JSONArray interactionList ,OnRecommendationsHelperListener onRecommendationsHelperListener){
        try {
            if(productsList!= null && productsList.length()>0) {
                objectCount = 0;
                totalObjectCount = productsList.length();

                finalRecommObjects = new JSONObject();
                jsonArrayRecommendations = new JSONArray();
                finalRecommObjects.put("recommendations",jsonArrayRecommendations);
                createRecommendationsObjects(productsList, interactionList,onRecommendationsHelperListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject checkProductExistInInteractionList(String productId, JSONArray interactionList){

        JSONObject interactionObj = new JSONObject();
        try {
            if(interactionList!=null && interactionList.length()>0) {
                for (int i = 0; i < interactionList.length(); i++) {
                    interactionObj = interactionList.getJSONObject(i);
                    if (interactionObj.has(productId))
                        return interactionObj;
                }
            }else
                return interactionObj;

        }catch (JSONException e) {
            e.printStackTrace();

        }
      return interactionObj;
    }

    private void createRecommendationsObjects(JSONArray productsList,JSONArray interactionList,OnRecommendationsHelperListener onRecommendationsHelperListener) {
        try {
            //Log.e(TAG,"createRecommendationsObjects:----------"+objectCount+" <"+ totalObjectCount);
            if(objectCount < totalObjectCount){

                JSONObject productObject = productsList.getJSONObject(objectCount);
                String productId = productObject.getString(modelUtil.getProductIdAttrName());
                createRecommendationObject(productId,productObject,checkProductExistInInteractionList(productId,interactionList), new OnJSONObjectCreated() {
                    @Override
                    public void onCreateJSONObject(JSONObject recommendationsObject) {
                        try {
                            Log.e(TAG,"+++++++++Print Final Recommendations Object Count:  "+objectCount +"  "+ recommendationsObject);
                            jsonArrayRecommendations.put(recommendationsObject);
                            objectCount++;
                            createRecommendationsObjects(productsList,interactionList,onRecommendationsHelperListener);

                        }catch (Exception e){
                            e.printStackTrace();
                            Log.e(TAG, "Error productOrderQtyListMap*****Error:-" + e.getMessage());
                        }
                    }

                    @Override
                    public void onFailedCreation(String errorMsg) {

                    }
                });


            }else if(objectCount == totalObjectCount){
                if(onRecommendationsHelperListener != null)
                    onRecommendationsHelperListener.onSuccess(finalRecommObjects);
            }else {
                if(onRecommendationsHelperListener != null)
                    onRecommendationsHelperListener.onFailed();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error createRecommendationsObjects:-" + e.getMessage());
        }

    }

    private JSONObject createRecommendationObject( String productId,JSONObject productObject,JSONObject interactionObject, OnJSONObjectCreated onJSONObjectCreated) {

        JSONObject recommendationObject = new JSONObject();
        try {
            //recommendationObject.put("score","");
            recommendationObject.put("product_id",productObject.getString(modelUtil.getProductIdAttrName())); //string
            JSONObject currentStageObject = new JSONObject();
            recommendationObject.put("current_stage",currentStageObject);  //JSONObject
            recommendationObject.put("product_attributes",productObject); //JSONObject
           // recommendationObject.put("recommendation_number","");


            currentStageObject.put("customer_id",strCustomerId);
            currentStageObject.put("product_id",productId);

            //_interaction_attributes from perspective
            ArrayList interactionAttr = daveAIPerspective.getInteraction_stage_attr_list();
            JSONObject inAttJson = new JSONObject();
            if (interactionAttr != null && interactionAttr.size() > 0) {
                for (int j = 0; j < interactionAttr.size(); j++) {
                    String key = interactionAttr.get(j).toString();
                    if (interactionObject.has(key))
                        inAttJson.put(key, interactionObject.get(key));
                }
            }
            currentStageObject.put("_interaction_attributes", inAttJson);//JSONObject


            String previousStage = "";
            String nextStage = "";
            List<String> qtyAttr = null;

            //set current stage details
            if(interactionObject.length()>0){

                String currentStage = interactionObject.getString(modelUtil.getInteractionStageAttrName());
                currentStageObject.put("interaction_stage",currentStage);
                InteractionStages csInstance = ModelUtil.getStageInstance(mContext,currentStage);
                previousStage = csInstance.getPrevious_stage();
                nextStage = csInstance.getNext_stage();

                assert csInstance != null;
                // stage.get('interaction_time_attribute')
              /*  if (interactionObject.has(csInstance.getInteraction_time_attribute())) {
                    recommendationObject.put("interaction_timestamp", interactionObject.get(csInstance.getInteraction_time_attribute()));//String
                }*/



                //stage.get('tagged_products_attribute')
                if (interactionObject.has(csInstance.getTagged_products_attribute()) && !interactionObject.isNull(csInstance.getTagged_products_attribute())) {
                    currentStageObject.put("tagged_products", interactionObject.get(csInstance.getTagged_products_attribute()));//String
                } else {
                    currentStageObject.put("tagged_products", JSONObject.NULL);//String
                }


                // set(st.interaction_number_attributes + stage.get('quantity_attributes',[]))
                Model interactionModel = Model.getModelInstance(mContext, modelUtil.getInteractionModelName());
                assert interactionModel != null;
                List<String> interactionNumAttr = interactionModel.getModelAttributes(ATTR_NAME_UI_ELEMENT, ATTR_TYPE_NUMBER);
                JSONObject createQtyJson = new JSONObject();
                if (interactionNumAttr != null && interactionNumAttr.size() > 0) {
                    for (int i = 0; i < interactionNumAttr.size(); i++) {
                        String attr = interactionNumAttr.get(i);
                        if (interactionObject.has(attr))
                            createQtyJson.put(attr, interactionObject.get(attr));
                    }
                }
                qtyAttr = csInstance.getQuantity_attributes();
                if (qtyAttr != null && qtyAttr.size() > 0) {
                    for (int i = 0; i < qtyAttr.size(); i++) {
                        String attr = qtyAttr.get(i);
                        if (interactionObject.has(attr))
                            createQtyJson.put(attr, interactionObject.get(attr));
                    }
                }
                currentStageObject.put("quantity_attributes", createQtyJson); //JSONObject

            }else{

                // productId doesn't exist in interaction objects set next stage is first positive stage and previous stage is first negative stage...
                previousStage = defaultPreviousStage;
                nextStage = defaultNextStage;

                currentStageObject.put("interaction_stage",JSONObject.NULL);
                currentStageObject.put("tagged_products", JSONObject.NULL);//String
                currentStageObject.put("quantity_attributes", new JSONObject()); //JSONObject

            }

            //set pervious stage details
            if(previousStage!= null && !previousStage.equalsIgnoreCase("__NULL__")) {
                //set previous stage
                currentStageObject.put("previous_stage", previousStage); //String

                InteractionStages pInstance = ModelUtil.getStageInstance(mContext,previousStage);
                assert pInstance != null;

                //todo :- how to get it
                currentStageObject.put("previous_istage", ""); //String

                // interaction['details'].get(ps.get('tagged_products_attribute'))
                if (interactionObject.has(pInstance.getTagged_products_attribute()) && !interactionObject.isNull(pInstance.getTagged_products_attribute())) {
                    currentStageObject.put("previous_stage_tagged_products", interactionObject.get(pInstance.getTagged_products_attribute())); //JSONArray
                } else {
                    currentStageObject.put("previous_stage_tagged_products", new JSONArray()); //JSONArray
                }

                //  ps.get('quantity_attributes',[])
                if (pInstance.getQuantity_attributes() != null) {
                    currentStageObject.put("previous_stage_quantity_attributes", new JSONArray(pInstance.getQuantity_attributes())); //JSONArray
                } else {
                    currentStageObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ps.get('new_interaction_attributes', [])
                if (pInstance.getNew_interaction_attributes() != null) {
                    currentStageObject.put("previous_stage_new_attributes", new JSONArray(pInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    currentStageObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
                }



            }else{
                currentStageObject.put("previous_stage", "__NULL__"); //String
                currentStageObject.put("previous_stage_tagged_products", JSONObject.NULL); //JSONArray
                currentStageObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
            }

            //set next stage details
            if(nextStage != null && !nextStage.equalsIgnoreCase("__NULL__")) {
                // set next stage
                currentStageObject.put("next_stage", nextStage); //String

                InteractionStages nInstance = ModelUtil.getStageInstance(mContext,nextStage);
                assert nInstance != null;
                //todo :- how to get it
                currentStageObject.put("next_istage", ""); //String

                // list(set(interaction['quantity_attributes'].keys()) - set(interaction['next_stage_quantity_attributes']))
                List<String> nsQtrAttr = nInstance.getQuantity_attributes();
                if (qtyAttr != null && qtyAttr.size() > 0) {
                    if (nsQtrAttr != null && nsQtrAttr.size() > 0)
                        qtyAttr.removeAll(nsQtrAttr);
                    currentStageObject.put("new_quantity_attributes", new JSONArray(qtyAttr)); //JSONArray
                } else
                    currentStageObject.put("new_quantity_attributes", new JSONArray()); //JSONArray


                // interaction['details'].get(ns.get('tagged_products_attribute'))
                if (interactionObject.has(nInstance.getTagged_products_attribute()) && !interactionObject.isNull(nInstance.getTagged_products_attribute())) {
                    currentStageObject.put("next_stage_tagged_products", interactionObject.get(nInstance.getTagged_products_attribute())); //JSONArray
                } else {
                    currentStageObject.put("next_stage_tagged_products", new JSONArray()); //JSONArray
                }

                //  ns.get('quantity_attributes',[])
                if (nInstance.getQuantity_attributes() != null) {
                    currentStageObject.put("next_stage_quantity_attributes", new JSONArray(nInstance.getQuantity_attributes())); //JSONArray
                } else {
                    currentStageObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ns.get('new_interaction_attributes', [])
                if (nInstance.getNew_interaction_attributes() != null) {
                    currentStageObject.put("next_stage_new_attributes", new JSONArray(nInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    currentStageObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
                }


            }else{
                currentStageObject.put("next_stage", "__NULL__"); //String
                currentStageObject.put("new_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_tagged_products", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
            }


            Log.d(TAG, " Print Final Recommendations Object :------" + recommendationObject.length() + " " + recommendationObject);

            if(onJSONObjectCreated!=null)
                onJSONObjectCreated.onCreateJSONObject(recommendationObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return recommendationObject;
    }

    public JSONObject getDefauleCurrentStageDetails(){

        JSONObject currentStageObject = new JSONObject();
        try {
            currentStageObject.put("customer_id",strCustomerId);
            currentStageObject.put("product_id",strProductId);


            String previousStage = defaultPreviousStage;
            String nextStage = defaultNextStage;
            List<String> qtyAttr = null;

            Log.e(TAG,"Print previous stage :- "+ previousStage +" NextStage: "+ nextStage);

            // productId doesn't exist in interaction objects set next stage is first positive stage and previous stage is first negative stage...

            currentStageObject.put("interaction_stage",JSONObject.NULL);
            currentStageObject.put("tagged_products", JSONObject.NULL);//String
            currentStageObject.put("quantity_attributes", new JSONObject()); //JSONObject

            //set pervious stage details
            if(previousStage!= null && !previousStage.equalsIgnoreCase("__NULL__")) {
                //set previous stage
                currentStageObject.put("previous_stage", previousStage); //String

                InteractionStages pInstance = ModelUtil.getStageInstance(mContext,previousStage);
                assert pInstance != null;

                //todo :- how to get it
                currentStageObject.put("previous_istage", ""); //String

                //  ps.get('quantity_attributes',[])
                if (pInstance.getQuantity_attributes() != null) {
                    currentStageObject.put("previous_stage_quantity_attributes", new JSONArray(pInstance.getQuantity_attributes())); //JSONArray
                } else {
                    currentStageObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ps.get('new_interaction_attributes', [])
                if (pInstance.getNew_interaction_attributes() != null) {
                    currentStageObject.put("previous_stage_new_attributes", new JSONArray(pInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    currentStageObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
                }



            }else{
                currentStageObject.put("previous_stage", "__NULL__"); //String
                currentStageObject.put("previous_stage_tagged_products", JSONObject.NULL); //JSONArray
                currentStageObject.put("previous_stage_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("previous_stage_new_attributes", new JSONArray()); //JSONArray
            }

            //set next stage details
            if(nextStage != null && !nextStage.equalsIgnoreCase("__NULL__")) {
                // set next stage
                currentStageObject.put("next_stage", nextStage); //String

                InteractionStages nInstance = ModelUtil.getStageInstance(mContext,nextStage);
                assert nInstance != null;
                //todo :- how to get it
                currentStageObject.put("next_istage", ""); //String

                // list(set(interaction['quantity_attributes'].keys()) - set(interaction['next_stage_quantity_attributes']))
                List<String> nsQtrAttr = nInstance.getQuantity_attributes();
                if (qtyAttr != null && qtyAttr.size() > 0) {
                    if (nsQtrAttr != null && nsQtrAttr.size() > 0)
                        qtyAttr.removeAll(nsQtrAttr);
                    currentStageObject.put("new_quantity_attributes", new JSONArray(qtyAttr)); //JSONArray
                } else
                    currentStageObject.put("new_quantity_attributes", new JSONArray()); //JSONArray


                //  ns.get('quantity_attributes',[])
                if (nInstance.getQuantity_attributes() != null) {
                    currentStageObject.put("next_stage_quantity_attributes", new JSONArray(nInstance.getQuantity_attributes())); //JSONArray
                } else {
                    currentStageObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                }

                //  ns.get('new_interaction_attributes', [])
                if (nInstance.getNew_interaction_attributes() != null) {
                    currentStageObject.put("next_stage_new_attributes", new JSONArray(nInstance.getNew_interaction_attributes())); //JSONArray
                } else {
                    currentStageObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
                }


            }else{
                currentStageObject.put("next_stage", "__NULL__"); //String
                currentStageObject.put("new_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_tagged_products", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_quantity_attributes", new JSONArray()); //JSONArray
                currentStageObject.put("next_stage_new_attributes", new JSONArray()); //JSONArray
            }


            Log.d(TAG, " Print Final CurrentStageDetails :------" + currentStageObject);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return currentStageObject;

    }

    // next stage is first positive stage
    private String getFirstPositiveStage(){
        return getStagesNamesbasedOnSort(SORT_BY_POSITIVE);
    }

    //previous stage is first negative stage...
    private String getFirstNegativeStage(){
        return  getStagesNamesbasedOnSort(SORT_BY_NEGATIVE);
    }


    private String getStagesNamesbasedOnSort(String sortBy)
    {
        String stageName ="";
        try {
            JSONObject jsonObject = DatabaseManager.getInstance(mContext).getAllObjectsData(INTERACTION_STAGES_TABLE_NAME,new HashMap<>(),new HashMap<>(),0,0);
            if(jsonObject.getJSONArray("data").length()>0 )
            {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                JSONArray jsonArraySorted = new JSONArray();

                for(int i = 0; i< jsonArray.length(); i++)
                {
                    double scoreValue = jsonArray.getJSONObject(i).getDouble("default_positivity_score");
                    //boolean isDisplay = jsonArray.getJSONObject(i).getBoolean("display");
                    //Log.d(TAG,"getFirstPositiveStage >>>>>>>>>>>>>>> "+i +" StageNAme:- "+jsonArray.getJSONObject(i).getString("name")+" "+scoreValue );

                    if (sortBy.equals(SORT_BY_POSITIVE)) {
                        if (scoreValue > 0) {
                            jsonArraySorted.put(jsonArray.get(i));
                        }
                    } else if (sortBy.equals(SORT_BY_NEGATIVE)) {
                        if (scoreValue < 0) {
                            jsonArraySorted.put(jsonArray.get(i));

                        }
                    }

                }

                if(jsonArraySorted.length()>0 )
                {
                    stageName =  jsonArraySorted.getJSONObject(0).getString("name");
                    double tempScore = jsonArraySorted.getJSONObject(0).getDouble("default_positivity_score");
                    for (int i = 1; i < jsonArraySorted.length(); i++){
                        double score = jsonArraySorted.getJSONObject(i).getDouble("default_positivity_score");
                        //Log.e(TAG,"getStage >>>>>>>>>>>>>>>"+tempScore +" tempStageName:--"+ stageName +" score "+ score +"  "+jsonArraySorted.getJSONObject(i).getString("name") );
                        if(tempScore > score){
                            tempScore= score;
                            stageName= jsonArraySorted.getJSONObject(i).getString("name");

                        }
                    }
                    Log.i(TAG,"Print  Stage Name sortBy>>>>>>>>>>"+sortBy+" StageName:-  "+stageName);
                }
                return stageName;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return stageName;
        }

        return stageName;
    }


   /* private InteractionStages getStageInstance(String stageName) {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        String stageDetail = databaseManager.getObjectData(INTERACTION_STAGES_TABLE_NAME, stageName).toString();
        if (stageDetail != null && !stageDetail.isEmpty()) {
            try {
                Gson gson = new Gson();
                return gson.fromJson(stageDetail, InteractionStages.class);

            } catch (Exception exception) {
                exception.printStackTrace();
                Log.e(TAG, "Gson Error setting Stage Detail in InteractionStages class*****  " + stageName + " Error:-" + exception.getMessage());
            }
        }
        return null;
    }*/

}



//recommendation format

 /*   {
        "is_first": true,
        "recommendation_type": "recommendations",
        "customer_id": "MTU0OTg3NTM1MTk5ODA4MzgxNjU_",
        "total_number": 3,
        "timestamp": "2019-04-08T06:27:41.940531+00:00",
        "recommendation_id": "385fe40d-e21f-3aa5-8edd-4eda5c686b71",
        "page_size": 10,
        "sort_by": null,
        "num_similar": null,
        "is_last": true,
        "filter_list": null,
        "filters": {
            "category": "NA"
        },
        "recommendations": [
        {
            "score": 0.3337398286092672,
            "product_id": "T4050BC",
            "current_stage": {
                "stage": null,
                "timestamp": null,
                "next_stage": "interested",
                "product_id": "T4050BC",
                "customer_id": "MTU0OTg3NTM1MTk5ODA4MzgxNjU_",
                "next_istage": "interested",
                "recommended": true,
                "previous_stage": "rejected",
                "previous_istage": "rejected",
                "interaction_stage": null,
                "quantity_attributes": {},
                "next_stage_new_attributes": [],
                "next_stage_tagged_products": null,
                        "previous_stage_new_attributes": [],
                "next_stage_quantity_attributes": [],
                "previous_stage_quantity_attributes": []
            },
            "product_attributes": {
                    "size": "NA",
                    "active": true,
                    "bar_code": "http://d3chc9d4ocbi4o.cloudfront.net/static/uploads/urban9/bar_code/tmp67dPLk.svg",
                    "category": [
                     "NA"
                    ],
                    "product_id": "T4050BC",
                    "_s_category": "NA",
                    "product_code": "P&W BSN WITH BRASS PLUG C/P",
                    "product_name": "P&W BSN WITH BRASS PLUG C/P",
                    "concept_image": "https://d3chc9d4ocbi4o.cloudfront.net/urban9/Sanitary_images/T4050BC.jpg",
                    "visualization": [],
                    "pieces_per_box": 1,
                    "application_area": [
                    "NA"
                    ],
                    "application_room": [
                       "other"
                    ],
                    "application_design": "simple",
                    "technical_diagram_s": "NA"
            },
            "recommendation_number": 0
        }
        ],
        "influencers": null,
        "page_number": 1,
        "now": null,
        "quantity_predict": null,
        "filter_influencers": null,
        "additional_values": {
             "_s_category": "NA"
        }
    }*/


/*

"quantity_predictions": {
                "rejected": {
                    "__insight__": {}
                },
                "returned": {
                    "qty": 0,
                    "__insight__": {
                        "qty": "Retailers that are similar (same area_manager_name) returned at least a Ordered Quantity of 0.0 units"
                    }
                },
                "purchased": {
                    "qty": 3.6,
                    "__insight__": {
                        "qty": "Retailers that are similar (same area_manager_name) purchased at least a Ordered Quantity of 3.6 units"
                    }
                }
            },
*/


