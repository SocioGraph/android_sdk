package com.example.admin.daveai.others;

import android.content.Context;

import com.example.admin.daveai.database.DatabaseConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;
import java.util.ArrayList;

/**
 * Created by soham on 13/8/18.
 */

public class CacheModelHelper implements DatabaseConstants {
    public void addModelForCache(ArrayList<CacheModelPOJO> cacheModelPOJOArrayList, Context context){
        for(int i = 0 ; i < cacheModelPOJOArrayList.size() ; i ++){
            addModelForCache(cacheModelPOJOArrayList.get(i),context);
        }
    }

    public void addModelForCache(CacheModelPOJO cacheModelPOJO, Context context){
        try {
            Gson gson = new GsonBuilder().create();
            String json = gson.toJson(cacheModelPOJO);
            JSONObject jsonObj = new JSONObject(json);
            new DaveSharedPreference(context).writeString("_cache_meta_" + cacheModelPOJO.modelName, jsonObj.toString());
//            DatabaseManager.getInstance(context).insertMetaData(cacheModelPOJO);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public CacheModelPOJO getModelCacheMeta(String modelName,Context context){
        String json = new DaveSharedPreference(context).readString("_cache_meta_" + modelName);
        if(json==null){
            return new CacheModelPOJO("",0,0,0,"",0);
        }else {
            try {
                CacheModelPOJO cacheModelPOJO = new Gson().fromJson(json, CacheModelPOJO.class);
                return cacheModelPOJO;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return new CacheModelPOJO("",0,0,0,"",0);
    }


    public void updateLastUpdated(String modelName, Context context){
        CacheModelPOJO cacheModelPOJO = getModelCacheMeta(modelName,context);
        cacheModelPOJO.last_updated = System.currentTimeMillis();
        addModelForCache(cacheModelPOJO,context);
    }

    public CacheRefreshStatus checkCacheUpdateRequired(String modelName, Context context){
        long currentTimeStamp = System.currentTimeMillis();
        CacheModelPOJO modelCacheMeta = getModelCacheMeta(modelName,context);
        long timeDelta = currentTimeStamp - modelCacheMeta.last_updated;
        if(timeDelta>modelCacheMeta.hot_refresh_time_limit){
            return CacheRefreshStatus.HOT_UPDATE_REQUIRED;
        }
        else if(timeDelta>modelCacheMeta.cold_refresh_time_limit){
            return CacheRefreshStatus.COLD_UPDATE_REQUIRED;
        }else {

            return CacheRefreshStatus.NOT_REQUIRED;
        }
    }







}
