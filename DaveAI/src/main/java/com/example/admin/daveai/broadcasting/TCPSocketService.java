package com.example.admin.daveai.broadcasting;


import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;


/**
 * Created by Rinki on 21,May,2019
 */

public class TCPSocketService {

    public static final String TAG = "TCPSocketService";
    private Context context;

    //client variable
   // public static final int SERVER_PORT = 8000;
    //public static final String SERVER_IP = "192.168.1.2";
    private int serverPort = 8000;
    private String strIpAddress = "";

    private ClientThread clientThread;
    private Thread thread;
    private Handler handler;

    // server variable
    private ServerSocket serverSocket;
    private Socket tempClientSocket;
    Thread serverThread = null;

    public TCPSocketService(Context context){
        this.context = context;
    }


    private OnTCPMessageRecievedListener onTCPMessageRecievedListener;
    public interface OnTCPMessageRecievedListener {
        public void onTCPMessageRecieved(String message);
    }

    public void registerTCPRecievedListener(OnTCPMessageRecievedListener callbackClass){
        this.onTCPMessageRecievedListener = callbackClass;
    }

    public void startTCPServerSocket(){

        handler = new Handler();
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
    }

    public void stopTCPServerSocket(){
        if (null != serverThread) {
            sendMessageFromTCPServer("Disconnect");
            serverThread.interrupt();
            serverThread = null;
        }
    }


    private void sendMessageFromTCPServer(final String message) {
        try {
            if (null != tempClientSocket) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        // PrintWriter out = null;
                        try {
                            PrintWriter  out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(tempClientSocket.getOutputStream())), true);
                            out.println(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void connectToTCPServerSocket(String strIpAddress, int serverPort){

        if(strIpAddress == null || strIpAddress.isEmpty())
            throw new NullPointerException(" must provide Ip Address of that device in which server socket open. ");
        else {
            this.strIpAddress = strIpAddress;
            this.serverPort = serverPort;
            handler = new Handler();
            clientThread = new ClientThread();
            thread = new Thread(clientThread);
            thread.start();
        }
    }

    public void sendMessageFromTCPClient(String clientMessage){
        //showClientMessage(clientMessage, Color.BLUE);

        //todo check connected with server or not
        if (null != clientThread) {
            clientThread.sendMessage(clientMessage);
        }

    }

    public void stopTCPClientSocket(){
        if (null != clientThread) {
            clientThread.sendMessage("Disconnect");
            clientThread = null;
        }
    }

    class ClientThread implements Runnable {

        private Socket socket;
        private BufferedReader input;

        @Override
        public void run() {

            try {
                Log.e(TAG,"************ClientThread connecting with server socket ipAddress  "+ strIpAddress +" ServerPort: "+ serverPort );
                //InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
                InetAddress serverAddr = InetAddress.getByName(strIpAddress);
                socket = new Socket(serverAddr, serverPort);  //connect with server socket
                Log.e(TAG,"*************ClientThread connected with server socket " );
                showClientMessage("Client: " + "Connected with Server " +strIpAddress);
                //read message from server
                while (!Thread.currentThread().isInterrupted()) {
                    this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String message = input.readLine();
                    if (null == message || "Disconnect".contentEquals(message)) {
                        Thread.interrupted();
                        message = "Server Disconnected.";
                       // showClientMessage(message, Color.RED);
                        Log.e(TAG,"ClientThread server socket disconnected**********  " +"Server Disconnected.");
                        break;
                    }
                    //showClientMessage("Server: " + message);
                }

            } catch (UnknownHostException e1) {
                e1.printStackTrace();
                showClientMessage("Client: " + "Connection failed " +e1.getMessage());
            } catch (IOException e1) {
                e1.printStackTrace();
                showClientMessage("Client: " + "Connection failed " +e1.getMessage());
            }

        }

        //send message from client
        void sendMessage(final String message) {

            Log.e(TAG," ClientThread sendMessage *****************  "+ message +""+ socket);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (null != socket) {
                            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
                            out.println(message);
                            Log.e(TAG,"sent meg from client *****************  "+ message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

    }

    private void showClientMessage(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG,"Print message send from server :---- "+ message);
                if(message.contains("Connected with Server"))
                    Toast.makeText(context,"Connected with server: "+ strIpAddress,Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(context,message,Toast.LENGTH_LONG).show();

            }
        });
    }


    public void showServerReceivedMessage(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG,"Print  message from client :---- "+ message);
                if(onTCPMessageRecievedListener!=null)
                    onTCPMessageRecievedListener.onTCPMessageRecieved(message);
            }
        });
    }

    //start server socket
    class ServerThread implements Runnable {

        public void run() {
            Socket socket;
            try {
                serverSocket = new ServerSocket(serverPort);

            } catch (IOException e) {
                e.printStackTrace();
                Log.e(TAG,"Error Starting Server : "+ e.getMessage());
                //showServerMessage("Error Starting Server : " + e.getMessage(), Color.RED);
            }
            if (null != serverSocket) {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        Log.e(TAG,"*************server socket is waiting for client  : ");
                        //Server is waiting for client here, if needed
                        socket = serverSocket.accept();
                        Log.e(TAG,"*************server socket isconnected with client  : ");
                        CommunicationThread commThread = new CommunicationThread(socket);
                        new Thread(commThread).start();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG,"Error Communicating to Client : : "+ e.getMessage());
                        //showServerMessage("Error Communicating to Client :" + e.getMessage(), Color.RED);
                    }
                }
            }
        }
    }

    //read received message from client
    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {
            this.clientSocket = clientSocket;
            tempClientSocket = clientSocket;
            try {
                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
                //showServerMessage("Error Connecting to Client!!", Color.RED);
                Log.e(TAG,"Error Connecting to Client!!"+ e.getMessage());
            }
            //showServerMessage("Connected to Client!!", Color.GREEN);
            Log.e(TAG,"******************Connected to Client!!");
        }

        public void run() {

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    String read = input.readLine();
                    if (null == read || "Disconnect".contentEquals(read)) {
                        Thread.interrupted();
                        //read = "Client Disconnected";
                        //showServerMessage("Client : " + read, Color.GREEN);
                        Log.e(TAG,"*********************Client : " +"Client Disconnected");
                        break;
                    }
                    Log.e(TAG,"Print send message  from Client   :---- "+ read);
                    showServerReceivedMessage(read);
                   /* new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG,"Print send message  from Client  :---- "+ read);
                            if(onTCPMessageRecievedListener!=null)
                                onTCPMessageRecievedListener.onTCPMessageRecieved(read);
                        }
                    });*/
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }

/************************************ 2nd way to implement socket connection**************************************************************************/
    private void sendMessage(final String msg) {

        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    //Replace below IP with the IP of that device in which server socket open.
                    //If you change port then change the port number in the server side code also.
                    Socket s = new Socket("xxx.xxx.xxx.xxx", 9002);

                    //send message from  client
                    OutputStream out = s.getOutputStream();

                    PrintWriter output = new PrintWriter(out);

                    output.println(msg);
                    output.flush();

                    //received server socket message
                    BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                    final String st = input.readLine();

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e(TAG,"Print message sent from Server socket :---------  "+ st);


                        }
                    });

                    output.close();
                    out.close();
                    s.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    private boolean end = false;
    private void startServerSocket() {


        Thread thread = new Thread(new Runnable() {

            private String stringData = null;

            @Override
            public void run() {

                try {

                    ServerSocket ss = new ServerSocket(9002);

                    while (!end) {
                        //Server is waiting for client here, if needed
                        Socket s = ss.accept();

                        //read message  from clientSocket
                        BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
                        PrintWriter output = new PrintWriter(s.getOutputStream());

                        stringData = input.readLine();
                        output.println("FROM SERVER - " + stringData.toUpperCase());
                        output.flush();

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG,"Print message sent from client socket :---------  "+ stringData);
                            }
                        });
                        if (stringData.equalsIgnoreCase("STOP")) {
                            end = true;
                            output.close();
                            s.close();
                            break;
                        }

                        output.close();
                        s.close();
                    }
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });
        thread.start();
    }


}

/*public class Client extends AsyncTask<String, Void, String> {

    String dstAddress;
    int dstPort;
    String response = "";
    TextView textResponse;

    Client(String addr, int port, TextView textResponse) {
        dstAddress = addr;
        dstPort = port;
        this.textResponse = textResponse;
    }

    @Override
    protected String doInBackground(String... arg0) {

        Socket socket = null;

        try {
            socket = new Socket(dstAddress, dstPort);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
            byte[] buffer = new byte[1024];

            int bytesRead;
            InputStream inputStream = socket.getInputStream();

            *//*
             * notice: inputStream.read() will block if no data return
             *//*
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
                response += byteArrayOutputStream.toString("UTF-8");
            }

        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "UnknownHostException: " + e.toString();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            response = "IOException: " + e.toString();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        textResponse.setText(response);

    }

}*/
