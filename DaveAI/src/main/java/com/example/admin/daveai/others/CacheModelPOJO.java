package com.example.admin.daveai.others;

/**
 * Created by soham on 13/8/18.
 */

public class CacheModelPOJO {
    public String modelName;
    public long hot_refresh_time_limit;
    public long cold_refresh_time_limit;
    public long last_updated;
    public String model_id_name;
    public int isDefault = 1;
    public long master_timestamp = 0;

    public CacheModelPOJO(String modelName, long hot_refresh_time_limit, long cold_refresh_time_limit, long last_updated, String model_id_name, int isDefault) {
        this.modelName = modelName;
        this.hot_refresh_time_limit = hot_refresh_time_limit;
        this.cold_refresh_time_limit = cold_refresh_time_limit;
        this.last_updated=last_updated;
        this.model_id_name=model_id_name;
        this.isDefault=isDefault;
        master_timestamp = System.currentTimeMillis();
    }
    public CacheModelPOJO(String modelName, long hot_refresh_time_limit, long cold_refresh_time_limit, long last_updated, String model_id_name,long master_timestamp) {
        this.modelName = modelName;
        this.hot_refresh_time_limit = hot_refresh_time_limit;
        this.cold_refresh_time_limit = cold_refresh_time_limit;
        this.last_updated=last_updated;
        this.model_id_name=model_id_name;
        this.isDefault = 0;
        this.master_timestamp = master_timestamp;
    }
}
