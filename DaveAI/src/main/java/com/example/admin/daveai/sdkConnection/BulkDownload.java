package com.example.admin.daveai.sdkConnection;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.InteractionsHelper;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.BulkDownloadUtil;
import com.example.admin.daveai.daveUtil.DaveCachedUtil;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.model.ObjectData;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.ConnectDaveAI;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIStatic;
import com.example.admin.daveai.others.DaveException;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.admin.daveai.database.DatabaseConstants.INTERACTIONS_TABLE_NAME;
import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;


/**
 * Created by Rinki on 28,June,2019
 */
public class BulkDownload  implements DaveConstants {

    private static final String TAG = "BulkDownload";
    //private List<String> CORE_MODEL_LIST = Arrays.asList("customer", "product","interaction");
    private Context mContext;

    private DatabaseManager databaseManager;
    private DaveSharedPreference daveSharedPreference;

    public BulkDownload(Context mContext){

        this.mContext = mContext;
        databaseManager = DatabaseManager.getInstance(mContext);
        daveSharedPreference = new DaveSharedPreference(mContext);
    }

    public JSONObject createBulkDownloadPostBody(BulkDownloadUtil bulkDownloadUtil){
        JSONObject bulkPostBody = new JSONObject();
        try {

            JSONObject jobjPivot = new JSONObject();
            JSONObject objectModels = new JSONObject();

            daveSharedPreference.writeBoolean(IS_CONVERT_INTERACTIONS,bulkDownloadUtil.isConvertIntoInteractions());

            //add objects of core models
            HashMap<String, Object> coreModelObjectsMap = bulkDownloadUtil.getCoreModelObjectsMap();
            if(coreModelObjectsMap!=null && coreModelObjectsMap.size()>0)
            {
                for (Map.Entry<String, Object> entry : coreModelObjectsMap.entrySet()) {
                    String modelName = entry.getKey();
                    Log.e(TAG, "Print model name of objects " + modelName + " objectsCount: " + entry.getValue());
                    objectModels.put(databaseManager.getModelNameBasedOnType(modelName),entry.getValue());

                }
            }

            //add objects of models
            HashMap<String, Object> modelObjectsMap = bulkDownloadUtil.getModelObjectsMap();
            if(modelObjectsMap!=null && modelObjectsMap.size()>0)
            {
                for (Map.Entry<String, Object> entry : modelObjectsMap.entrySet()) {
                    String modelName = entry.getKey();
                    Log.e(TAG, "Print model name of objects " + modelName + " objectsCount: " + entry.getValue());
                    objectModels.put(modelName,entry.getValue());

                }
            }

            //add Hierarchy
            HashMap<String, ArrayList<String>> hierarchyMap = bulkDownloadUtil.getModelHierarchyMap();
            if(hierarchyMap!=null && hierarchyMap.size()>0)
            {
                for (Map.Entry<String, ArrayList<String>> entry : hierarchyMap.entrySet()) {
                    String modelName = entry.getKey();
                    if(CORE_MODEL_LIST.contains(modelName))
                        modelName = databaseManager.getModelNameBasedOnType(modelName);
                    Log.e(TAG, "Print hierarchy model name and hierarchy" + modelName + "  hierarchy:-" + entry.getValue());
                    String hierarchyPivot = getModelHierarchyAPI(entry.getKey(), entry.getValue());
                    if (!TextUtils.isEmpty(hierarchyPivot)) {
                        jobjPivot.put(modelName + MODEL_HIERARCHY + "___" + modelName + MODEL_HIERARCHY, hierarchyPivot);
                    }
                }
            }

            //add filter list
            HashMap<String, ArrayList<String>> filtersMap = bulkDownloadUtil.getModelFiltersMap();
            if(filtersMap!=null && filtersMap.size()>0)
            {
                for (Map.Entry<String, ArrayList<String>> entry : filtersMap.entrySet()) {
                    String modelName = entry.getKey();
                    if(CORE_MODEL_LIST.contains(modelName))
                        modelName = databaseManager.getModelNameBasedOnType(modelName);
                    Log.e(TAG, "Print filters model name and hierarchy" + modelName + " Filter value:- " + entry.getValue());
                    HashMap<String,String> customerFilterPivots = getModelFiltersAPI(modelName,entry.getValue());
                    for (Map.Entry<String, String> entry1 : customerFilterPivots.entrySet()) {
                        jobjPivot.put(entry1.getKey() , entry1.getValue());
                    }
                }
            }

            if(objectModels.length()>0)
                bulkPostBody.put("objects",objectModels);
            if(jobjPivot.length()>0)
                bulkPostBody.put("pivots",jobjPivot);

            Log.e(TAG,"BulkDownload  final post body ============================= "+bulkPostBody);
            return bulkPostBody;

        }catch (Exception e){
            e.printStackTrace();
            return bulkPostBody;
        }
    }

    private String getModelHierarchyAPI(String modelName,ArrayList<String> hierarchyList )
    {
        StringBuilder categoryParam = new StringBuilder();
        if(hierarchyList != null && hierarchyList.size()>0) {
            categoryParam.append("/pivot/" + modelName + "/");
            for (int i = 0; i < hierarchyList.size(); i++) {
                categoryParam.append(hierarchyList.get(i));
                if ((i + 1) < hierarchyList.size())
                    categoryParam.append("/");
            }

        }
        return categoryParam.toString();
    }

    //filter feature for type: [name, uid, phone number, email] to directly type instead of pivot
    private boolean checkFilterAttrType(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }

    private HashMap<String,String> getModelFiltersAPI(String modelName, ArrayList<String> filterList){

        HashMap<String,String> pivots = new HashMap<>();
        Model modelInstance = Model.getModelInstance(mContext, modelName);
        if (modelInstance != null && filterList!=null) {
            for (int i = 0; i < filterList.size(); i++) {
                String attrName = filterList.get(i);
                String attrType = modelInstance.getAttributeType(attrName);

                //Log.i(TAG,"getFilterAttributePivots:- AttrNAme:-" + attrName +" AttrType:- "+ attrType);
                if(!checkFilterAttrType(attrType)) {
                    if (attrType.equals("price") || attrType.equals("discount")) {

                        pivots.put(modelName + "_min_" + attrName + "___" + "pivot_min_" + attrName, APIRoutes.priceRangeAPI("min", attrName).split(DaveAIStatic.base_url)[1]);

                        pivots.put(modelName + "_max_" + attrName + "___" + "pivot_max_" + attrName, APIRoutes.priceRangeAPI("max", attrName).split(DaveAIStatic.base_url)[1]);

                    } else {

                        pivots.put(modelName + "_" + attrName + "___" + "pivot_" + attrName, APIRoutes.pivotAttributeAPI(modelName, attrName).split(DaveAIStatic.base_url)[1]);

                    }

                }else{
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(ADD_TYPE_VIEW);
                    //saveFilterListInDb(modelName, attrName,jsonArray);
                    insertOrUpdateFilterValue(modelName,attrName, jsonArray);
                }
            }
        }
        return pivots;
    }

    /**
     *
     * @param modelNames  Post Body with Object{} and pivots{} with counts for number of objects requested per model/pivot
     * @param daveAIListener provide  DaveAIListener callback to get  API Response.if its null it will create own callback.
     * @return  return JSONObject response
     * @throws DaveException  throw ModelNotFound if model name is null or empty.
     */

    private void postBulkRequest(final JSONObject modelNames, DaveAIListener daveAIListener) throws DaveException {

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {

                if (daveAIListener != null) {
                    try {
                        daveAIListener.onReceivedResponse(new JSONObject(response));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                if(daveAIListener!=null) {
                    daveAIListener.onResponseFailure(requestCode,responseMsg);
                }

            }
        };
        if (CheckNetworkConnection.networkHasConnection(mContext)) {
            RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, modelNames.toString());
            new APICallAsyncTask(postTaskListener, mContext, "POST", body_part, false).execute(APIRoutes.bulkDownloadAPI());
        }
        else {
            CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
        }



    }

    private void getBulkData(String bulkDownloadId, DaveAIListener daveAIListener, long maxWaitTime, long bulkStartTime){
        Timer timer = new Timer();
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Log.i(TAG,"**********************bulk data download completed******************************88");
                if (daveAIListener != null)
                {
                   // timer.cancel();
                    try {
                        JSONObject responseJSON = new JSONObject(response);
                        if (responseJSON.has("error")) {
                            daveAIListener.onResponseFailure(-1, responseJSON.getString("error"));

                        }else if(responseJSON.has("completed")){
                            Log.e(TAG,"Print bulk download status:- "+ responseJSON.getDouble("completed"));

                        }else {
                            timer.cancel();
                            daveAIListener.onReceivedResponse(responseJSON);
                        }
                    }catch (Exception e){
                        daveAIListener.onResponseFailure(-1, "Error processing bulk data download. Please try again.");
                        e.printStackTrace();
                        Log.e(TAG,"Exception during bulk download:------- "+ e.getMessage());
                    }


                }

            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {
                //IGNORE ERROR and Wait for successful response
                if(requestCode!=404) {
                    daveAIListener.onResponseFailure(requestCode, responseMsg);
                }
            }
        };
        Handler mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if (CheckNetworkConnection.networkHasConnection(mContext)) {
                    new APICallAsyncTask(postTaskListener, mContext, "GET", false)
                            //.execute(APIRoutes.getBulkDownloadAPI(bulkDownloadId,"true"));
                            .execute(APIRoutes.getBulkDownloadAPI(bulkDownloadId));
                }
                else {
                    CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
                }
            }
        };
        Handler mHandler2 = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if(daveAIListener!=null){
                    daveAIListener.onResponseFailure(408,"Response Time exceeded maxWaitTime of "+maxWaitTime);
                }
            }
        };
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if((System.currentTimeMillis()-bulkStartTime) < maxWaitTime) {
                    mHandler.sendMessage(new Message());
                }else{
                    timer.cancel();
                    mHandler2.sendMessage(new Message());
                }
            }
        }, 0, 10*1000);

    }

    public interface OnDownloadBulkDataListener {
        void onDownloadBulkDataCompleted(JSONObject bulkDataResponse);
        void onDownLoadBulkDataFailed(int errorCode, String errorMessage);
    }

    public void downloadBulkData(JSONObject requestModelsPivots, long maxWaitTime, OnDownloadBulkDataListener onDownloadBulkDataListener ){
        DaveService daveService = new DaveService();
        daveService.startBackgroundService(mContext);
        long bulkStartTime = System.currentTimeMillis();

        DatabaseManager.getInstance(mContext).alertOnAPIQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
            @Override
            public void isEmpty() {

                ((android.app.Activity)mContext).runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            postBulkRequest(requestModelsPivots, new DaveAIListener() {
                                @Override
                                public void onReceivedResponse(JSONObject jsonObject) {
                                    try {
                                        getBulkData(jsonObject.getString("download_id"), new DaveAIListener() {
                                            @Override
                                            public void onReceivedResponse(JSONObject jsonObject) {
                                                onDownloadBulkDataListener.onDownloadBulkDataCompleted(jsonObject);
                                            }

                                            @Override
                                            public void onResponseFailure(int i, String s) {
                                                onDownloadBulkDataListener.onDownLoadBulkDataFailed(i,s);
                                            }
                                        },maxWaitTime,bulkStartTime);

                                    }catch (Exception e){
                                        e.printStackTrace();
                                        onDownloadBulkDataListener.onDownLoadBulkDataFailed(-1,"Error Bulk Download Data");
                                    }
                                }

                                @Override
                                public void onResponseFailure(int i, String s) {
                                    onDownloadBulkDataListener.onDownLoadBulkDataFailed(i,s);

                                }
                            });

                        }catch (Exception e){
                            e.printStackTrace();
                            onDownloadBulkDataListener.onDownLoadBulkDataFailed(-1,"Error Bulk Download Data");
                        }
                    }
                });
            }
        },5*1000);
    }


    public interface OnSaveDataListener {
        void onSaveBulkData(boolean isDateSaved);

    }

    public void saveBulkDataInLocalDb(JSONObject bulkDataResponse, long coldUpdateTime, long hotUpdateTime , OnSaveDataListener onSaveDataListener) {

        Handler mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                if(onSaveDataListener!=null)
                    onSaveDataListener.onSaveBulkData(true);
            }
        };

        Thread t = new Thread(){
            @Override
            public void run() {
                super.run();
                saveBulkDataInLocalDb(bulkDataResponse,coldUpdateTime,hotUpdateTime);
                mHandler.sendEmptyMessage(0);
            }
        };
        t.start();



    }
    private void saveBulkDataInLocalDb(JSONObject bulkDataResponse, long coldUpdateTime, long hotUpdateTime ) {
        try{
            Log.e(TAG,"************saveBulkDataInLocalDb**********response from bulk = "+bulkDataResponse);

            File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/");
            if (!folder.exists())
                folder.mkdirs();
            String finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/"
                    + "bulkData.txt";
            File file = new File(finalFileName);
            FileOutputStream os = new FileOutputStream(file);
            String data = bulkDataResponse.toString();
            os.write(data.getBytes());
            os.close();

            //Setting the state of db in sharedpref
            DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
            sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA,true);

            if(bulkDataResponse.has("pivots")){

                JSONObject objectsJSON = bulkDataResponse.getJSONObject("pivots");
                Log.e(TAG,"saveBulkDataInLocalDb Pivots list ******** "+objectsJSON);
                Iterator<String> iterator = objectsJSON.keys();
                while(iterator.hasNext()) {
                    String key = iterator.next();
                    String tableName = key.split("___")[0];  //modelName + "_min_ / _max_ / _" + attrName
                    String tableRowIdName = key.split("___")[1]; //"pivot_min_" + attrName
                    setupModelCacheMetaData(tableName, tableRowIdName, coldUpdateTime,hotUpdateTime);
                    Log.e(TAG,"saveBulkDataInLocalDb pivot table name:---- "+tableName +"  "+ tableRowIdName );
                    databaseManager.insertOrUpdateSingleton(
                            tableName,
                            new SingletonTableRowModel(tableName, objectsJSON.getString(key),System.currentTimeMillis(),tableRowIdName,"")
                    );

                    String modelName = tableName.split("_")[0];
                    saveFilterAttrData(modelName,tableRowIdName,objectsJSON.getString(key));
                }
            }

            if(bulkDataResponse.has("objects")){
                JSONObject objectsJSON = bulkDataResponse.getJSONObject("objects");
                Log.e(TAG,"Print objects response:--------- "+objectsJSON.keys());

                Cursor c = databaseManager.openDatabase().rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
                if (c.moveToFirst()) {
                    while ( !c.isAfterLast() ) {
                        if(c.getString(0).startsWith("old___")) {
                            Log.e(TAG, "Table Name=> " + c.getString(0));
                            databaseManager.dropTable(c.getString(0));
                        }
                        c.moveToNext();
                    }
                }
                c.close();

                //renaming old tables
                ArrayList<String> renamedTables = new ArrayList<>();
                Iterator<String> iterator = objectsJSON.keys();
                while(iterator.hasNext()) {
                    String key = iterator.next();
                    Log.e(TAG,"********* model name of objects key:------- "+key );
                    if(databaseManager.isTableExists(key)) {
                        databaseManager.renameTable(key, "old___" + key);
                        renamedTables.add("old___"+key);
                    }
                    databaseManager.createTable(key, DatabaseConstants.CacheTableType.OBJECTS);
                    JSONArray objsJar = objectsJSON.getJSONArray(key);

                    CacheModelPOJO modelMetaData = databaseManager.getMetaData(key);
                    for(int j = 0 ; j < objsJar.length() ; j++) {
                        databaseManager.insertObjectsData(key, new ObjectsTableRowModel(
                                objsJar.getJSONObject(j).getString(modelMetaData.model_id_name),
                                objsJar.getJSONObject(j).toString(),
                                System.currentTimeMillis()));
                    }

                    //convert interaction object in interactions object
                    boolean isInteractions = sharedPreference.readBoolean(IS_CONVERT_INTERACTIONS);
                    if(isInteractions && key.equalsIgnoreCase(new ModelUtil(mContext).getInteractionModelName())){
                        Log.e(TAG," if Model NAme is  interaction  save as Interactions object Print updated Table name:-------------"+key);
                        setupModelCacheMetaData(INTERACTIONS_TABLE_NAME,new ModelUtil(mContext).getModelIDAttrName(key),coldUpdateTime,hotUpdateTime);
                        InteractionsHelper interactionsHelper = new InteractionsHelper(mContext);
                        interactionsHelper.addObjectsAsInteractions(objsJar,null);

                    }

                    try {
                        JSONObject checkCacheJSON = new JSONObject();
                        checkCacheJSON.put("data", objsJar);
                        new DaveCachedUtil(mContext).checkForMediaCaching(key, checkCacheJSON.toString());

                        Log.e(TAG, "media queue size = " + databaseManager.checkAPIQueueCount(DatabaseConstants.MEDIA_QUEUE_TABLE_NAME));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                databaseManager.dropTables(renamedTables);

            }


        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            e.printStackTrace();
        }

    }

    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,long coldUpdateTime, long hotUpdateTime){
        DatabaseManager.getInstance(mContext).insertMetaData(
                new CacheModelPOJO(modelName,
                        hotUpdateTime,
                        coldUpdateTime,
                        0, modelIdAttrName,System.currentTimeMillis())
        );

    }

    public void saveFilterAttrData(String modelName, String updateTableRowId, String response ){
        if(response!= null) {
            try {
                Log.e(TAG,"<<<<<<<<<<saveFilterAttrData : "+modelName +"  Response:- " + response);
                JSONObject pivotResponse = new JSONObject(response).getJSONObject("data");
                if(updateTableRowId.startsWith("pivot_min_")){
                    String[] parts = updateTableRowId.split("pivot_min_");
                    Log.e(TAG,"Print pivot attr  pivot_min_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                    double d = pivotResponse.getDouble(parts[1]);
                    int price = (int) d;
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(price);

                   // saveFilterListInDb(modelName,parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse));

                    insertOrUpdateFilterMinMaxValue(modelName,parts[1],price,0);



                }else if(updateTableRowId.startsWith("pivot_max_")){
                    String[] parts = updateTableRowId.split("pivot_max_");
                    Log.d(TAG,"Print pivot attr  pivot_max_>>>>>>>>>>>>>>>>>>>"+parts[1]);

                    double d = pivotResponse.getDouble(parts[1]);
                    int price = (int) d;
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(price);

                    //saveFilterListInDb(modelName,parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse));

                    insertOrUpdateFilterMinMaxValue(modelName,parts[1], price,1);


                }else if(updateTableRowId.startsWith("pivot_")){
                    String[] parts = updateTableRowId.split("pivot_");
                    Log.d(TAG,"Print pivot attr  pivot_>>>>>>>>>>>>>>>>>>>"+parts[1]);
                   // saveFilterListInDb(modelName,parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse));
                    insertOrUpdateFilterValue(modelName,parts[1], ObjectData.getKeysAsJSONArrayOfJSONObject(pivotResponse));


                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Error in Customer Save Filter Attr+++++++++++ " + e.getMessage());
            }


        }

    }


    private void saveFilterListInDb(String modelName ,String key , JSONArray value ){
        try {
            ModelUtil modelUtil = new ModelUtil(mContext);
            modelUtil.setupModelCacheMetaData(modelName+MODEL_FILTER_LIST, modelName+MODEL_FILTER_LIST);
            JSONObject modelFilters = databaseManager.getSingletonRow(modelName+MODEL_FILTER_LIST);
            Log.e(TAG,"saveModelFilterList before update saveProductFilterAttr:------------"+value);
            if(modelFilters != null && modelFilters.length() >0 ){
                if(modelFilters.has(key)) {
                    JSONArray priceRange = modelFilters.getJSONArray(key);
                    for(int i = 0; i<value.length();i++)
                        priceRange.put(value.get(i));
                    modelFilters.put(key, priceRange);
                }else {
                    modelFilters.put(key,value);
                }

            }else{
                modelFilters = new JSONObject();
                modelFilters.put(key,value);
            }
            Log.e(TAG,"saveModelFilterAttr After update :------------"+ modelName +" "+ modelFilters.length() + modelFilters);
            databaseManager.insertOrUpdateSingleton(
                    modelName+MODEL_FILTER_LIST,
                    new SingletonTableRowModel(modelName+MODEL_FILTER_LIST,modelFilters.toString(),
                            System.currentTimeMillis(),modelName+MODEL_PIVOT_REQUEST_TYPE)
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void insertOrUpdateFilterValue(String modelName, String key , JSONArray value ){
        try {
            JSONObject modelFilters = getFilterListFromDB(modelName);
            if(modelFilters == null)
                modelFilters = new JSONObject();
            modelFilters.put(key,value);
            Log.i(TAG,"After update sproductFil:------------"+modelFilters.length() + modelFilters);
           saveFilterListInDB(modelName,modelFilters);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void insertOrUpdateFilterMinMaxValue(String modelName, String key , int value, int position){
        try {
            JSONObject modelFilters = getFilterListFromDB(modelName);
            Log.i(TAG," before update saveFilterAttr:------------"+value);
            if(modelFilters != null && modelFilters.length() >0 ){
                if(modelFilters.has(key)) {
                    JSONArray priceRange = modelFilters.getJSONArray(key);
                    priceRange.put(position,value);
                    modelFilters.put(key, priceRange);
                }else {
                    modelFilters.put(key,value);
                }

            }else{
                modelFilters = new JSONObject();
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(position,value);
                modelFilters.put(key,jsonArray);
            }
            Log.i(TAG,"After update sproductFil:------------"+modelFilters.length() + modelFilters);

            saveFilterListInDB(modelName,modelFilters);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private JSONObject getFilterListFromDB(String modelName){

        ModelUtil modelUtil = new ModelUtil(mContext);
        modelUtil.setupModelCacheMetaData(modelName+MODEL_FILTER_LIST, modelName+MODEL_FILTER_LIST);
        JSONObject modelFilters = databaseManager.getSingletonRow(modelName+MODEL_FILTER_LIST);
        Log.e(TAG,"getFilterList before update saveProductFilterAttr:------------"+modelFilters);

        return modelFilters;
    }

    private void saveFilterListInDB(String modelName, JSONObject modelFilters){

        Log.e(TAG,"saveModelFilterAttr After update :------------"+ modelName +" "+ modelFilters.length() + modelFilters);
        databaseManager.insertOrUpdateSingleton(
                modelName+MODEL_FILTER_LIST,
                new SingletonTableRowModel(modelName+MODEL_FILTER_LIST,modelFilters.toString(),
                        System.currentTimeMillis(),modelName+MODEL_PIVOT_REQUEST_TYPE)
        );
    }


}
