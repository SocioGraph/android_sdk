package com.example.admin.daveai.database.models;

/**
 * Created by soham on 14/9/18.
 */

public class RecommendationsTableRowModel {
    public String object_id = "";
    public String data_cached = "";
    public long last_synced;
    public long error_message;

    public RecommendationsTableRowModel(String object_id, String data_cached, long last_synced, long error_message) {
        this.object_id = object_id;
        this.data_cached = data_cached;
        this.last_synced = last_synced;
        this.error_message = error_message;
    }

    public RecommendationsTableRowModel(String object_id, String data_cached, long last_synced) {
        this.object_id = object_id;
        this.data_cached = data_cached;
        this.last_synced = last_synced;
    }
}
