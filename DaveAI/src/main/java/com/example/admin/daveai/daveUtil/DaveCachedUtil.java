package com.example.admin.daveai.daveUtil;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;

import com.example.admin.daveai.broadcasting.MediaService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.MediaQueueTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DaveCachedUtil  implements DatabaseConstants {

    private final static String TAG = "DaveCachedUtil";
    private Context mContext;
    private DatabaseManager databaseManager;
    private DaveSharedPreference sharedPreference;

    public DaveCachedUtil(Context mContext){
        this.mContext = mContext;
        this.databaseManager = DatabaseManager.getInstance(mContext);
        sharedPreference = new DaveSharedPreference(mContext);
        databaseManager =DatabaseManager.getInstance(mContext);

    }

    /*public void checkForMediaCaching(String modelName, String response){
        try{
            Model modelInstance = ModelUtil.getModelInstance(mContext,modelName);
            if(modelInstance!=null) {
                List<String> cachedAttributes = modelInstance.getQuick_views().getCached();
                Log.i(TAG, "Print cached attributes name for model " + cachedAttributes);
                if (cachedAttributes != null && cachedAttributes.size() > 0) {
                    Thread media_caching_thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject respJson = new JSONObject(response);
                                if (respJson.has("data")) {

                                    JSONArray dataJar = respJson.getJSONArray("data");
                                    Log.i("getObjects", " Total COunt of Objects:-" + dataJar.length());
                                    CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                                    for (int i = 0; i < dataJar.length(); i++) {
                                        for (int x = 0; x < cachedAttributes.size(); x++) {
                                            String attrType = modelInstance.getAttributeType(cachedAttributes.get(x));
                                            addToMediaQueue(cachedAttributes.get(x), modelInstance, dataJar.getJSONObject(i), modelName, modelMetaData);
                                        }
                                    }

                                    Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
                                    if(cur.getCount()>0){
                                        new MediaService().startBackgroundService(mContext);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    media_caching_thread.start();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }*/

    public void checkForMediaCaching(String modelName, JSONArray data){
        try{
            Model modelInstance = ModelUtil.getModelInstance(mContext,modelName);
            if(modelInstance!=null) {
                List<String> cachedAttributes = modelInstance.getQuick_views().getCached();
                Log.i(TAG, "Print cached attributes name for model " + cachedAttributes);
                if (cachedAttributes != null && cachedAttributes.size() > 0) {
                    //Log.i(TAG, " Total COunt of Objects:-" + data.length());
                    CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                    for (int i = 0; i < data.length(); i++) {
                        for (int x = 0; x < cachedAttributes.size(); x++) {
                            String attrType = modelInstance.getAttributeType(cachedAttributes.get(x));
                            addToMediaQueue(cachedAttributes.get(x), modelInstance, data.getJSONObject(i), modelName, modelMetaData);
                        }
                    }
                    Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
                    if(cur.getCount()>0){
                        new MediaService().startBackgroundService(mContext);
                    }


                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }



    public void checkForMediaCaching(String modelName, String response){
        try{
            Model modelInstance = ModelUtil.getModelInstance(mContext,modelName);
            if(modelInstance!=null) {
                List<String> cachedAttributes = modelInstance.getQuick_views().getCached();
                Log.i(TAG, "Print cached attributes name for model " + cachedAttributes);

                if (cachedAttributes != null && cachedAttributes.size() > 0) {

                    JSONObject respJson = new JSONObject(response);
                    if (respJson.has("data")) {

                        JSONArray dataJar = respJson.getJSONArray("data");
                        Log.i(TAG, " Total COunt of Objects:-" + dataJar.length());
                        CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                        for (int i = 0; i < dataJar.length(); i++) {
                            for (int x = 0; x < cachedAttributes.size(); x++) {
                                String attrType = modelInstance.getAttributeType(cachedAttributes.get(x));
                                addToMediaQueue(cachedAttributes.get(x), modelInstance, dataJar.getJSONObject(i), modelName, modelMetaData);
                            }
                        }

                        Cursor cur = databaseManager.getDataFromTable(MEDIA_QUEUE_TABLE_NAME);
                        if(cur.getCount()>0){
                            new MediaService().startBackgroundService(mContext);
                        }

                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public void addToMediaQueue(String cachedAttribute, Model mod, JSONObject jsonObject, String modelName, CacheModelPOJO modelMetaData){
        try {
            if(jsonObject.has(cachedAttribute) && !jsonObject.isNull(cachedAttribute)) {
                Log.i(TAG, "media cached data type for " + cachedAttribute + " " + mod.getAttributeType(cachedAttribute));
                if (mod.getAttributeType(cachedAttribute).equals("image") || mod.getAttributeType(cachedAttribute).equals("video") || mod.getAttributeType(cachedAttribute).equals("file") || mod.getAttributeType(cachedAttribute).equals("apk") || mod.getAttributeType(cachedAttribute).equals("url")) {
                    databaseManager.insertMediaQueueData(
                            new MediaQueueTableRowModel(
                                    0,
                                    jsonObject.getString(cachedAttribute),
                                    mod.getAttributeType(cachedAttribute),
                                    modelName,
                                    jsonObject.getString(modelMetaData.model_id_name),
                                    cachedAttribute,
                                    "pending"
                            )
                    );
                } else if (mod.getAttributeType(cachedAttribute).equals("media_map") || mod.getAttributeType(cachedAttribute).equals("nested_object")) {
                    JSONObject json = new JSONObject();
                    json = jsonObject.getJSONObject(cachedAttribute);
                    Iterator<String> iter = json.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            String value = json.getString(key);
                            databaseManager.insertMediaQueueData(
                                    new MediaQueueTableRowModel(
                                            0,
                                            value,
                                            mod.getAttributeType(cachedAttribute),
                                            modelName,
                                            jsonObject.getString(modelMetaData.model_id_name),
                                            cachedAttribute,
                                            "pending"
                                    )
                            );
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                    }

                } else if (mod.getAttributeType(cachedAttribute).equals("image_object")) {

                    JSONArray json = new JSONArray();
                    json = jsonObject.getJSONArray(cachedAttribute);
                    for (int l = 0; l < json.length(); l++) {
                        JSONObject jsobj = json.getJSONObject(l);
                        if (jsobj.has("image")) {
                            databaseManager.insertMediaQueueData(
                                    new MediaQueueTableRowModel(
                                            0,
                                            jsobj.getString("image"),
                                            mod.getAttributeType(cachedAttribute),
                                            modelName,
                                            jsonObject.getString(modelMetaData.model_id_name),
                                            cachedAttribute,
                                            "pending"
                                    )
                            );
                            if (jsobj.has("thumbnail")) {
                                databaseManager.insertMediaQueueData(
                                        new MediaQueueTableRowModel(
                                                0,
                                                jsobj.getString("thumbnail"),
                                                mod.getAttributeType(cachedAttribute),
                                                modelName,
                                                jsonObject.getString(modelMetaData.model_id_name),
                                                cachedAttribute,
                                                "pending"
                                        )
                                );
                            }
                        }
                    }
                }
                else if (mod.getAttributeType(cachedAttribute).equals("list")) {
                    JSONArray json = new JSONArray();
                    json = jsonObject.getJSONArray(cachedAttribute);
                    for (int l = 0; l < json.length(); l++) {
                        String url = json.getString(l);
                        databaseManager.insertMediaQueueData(
                                new MediaQueueTableRowModel(
                                        0,
                                        url,
                                        mod.getAttributeType(cachedAttribute),
                                        modelName,
                                        jsonObject.getString(modelMetaData.model_id_name),
                                        cachedAttribute,
                                        "pending"
                                )
                        );


                    }
                }else if (mod.getAttributeType(cachedAttribute).equals("action_settings")) {
                    JSONObject json = new JSONObject();
                    json = jsonObject.getJSONObject(cachedAttribute);
                    Iterator<String> iter = json.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            JSONObject jb = json.getJSONObject(key);
                            if(jb.has("url")) {
                                databaseManager.insertMediaQueueData(
                                        new MediaQueueTableRowModel(
                                                0,
                                                jb.getString("url"),
                                                mod.getAttributeType(cachedAttribute),
                                                modelName,
                                                jsonObject.getString(modelMetaData.model_id_name),
                                                cachedAttribute,
                                                "pending"
                                        )
                                );
                            }
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error  addToMediaQueue:------------"+e.getMessage());
        }
    }


    public JSONObject replaceWithLocalFileCachePath(String modelName, JSONObject respJson){
        JSONArray cachedAttrs = new JSONArray();
        databaseManager = DatabaseManager.getInstance(mContext);
        List<String> modelMediaCache = new ArrayList<>();
        Model mod = new Model(mContext);
        String newJson = respJson.toString();
        try {
            JSONObject modelJsn = databaseManager.getModelData(modelName);
            Gson gson = new Gson();
            mod = gson.fromJson(modelJsn.toString(),Model.class);
            try {
                cachedAttrs = modelJsn.getJSONObject("quick_views").getJSONArray("cached");
                modelMediaCache = mod.getQuick_views().getCached();
            }catch (Exception e){
                e.printStackTrace();
                cachedAttrs = new JSONArray();
                modelMediaCache = new ArrayList<>();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.e(TAG,"replaceWithLocalFileCachePath -- trying to replace "+cachedAttrs+" for model = "+modelName);
        try {
            if (respJson.has("data")) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                JSONArray dataJar = respJson.getJSONArray("data");
                CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                for (int i = 0; i < dataJar.length(); i++) {
                    for(int x = 0 ; x < modelMediaCache.size(); x++ ){
                        if (mod.getAttributeType(modelMediaCache.get(x)).equals("image") || mod.getAttributeType(modelMediaCache.get(x)).equals("video") || mod.getAttributeType(modelMediaCache.get(x)).equals("file") || mod.getAttributeType(modelMediaCache.get(x)).equals("apk") || mod.getAttributeType(modelMediaCache.get(x)).equals("url")|mod.getAttributeType(modelMediaCache.get(x)).equals("action_settings"))
                        {

                            if(dataJar.getJSONObject(i).has(modelMediaCache.get(x))) {
                                HashMap<String, Object> param = new HashMap<>();
                                param.put(URL, dataJar.getJSONObject(i).getString(modelMediaCache.get(x)));
                                Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                if (imagedata.getCount() > 0) {
                                    imagedata.moveToFirst();
                                    String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));

                                    JSONObject jobj = new JSONObject();
                                    jobj.put("asdf", dataJar.getJSONObject(i).getString(modelMediaCache.get(x)));
                                    String oldUrl = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);
                                    while (newJson.indexOf(dataJar.getJSONObject(i).getString(modelMediaCache.get(x))) >= 0) {

                                        newJson = newJson.replace(dataJar.getJSONObject(i).getString(modelMediaCache.get(x)), localPath);
                                    }

                                    while (newJson.contains(oldUrl)) {

                                        newJson = newJson.replace(oldUrl, localPath);
                                    }

                                }else if(dataJar.getJSONObject(i).getString(modelMediaCache.get(x)).contains("http:")
                                        &&
                                        dataJar.getJSONObject(i).getString(modelMediaCache.get(x)).contains("https:")){
                                    addToMediaQueue(modelMediaCache.get(x),mod,dataJar.getJSONObject(i),modelName,modelMetaData);
                                }
                            }
                        }else if(mod.getAttributeType(modelMediaCache.get(x)).equals("list")) {
                            if(dataJar.getJSONObject(i).has(modelMediaCache.get(x))) {
                                JSONArray listUrls = dataJar.getJSONObject(i).getJSONArray(modelMediaCache.get(x));
                                for(int a = 0 ; a < listUrls.length() ; a++) {

                                    HashMap<String, Object> param = new HashMap<>();
                                    param.put(URL, listUrls.getString(a));
                                    Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                    if (imagedata.getCount() > 0) {
                                        imagedata.moveToFirst();
                                        String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                        JSONObject jobj = new JSONObject();
                                        jobj.put("asdf",listUrls.getString(a));
                                        String oldUrl = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);

                                        while (newJson.contains(oldUrl)) {
                                            newJson = newJson.replace(oldUrl, localPath);
                                        }

                                    }else if(dataJar.getJSONObject(i).getString(modelMediaCache.get(x)).contains("http:")
                                            &&
                                            dataJar.getJSONObject(i).getString(modelMediaCache.get(x)).contains("https:")){
                                        addToMediaQueue(modelMediaCache.get(x),mod,dataJar.getJSONObject(i),modelName,modelMetaData);
                                    }
                                }
                            }
                        }else if(mod.getAttributeType(modelMediaCache.get(x)).equals("media_map")||mod.getAttributeType(modelMediaCache.get(x)).equals("nested_object")){
                            JSONObject json = new JSONObject();
                            json = dataJar.getJSONObject(i).getJSONObject(modelMediaCache.get(x));
                            Iterator<String> iter = json.keys();
                            while (iter.hasNext()) {
                                String key = iter.next();
                                try {


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    // Something went wrong!
                                }
                            }
                        }else if (mod.getAttributeType(modelMediaCache.get(x)).equals("image_object")) {
                            JSONArray jar = dataJar.getJSONObject(i).getJSONArray(modelMediaCache.get(x));
                            for(int imageObjCount = 0 ; imageObjCount<jar.length() ; imageObjCount++) {
                                JSONObject json = jar.getJSONObject(imageObjCount);

                                if (json.has("image")) {
                                    try {
                                        HashMap<String, Object> param = new HashMap<>();
                                        param.put(URL, json.getString("image"));
                                        Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                        if (imagedata.getCount() > 0) {
                                            imagedata.moveToFirst();
                                            String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                            JSONObject jobj = new JSONObject();
                                            jobj.put("asdf", json.getString("image"));
                                            String oldUrl = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);
                                            while (newJson.contains(oldUrl)) {
                                                newJson = newJson.replace(oldUrl, localPath);
                                            }
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (json.has("thumbnail")) {
                                    try {
                                        HashMap<String, Object> param = new HashMap<>();
                                        param.put(URL, json.getString("thumbnail"));
                                        Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                        if (imagedata.getCount() > 0) {
                                            imagedata.moveToFirst();
                                            String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                            JSONObject jobj = new JSONObject();
                                            jobj.put("asdf", json.getString("thumbnail"));
                                            String oldUrl = jobj.toString().substring(jobj.toString().indexOf(":") + 2, jobj.toString().lastIndexOf("}") - 1);
                                            while (newJson.contains(oldUrl)) {
                                                newJson = newJson.replace(oldUrl, localPath);
                                            }
                                        }


                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        }/*else if (mod.getAttributeType(modelMediaCache.get(x)).equals("action_settings")) {

                        }*/

                    }

                }


            }
            return new JSONObject(newJson);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return respJson;

    }
    public  JSONObject replaceServerPathWithLocalFilePath(String modelName, JSONObject objectDetails){

        try {
            databaseManager = DatabaseManager.getInstance(mContext);
            Model modelInstance = ModelUtil.getModelInstance(mContext,modelName);
            if(modelInstance!=null) {
                List<String> mediaCachedList = modelInstance.getQuick_views().getCached();

                Log.e(TAG, "replaceServerPathWithLocalFilePath -- mediaCachedList " + mediaCachedList + " for model = " + modelName);

                if (mediaCachedList != null && mediaCachedList.size() > 0) {
                    for (int x = 0; x < mediaCachedList.size(); x++) {
                        String attName = mediaCachedList.get(x);
                        String attrType = modelInstance.getAttributeType(attName);
                        Log.e(TAG, "replaceServerPathWithLocalFilePath>>>>>>ForLoop Count:->>>>>>> " + x + " attrType:-" + attName);
                        if (objectDetails.has(attName) && !objectDetails.isNull(attName)) {

                            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                            CacheModelPOJO modelMetaData = databaseManager.getMetaData(modelName);
                            if (attrType.equals("image") || attrType.equals("video") || attrType.equals("file") ||
                                    attrType.equals("apk") || attrType.equals("url"))
                            {
                                HashMap<String, Object> param = new HashMap<>();
                                param.put(URL, objectDetails.getString(attName));
                                Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                if (imagedata.getCount() > 0) {
                                    imagedata.moveToFirst();
                                    String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                    objectDetails.put(attName, localPath);

                                } else if (objectDetails.getString(attName).contains("http")
                                        &&
                                        objectDetails.getString(attName).contains("https:")) {
                                    addToMediaQueue(attName, modelInstance, objectDetails, modelName, modelMetaData);
                                }


                            } else if (attrType.equals("list")) {

                                JSONArray listUrls = objectDetails.getJSONArray(attName);
                                for (int a = 0; a < listUrls.length(); a++) {
                                    HashMap<String, Object> param = new HashMap<>();
                                    param.put(URL, listUrls.getString(a));
                                    Cursor imagedata = databaseManager.getDataFromTable(MEDIA_TABLE_NAME, param, new HashMap<>(), "=", 0, "");
                                    if (imagedata.getCount() > 0) {
                                        imagedata.moveToFirst();
                                        String localPath = imagedata.getString(imagedata.getColumnIndex(LOCAL_PATH));
                                        listUrls.put(a, localPath);

                                    } else if (objectDetails.getString(mediaCachedList.get(x)).contains("http") &&
                                            objectDetails.getString(mediaCachedList.get(x)).contains("https")) {
                                        addToMediaQueue(attName, modelInstance, objectDetails, modelName, modelMetaData);
                                    }
                                }
                                objectDetails.put(attName, listUrls);

                            } else if (attrType.equals("media_map") || attrType.equals("nested_object")) {
                                //todo implement for media_map 0r nested_object
                                JSONObject json = new JSONObject();
                                json = objectDetails.getJSONObject(mediaCachedList.get(x));
                                Iterator<String> iter = json.keys();
                                while (iter.hasNext()) {
                                    String key = iter.next();

                                }
                            }
                        }
                    }
                }
            }
            return objectDetails;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objectDetails;

    }





}
