package com.example.admin.daveai.expandViewRecycler.ViewItem;


public class MailItem {
    private String text_key;
    private String text_value;


    public MailItem(String text_key, String text_value) {
        this.text_key = text_key;
        this.text_value = text_value;
    }

    public String getText_key() {
        return text_key;
    }

    public String getText_value() {
        return text_value;
    }

}
