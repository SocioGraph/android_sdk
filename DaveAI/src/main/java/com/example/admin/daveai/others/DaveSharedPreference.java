package com.example.admin.daveai.others;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;




 public class DaveSharedPreference implements DaveConstants {

    private final String TAG = getClass().getSimpleName();
    public static final String EnterpriseId = "enterprise_id";
    public static final String BASE_URL= "base_url";
    public static final String API_KEY = "api_key";
    public static final String Role = "role";
    public static final String USER_ID = "user_id";
    public static final String USER_LOGIN_DETAILS = "user_login_details";
    public static final String PROCESSING_BULK_DATA = "processing_bulk_data";
    public static final String CONNECT_STATUS = "connect_status";
    public static final String PUSH_TOKEN = "push_token";
    public static final String COLD_UPDATE_TIME_LIMIT = "cold_update_time_limit";
    public static final String HOT_UPDATE_TIME_LIMIT = "hot_update_time_limit";
    private SharedPreferences sharedPreferences;




     // /data/data/YOUR_PACKAGE_NAME/shared_prefs/YOUR_PREFS_NAME.xml (YOUR_PACKAGE_NAME = appId)
     // /data/data/YOUR_PACKAGE_NAME/shared_prefs/YOUR_PACKAGE_NAME_preferences.xml (Default Location)



   /*  public DaveSharedPreference(Context context){
         sharedPreferences = context.getSharedPreferences("daveAI_sdk_" + MultiUtil.getManifestMetadata(context,MANIFEST_ENTERPRISE_ID), 0);
     }*/

    public DaveSharedPreference(Context context){
        sharedPreferences = context.getSharedPreferences("daveAI_sdk", 0);
    }



     private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public  void writeBoolean(String key, boolean value) {
        getEditor().putBoolean(key, value).commit();
    }

    public boolean readBoolean( String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public  void writeInteger(String key, int value) {
        getEditor().putInt(key, value).commit();
    }

    public  int readInteger(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public  void writeFloat(String key, float value) {
        getEditor().putFloat(key, value).commit();
    }

    public  float readFloat(String key) {
        return sharedPreferences.getFloat(key, 0.0f);
    }

    public  void writeLong(String key, long value) {
        getEditor().putLong(key, value).commit();
    }

    public  long readLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public  void writeString( String key, String value) {
        getEditor().putString(key, value).commit();

    }

     public  String readString(String key) {
        return sharedPreferences.getString(key, null);
    }


     public  long getColdUpdateTime() {
         return sharedPreferences.getLong(COLD_UPDATE_TIME_LIMIT, 6 * 3600 * 1000);
     }

     public  long getHotUpdateTime() {
         return sharedPreferences.getLong(HOT_UPDATE_TIME_LIMIT, 24 * 3600 * 1000);
     }

   /* public void saveList(Context context, String name_of_key, ArrayList<HashMap<String,Object>> value) {

        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = myPrefs.edit();
        LinkedHashSet<String> hs = new LinkedHashSet<String>();
        Set<String> set = new HashSet<String>();
        set.addAll(value);
        editor.putStringSet(name_of_key, set);
        editor.apply();
    }

    public ArrayList<String> getList(Context context,String name_of_key) {
        ArrayList<String> array_list = new ArrayList<String>();
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = myPrefs.getStringSet(name_of_key, null);
        if (set != null) {
            array_list.addAll(set);
        }
        return array_list;
    }*/


    /**
     *     Save and get ArrayList in SharedPreference
     */

    public void saveArrayList(String key, ArrayList<String> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);
        getEditor().putString(key, json).apply();// This line is IMPORTANT !!!

    }

    public ArrayList<String> getArrayList(String key){
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public void saveHashMapObjectType(String key ,HashMap<String,Object> obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        getEditor().putString(key,json).apply();

    }

    public HashMap<String, Object> getHashMapObjectType(String key) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key,"");
        java.lang.reflect.Type type = new TypeToken<HashMap<String,Object>>(){}.getType();
        return gson.fromJson(json, type);
    }

    public void saveHashMap(String key ,HashMap<String,ArrayList<String>> obj) {
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        getEditor().putString(key,json).apply();
        // getEditor().apply();     // This line is IMPORTANT !!!
    }

    public HashMap<String, ArrayList<String>> getHashMap(String key) {
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(key,"");
       // java.lang.reflect.Type type = new TypeToken<HashMap<String,Object>>(){}.getType();
        java.lang.reflect.Type type = new TypeToken<HashMap<String, ArrayList<String>>>(){}.getType();
        HashMap<String, ArrayList<String>> obj = gson.fromJson(json, type);

        Log.e(TAG,"getHashMap Details from SharePreference:--  "+obj);

        return obj;
    }

/*    public void saveQueueLinkedList(Context context, String keyName, Queue value) {

        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = myPrefs.edit();
        Set<String> set = new HashSet<>();
        set.addAll(value);
        editor.putStringSet(keyName, set);
        editor.apply();
    }

    public ArrayList<String> getQueueLinkedList(Context context,String name_of_key) {
        Queue  breadCrumbs = new LinkedList<>();
        ArrayList<String> array_list = new ArrayList<String>();
        SharedPreferences myPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = myPrefs.getStringSet(name_of_key, null);
        if (set != null) {
            breadCrumbs.addAll(set);
        }
        return array_list;
    }*/

    public  void removeKey( String key) {
        SharedPreferences.Editor editor = getEditor();
        editor.remove(key);
        editor.commit();
    }

    public  void removeAll() {
        SharedPreferences.Editor editor = getEditor();
        editor.clear();
        editor.commit();
    }

    public boolean preferenceFileExist(Context context, String fileName) {
        File f = new File(context.getApplicationInfo().dataDir + "/shared_prefs/" + fileName + ".xml");
        Log.e(TAG,"Check File exist or not in Share Prefrence****** "+f.exists());
        return f.exists();
    }



    public String getJsonResponseString(String key) {
        return sharedPreferences.getString(key,null);
    }

   public boolean checkStringExist(String keyName){
       return readString(keyName) != null;
   }



    /*public void getMetadata() {
        try {
            ApplicationInfo applicationInfo = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            baseUrl.setText("URL: "+bundle.getString("BASE_URL"));

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to Get URL OR ENTERPRISE From SP, NullPointer: " + e.getMessage());
        }
    }*/

}
