package com.example.admin.daveai.broadcasting;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;


public class DownloadFile extends AsyncTask<String, String, String> {

    public static final String TAG = "DownloadFile";
    private Context context;
    private String fileName;
    private File directoryPath;
    private boolean isDownloaded;
    private int storageType;


    private OnDownloadFileListener onDownloadFileListener;
    public interface OnDownloadFileListener {
        void onDownloaded(String fileName);

    }


    public DownloadFile(Context context,String fileName,OnDownloadFileListener onDownloadFileListener){
        this.context = context;
        this.fileName =fileName;
        this.onDownloadFileListener = onDownloadFileListener;
    }


    public DownloadFile(Context context,String fileName, File directoryPath , int storageType){
        this.context = context;
        this.fileName =fileName;
        this.directoryPath = directoryPath;
        this.storageType =storageType;
    }

    enum StorageType{
        INTERNAL (1),
        EXTERNAL(0)
        ;

        private final int storageCode;

        private StorageType(int storageCode) {
            this.storageCode = storageCode;
        }

        public int getValue() {
            return storageCode;
        }
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    //Downloading file in background thread
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(f_url[0]);
            URLConnection connection = url.openConnection();
            connection.connect();
            // getting file length
            int lengthOfFile = connection.getContentLength();


            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream(), 8192);


            if(directoryPath == null ) {
                // create External directory path to save file
                directoryPath = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/dave/cached_files/");
            }
            if(!directoryPath.exists())
                directoryPath.mkdirs();

            if(fileName == null || fileName .isEmpty()) {
                fileName = directoryPath + MediaService.generateNameFromUrl(url.toString());
            }else {
                fileName = directoryPath +fileName;
            }


           /* if(storageType == StorageType.EXTERNAL.getValue()){



            }else if(storageType == StorageType.INTERNAL.getValue()){


                if(fileName== null || fileName.isEmpty())
                    fileName = MediaService.generateNameFromUrl(url.toString());

                new File(context.getFilesDir(), fileName);

            }*/


            // Output stream to write file
            OutputStream output = new FileOutputStream(fileName);
            byte data[] = new byte[1024];

            while ((count = input.read(data)) != -1) {
                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
            return fileName ;

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"**********Exception during downloading file: "+ e.getMessage());
        }

        return "Download failed";
    }

    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
       // progressDialog.setProgress(Integer.parseInt(progress[0]));
    }


    @Override
    protected void onPostExecute(String message) {
        // Display File path after downloading

        Log.e(TAG,"*********************onPostExecute Print file:---  "+message);

        if(message != null && !message.equals("Download failed") && onDownloadFileListener!= null ){
            onDownloadFileListener.onDownloaded(message);
        }
    }

}
