package com.example.admin.daveai.others;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.example.admin.daveai.broadcasting.DaveService;
import com.example.admin.daveai.broadcasting.MediaService;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.dialogbox.ProgressLoaderDialog;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.sdkConnection.ConnectWithDaveAI;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import static com.example.admin.daveai.daveUtil.DaveConstants.ADD_TYPE_VIEW;
import static com.example.admin.daveai.others.DaveAIStatic.BULK_TIME_OUT_MINUTES;
import static com.example.admin.daveai.others.DaveAIStatic.quantityPredictStatus;


public class ConnectDaveAI implements DatabaseConstants , DaveConstants {


    private static final String TAG = "ConnectDaveAI";
    private Context mContext;

    private int DEFAULT_OBJECT_COUNT = 200;
    List<String> CORE_MODEL_LIST = Arrays.asList("customer", "product","interaction");
    ArrayList<String> getModelList = new ArrayList<>();
    Queue<String> tempGetModelList = new LinkedList<>();
    Queue<String> tempPreflList = new LinkedList<>();


    private Dialog pDialog;

    JSONObject loginDetails = new JSONObject();
    private Model model;
    private DaveSharedPreference sharedPreferences;
    private DaveAIPerspective daveAIPerspective;
    OnSyncDataListener onSyncDataListener = null;



    public ConnectDaveAI(Context context){
        this.mContext = context;
        model = new Model(mContext);
        sharedPreferences = new DaveSharedPreference(mContext);
        daveAIPerspective=DaveAIPerspective.getInstance();

    }

    private void showLoader(String loaderMsg){

        ProgressLoaderDialog progressLoaderDialog = new ProgressLoaderDialog(mContext);
        pDialog = progressLoaderDialog.progressDialog();
        if(TextUtils.isEmpty(loaderMsg)) {
            progressLoaderDialog.setMessage("Please wait...");
        }else {
            progressLoaderDialog.setMessage(loaderMsg);
        }
        pDialog.setCancelable(false);
        pDialog.show();

        APIRoutes.getMetadata(mContext);

    }

    public void setURLAndEnterpriseID(String enterpriseId, String baseURL){

        Log.i(TAG,"setURLAndEnterpriseID:- "+enterpriseId +" && baseURL == "+baseURL);
        sharedPreferences.writeString(DaveSharedPreference.EnterpriseId,enterpriseId);
        sharedPreferences.writeString(DaveSharedPreference.BASE_URL,baseURL);
    }

    private OnLoginSuccessListener onLoginAction;
    public interface OnLoginSuccessListener {
        void loginSucess(JSONObject userLoginDetails);
        void loginFailed(String userLoginFailed);
    }
    public void registerLoginCallback(OnLoginSuccessListener callbackClass){
        onLoginAction = callbackClass;
    }

    private void deleteLocalDatabase() {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

        ModelUtil modelUtil = new ModelUtil(mContext);
        if(databaseManager.isTableExists(modelUtil.getCustomerModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getCustomerModelName());

        if(databaseManager.isTableExists(modelUtil.getProductModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getProductModelName());

        if(databaseManager.isTableExists(modelUtil.getInteractionModelName()))
            databaseManager.deleteAllRowsInTable(modelUtil.getInteractionModelName());

        if(databaseManager.isTableExists(daveAIPerspective.getInvoice_model_name()))
            databaseManager.deleteAllRowsInTable(daveAIPerspective.getInvoice_model_name());

        databaseManager.deleteAllRowsInTable(META_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(INTERACTIONS_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(INTERACTION_STAGES_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(POST_QUEUE_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(MODEL_TABLE_NAME);
        databaseManager.deleteAllRowsInTable(SINGLETON_TABLE_NAME);

    }

    private void setUpLocalDatabase() {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        databaseManager.createTable(META_TABLE_NAME, CacheTableType.META_DATA);
        databaseManager.createTable(SINGLETON_TABLE_NAME, CacheTableType.SINGLETON);
        databaseManager.createTable(MODEL_TABLE_NAME,CacheTableType.MODEL);
        databaseManager.createTable(INTERACTIONS_TABLE_NAME, CacheTableType.INTERACTIONS);
        databaseManager.createTable(INTERACTION_STAGES_TABLE_NAME, CacheTableType.OBJECTS);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.POST_QUEUE);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.MEDIA_QUEUE_TABLE);
        databaseManager.createTable(POST_QUEUE_TABLE_NAME, CacheTableType.MEDIA_TABLE);

        setupModelCacheMetaData(SINGLETON_TABLE_NAME, SINGLETON_TABLE_NAME, mContext);
        setupModelCacheMetaData(INTERACTION_STAGES_TABLE_NAME, INTERACTION_STAGES_TABLE_NAME, mContext);
        //setupModelCacheMetaData(INTERACTIONS_TABLE_NAME, INTERACTIONS_TABLE_NAME, mContext);


        Log.i(TAG,"Set up all default table:-----");
    }

    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,Context context){
        Log.i(TAG,"SEt Cache meta data  hot and cold update for model:---"+modelName+"=="+daveAIPerspective.getHot_update_time_limit()+" "+ daveAIPerspective.getCold_update_time_limit());
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        daveAIPerspective.getHot_update_time_limit(),
                        daveAIPerspective.getCold_update_time_limit(),
                        0, modelIdAttrName,System.currentTimeMillis())
        );


    }
    public void saveOrUpdateSingletonObject(String modelName, String requestType, String response){
        saveOrUpdateSingletonObject(modelName,requestType,response,"");

    }

    public void saveOrUpdateSingletonObject(String modelName, String requestType, String response ,String errorMsg){

        Log.e(TAG,"saveOrUpdateSingletonObject modelName :---------------"+modelName +" requestType:- "+requestType);
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        if(!databaseManager.isTableExists(SINGLETON_TABLE_NAME)) {
            databaseManager.createTable(SINGLETON_TABLE_NAME, CacheTableType.SINGLETON);
        }
        databaseManager.insertOrUpdateSingleton(
                modelName,
                new SingletonTableRowModel(modelName,response,System.currentTimeMillis(),requestType,errorMsg)
        );

    }


    private OnConnectedWithDaveListener connectedWithDaveListener;
    public interface OnConnectedWithDaveListener {
        void onDaveConnected(boolean daveMsg);

    }
    public void registerConnectDaveCallback(OnConnectedWithDaveListener callbackClass){
        connectedWithDaveListener = callbackClass;
    }


    public interface OnSyncDataListener {
        void onDataSynced(boolean isSynced,String daveMsg);
        void onDataSyncFailed(boolean isSynced,String daveMsg);
    }
    public void syncBulkData(String loaderMsg,OnSyncDataListener onSyncDataListener){
        this.onSyncDataListener = onSyncDataListener;
        DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
        sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA, false);
        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.READY_TO_CONNECT.getValue());
        connect(loaderMsg);
    }

    public void syncDataWithServer(String loaderMsg, OnSyncDataListener onSyncDataListener){

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        long queueSize = databaseManager.forceUpdateQueueSize();
        if(queueSize>0) {
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                if (loaderMsg == null || loaderMsg.isEmpty())
                    loaderMsg = "Syncing with the server, \nThis may take some time.";
                    showLoader(loaderMsg);
                    saveAllLoginApiInQueueTable(mContext);

                    long endTime = System.currentTimeMillis() + 300000;
                    final Handler handler = new Handler();
                    handler.postDelayed(runnable = new Runnable() {
                        @Override
                        public void run() {
                            long queueSize = databaseManager.forceUpdateQueueSize();
                            //long queueSize = databaseManager.getQueueSize();
                            long currentTime = System.currentTimeMillis();
                            Log.e(TAG, "<<<<<<<<<Check queue size during Sync Data " + queueSize + " \n EndTime== " + endTime + " == CurrentTime:-" +
                                    currentTime);
                            if (currentTime >= endTime && queueSize > 0) {
                                pDialog.dismiss();
                                if (onSyncDataListener != null) {
                                    onSyncDataListener.onDataSynced(false, "Sync taking too long, please try again.");
                                }

                            } else {
                                if (queueSize == 0) {
                                    pDialog.dismiss();
                                    if (onSyncDataListener != null) {
                                        onSyncDataListener.onDataSynced(true, "sync_data successful.");

                                    }
                                } else {
                                    new DaveService().startBackgroundService(mContext);
                                    handler.postDelayed(runnable, 1000);
                                }

                            }
                        }
                    }, 1000);

            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }
        }else {
            if (onSyncDataListener != null) {
                onSyncDataListener.onDataSynced(true, "Data is already synced.");

            }
        }
    }

    public void saveInteractionStagesDetails(String response){

        //collect and keep all the stage names,
        // all new_interaction_attributes,
        // all quantity attributes and
        // all tagged product attributes during the init itself...

        ArrayList<String> interactionStageList = new ArrayList<>();
        HashMap<String,Object> shareButtonListMap = new HashMap<>();
        HashMap<String,Object> shareTemplateListMap = new HashMap<>();
        JSONObject shareButtonDetails = new JSONObject();
        JSONObject shareTemplateDetails = new JSONObject();
        if(response!=null) {
            try {
                boolean isFirstPositiveStage = true;
                boolean isFirstNegativeStage = true;
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                JSONObject data = new JSONObject(response);
                Iterator<String> keys = data.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray data_array = data.getJSONArray(key);

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject get_data = data_array.getJSONObject(i);
                            String stageName = get_data.getString("name");
                            if(get_data.getBoolean("display")) {

                                interactionStageList.add(stageName);
                                /*double defaultScoreValue = get_data.getDouble("default_positivity_score");

                                Log.e(TAG,"Testing saveInteractionStagesDetails:--------"+stageName +" scoreVAlue:--- "+ defaultScoreValue);
                                if(isFirstPositiveStage && defaultScoreValue > 0){
                                    isFirstPositiveStage = false;
                                    databaseManager.insertOrUpdateCustomSingleton( "_first_positive_stage_name",
                                            new SingletonTableRowModel("_first_positive_stage_name",
                                                    stageName,
                                                    System.currentTimeMillis(),"firstPositiveStageName"));
                                }
                                if(isFirstNegativeStage && defaultScoreValue < 0){
                                    isFirstNegativeStage = false;
                                    databaseManager.insertOrUpdateCustomSingleton( "_first_negative_stage_name",
                                            new SingletonTableRowModel("_first_negative_stage_name",
                                                    stageName,
                                                    System.currentTimeMillis(),"firstNegativeStageName"));
                                }*/
                            }

                            double defaultScoreValue = get_data.getDouble("default_positivity_score");

                            Log.e(TAG,"Testing saveInteractionStagesDetails:--------"+stageName +" scoreVAlue:--- "+ defaultScoreValue);
                            if(isFirstPositiveStage && defaultScoreValue > 0){
                                isFirstPositiveStage = false;
                                databaseManager.insertOrUpdateCustomSingleton( "_first_positive_stage_name",
                                        new SingletonTableRowModel("_first_positive_stage_name",
                                                stageName,
                                                System.currentTimeMillis(),"firstPositiveStageName"));
                            }
                            if(isFirstNegativeStage && defaultScoreValue < 0){
                                isFirstNegativeStage = false;
                                databaseManager.insertOrUpdateCustomSingleton( "_first_negative_stage_name",
                                        new SingletonTableRowModel("_first_negative_stage_name",
                                                stageName,
                                                System.currentTimeMillis(),"firstNegativeStageName"));
                            }

                            if(get_data.has("quantity_attributes")&& !get_data.isNull("quantity_attributes")){
                                quantityPredictStatus = true;
                            }
                            if(get_data.has("share_button_name")&& !get_data.get("share_button_name").equals("__NULL__")){
                                shareButtonListMap.put(get_data.getString("name"),get_data.getString("share_button_name"));
                                shareButtonDetails.put(get_data.getString("name"),get_data.getString("share_button_name"));
                            }
                            if(get_data.has("share_button_template")&& !get_data.isNull("share_button_template")){
                                shareTemplateListMap.put(get_data.getString("name"),get_data.getString("share_button_template"));
                                shareTemplateDetails.put(get_data.getString("name"),get_data.getString("share_button_template"));
                            }

                            //Log.e(TAG,"get Interaction Stages name and object details:-"+get_data.getString("name")+" "+get_data);
                            DatabaseManager.getInstance(mContext).insertOrUpdateObjectData(
                                    INTERACTION_STAGES_TABLE_NAME,
                                    new ObjectsTableRowModel(
                                            get_data.getString("name"),
                                            get_data.toString(),
                                            System.currentTimeMillis()
                                    )
                            );

                        }
                       // Log.e(TAG, " Print shareButtonListMap+++++++++++" + shareButtonListMap);
                    }
                }


                sharedPreferences.saveArrayList("_interaction_stage_names", interactionStageList);
                sharedPreferences.saveHashMapObjectType("_stage_button_list", shareButtonListMap);
                sharedPreferences.saveHashMapObjectType("_stage_template_list", shareTemplateListMap);

                // save stageNAme list , stageButtonList, stageTemplateList  in Singeleton table
                databaseManager.insertOrUpdateCustomSingleton( "_interaction_stage_names",
                        new SingletonTableRowModel("_interaction_stage_names",interactionStageList.toString(),System.currentTimeMillis(),"stageName"));

                databaseManager.insertOrUpdateCustomSingleton( "_stage_button_list",
                        new SingletonTableRowModel("_stage_button_list",shareButtonListMap.toString(),System.currentTimeMillis(),"stage_button_list"));

                databaseManager.insertOrUpdateCustomSingleton( "_stage_template_list",
                        new SingletonTableRowModel("_stage_template_list",shareTemplateListMap.toString(),System.currentTimeMillis(),"stage_template_list"));

                Log.e(TAG,"<<<<<<<<<<<<<Get Frist Positive Stage NAme "+  databaseManager.getCustomSingletonRow("_first_positive_stage_name"));
                Log.e(TAG,"<<<<<<<<<<<<Get Frist negative Stage NAme "+  databaseManager.getCustomSingletonRow("_first_negative_stage_name"));



            }
            catch (Exception exception){
                exception.printStackTrace();
                Log.e(TAG, "Error in Interaction_stages Objects+++++++++++" + exception.getMessage());
            }
        }
        //return interactionStageList;
    }

    //**************************************cold update login method start****************************************************

    private Runnable runnable;
    public  void initSDK(final Context appContext ,String loaderMsg){

        showLoader(loaderMsg);
        //saveAllLoginApiInQueueTable(appContext);
        initSdkCheckTableUpdate(appContext);
        DatabaseManager databaseManager = DatabaseManager.getInstance(appContext);
        long endTime = System.currentTimeMillis() + 300000;
        final Handler handler = new Handler();
        handler.postDelayed(runnable = new Runnable() {
            @Override
            public void run() {
                long queueSize = databaseManager.checkLoginQueueSize();
                long currentTime = System.currentTimeMillis();
                Log.i(TAG,"<<<<<<<<<Check queue size during initSDK=="+ queueSize +" TotalQueueSize :-----"+databaseManager.getQueueSize()
                        +" \n EndTime== "+endTime +" CurrentTime=="+ currentTime);

                if(currentTime >= endTime && queueSize > 0){
                    pDialog.dismiss();
                    if (onLoginAction != null) {
                        //todo clear api queue table
                        if(databaseManager.isTableExists(POST_QUEUE_TABLE_NAME))
                            databaseManager.deleteAllRowsInTable(POST_QUEUE_TABLE_NAME);
                        onLoginAction.loginFailed("The Network is too slow to login. Please try again with a better internet connection");

                    }
                    if(connectedWithDaveListener!=null)
                        connectedWithDaveListener.onDaveConnected(false);

                }else {
                    if(queueSize == 0){
                        pDialog.dismiss();
                        if(connectedWithDaveListener!=null)
                            connectedWithDaveListener.onDaveConnected(true);

                        if(onLoginAction!=null)
                            onLoginAction.loginSucess(loginDetails);

                    }else{
                        new DaveService().startBackgroundService(mContext);
                        handler.postDelayed(runnable, 1000);

                    }

                }
            }
        }, 1000);

    }


    private void initSdkCheckTableUpdate(Context context){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject perspectiveSettings = databaseManager.getSingletonRow("perspective_settings");
            if(perspectiveSettings!=null && perspectiveSettings.length()>0){
                daveAIPerspective.savePerspectivePreferenceValue(mContext,perspectiveSettings.toString());
                getPerspectivePreference(false,APIQueuePriority.ONE.getValue());
            }

            if(!databaseManager.isTableEmpty(INTERACTION_STAGES_TABLE_NAME)){
                getInteractionStagesList(false,APIQueuePriority.ONE.getValue());

            }

            String customerModel = databaseManager.getModelNameBasedOnType("customer");
            Log.e(TAG,"Print Customer model NAme:---------------"+ customerModel);
            if(customerModel!= null && !customerModel.isEmpty()){
                getModel(customerModel,false,APIQueuePriority.ONE.getValue());

                JSONObject customerHierarchy = databaseManager.getSingletonRow("customer_hierarchy");
                if(customerHierarchy!=null && customerHierarchy.length()>0){
                    getCategoryHierarchyList("customer",customerModel,daveAIPerspective.getCustomer_category_hierarchy(),
                            false,APIQueuePriority.ONE.getValue());
                }
                getFilterAttr(customerModel,daveAIPerspective.getCustomer_filter_attr(),false,APIQueuePriority.ONE.getValue());
            }

            String productModel = databaseManager.getModelNameBasedOnType("product");
            if(productModel!= null && !productModel.isEmpty()){
                getModel(productModel,false,APIQueuePriority.ONE.getValue());

                JSONObject productHierarchy = databaseManager.getSingletonRow("product_hierarchy");
                if(productHierarchy!=null && productHierarchy.length()>0){
                    getCategoryHierarchyList("product",productModel,daveAIPerspective.getProduct_category_hierarchy(),
                            false,APIQueuePriority.ONE.getValue());
                }
                getFilterAttr(productModel,daveAIPerspective.getProduct_filter_attr(),false,APIQueuePriority.ONE.getValue());
            }

            String interactionModel = databaseManager.getModelNameBasedOnType("interaction");
            if(interactionModel!= null && !interactionModel.isEmpty()){
                getModel(interactionModel,false,APIQueuePriority.ONE.getValue());
               // getObjectsList(interactionModel,false,APIQueuePriority.ONE.getValue());
            }

            if(daveAIPerspective.getInvoice_model_name()!= null && !daveAIPerspective.getInvoice_model_name().isEmpty()
                    && getModelFromDB(daveAIPerspective.getInvoice_model_name()) ) {

                getModel(daveAIPerspective.getInvoice_model_name(),false,APIQueuePriority.ONE.getValue());
               // getObjectsList(daveAIPerspective.getInvoice_model_name(),false,APIQueuePriority.ONE.getValue());
            }

            if(daveAIPerspective.getUser_login_model_name()!=null && !daveAIPerspective.getUser_login_model_name().isEmpty()){
                getModel(daveAIPerspective.getUser_login_model_name(),false,APIQueuePriority.ONE.getValue());

                JSONObject userSingleton = databaseManager.getSingletonRow(daveAIPerspective.getUser_login_model_name());
                if(daveAIPerspective.getUser_login_id_attr_name()!=null && !daveAIPerspective.getUser_login_id_attr_name().isEmpty()) {
                    if(userSingleton!= null && userSingleton.length()>0 && userSingleton.has(daveAIPerspective.getUser_login_id_attr_name())){
                        getUserSingleton(daveAIPerspective.getUser_login_model_name(),userSingleton.getString(daveAIPerspective.getUser_login_id_attr_name()), false, APIQueuePriority.ONE.getValue());
                    }
                }
            }

            new DaveService().startBackgroundService(context);
            new MediaService().startBackgroundService(context);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public void login(Context context, JSONObject postBody,boolean connectWithDaveAI){
        login(context,postBody,connectWithDaveAI,new ArrayList<String>());
    }



    public void login(Context context, JSONObject postBody,final boolean connectWithDaveAI,ArrayList<String> extraPerspectives){

        this.mContext=context;
        this.tempPreflList = new LinkedList<>();
        tempPreflList.addAll(extraPerspectives);

        APIResponse postTaskListener  = new APIResponse(){
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    try {
                        Log.e("LOGGER","LOGIN DATA 1 = "+response);
                         loginDetails = new JSONObject(response);
                        Log.e("LOGGER","LOGIN DATA 2 = "+loginDetails);

                        //JSONObject customer_details = new JSONObject(response);

                        sharedPreferences.writeString(DaveSharedPreference.USER_LOGIN_DETAILS, loginDetails.toString());
                        sharedPreferences.writeString(DaveSharedPreference.EnterpriseId, loginDetails.optString("enterprise_id"));
                        sharedPreferences.writeString(DaveSharedPreference.Role, loginDetails.optString("role"));
                        sharedPreferences.writeString(DaveSharedPreference.USER_ID, loginDetails.optString("user_id"));
                        sharedPreferences.writeString(DaveSharedPreference.API_KEY, loginDetails.optString("api_key"));
                        sharedPreferences.writeString(DaveSharedPreference.PUSH_TOKEN, loginDetails.optString("push_token"));

                        setUpLocalDatabase();
                        saveLoginDetails(loginDetails.optString("role"),loginDetails.optString("user_id"));
                        String apiKey = loginDetails.optString("api_key");
                        if(apiKey!=null && !apiKey.isEmpty()){

                            if(connectWithDaveAI) {
                                Log.e(TAG,"got into this if");
                                if(DatabaseManager.getInstance(mContext).checkErrorRows().size()==0) {
                                    connect("First Time Login: This may take a long time");
                                }else{
                                    onLoginAction.loginFailed("Unsynced Data found in cache. Please fix them before updating.");
                                }
                            }
                            else {
                                Log.e(TAG,"got into this else");

                                if(onLoginAction!=null) {
                                    onLoginAction.loginSucess(loginDetails);
                                }

                            }

                        }else{
                            if(pDialog!=null)
                                pDialog.dismiss();
                            onLoginAction.loginFailed("Login Failed");
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error User LogIn :-  " + e.getMessage());
                    }
                }
            }

            @Override
            public void onTaskFailure(int responseCode, String responseMsg) {
                if(pDialog!=null)
                    pDialog.dismiss();
                onLoginAction.loginFailed(responseMsg);


            }
        };
        try {
            if(sharedPreferences.checkStringExist(DaveSharedPreference.USER_ID) &&
                    !postBody.getString("user_id").equalsIgnoreCase(sharedPreferences.readString(DaveSharedPreference.USER_ID))){

                //if previous login details are different from current attempt.
                Log.i(TAG,"MAtch USer Id from db And Current "+postBody.getString("user_id")+" =="+
                        sharedPreferences.readString(DaveSharedPreference.USER_ID));
                deleteLocalDatabase();
                sharedPreferences.removeAll();

            }/*else if(sharedPreferences.readString(DaveSharedPreference.API_KEY)!=null && !TextUtils.isEmpty(sharedPreferences.readString(DaveSharedPreference.API_KEY))){
                // if previous attempt to login was successful and received valid api key and user id and bulkdownload etc.
                Log.e(TAG,"Previous user loggedIn again. is bulk data downloaded ?  == "+sharedPreferences.readBoolean(DaveSharedPreference.PROCESSING_BULK_DATA));
                connect("First Time Login: This may take a long time");


            }*/

            APIRoutes.getMetadata(mContext);
            RequestBody body_part = RequestBody.create(APIRoutes.MEDIA_TYPE_JSON, postBody.toString());
            if (CheckNetworkConnection.networkHasConnection(mContext)) {
                new APICallAsyncTask(postTaskListener, mContext, "POST", body_part,
                        true, "First Time Login: This may take a long time").execute(APIRoutes.userLoginAPI());
            } else {
                CheckNetworkConnection.showNetDisabledAlertToUser(mContext);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, " Error During Login with DaveAI:- " + e.getLocalizedMessage());
        }
    }
    enum ConnectStatus{

        CONNECT_SUCCESS (2),  //different models in the same table
        STARTED (1),  //different models in the same table
        READY_TO_CONNECT(0),
        CONNECT_FAILED (-1)  //different models in the same table
        ;


        private final int levelCode;

        private ConnectStatus(int levelCode) {
            this.levelCode = levelCode;
        }

        public int getValue() {
            return levelCode;
        }
    }



    private String getPivotHierarchy(ArrayList<String> customerHierarchyList,String modelName){
        StringBuilder categoryParam = new StringBuilder();
        if(customerHierarchyList!=null&&customerHierarchyList.size()>0) {
            categoryParam.append("/pivot/" + modelName + "/");
            if (customerHierarchyList != null) {
                for (int i = 0; i < customerHierarchyList.size(); i++) {
                    categoryParam.append(customerHierarchyList.get(i).toString());
                    if ((i + 1) < customerHierarchyList.size())
                        categoryParam.append("/");
                }
            }
        }
        return categoryParam.toString();
    }

    /*private HashMap<String,String> getFilterAttributePivots(ArrayList<String> filterList,String modelName){
        HashMap<String,String> pivots = new HashMap<>();
        Model modelInstance = Model.getModelInstance(mContext, modelName);
        ModelUtil modelUtil = new ModelUtil(mContext);
        DaveModels daveModels = new DaveModels(mContext,true);
        if (modelInstance != null && filterList!=null) {
            for (int i = 0; i < filterList.size(); i++) {
                String attrName = filterList.get(i).toString();
                String attrType = modelInstance.getAttributeType(attrName);

                Log.e(TAG,"getFilterAttributePivots:- AttrNAme:-" + attrName +" AttrType:- "+ attrType);
                if(!checkFilterAttrType(attrType)) {
                    if (attrType.equals("price") || attrType.equals("discount")) {

                        pivots.put(modelName + "_min_" + attrName + "___" + "pivot_min_" + attrName, APIRoutes.priceRangeAPI("min", attrName).split(DaveAIStatic.base_url)[1]);

                        pivots.put(modelName + "_max_" + attrName + "___" + "pivot_max_" + attrName, APIRoutes.priceRangeAPI("max", attrName).split(DaveAIStatic.base_url)[1]);

                    } else {

                        pivots.put(modelName + "_" + attrName + "___" + "pivot_" + attrName, APIRoutes.pivotAttributeAPI(modelName, attrName).split(DaveAIStatic.base_url)[1]);

                    }

                }else{
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(ADD_TYPE_VIEW);
                    if(modelName.equals(modelUtil.getCustomerModelName())){
                        daveModels.saveCustomerFilterAttr(attrName,jsonArray);

                    }else if(modelName.equals(modelUtil.getProductModelName())){
                        daveModels.saveProductFilterAttr(attrName,jsonArray);
                    }

                }
            }
        }
        return pivots;
    }*/

    public HashMap<String,String> getFilterAttributePivots(ArrayList<String> filterList,String modelName){
        HashMap<String,String> pivots = new HashMap<>();
        Model modelInstance = Model.getModelInstance(mContext, modelName);
        ModelUtil modelUtil = new ModelUtil(mContext);
        DaveModels daveModels = new DaveModels(mContext,true);
        if (modelInstance != null && filterList!=null) {
            for (int i = 0; i < filterList.size(); i++) {
                String attrName = filterList.get(i).toString();
                String attrType = modelInstance.getAttributeType(attrName);

                Log.e(TAG,"getFilterAttributePivots:- AttrNAme:-" + attrName +" AttrType:- "+ attrType);
                if(!checkFilterAttrType(attrType)) {
                    if (attrType.equals("price") || attrType.equals("discount")) {

                        pivots.put(modelName + "_min_" + attrName + "___" + "pivot_min_" + attrName, APIRoutes.priceRangeAPI("min", attrName).split(DaveAIStatic.base_url)[1]);

                        pivots.put(modelName + "_max_" + attrName + "___" + "pivot_max_" + attrName, APIRoutes.priceRangeAPI("max", attrName).split(DaveAIStatic.base_url)[1]);

                    } else {

                        pivots.put(modelName + "_" + attrName + "___" + "pivot_" + attrName, APIRoutes.pivotAttributeAPI(modelName, attrName).split(DaveAIStatic.base_url)[1]);

                    }

                }else{
                    Log.e(TAG,"ASDFASF  adding in else case for model = "+modelName+" attr = "+attrName+"  type  = "+attrType);
                    JSONArray jsonArray = new JSONArray();
                    jsonArray.put(ADD_TYPE_VIEW);
                    if(modelName.equals(modelUtil.getCustomerModelName())){
                        Log.e(TAG,"ASDFASF  customer");
                        daveModels.saveCustomerFilterAttr(attrName,jsonArray);

                    }else if(modelName.equals(modelUtil.getProductModelName())){
                        Log.e(TAG,"ASDFASF  customer");
                        daveModels.saveProductFilterAttr(attrName,jsonArray);
                    }

                }
            }
        }
        return pivots;
    }

    //filter feature for type: [name, uid, phone number, email] to directly type instead of pivot
    private boolean checkFilterAttrType(String filterAttrType){

        return filterAttrType.equals("name") || filterAttrType.equals("uid") || filterAttrType.equals("email")
                || filterAttrType.equals("phone_number");

    }


    private boolean isReadyForNewConnect(){


        DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
        Log.e(TAG,"Connect Status : "+ sharedPreference.readInteger(DaveSharedPreference.CONNECT_STATUS));
        Log.e(TAG,"Bulk Download Status: "+ sharedPreference.readBoolean(DaveSharedPreference.PROCESSING_BULK_DATA));
        if((sharedPreference.readInteger(DaveSharedPreference.CONNECT_STATUS) == ConnectStatus.CONNECT_SUCCESS.getValue())
                &&
                sharedPreference.readBoolean(DaveSharedPreference.PROCESSING_BULK_DATA)){
            return false;
        }
        return true;
    }
    private void callBulkDownload() {
        callBulkDownload(new ArrayList<String>());
    }

    private void callBulkDownload(ArrayList<String> extraModelNames) {
        try {
            //TODO call bulkdownload from here
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    setupModelCacheMetaData(INTERACTION_STAGES_TABLE_NAME, INTERACTION_STAGES_TABLE_NAME, mContext);
                    saveInteractionStagesDetails(response);
                    try {
                        JSONObject bulkPostBody = new JSONObject();
                        try {
                            JSONObject jobj = new JSONObject();
                            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                            String customerModel = databaseManager.getModelNameBasedOnType("customer");
                            String productModel = databaseManager.getModelNameBasedOnType("product");
                            String customerHierarchyPivot = getPivotHierarchy(daveAIPerspective.getCustomer_category_hierarchy(), customerModel);
                            String productHierarchyPivot = getPivotHierarchy(daveAIPerspective.getProduct_category_hierarchy(), productModel);
                            if (!TextUtils.isEmpty(customerHierarchyPivot)) {
                                jobj.put("customer_hierarchy___customer_hierarchy", customerHierarchyPivot);
                            }
                            if (!TextUtils.isEmpty(productHierarchyPivot)) {
                                jobj.put("product_hierarchy___product_hierarchy", productHierarchyPivot);
                            }

                            HashMap<String, String> customerFilterPivots = getFilterAttributePivots(daveAIPerspective.getCustomer_filter_attr(), customerModel);
                            HashMap<String, String> productFilterPivots = getFilterAttributePivots(daveAIPerspective.getProduct_filter_attr(), productModel);


                            for (Map.Entry<String, String> entry : customerFilterPivots.entrySet()) {
                                jobj.put(entry.getKey(), entry.getValue());
                            }
                            for (Map.Entry<String, String> entry : productFilterPivots.entrySet()) {
                                jobj.put(entry.getKey(), entry.getValue());
                            }

                            Log.e("LOGGER", "got filters =============================" + jobj);
                            Log.e("LOGGER", "got filters for " + customerModel + "   == " + customerFilterPivots.toString());
                            Log.e("LOGGER", "got filters for " + productModel + "   == " + productFilterPivots.toString());

                            JSONObject objectModels = new JSONObject();

                            for (int i = 0; i < getModelList.size(); i++) {
                                if (CORE_MODEL_LIST.contains(getModelList.get(i))) {
                                    objectModels.put(databaseManager.getModelNameBasedOnType(getModelList.get(i)), DEFAULT_OBJECT_COUNT);
                                } else {
                                    objectModels.put(getModelList.get(i), DEFAULT_OBJECT_COUNT);
                                }
                            }
                            for (int i = 0; i < extraModelNames.size(); i++) {

                                objectModels.put(extraModelNames.get(i), DEFAULT_OBJECT_COUNT);

                            }


                            bulkPostBody.put("objects", objectModels);
                            bulkPostBody.put("pivots", jobj);

                        }catch (Exception e) {
                            e.printStackTrace();
                            onLoginAction.loginFailed("Error parsing perspective data. " + e.getMessage() + " Please try again.");
                            return;
                        }
//                        Log.e(TAG,"bulkPostBody ============================="+bulkPostBody);

//                        showLoader("Loading core model data. Please wait.");
                        syncBulkData(bulkPostBody, BULK_TIME_OUT_MINUTES * 60 * 1000, new OnSyncBulkDataListener() {
                            @Override
                            public void onSyncBulkDataCompleted() {

                                DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
                                sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA, true);
                                sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.CONNECT_SUCCESS.getValue());
                                Log.e(TAG,"*****************Login bulk data download done *************************");
                                if(pDialog!=null&&pDialog.isShowing()){
                                    pDialog.cancel();
                                }
                                if(onLoginAction!=null)
                                    onLoginAction.loginSucess(loginDetails);
                                if(onSyncDataListener!=null){
                                    onSyncDataListener.onDataSynced(true,"data sync completed.");
                                }
                            }

                            @Override
                            public void onSyncBulkDataFailed(int errorCode, String errorMessage) {
                                if(pDialog!=null&&pDialog.isShowing()){
                                    pDialog.cancel();
                                }
                                if(onLoginAction!=null)
                                    onLoginAction.loginFailed(errorMessage);
                                if(onSyncDataListener!=null){
                                    onSyncDataListener.onDataSynced(true,"data sync failed.");
                                }
                            }
                        });
                    }catch (Exception e){
                        if(pDialog!=null&&pDialog.isShowing()){
                            pDialog.cancel();
                        }
                        e.printStackTrace();
                        onLoginAction.loginFailed("Error on bulk data download. Please try again.");
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    if(pDialog!=null&&pDialog.isShowing()){
                        pDialog.cancel();
                    }

                    onLoginAction.loginFailed("Error on bulk data download. Please try again.");
                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.interactionStagesAPI());

        }catch (Exception e){
            e.printStackTrace();

            onLoginAction.loginFailed("Error on bulk data download. Please try again.");
        }
    }



    private void getModels(String model){
        if(CORE_MODEL_LIST.contains(model)){
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        Log.e(TAG, "completed get model = " + response);
                        JSONArray modelJArray = new JSONArray(response);
                        JSONObject modelResponse = modelJArray.getJSONObject(0);
                        String modelName = modelResponse.getString("name");
                        String modelType = modelResponse.getString("model_type");
                        String modelIDAttrName = new ModelUtil(mContext).saveIDAttrNameOfCoreModel(modelType, modelResponse.toString());
                        Log.i(TAG, "getCoreModel model name modelIDAttrName>>>>" + modelName + " ModelType:-- " + modelType + " modelIDAttrName:--" + modelIDAttrName);
                        if (!modelIDAttrName.isEmpty()) {
                            setupModelCacheMetaData(modelName, modelIDAttrName, mContext);
                        }
                        if (modelJArray.length() > 0) {
                            DatabaseManager.getInstance(mContext).insertORUpdateModelData(modelName,
                                    new ModelTableRowModel(modelType, modelName, modelResponse.toString(), System.currentTimeMillis(), ""));


                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(tempGetModelList.size()>0){
                        getModels(tempGetModelList.remove());
                    }else{
                        try {
                            new ModelUtil(mContext).saveInteractionModelAttributes(new ModelUtil(mContext).getInteractionModelName());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.CONNECT_SUCCESS.getValue());
                        callBulkDownload();
                    }

                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG,"ON ERROR");

                    if(pDialog!=null&&pDialog.isShowing()){
                        pDialog.cancel();
                    }
                    sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.CONNECT_FAILED.getValue());
                    if(onLoginAction!=null)
                        onLoginAction.loginFailed(errorMsg);
                    if(onSyncDataListener!=null){
                        onSyncDataListener.onDataSyncFailed(false,errorMsg);
                    }
                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.getCoreModel(model), APIRequestMethod.GET.name());

        }else{
            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        Log.e(TAG, "completed get model = " + response);
                        JSONObject modelResponse = new JSONObject(response);
                        String modelName = modelResponse.getString("name");

                        String modelType = modelResponse.getString("model_type");
                        String modelIDAttrName = new ModelUtil(mContext).getIDAttrNameOfModel(modelResponse.toString());
                        Log.e(TAG, "get model name && modelIDAttrName>>>>" + modelName + " ModelType:-- " + modelType + " modelIDAttrName:--" + modelIDAttrName);
                        if (!modelIDAttrName.isEmpty()) {
                            setupModelCacheMetaData(modelName, modelIDAttrName, mContext);
                        }
                        DatabaseManager.getInstance(mContext).insertORUpdateModelData(modelName,
                                new ModelTableRowModel(modelType, modelName,
                                        modelResponse.toString(),
                                        System.currentTimeMillis(), ""));

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if(tempGetModelList.size()>0){
                        getModels(tempGetModelList.remove());
                    }else{
                        try {
                            new ModelUtil(mContext).saveInteractionModelAttributes(new ModelUtil(mContext).getInteractionModelName());
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.CONNECT_SUCCESS.getValue());
                        callBulkDownload();
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG,"ON ERROR");
                    if(pDialog!=null&&pDialog.isShowing()){
                        pDialog.cancel();
                    }
                    sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS,ConnectStatus.CONNECT_FAILED.getValue());
                    if(onLoginAction!=null)
                        onLoginAction.loginFailed(errorMsg);
                    if(onSyncDataListener!=null){
                        onSyncDataListener.onDataSyncFailed(false,errorMsg);
                    }
                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.modelAPI(model));
        }
    }

    private void getLoginDetails(){
        if(loginDetails!=null && loginDetails.length() == 0){
            loginDetails = new JSONObject();
            try{
                loginDetails.put("enterprise_id",sharedPreferences.readString(DaveSharedPreference.EnterpriseId));
                loginDetails.put("role",sharedPreferences.readString(DaveSharedPreference.Role));
                loginDetails.put("user_id",sharedPreferences.readString(DaveSharedPreference.USER_ID));
                loginDetails.put("api_key",sharedPreferences.readString(DaveSharedPreference.API_KEY));
                loginDetails.put("push_token",sharedPreferences.readString(DaveSharedPreference.PUSH_TOKEN));

                //loginDetails = new JSONObject(sharedPreferences.readString(DaveSharedPreference.USER_LOGIN_DETAILS));
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /*private void connect(String loaderMSG) {
        //todo check cached data ......track api call
        if(!loaderMSG.isEmpty())
            showLoader(loaderMSG);

        getLoginDetails();
        Log.e(TAG,"isReadyForNewConnect() == "+isReadyForNewConnect());
        if (isReadyForNewConnect()) {
            //setting connect status as startedConnect
            sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.STARTED.getValue());
            APIRoutes.getMetadata(mContext);
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        Log.e(TAG, "completed perspective = " + response);
                        daveAIPerspective.savePerspectivePreferenceValue(mContext, response);
                        setupModelCacheMetaData("perspective_settings", "perspective_settings", mContext);
                        databaseManager.insertOrUpdateSingleton(
                                "perspective_settings",
                                new SingletonTableRowModel("perspective_settings", response, System.currentTimeMillis(), "perspective_settings", "")
                        );


                        //getting interactions stages

                        tempGetModelList.clear();
                        getModelList.clear();
                        String customerModel = "customer";
                        String productModel = "product";
                        String interactionModel = "interaction";
                        String invoiceModel = daveAIPerspective.getInvoice_model_name();
                        String userLoginModel = daveAIPerspective.getUser_login_model_name();

                        getModelList.add(customerModel);
                        getModelList.add(productModel);
                        getModelList.add(interactionModel);
                        getModelList.add(invoiceModel);
                        getModelList.add(userLoginModel);

                        tempGetModelList.addAll(getModelList);
                        Log.e(TAG,"Print modelList :-------------"+ tempGetModelList);

                        if(tempPreflList.size()>0){
                            getOtherMobilePerspectives(tempPreflList.remove());
                        }
                        else if (tempGetModelList.size() > 0) {
                            getModels(tempGetModelList.remove());

                        } else {
                            sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());
                            callBulkDownload();
                        }
                        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());

                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG, "ON ERROR");
                    if(pDialog!=null&&pDialog.isShowing()){
                        pDialog.cancel();
                    }
                    if(onLoginAction!=null)
                    onLoginAction.loginFailed(errorMsg);
                    if(onSyncDataListener!=null){
                        onSyncDataListener.onDataSyncFailed(false,errorMsg);
                    }
                    sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());

                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.getPerspectiveAPI("mobile_application"), APIRequestMethod.GET.name());


        }
        else {

            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            DaveSharedPreference sharedPreference = new DaveSharedPreference(mContext);
            if (sharedPreference.readInteger(DaveSharedPreference.CONNECT_STATUS) == ConnectStatus.CONNECT_SUCCESS.getValue() && sharedPreference.readBoolean(DaveSharedPreference.PROCESSING_BULK_DATA)) {
                if(databaseManager.isTableExists(new ModelUtil(mContext).getCustomerModelName())||databaseManager.isTableExists(new ModelUtil(mContext).getProductModelName())) {
                    Cursor c = databaseManager.openDatabase().rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
                    if (c.moveToFirst()) {
                        while (!c.isAfterLast()) {
                            if (c.getString(0).startsWith("old___")) {
                                String[] newTableName = c.getString(0).split("old___");
                                if (databaseManager.isTableExists(newTableName[1])) {
                                    databaseManager.dropTable(newTableName[1]);
                                }
                                databaseManager.renameTable(c.getString(0), newTableName[1]);


//                            DatabaseManager.getInstance(mContext).dropTable(c.getString(0));
                            }
                            c.moveToNext();
                        }

                        sharedPreference.writeBoolean(DaveSharedPreference.PROCESSING_BULK_DATA, false);
                    }
                    File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/");
                    if (!folder.exists())
                        folder.mkdirs();
                    String finalFileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/dave/cached_files/"
                            + "bulkData.txt";
                    File file = new File(finalFileName);
                    if (file.exists()) {

                        StringBuilder text = new StringBuilder();
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(file));
                            String line;

                            while ((line = br.readLine()) != null) {
                                text.append(line);
                                text.append('\n');
                            }
                            br.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            //You'll need to add proper error handling here
                        }
                        if (!TextUtils.isEmpty(text.toString())) {
                            new DaveModels(mContext, true).processBulkData(text.toString(), new JSONObject(), file);
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.cancel();
                            }
                            onLoginAction.loginSucess(loginDetails);
                        }
                    }else{
                        if(pDialog!=null&&pDialog.isShowing()){
                            pDialog.cancel();
                        }
                        onLoginAction.loginFailed("Error in syncing data.");
                    }
                    c.close();
                }else{
                    callBulkDownload();
                }
            } else {
                if(pDialog!=null&&pDialog.isShowing()){
                    pDialog.cancel();
                }
                onLoginAction.loginFailed("Error in syncing data.");
            }
        }
    }*/

    private void connect(String loaderMSG) {
        //todo check cached data ......track api call

        getLoginDetails();
        Log.e(TAG,"isReadyForNewConnect() == "+isReadyForNewConnect());
        if (isReadyForNewConnect()) {
            if(!loaderMSG.isEmpty())
                showLoader(loaderMSG);
            //setting connect status as startedConnect
            sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.STARTED.getValue());
            APIRoutes.getMetadata(mContext);
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);

            APIResponse postTaskListener = new APIResponse() {
                @Override
                public void onTaskCompleted(String response) {
                    try {
                        Log.e(TAG, "completed perspective = " + response);
                        daveAIPerspective.savePerspectivePreferenceValue(mContext, response);
                        setupModelCacheMetaData("perspective_settings", "perspective_settings", mContext);
                        databaseManager.insertOrUpdateSingleton(
                                "perspective_settings",
                                new SingletonTableRowModel("perspective_settings", response, System.currentTimeMillis(), "perspective_settings", "")
                        );


                        //getting interactions stages

                        tempGetModelList.clear();
                        getModelList.clear();
                        String customerModel = "customer";
                        String productModel = "product";
                        String interactionModel = "interaction";
                        String invoiceModel = daveAIPerspective.getInvoice_model_name();
                        String userLoginModel = daveAIPerspective.getUser_login_model_name();

                        getModelList.add(customerModel);
                        getModelList.add(productModel);
                        getModelList.add(interactionModel);
                        getModelList.add(invoiceModel);
                        getModelList.add(userLoginModel);

                        tempGetModelList.addAll(getModelList);
                        Log.e(TAG,"Print modelList :-------------"+ tempGetModelList);

                        if(tempPreflList.size()>0){
                            getOtherMobilePerspectives(tempPreflList.remove());
                        }
                        else if (tempGetModelList.size() > 0) {
                            getModels(tempGetModelList.remove());

                        } else {
                            sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());
                            callBulkDownload();
                        }
                        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());

                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(mContext,e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onTaskFailure(int responseCode, String errorMsg) {
                    Log.e(TAG, "ON ERROR");
                    if(pDialog!=null&&pDialog.isShowing()){
                        pDialog.cancel();
                    }
                    if(onLoginAction!=null)
                        onLoginAction.loginFailed(errorMsg);
                    if(onSyncDataListener!=null){
                        onSyncDataListener.onDataSyncFailed(false,errorMsg);
                    }
                    sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());

                }
            };
            new APICallAsyncTask(postTaskListener, mContext, "GET", false).execute(APIRoutes.getPerspectiveAPI("mobile_application"), APIRequestMethod.GET.name());


        }
        else {
            initSDK(mContext,"");
        }
    }



    private void getOtherMobilePerspectives(String prefName) {

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                try {
                    setupModelCacheMetaData(prefName, "perspective_settings",mContext);
                    databaseManager.insertOrUpdateSingleton(
                            prefName,
                            new SingletonTableRowModel(prefName, response, System.currentTimeMillis(), "perspective_settings", "")
                    );

                    if(tempPreflList.size()>0){
                        getOtherMobilePerspectives(tempPreflList.remove());
                    }
                    else if (tempGetModelList.size() > 0) {
                        getModels(tempGetModelList.remove());

                    } else {
                        sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_SUCCESS.getValue());
                        callBulkDownload();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG,"Error get other perspective:----------"+ e.getMessage());

                }
            }

            @Override
            public void onTaskFailure(int responseCode, String errorMsg) {
                Log.e(TAG, " Get Other Pref ON ERROR" + responseCode +" "+ errorMsg);
                if(pDialog!=null&&pDialog.isShowing()){
                    pDialog.cancel();
                }
                if(onLoginAction!=null)
                    onLoginAction.loginFailed(errorMsg);
                if(onSyncDataListener!=null){
                    onSyncDataListener.onDataSyncFailed(false,errorMsg);
                }
                sharedPreferences.writeInteger(DaveSharedPreference.CONNECT_STATUS, ConnectStatus.CONNECT_FAILED.getValue());
            }
        };
        new APICallAsyncTask(postTaskListener, mContext, "GET", false)
                .execute(APIRoutes.getPerspectiveAPI(prefName), APIRequestMethod.GET.name());
    }


    private void saveAllLoginApiInQueueTable(Context context){
        try {
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            JSONObject perspectiveSettings = databaseManager.getSingletonRow("perspective_settings");
            if(perspectiveSettings!=null && perspectiveSettings.length()>0){
                daveAIPerspective.savePerspectivePreferenceValue(mContext,perspectiveSettings.toString());
                getPerspectivePreference(false,APIQueuePriority.ONE.getValue());
            }else{
                getPerspectivePreference(true,APIQueuePriority.LOGIN.getValue());
            }

            if(databaseManager.isTableEmpty(INTERACTION_STAGES_TABLE_NAME)){
                getInteractionStagesList(true,APIQueuePriority.LOGIN.getValue());

            }else{
                getInteractionStagesList(false,APIQueuePriority.ONE.getValue());
            }

            String customerModel = databaseManager.getModelNameBasedOnType("customer");
            if(customerModel!= null && !customerModel.isEmpty()){
                getModel(customerModel,false,APIQueuePriority.ONE.getValue());
                getObjectsList(customerModel,false,APIQueuePriority.ONE.getValue());

                JSONObject customerHierarchy = databaseManager.getSingletonRow("customer_hierarchy");
                if(customerHierarchy!=null && customerHierarchy.length()>0){
                    getCategoryHierarchyList("customer",customerModel,daveAIPerspective.getCustomer_category_hierarchy(),
                            false,APIQueuePriority.ONE.getValue());
                }
                getFilterAttr(customerModel,daveAIPerspective.getCustomer_filter_attr(),false,APIQueuePriority.ONE.getValue());
            }else{
                getCoreModel("customer");
            }

            String productModel = databaseManager.getModelNameBasedOnType("product");
            if(productModel!= null && !productModel.isEmpty()){
                getModel(productModel,false,APIQueuePriority.ONE.getValue());
                getObjectsList(productModel,false,APIQueuePriority.ONE.getValue());

                JSONObject productHierarchy = databaseManager.getSingletonRow("product_hierarchy");
                if(productHierarchy!=null && productHierarchy.length()>0){
                    getCategoryHierarchyList("product",productModel,daveAIPerspective.getProduct_category_hierarchy(),
                            false,APIQueuePriority.ONE.getValue());
                }
                getFilterAttr(productModel,daveAIPerspective.getProduct_filter_attr(),false,APIQueuePriority.ONE.getValue());
            }else{
                getCoreModel("product");
            }

            String interactionModel = databaseManager.getModelNameBasedOnType("interaction");
            if(interactionModel!= null && !interactionModel.isEmpty()){
                getModel(interactionModel,false,APIQueuePriority.ONE.getValue());
                getObjectsList(interactionModel,false,APIQueuePriority.ONE.getValue());
            }else{
                getCoreModel("interaction");
            }

            if(daveAIPerspective.getInvoice_model_name()!= null && !daveAIPerspective.getInvoice_model_name().isEmpty()
                    && getModelFromDB(daveAIPerspective.getInvoice_model_name()) ) {

                getModel(daveAIPerspective.getInvoice_model_name(),false,APIQueuePriority.ONE.getValue());
                getObjectsList(daveAIPerspective.getInvoice_model_name(),false,APIQueuePriority.ONE.getValue());
            }

            if(daveAIPerspective.getUser_login_model_name()!=null && !daveAIPerspective.getUser_login_model_name().isEmpty()){
                getModel(daveAIPerspective.getUser_login_model_name(),false,APIQueuePriority.ONE.getValue());
                //JSONObject userSingleton = databaseManager.getSingletonBasedOnRequestType("login");
                JSONObject userSingleton = databaseManager.getSingletonRow(daveAIPerspective.getUser_login_model_name());
                if(daveAIPerspective.getUser_login_id_attr_name()!=null && !daveAIPerspective.getUser_login_id_attr_name().isEmpty()) {
                    if(userSingleton!= null && userSingleton.length()>0 && userSingleton.has(daveAIPerspective.getUser_login_id_attr_name())){
                        getUserSingleton(daveAIPerspective.getUser_login_model_name(),userSingleton.getString(daveAIPerspective.getUser_login_id_attr_name()), false, APIQueuePriority.ONE.getValue());
                    }
                }
            }

//            Log.e("LOGGER","pivots found = "+pivots.toString());


            new DaveService().startBackgroundService(context);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * method to save user details as Singleton and user Model in Database.
     * @param modelName user Login model name
     * @param objectId user id
     */
    private void saveLoginDetails(final String modelName, final String objectId){

        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        boolean isUserDetailsFresh = true;
        int priorityLevel = APIQueuePriority.LOGIN.getValue();
        CacheModelPOJO cacheModelPOJO = databaseManager.getMetaData(modelName);
        if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
            CacheRefreshStatus refreshStatus = databaseManager.updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
            switch (refreshStatus) {
                case COLD_UPDATE_REQUIRED:
                    isUserDetailsFresh = true;
                    break;
                case HOT_UPDATE_REQUIRED:
                    isUserDetailsFresh = true;
                    priorityLevel = APIQueuePriority.ONE.getValue();
                    break;
                case NOT_REQUIRED:
                    isUserDetailsFresh = false;
                    break;
            }
            Log.i(TAG, "<<<<<<<<<<< Login Details refreshStatus:-" + refreshStatus +" isUserDetailsFresh:-"+ isUserDetailsFresh);
        }
        if(isUserDetailsFresh) {

            if(databaseManager.isTableExists(POST_QUEUE_TABLE_NAME))
               databaseManager.createTable(POST_QUEUE_TABLE_NAME,CacheTableType.POST_QUEUE);

            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.updateOrGetObjectAPI(modelName, objectId), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", DatabaseConstants.APIQueueStatus.PENDING.name(),
                            priorityLevel, CacheTableType.SINGLETON.getValue()
                    )
            );

            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.modelAPI(modelName), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", DatabaseConstants.APIQueueStatus.PENDING.name(),
                            priorityLevel,CacheTableType.MODEL.getValue()
                    )
            );
        }

    }


    private void getPerspectivePreference(boolean isFresh, int priorityLevel){
        try {
            if(!isFresh) {
                CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData("perspective_settings");
                if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {

                    CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case NOT_REQUIRED:
                            isFresh = false;
                            break;
                    }
                     Log.e(TAG, "readPerspectivePreference check meta update:- " + cacheModelPOJO.modelName +" status:- "+ refreshStatus);
                }
            }
            if (isFresh) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.getPerspectiveAPI("mobile_application"), APIRequestMethod.GET.name(),
                                "", "perspective_settings",
                                "perspective_settings", DatabaseConstants.APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.SINGLETON.getValue()
                        )
                );
            }

        }catch (Exception e){
            Log.e(TAG, " Error During readPerspectivePreference " + e.getMessage());
        }
    }

    private void getInteractionStagesList(boolean isFresh, int priorityLevel){
        try {
            if (!isFresh){
                CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(INTERACTION_STAGES_TABLE_NAME);
                if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                    CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.OBJECTS);
                    switch (refreshStatus) {
                        case COLD_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case HOT_UPDATE_REQUIRED:
                            isFresh = true;
                            break;
                        case NOT_REQUIRED:
                            isFresh = false;
                            break;
                    }
                    Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<getInteractionList refreshStatus:- " + refreshStatus);
                }
            }
            if(isFresh) {
                DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.interactionStagesAPI(), DaveConstants.APIRequestMethod.GET.name(),
                                "", INTERACTION_STAGES_TABLE_NAME,
                                INTERACTION_STAGES_TABLE_NAME, DatabaseConstants.APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.OBJECTS.getValue()
                        )
                );
            }
        }catch (Exception e){
            Log.e(TAG, " Error During getInteractionList " + e.getMessage());
        }
    }

    private void getCoreModel(final String modelType){
       // Log.e(TAG,"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<getCoreModel of ModelType :---------"+modelType );
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        databaseManager.insertAPIQueueData(
                new APIQueueTableRowModel(
                        APIRoutes.getCoreModel(modelType), DaveConstants.APIRequestMethod.GET.name(),
                        "", modelType,
                        modelType, DatabaseConstants.APIQueueStatus.PENDING.name(),
                        APIQueuePriority.LOGIN.getValue(),CacheTableType.MODEL.getValue()
                )
        );


    }

    public void getObjectsList(final String modelName, boolean isFresh,int priorityLevel){
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        if (!databaseManager.isTableExists(modelName))
            databaseManager.createTable(modelName, CacheTableType.OBJECTS);
        if(!isFresh) {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName, CacheTableType.MODEL);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<getObjectsList " + modelName + " && refreshStatus:- " + refreshStatus);
            }
        }
        if(isFresh) {
            HashMap<String,Object> queryParam = new HashMap<>();
            queryParam.put("_page_size", DaveHelper.getInstance().getObjectPageSize());
           // queryParam.put("_page_number", 1);
            if(priorityLevel!= APIQueuePriority.LOGIN.getValue()) {
                queryParam.put("_last_updated", (databaseManager.getLastupdated(modelName, CacheTableType.OBJECTS) / 1000));
            }
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.getObjectsAPI(modelName,queryParam), DaveConstants.APIRequestMethod.GET.name(),
                            "", modelName,
                            "", DatabaseConstants.APIQueueStatus.PENDING.name(),
                            priorityLevel,CacheTableType.OBJECTS.getValue()
                    )
            );
        }

    }

    public void getCategoryHierarchyList(String modelType,String modelName, ArrayList hierarchyList, boolean isFresh,int priorityLevel ){
        if (!isFresh){
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelType+"_hierarchy");
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<getCategoryHierarchyList refreshStatus:- " + refreshStatus);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            StringBuilder categoryParam = new StringBuilder();
            if(hierarchyList != null){
                for (int i=0;i<hierarchyList.size();i++){
                    categoryParam.append(hierarchyList.get(i).toString());
                    if((i+1)< hierarchyList.size())
                        categoryParam.append("/");
                }

//                pivots.put(modelType+"_hierarchy"+"___"+modelType+"_hierarchy",APIRoutes.pivotAttributeAPI(modelName,categoryParam.toString()));
                databaseManager.insertAPIQueueData(
                        new APIQueueTableRowModel(
                                APIRoutes.pivotAttributeAPI(modelName,categoryParam.toString()), DaveConstants.APIRequestMethod.GET.name(),
                                "", modelType+"_hierarchy",
                                modelType+"_hierarchy", DatabaseConstants.APIQueueStatus.PENDING.name(),
                                priorityLevel,CacheTableType.SINGLETON.getValue()
                        )
                );

            }
        }


    }



//    HashMap<String,String> pivots = new HashMap<>();
    public void getFilterAttr(String modelName , ArrayList filterList, boolean isFresh,int priorityLevel) {

       Model modelInstance = Model.getModelInstance(mContext, modelName);
       if (modelInstance != null && filterList!=null) {
           for(int i= 0;i<filterList.size();i++){
               String attrName = filterList.get(i).toString();
               String attrType = modelInstance.getAttributeType(attrName);

               if(!checkFilterAttrType(attrType)) {
                   if (!isFresh) {
                       CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(attrName);
                       if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                           CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(cacheModelPOJO.modelName, CacheTableType.SINGLETON);
                           switch (refreshStatus) {
                               case COLD_UPDATE_REQUIRED:
                                   isFresh = true;
                                   break;
                               case HOT_UPDATE_REQUIRED:
                                   isFresh = true;
                                   break;
                               case NOT_REQUIRED:
                                   isFresh = false;
                                   break;
                           }
                           Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<getFilterAttr refreshStatus:- " + refreshStatus);
                       }
                   }
                   if (isFresh) {
                       Log.e(TAG, " Call FILTER_TYPE_LIST Method  :--- " + attrName + " attrType:" + attrType);
                       DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
                       if (attrType.equals("price") || attrType.equals("discount")) {
                           databaseManager.insertAPIQueueData(
                                   new APIQueueTableRowModel(

                                           APIRoutes.priceRangeAPI("min", attrName), DaveConstants.APIRequestMethod.GET.name(),
                                           "", modelName + "_min_" + attrName,

                                           "pivot_min_" + attrName, DatabaseConstants.APIQueueStatus.PENDING.name(),
                                           priorityLevel, CacheTableType.SINGLETON.getValue()
                                   )
                           );

                           databaseManager.insertAPIQueueData(
                                   new APIQueueTableRowModel(

                                           APIRoutes.priceRangeAPI("max", attrName), DaveConstants.APIRequestMethod.GET.name(),
                                           "", modelName + "_max_" + attrName,

                                           "pivot_max_" + attrName, DatabaseConstants.APIQueueStatus.PENDING.name(),
                                           priorityLevel, CacheTableType.SINGLETON.getValue()
                                   )
                           );
                       } else {

                           databaseManager.insertAPIQueueData(
                                   new APIQueueTableRowModel(
                                           APIRoutes.pivotAttributeAPI(modelName, attrName), DaveConstants.APIRequestMethod.GET.name(),
                                           "", modelName + "_" + attrName,
                                           "pivot_" + attrName, DatabaseConstants.APIQueueStatus.PENDING.name(),
                                           priorityLevel, CacheTableType.SINGLETON.getValue()
                                   )
                           );

                       }


                   }
               }

           }


       }else {
           Log.e(TAG," modelInstance or Filterlist is null:------"+modelInstance +" filterList:---- "+filterList);
       }


   }



    public void getModel(final String modelName, boolean isFresh, int priorityLevel){
        if(!isFresh) {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName, CacheTableType.MODEL);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<getModel " + modelName + " && refreshStatus:- " + refreshStatus);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.modelAPI(modelName), DaveConstants.APIRequestMethod.GET.name(),
                            "", modelName,
                            modelName, DatabaseConstants.APIQueueStatus.PENDING.name(),
                            priorityLevel,CacheTableType.MODEL.getValue()
                    )
            );
        }

    }


    public boolean getModelFromDB(String modelName){
        DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
        JSONObject modelResponse =  databaseManager.getModelData(modelName);
        return modelResponse != null && modelResponse.length() > 0;
    }

    public void getUserSingleton(final String modelName,String objectId, boolean isFresh, int priorityLevel){

        if(!isFresh) {
            CacheModelPOJO cacheModelPOJO = DatabaseManager.getInstance(mContext).getMetaData(modelName);
            if (cacheModelPOJO != null && !cacheModelPOJO.modelName.isEmpty()) {
                CacheRefreshStatus refreshStatus = DatabaseManager.getInstance(mContext).updateStatus(modelName, CacheTableType.MODEL);
                switch (refreshStatus) {
                    case COLD_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case HOT_UPDATE_REQUIRED:
                        isFresh = true;
                        break;
                    case NOT_REQUIRED:
                        isFresh = false;
                        break;
                }
                Log.i(TAG, "<<<<<<<<<<<<<<<<getUserSingleton " + modelName + " && refreshStatus:- " + refreshStatus+" isFresh:-"+isFresh);
            }
        }
        if(isFresh) {
            DatabaseManager databaseManager = DatabaseManager.getInstance(mContext);
            databaseManager.insertAPIQueueData(
                    new APIQueueTableRowModel(
                            APIRoutes.updateOrGetObjectAPI(modelName, objectId), APIRequestMethod.GET.name(),
                            "", modelName,
                            "login", DatabaseConstants.APIQueueStatus.PENDING.name(),
                            priorityLevel, CacheTableType.SINGLETON.getValue()
                    )
            );
        }

    }


    public interface OnSyncBulkDataListener{
        void onSyncBulkDataCompleted();
        void onSyncBulkDataFailed(int errorCode, String errorMessage);
    }

    private void syncBulkData(JSONObject requestModelsPivots, long maxWaitTime, OnSyncBulkDataListener onSyncBulkDataListener ){
        DaveService daveService = new DaveService();
        daveService.startBackgroundService(mContext);
        long bulkStartTime = System.currentTimeMillis();

        DatabaseManager.getInstance(mContext).alertOnAPIQueueIsEmpty(new DatabaseManager.OnAPIQueueEmptyListener() {
            @Override
            public void isEmpty() {

                ((android.app.Activity)mContext).runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            DaveModels daveModels = new DaveModels(mContext, false);
                            daveModels.postBulkRequest(requestModelsPivots, new DaveAIListener() {
                                @Override
                                public void onReceivedResponse(JSONObject jsonObject) {
                                    try {
                                        daveModels.getBulkData(jsonObject.getString("download_id"),requestModelsPivots, new DaveAIListener() {
                                            @Override
                                            public void onReceivedResponse(JSONObject jsonObject) {
                                                onSyncBulkDataListener.onSyncBulkDataCompleted();
                                            }

                                            @Override
                                            public void onResponseFailure(int i, String s) {
                                                onSyncBulkDataListener.onSyncBulkDataFailed(i,s);
                                            }
                                        },maxWaitTime,bulkStartTime);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        onSyncBulkDataListener.onSyncBulkDataFailed(-1,"Error Syncing Data");
                                    }
                                }

                                @Override
                                public void onResponseFailure(int i, String s) {
                                    onSyncBulkDataListener.onSyncBulkDataFailed(i,s);

                                }
                            });

                        }catch (Exception e){
                            e.printStackTrace();
                            onSyncBulkDataListener.onSyncBulkDataFailed(-1,"Error Syncing Data");
                        }
                    }
                });
            }
        },5*1000);
    }


}





