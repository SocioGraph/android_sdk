package com.example.admin.daveai.broadcasting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;



/**
 * Created by Rinki on 08,August,2019
 */
public class AlarmManagerService {

    private final static String TAG = "AlarmManagerService";
    public static final int ALARM_REQUEST_CODE = 101;
    private Context context;

    public boolean isAlarmBroadcastRegistered(Context context, String action, Class clazz) {
        Intent intent = new Intent(action);
        intent.setClass(context, clazz);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_NO_CREATE) != null;
    }


    public AlarmManagerService(Context context){
        this.context=context.getApplicationContext();
    }

    /**
     * method to schedule some job
     * @param intent pass contructed intent that will execute the AlarmReceiver
     * @param intervalMillis pass time in Minute after that repeat task
     */
    public void scheduleAlarm(Intent intent ,long intervalMillis) {

        // Construct an intent that will execute the AlarmReceiver
        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
        // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
        Log.e(TAG, "******************Print  scheduleAlarm()*********************** " + alarm);
        if (alarm != null) {
            //alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis, AlarmManager.INTERVAL_HOUR, pIntent);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), intervalMillis, pIntent);
            //alarmManager.setInexactRepeating(android.app.AlarmManager.ELAPSED_REALTIME_WAKEUP, System.currentTimeMillis() + android.app.AlarmManager.INTERVAL_HOUR, android.app.AlarmManager.INTERVAL_HOUR, pendingIntent);
        }
    }


    private void scheduleAlarm(long intervalMillis) {

        if(intervalMillis ==0)
            intervalMillis = AlarmManager.INTERVAL_HOUR;

        // Construct an intent that will execute the AlarmReceiver
        Intent intent = new Intent(context.getApplicationContext(), DaveReceiver.class);

        // Create a PendingIntent to be triggered when the alarm goes off
        final PendingIntent pIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis(); // alarm is set right away
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Log.e(TAG, "******************Print  scheduleAlarm()*********************** " + alarm);
        if (alarm != null) {
            // First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
            // Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY

            //alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis, AlarmManager.INTERVAL_HOUR, pIntent);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent); // Millisec * Second * Minute
            Log.e(TAG, "******************Print  scheduleAlarm()*********************** " + alarm);
        }
    }



    private void startAlarm() {
        //create alarmManager
        android.app.AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, DaveReceiver.class);
        /* 1st Param : Context  2nd Param : Integer request code  3rd Param : Wrapped Intent  4th Intent: Flag */
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ALARM_REQUEST_CODE, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 0, pendingIntent);

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, 0, pendingIntent);

        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, 0, pendingIntent);
        }
    }


}

/*  private void cancelAlarm() {
        alarmManager.cancel(pendingIntent);
        Log.e(TAG,"Alarm Cancelled");
    }*/

    /*
    With FLAG_NO_CREATE it will return null if the PendingIntent doesnt already exist. If it already exists it returns
    reference to the existing PendingIntent
   */
   /* private void cancelHourlyAlarm(){

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, ALARM_REQUEST_CODE, intent, PendingIntent.FLAG_NO_CREATE);
        if (pendingIntent != null)
            alarmManager.cancel(pendingIntent);
    }*/
