package com.example.admin.daveai.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.util.HashMap;

import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;
import static com.example.admin.daveai.network.APIRoutes.getMetadata;
import static com.example.admin.daveai.others.DaveAIStatic.base_url;

/**
 * Created by Rinki on 08,July,2019
 */
public class APICallUtil {

    private Context context;


    public  APICallUtil(Context context){
        this.context =context;

    }

    public void postObject(final String model_name, JSONObject postBody, boolean loader, APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader).execute(APIRoutes.postObjectAPI(model_name));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }


    }


    public void getObjects(final String model_name, HashMap<String,Object> queryParams, boolean loader, APIResponse postTaskListener){
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"GET",loader).execute(APIRoutes.getObjectsAPI(model_name,queryParams));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public void updateObject(final String model_name, String id, JSONObject postBody, boolean loader, APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"UPDATE",body_part,loader).execute(APIRoutes.updateOrGetObjectAPI(model_name,id));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public void getPivot(final String model_name, HashMap<String,String> queryParams, boolean loader, APIResponse postTaskListener){
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"GET",loader).execute(APIRoutes.getPivotAPI(model_name,queryParams));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public void shareCatalogue(final String customerId,JSONObject postBody,boolean loader,APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {

            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader,60L)
                    .execute(APIRoutes.catalogueAPI(customerId));

        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }


    }



    public void getAppConfigurationDetails(String userId, String apiKey ,APIResponse postTaskListener){
        if(base_url==null)
            getMetadata(context);

        DaveSharedPreference sharedPreference = new DaveSharedPreference(context);
        sharedPreference.writeString(DaveSharedPreference.USER_ID,userId);
        sharedPreference.writeString(DaveSharedPreference.API_KEY, apiKey);
        if (CheckNetworkConnection.networkHasConnection(context)) {
            System.out.println("*****************Get APp Config Details:-------------"+APIRoutes.appConfigureAPI());
            new APICallAsyncTask(postTaskListener, context,"GET",true,"Checking for App Update...",10L).execute(APIRoutes.appConfigureAPI());
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }


    public void postConversation(String customerId,String conversation_id, JSONObject postBody,boolean loader,APIResponse postTaskListener){

        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader)
                    .execute(APIRoutes.conversationAPI(customerId,conversation_id));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }
}
