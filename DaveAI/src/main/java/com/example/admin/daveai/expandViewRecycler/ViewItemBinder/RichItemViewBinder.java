package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.RichItem;

import me.drakeet.multitype.ItemViewBinder;


public class RichItemViewBinder extends ItemViewBinder<RichItem, RichItemViewBinder.RichHolder> {

    @Override
    protected @NonNull
    RichHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_rich, parent, false);
        return new RichHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull RichHolder holder, @NonNull RichItem richContent) {
        holder.text.setText(richContent.text);
        holder.image.setImageResource(richContent.imageResId);
    }

    static class RichHolder extends RecyclerView.ViewHolder {

        @NonNull
        final TextView text;
        @NonNull
        final ImageView image;


        RichHolder(@NonNull View itemView) {
            super(itemView);
            this.text = itemView.findViewById(R.id.text);
            this.image = itemView.findViewById(R.id.image);
        }
    }
}
