package com.example.admin.daveai.broadcasting;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;
import java.util.HashMap;



public class SendBroadcast {

    private final static String TAG = "SendBroadcast";
    private Context context;
    DaveReceiver daveReceiver;

    public static final String REFRESHED_TABLE_ACTION = ".RefreshedTableAction";


    public static final String REFRESHED_TABLE_MSG ="TableRefreshed";


    public SendBroadcast(Context context){
        this.context= context;

    }

    public void sendBroadcast(String actionName, String broadcastMsg, HashMap<String,Object> data){

        Log.e(TAG,"Send Broadcast action name :--- "+context.getPackageName()+actionName );

        Intent intent = new Intent();
        intent.setAction(context.getPackageName()+actionName);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.putExtra("broadcast_msg",broadcastMsg);
        intent.putExtra("broadcast_data",data);
        context.sendBroadcast(intent);
    }

    public static void sendCustomBroadcast(Context context, String actionName, String broadcastMsg, String data){

        Log.e(TAG,"Send Broadcast action name :--- "+context.getPackageName()+actionName );

        Intent intent = new Intent();
        intent.setAction(context.getPackageName()+actionName);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.putExtra("broadcast_msg",broadcastMsg);
        intent.putExtra("broadcast_data",data);
        context.sendBroadcast(intent);
    }


    private void registerWifiReceiver() {

      /*  IntentFilter filter = new IntentFilter();
        filter.addAction(getPackageName() + "android.net.conn.CONNECTIVITY_CHANGE");*/

        daveReceiver = new DaveReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(daveReceiver, filter);
    }

    private void unregisterWifiReceiver() {
        context.unregisterReceiver(daveReceiver);
    }

    private void registerNetworkBroadcast() {
        /*IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        this.registerReceiver(br, filter);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.registerReceiver(daveReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.registerReceiver(daveReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    protected void unregisterNetworkChanges() {
        try {
            context.unregisterReceiver(daveReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


}
