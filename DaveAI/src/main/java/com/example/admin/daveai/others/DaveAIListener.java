package com.example.admin.daveai.others;

import org.json.JSONObject;

public interface DaveAIListener {

    void onReceivedResponse(JSONObject response);
    void onResponseFailure( int responseCode, String responseMsg);
}
