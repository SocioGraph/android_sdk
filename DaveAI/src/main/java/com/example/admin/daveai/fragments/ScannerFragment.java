package com.example.admin.daveai.fragments;



import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.google.zxing.Result;
import java.util.Objects;
import me.dm7.barcodescanner.zxing.ZXingScannerView;



public class ScannerFragment extends Fragment implements  ZXingScannerView.ResultHandler {

    private final String TAG = getClass().getSimpleName();
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private ZXingScannerView mScannerView;
    private OnScanCompleteListener mListener;
    private DaveAIPerspective daveAIPerspective;

   /* public static ScannerFragment newInstance(String customerId) {
        ScannerFragment fragment = new ScannerFragment();
        Bundle args = new Bundle();
        args.putString(CUSTOMER_ID, customerId);
        return fragment;
    }*/

    public interface OnScanCompleteListener {
        void onScanCompleted(Result scanResult);
       // void onScanProductDetail(JSONObject productDetails);
       // void onScanCurrentStageDetail(JSONObject currentDetails);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnScanCompleteListener) {
            mListener = (OnScanCompleteListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnScanCompleteListener");
        }
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        /*if (getArguments() != null) {
            customerId = getArguments().getString(CUSTOMER_ID);
        }*/

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state) {

        mScannerView = new ZXingScannerView(getActivity());
        mScannerView.setAutoFocus(true);

        daveAIPerspective = DaveAIPerspective.getInstance();

        return mScannerView;
    }



    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
            if (!hasCameraPermission()) {
                requestForCameraPermission();
            } else {
                startCameraForScan();
            }
        }else {
            startCameraForScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "camera permission granted", Toast.LENGTH_LONG).show();
                startCameraForScan();
            } else {
                Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void handleResult(Result scanResult) {
        Log.e("ScanResult","Contents = " + scanResult.getText() + ", Format = " + scanResult.getBarcodeFormat().toString());

        if (mListener != null)
            mListener.onScanCompleted(scanResult);

        /*if (scanResult.getText() != null) {
            getScannedProductDetails(getActivity(), scanResult.getText());
        }else {
            Toast.makeText(getActivity(),"Product Not Found",Toast.LENGTH_LONG).show();
        }*/


    }



    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M  ) {
            if(hasCameraPermission()){
                mScannerView.stopCamera();
            }
        }else {
            // Stop camera on pause
            mScannerView.stopCamera();
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void startCameraForScan(){
        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean hasCameraPermission() {
        return Objects.requireNonNull(getActivity()).checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestForCameraPermission() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
    }


   /* public void getScannedProductDetails(final Context context, final String scannedCode) {
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    try {
                        JSONObject getJsonResponse = new JSONObject(response);
                        Iterator<String> keys = getJsonResponse.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            if (getJsonResponse.get(key) instanceof JSONArray) {
                                JSONArray data_array = getJsonResponse.getJSONArray(key);
                                for (int i = 0; i < data_array.length(); i++) {
                                    JSONObject productDetails = data_array.getJSONObject(i);
                                    if(mListener != null)
                                        mListener.onScanProductDetail(productDetails);
                                   *//* if(productDetails.has(productId) && customerId!=null && !customerId.isEmpty()){
                                        getCurrentStageDetails(productId);
                                    }*//*
                                }
                            }
                        }
                    }
                    catch (Exception e) {
                        Log.e("Details", "==" + e.getMessage());
                    }
                } else {
                    Toast.makeText(getActivity(), "Product is not available in stock", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        Model model = new Model(context);
        if(productModelName!=null){
            model.getObjects(productModelName, queryParamMap(scannedCode), true, postTaskListener);
        }
    }

    private HashMap<String,String> queryParamMap(String scannedValue){
        HashMap<String,String> queryParam = new HashMap<>();
        queryParam.put(daveAIPerspective.getScan_code_attribute_name(), scannedValue);
        return queryParam;
    }*/

/*
    public void getCurrentStageDetails(final String productId) {
        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                if (response != null) {
                    try {
                       JSONObject currentStageDetail = new JSONObject(response);
                       if(mListener!=null)
                           mListener.onScanCurrentStageDetail(currentStageDetail);
                    } catch (Exception e) {
                        Log.e("ScanFragment", "Error" + e.getMessage());
                    }
                }
            }
        };
        new APICallAsyncTask(postTaskListener, getActivity(), "GET", false).
                execute(APIRoutes.nextInteractionAPI(customerId, productId));
    }
*/



}
