package com.example.admin.daveai.others;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.JsonAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.admin.daveai.database.DatabaseConstants.MODEL_NAME;
import static com.example.admin.daveai.database.DatabaseConstants.SINGLETON_TABLE_NAME;

/**
 * Created by soham on 12/9/18.
 */

public class DaveAI {
    /**
    * @param context Should be Application Context
    * @apiNote To be Called from apps Application Class with Application Context to initialise the sdk.
    * */
    public void initSDK(Context context){
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
        databaseManager.createTable("", DatabaseConstants.CacheTableType.META_DATA);

        //TODO CREATE A FUNC to check if meta defaults is there  for model name.
        databaseManager.insertMetaData(new CacheModelPOJO("models",168*60*60*1000,24*60*60*1000,0,"model_name",1));
        databaseManager.insertMetaData(new CacheModelPOJO("objects",168*60*60*1000,24*60*60*1000,0,"object_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("pivots",168*60*60*1000,24*60*60*1000,0,"object_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("preferences",168*60*60*1000,24*60*60*1000,0,"preference_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("singletons",168*60*60*1000,24*60*60*1000,0,"model_name",1));
        databaseManager.insertMetaData(new CacheModelPOJO("recommendations",168*60*60*1000,24*60*60*1000,0,"recommendation_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("interactions",168*60*60*1000,24*60*60*1000,0,"interaction_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("interaction",168*60*60*1000,24*60*60*1000,0,"interaction_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("post_queue_table",168*60*60*1000,24*60*60*1000,0,"interaction_id",1));
        databaseManager.insertMetaData(new CacheModelPOJO("productsss",10*60*1000,60*1000,0,"product_id",1));

        databaseManager.createTable("interaction", DatabaseConstants.CacheTableType.OBJECTS);
        databaseManager.createTable("post_queue_table", DatabaseConstants.CacheTableType.POST_QUEUE);
        databaseManager.createTable("interactions", DatabaseConstants.CacheTableType.OBJECTS);
        databaseManager.createTable("", DatabaseConstants.CacheTableType.MODEL);
//        testDB(context);


    }





    private void testDB(Context context){
        DatabaseManager databaseManager = DatabaseManager.getInstance(context);
/*
        databaseManager.createTable(SINGLETON_TABLE_NAME, DatabaseConstants.CacheTableType.SINGLETON);
        databaseManager.insertOrUpdateSingleton(SINGLETON_TABLE_NAME,new SingletonTableRowModel("perspective_settings",new JSONObject().toString(),System.currentTimeMillis(),"post"));
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(MODEL_NAME,"perspective_settings");
        Cursor cur = databaseManager.getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);
        Log.e("LOGGER","SINGLETON DATA COUNT = "+cur.getCount());*/

        databaseManager.createTable("productsss", DatabaseConstants.CacheTableType.OBJECTS);
        JSONObject jobj1 = new JSONObject();
        try{
            jobj1.put("string","asdf");
            jobj1.put("int",1);
            jobj1.put("bool",true);
            JSONArray jar = new JSONArray();
            jar.put("asdfg");
            jar.put("lkjhg");
            jar.put("12345");
            jobj1.put("hhhh",jar);
        }catch (Exception e){
            e.printStackTrace();
        }
        JSONObject jobj2 = new JSONObject();
        try{
            jobj2.put("string","qwer");
            jobj2.put("int",2);
            jobj2.put("bool",false);
        }catch (Exception e){
            e.printStackTrace();
        }
        databaseManager.insertObjectsData("productsss",new ObjectsTableRowModel("123",jobj1.toString(),System.currentTimeMillis()));
        databaseManager.insertObjectsData("productsss",new ObjectsTableRowModel("321",jobj2.toString(),System.currentTimeMillis()));
        HashMap<String,Object> cacheQueryData = new HashMap<>();
        List<String> asdf = new ArrayList<>();
        asdf.add("12345");
        cacheQueryData.put("hhhh",asdf);
        Cursor cursor = databaseManager.getDataFromTable("productsss",new HashMap<>(),cacheQueryData,"=",0,"");
//        Cursor cursor = databaseManager.getDataFromTableWhereCacheContains("product",cacheQueryData);
        Log.e("LOGGER","query count1 = "+cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
                Log.e("LOGGER",cursor.getString(cursor.getColumnIndex(DatabaseConstants.DATA_CACHED)));
            } while (cursor.moveToNext());
        }
        cursor.close();

        /*Map<String,Object> cacheQueryData2 = new HashMap<>();
        cacheQueryData2.put("bool",false);
        cursor.close();
        cursor = databaseManager.getDataFromTableWhereCacheContains("product",cacheQueryData2);
        Log.e("LOGGER","query count2 = "+cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                Log.e("LOGGER",cursor.getString(cursor.getColumnIndex(DatabaseConstants.DATA_CACHED)));
            } while (cursor.moveToNext());
        }
*/
        //databaseManager.updateObjectsData()

       // databaseManager.insertObjectsData("product",new ObjectsTableRowModel("321",jobj1.toString(),System.currentTimeMillis()));
       /* databaseManager.insertOrUpdateObjectData("product",new ObjectsTableRowModel("321",jobj1.toString(),System.currentTimeMillis()));

        cursor.close();

        cursor = databaseManager.getDataFromTableWhereCacheContains("product",cacheQueryData);
        Log.e("LOGGER","query count3 = "+cursor.getCount());

        if (cursor.moveToFirst()) {
            do {
            } while (cursor.moveToNext());
        }

        JSONObject jarr = databaseManager.getAllObjectsData("product");
        Log.e("TestDB","getAllObjects:- "+jarr);

        cursor.close();*/



        /*

        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO("perspective_settings", 86400 * 1000, 18000 * 1000,
                        0, MODEL_NAME)
        );
        Log.e("STATUS",""+databaseManager.updateStatus("perspective_settings", DatabaseConstants.CacheTableType.SINGLETON));
*/

    }
}
