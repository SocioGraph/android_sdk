package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.admin.daveai.R;

import com.example.admin.daveai.dialogbox.SMSTemplate;
import com.example.admin.daveai.expandViewRecycler.ViewItem.CallItem;

import me.drakeet.multitype.ItemViewBinder;


public class CallItemViewBinder extends ItemViewBinder<CallItem, CallItemViewBinder.TextHolder> {


    private Context context;

    public CallItemViewBinder(Context context) {
        this.context = context;

    }

    @Override
    protected @NonNull
    TextHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.call_item_text, parent, false);
        return new TextHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull TextHolder holder, @NonNull final CallItem callItem) {


        holder.key_title.setText(callItem.getText_key());
        holder.key_value.setText(callItem.getText_value());
        holder.key_value.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ClikingText", "onClick: ");
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + callItem.getText_value()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.CALL_PHONE},
                            1);
                    return;
                }
                context.startActivity(intent);
            }
        });

        holder.btnSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("btnSendSMS", "onClick: ");
                //TODO implement call back func to open SMS Template
                SMSTemplate cdd = new SMSTemplate(context,callItem.getText_value(),SMSTemplate.SEND_SMS_TEMPLATE);
                cdd.show();


            }
        });

    }


    static class TextHolder extends RecyclerView.ViewHolder {
        private TextView key_title, key_value;
        private ImageView btnSendSMS;

        TextHolder(@NonNull View itemView) {
            super(itemView);
            this.key_title = itemView.findViewById(R.id.key_title);
            this.key_value = itemView.findViewById(R.id.key_value);
            this.btnSendSMS = itemView.findViewById(R.id.btnSendSMS);


        }
    }
}
