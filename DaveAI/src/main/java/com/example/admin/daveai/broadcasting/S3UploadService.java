package com.example.admin.daveai.broadcasting;

import android.Manifest;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.daveUtil.AmazonS3UploadUtil;
import com.example.admin.daveai.network.CheckNetworkConnection;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class S3UploadService extends IntentService implements DatabaseConstants {

    private  final static  String TAG = "S3UploadService";
    //context and database helper object
    private Context context;
    private DatabaseManager databaseManager;
    private int cacheTableType = 0 ;


    public static boolean isIntentServiceRunning = false;

    public S3UploadService() {
        super(S3UploadService.class.getName());
        Log.d(TAG, "****************Initialize  Service!");

    }


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param context
     */
    public static void verifyStoragePermissions(Context context) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"NO PERMISSION");
            // We don't have permission so prompt the user
            /*ActivityCompat.requestPermissions(
                    context,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );*/
        }else{
            Log.e(TAG," WRITE PERMISSION GRANTED");

        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "*********S3UploadService Started!");
        this.context = S3UploadService.this;
        isIntentServiceRunning = true;
        if(CheckNetworkConnection.networkHasConnection(context)) {
            databaseManager = DatabaseManager.getInstance(context);
            databaseManager.createTable(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME, CacheTableType.S3_UPLOAD_QUEUE);

            Cursor cursor = databaseManager.getDataFromTableWhere(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME, new HashMap<>(), 1, UPLOAD_ID);
            Log.e(TAG,"s3 upload count = "+databaseManager.checkAPIQueueCount(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME));

                syncDataWithServer();
            cursor.close();
        } /*else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);

        }*/
        //this.stopSelf();
        // Log.e(TAG, "****************Service Stopping!");
    }

    //TODO:call post queue api from databaseManager
    private void syncDataWithServer(){
        try {

            Cursor cursor = databaseManager.getDataFromTableWhere(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME, new HashMap<>(), 1, DatabaseConstants.UPLOAD_ID);
//            Log.e(TAG, "****************syncDataWithServer cursor count*************" + cursor.getCount());
            if (cursor.moveToFirst()) {
                int postId = cursor.getInt(cursor.getColumnIndex(UPLOAD_ID));
                String local_path = cursor.getString(cursor.getColumnIndex(LOCAL_PATH));
                String upload_file_name = cursor.getString(cursor.getColumnIndex(UPLOAD_FILE_NAME));
                String upload_folder_path = cursor.getString(cursor.getColumnIndex(UPLOAD_FOLDER_PATH));
                String upload_bucket_name = cursor.getString(cursor.getColumnIndex(UPLOAD_BUCKET));

                Log.e(TAG,"local_path = "+local_path );
                Log.e(TAG,"upload_file_name = "+upload_file_name );
                Log.e(TAG,"upload_folder_path = "+upload_folder_path );
                Log.e(TAG,"upload_bucket_name = "+upload_bucket_name );
                if(new File(local_path).exists()) {

                    new AmazonS3UploadUtil().upload(S3UploadService.this, new File(local_path), upload_file_name, upload_folder_path, upload_bucket_name,
                            new AmazonS3UploadUtil.AmazonS3UploadListener() {
                                @Override
                                public void uploadSuccess() {
                                    Log.i(TAG," BEFORE media queue size = "+databaseManager.getDataFromTable(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME).getCount());
                                    ArrayList<String> whereClauseArray = new ArrayList<>();
                                    whereClauseArray.add(UPLOAD_ID);
                                    databaseManager.deleteRow(
                                            DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME,
                                            databaseManager.createWhereClause(whereClauseArray),
                                            new String[]{Integer.toString(postId)}
                                    );
                                    Log.i(TAG," BEFORE media queue size after = "+databaseManager.getDataFromTable(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME).getCount());

                                    syncDataWithServer();
                                }

                                @Override
                                public void uploadFailed(int id, Exception ex) {
                                    Log.i(TAG," BEFORE media queue size = "+databaseManager.getDataFromTable(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME).getCount());
                                    ArrayList<String> whereClauseArray = new ArrayList<>();
                                    whereClauseArray.add(UPLOAD_ID);
                                    databaseManager.deleteRow(
                                            DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME,
                                            databaseManager.createWhereClause(whereClauseArray),
                                            new String[]{Integer.toString(postId)}
                                    );
                                    Log.i(TAG," BEFORE media queue size after = "+databaseManager.getDataFromTable(DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME).getCount());

                                    syncDataWithServer();
                                }
                            });

                }else{
                    ArrayList<String> whereClauseArray = new ArrayList<>();
                    whereClauseArray.add(UPLOAD_ID);
                    databaseManager.deleteRow(
                            DatabaseConstants.S3_UPLOAD_QUEUE_TABLE_NAME,
                            databaseManager.createWhereClause(whereClauseArray),
                            new String[]{Integer.toString(postId)}
                    );

                    syncDataWithServer();
                }



                cursor.close();

            }
        }catch (Exception e) {
            Log.e(TAG,"Error syncDataWithServer:-------------  "+e.getMessage());
            e.printStackTrace();

        }



    }




    public void startBackgroundService(Context mContext){
        if(CheckNetworkConnection.networkHasConnection(mContext)) {
            Log.i(TAG," check Is Media service running >>>>>>>>>>>>>>>>>"+isIntentServiceRunning );
            if(!isIntentServiceRunning){
                Intent msgIntent = new Intent(mContext,S3UploadService.class);
                mContext.startService(msgIntent);
            }else {
                Log.i(TAG, "<<<<<<<<<<<<<<< Service already Running>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }

    }

    @Override
    public void onDestroy() {
        isIntentServiceRunning = false;
        super.onDestroy();
    }
}