package com.example.admin.daveai.model;

import android.content.Context;
import android.util.Log;
import com.example.admin.daveai.database.DatabaseConstants;
import com.example.admin.daveai.database.DatabaseManager;
import com.example.admin.daveai.database.InteractionsHelper;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.others.CacheModelHelper;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.example.admin.daveai.others.DaveAIListener;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.CheckNetworkConnection;
import com.example.admin.daveai.others.DaveHelper;
import com.example.admin.daveai.others.DaveModels;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.RequestBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import static com.example.admin.daveai.model.ObjectData.getAllKeysNameOfObject;
import static com.example.admin.daveai.network.APIRoutes.MEDIA_TYPE_JSON;
import static com.example.admin.daveai.network.APIRoutes.getMetadata;
import static com.example.admin.daveai.others.DaveAIStatic.base_url;



public class Model implements DatabaseConstants, DaveConstants {


    private final static String TAG = "Model";
    // public static List<String> FILTER_TYPE_LIST = Arrays.asList("category", "tags", "price", "discount", "text");


    private boolean allow_anonymous_view;
    private String data_type;
    private boolean expand_attributes;
    private boolean has_login;
    private String model_type;
    private String name;
    private String plural;
    private QuickViews quick_views;
    private String timezone;
    private String title;
    private List<String> attached_matchers;
    private List<Attribute> attributes;
    private List<?> children;
    private List<?> emails;
    private List<String> ids;
    private List<?> images;
    private List<String> mobile_numbers;
    private List<String> names;
    private List<String> parents;
    private List<?> permissions;
    private List<?> phone_numbers;
    private List<?> push_tokens;
    private List<?> times;
    //private ActionsBean actions;
    //private Preferences preferences;





    //hidden,readonly- String List


    public boolean isAllow_anonymous_view() {
        return allow_anonymous_view;
    }

    public void setAllow_anonymous_view(boolean allow_anonymous_view) {
        this.allow_anonymous_view = allow_anonymous_view;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public boolean isExpand_attributes() {
        return expand_attributes;
    }

    public void setExpand_attributes(boolean expand_attributes) {
        this.expand_attributes = expand_attributes;
    }

    public boolean isHas_login() {
        return has_login;
    }

    public void setHas_login(boolean has_login) {
        this.has_login = has_login;
    }

    public String getModel_type() {
        return model_type;
    }

    public void setModel_type(String model_type) {
        this.model_type = model_type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlural() {
        return plural;
    }

    public void setPlural(String plural) {
        this.plural = plural;
    }

    public QuickViews getQuick_views() {
        return quick_views;
    }

    public void setQuick_views(QuickViews quick_views) {
        this.quick_views = quick_views;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAttached_matchers() {
        return attached_matchers;
    }

    public void setAttached_matchers(List<String> attached_matchers) {
        this.attached_matchers = attached_matchers;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<?> getChildren() {
        return children;
    }

    public void setChildren(List<?> children) {
        this.children = children;
    }

    public List<?> getEmails() {
        return emails;
    }

    public void setEmails(List<?> emails) {
        this.emails = emails;
    }

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public List<?> getImages() {
        return images;
    }

    public void setImages(List<?> images) {
        this.images = images;
    }

    public List<String> getMobile_numbers() {
        return mobile_numbers;
    }

    public void setMobile_numbers(List<String> mobile_numbers) {
        this.mobile_numbers = mobile_numbers;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<String> getParents() {
        return parents;
    }

    public void setParents(List<String> parents) {
        this.parents = parents;
    }

    public List<?> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<?> permissions) {
        this.permissions = permissions;
    }

    public List<?> getPhone_numbers() {
        return phone_numbers;
    }

    public void setPhone_numbers(List<?> phone_numbers) {
        this.phone_numbers = phone_numbers;
    }

    public List<?> getPush_tokens() {
        return push_tokens;
    }

    public void setPush_tokens(List<?> push_tokens) {
        this.push_tokens = push_tokens;
    }

    public List<?> getTimes() {
        return times;
    }

    public void setTimes(List<?> times) {
        this.times = times;
    }


    public static class QuickViews {
        @SerializedName("default")
        private List<String> defaultX;
        private List<String> mobile_card_view;
        private List<String> mobile_expand_view;
        private List<String> summation_attributes;
        private List<String> mobile_card_view_category;
        private List<String> cached;

        public List<String> getCached() {
            return cached;
        }

        public void setCached(List<String> cached) {
            this.cached = cached;
        }

        public List<String> getDefaultX() {
            return defaultX;
        }

        public void setDefaultX(List<String> defaultX) {
            this.defaultX = defaultX;
        }

        public List<String> getMobile_card_view() {
            return mobile_card_view;
        }

        public void setMobile_card_view(List<String> mobile_card_view) {
            this.mobile_card_view = mobile_card_view;
        }

        public List<String> getMobile_expand_view() {
            return mobile_expand_view;
        }

        public void setMobile_expand_view(List<String> mobile_expand_view) {
            this.mobile_expand_view = mobile_expand_view;
        }

        public List<String> getSummation_attributes() {
            return summation_attributes;
        }

        public void setSummation_attributes(List<String> summation_attributes) {
            this.summation_attributes = summation_attributes;
        }

        public List<String> getMobile_card_view_category() {
            return mobile_card_view_category;
        }

        public void setMobile_card_view_category(List<String> mobile_card_view_category) {
            this.mobile_card_view_category = mobile_card_view_category;
        }

    }



//************Custom  method start*********************



    public ArrayList<String> getModelAttributes(){
        ArrayList<String> key_list = new ArrayList<>();
        for(Attribute s :this.attributes){
            key_list.add(s.getName());
        }
        return  key_list;
    }

    public List<String> getModelAttributes(String filterKey,String filterValue){

        List<String> keyList = new ArrayList<>();
        if(filterKey==null || filterKey.isEmpty())
            Log.e(TAG,"Please Enter filter key");
        else if(filterValue==null || filterValue.isEmpty())
            Log.e(TAG,"Please Enter filter Value");
        else {
            for (Attribute s : this.attributes) {
                if(filterKey.equalsIgnoreCase(ATTR_NAME_UI_ELEMENT)) {
                    if (s.getUi_element().equalsIgnoreCase(filterValue))
                        keyList.add(s.getName());
                }
            }
        }
        return  keyList;
    }

    public  ArrayList<String> getAttributeList(Model model_name){
        ArrayList<String> key_list= new ArrayList<>();
        for(Attribute s :model_name.getAttributes()){
            key_list.add(s.getName());
        }
        return key_list;
    }

    public ArrayList<Attribute> getAttributeByType(String type){
        ArrayList<Attribute> attrs = new ArrayList<>();
        for(Attribute s : this.attributes){
            if(s.getType().equals(type))
                attrs.add(s);
        }
        return attrs;
    }

    public ArrayList<String> getListOfUIElementOfAttribute(){
        ArrayList<String> ui_list = new ArrayList<>();
        for(Attribute s :this.attributes){
            ui_list.add(s.getUi_element());
        }
        return  ui_list;
    }


    public Attribute getAttributeInstance(String name){
        //Log.e(TAG,"Print Model Attributes list"+this.attributes +""+getAttributes());
        if(this.attributes!=null) {
            for (Attribute s : this.attributes) {
                if (s.getName().equals(name))
                    return s;
            }
        }
        return null;
    }

    public String getAttributeUIElement(String att_name){
        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getUi_element();
        }
        return null;
    }

    public String getAttributePlaceHolder(String att_name){
        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getPlaceholder();
        }
        return null;
    }

    public Object getAttributeOptions(String att_name){
        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getOptions();
        }
        return null;
    }

    public boolean getAttributeOtherOptions(String att_name){
        Attribute a = getAttributeInstance(att_name);
        return a != null && a.isOption_others();
    }

    public String getAttributeType(String att_name){
        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getType();
        }
        return null;
    }


    public boolean getAutoUpdate(String att_name){

        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            if(a.isAuto_updater())
                return a.isAuto_updater();
        }
        return false;
    }

    public String getAttributeTitle(String att_name){

        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getTitle();
        }
        return null;
    }
    public String getAttributeAType(String att_name){

        Attribute a = getAttributeInstance(att_name);
        if(a != null){
            return a.getAtype();
        }
        return null;
    }
    public boolean getAttributeId(String att_name) {
        Attribute a = getAttributeInstance(att_name);
        return a != null && a.isId();
    }

    public boolean getAttributeRequired(String att_name) {

        Attribute a = getAttributeInstance(att_name);
        return a != null && a.isRequired();
    }
    public boolean checkAttrExistInModel(String attr_name){
        for(Attribute s : this.attributes){
            return s.getName().equals(attr_name);
        }
        return false;
    }

    public Object getAttrDefaultValues(String modelName, String attr_name){
        try {
            Model modelInstance = getModelInstance(context, modelName);
            if(modelInstance!=null) {
                for (Attribute s : modelInstance.getAttributes()) {
                    if(s.getName().equalsIgnoreCase(attr_name)){
                       // Log.e(TAG, "Get Model Attr name :---- " + s.getName() +" get DefaultValue:-  "+ s.getDefaultX());
                        return s.getDefaultX();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Attribute getModelIdAttributeInstance(String modelName){
        try {
            Model modelInstance = getModelInstance(context, modelName);
            if(modelInstance!=null && modelInstance.getAttributes()!=null) {
                for (Attribute s : modelInstance.getAttributes()) {
                    if(s.isId()){
                        //Log.e(TAG, "getModelIdAttributeInstance3 :---- " + s.getName() +" get Default:-  "+ s.getDefaultX());
                        return s;
                    }
                }
            }else{
                Log.e(TAG,"Error modelInstance or get Attribute is null :---"+modelInstance +""+ modelInstance.getAttributes());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getModelIDAttrName(String modelName){
        String modelIdAttrNAme = "";
        try {
            Attribute attribute = getModelIdAttributeInstance(modelName);
            if(attribute!=null) {
                modelIdAttrNAme = attribute.getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return modelIdAttrNAme;
    }

    /************************************************************Start Model Function ****************************************************************/


    private Context context;
    private DaveSharedPreference sharedPreference;
    private DaveAIPerspective daveAIPerspective;


    public Model(Context mContext){
        this.context = mContext;
        sharedPreference= new DaveSharedPreference(context);
        daveAIPerspective = DaveAIPerspective.getInstance();

    }

    //get modelInstance based on modelResponse
    public static Model getModelInstance(String modelResponse) {
        Model modelInstance = null;
        try {
           // Log.e("ModelInstance","before GSon:--------");
            if(modelResponse!= null && !modelResponse.isEmpty()){
                Gson gson = new Gson();
                modelInstance = gson.fromJson(modelResponse, Model.class);
               // Log.e("ModelInstance","After GSon:--------"+modelInstance);
                return gson.fromJson(modelResponse, Model.class);
            }
        }
        catch (StackOverflowError exception){
            exception.printStackTrace();
        }
        catch (Exception exception){
            exception.printStackTrace();
            Log.e("ModelInstance","Error during getting model:--------"+exception.getMessage());
        }
        return modelInstance;
    }


    //get model Instance based on model name
    public static Model getModelInstance(Context context, String modelName) {
        Model modelInstance;
        try {
           // Log.e("Model","Get Model Response from db of model :--------"+modelName);
            DatabaseManager databaseManager = DatabaseManager.getInstance(context);
            String modelJson = databaseManager.getModelData(modelName).toString();
           // Log.i("Model","Get Model Response from db of model :--------"+modelName+" modelResonse:-- "+modelJson);
            if(modelJson!= null){
                Gson gson = new Gson();
                modelInstance = gson.fromJson(modelJson, Model.class);
               // Log.i(TAG,"getModelInstance:--------"+modelInstance);
                return modelInstance;
            }
        }
        catch (StackOverflowError exception){
            exception.printStackTrace();
        }
        catch (Exception exception){
            exception.printStackTrace();
            Log.e("ModelInstance", "Gson Error setting Model respone in Model class*****  " + exception.getMessage());
        }
        return null;
    }

    public void setupModelCacheMetaData(String modelName,String modelIdAttrName,Context context){
        //Log.e(TAG,"SEt Cache meta data  hot and cold update for model:---"+modelName+"=="+daveAIPerspective.getHot_update_time_limit()+" "+ daveAIPerspective.getCold_update_time_limit());
        DatabaseManager.getInstance(context).insertMetaData(
                new CacheModelPOJO(modelName,
                        daveAIPerspective.getHot_update_time_limit(),
                        daveAIPerspective.getCold_update_time_limit(),
                        0, modelIdAttrName,System.currentTimeMillis())
        );


    }


    private String returnResponse =null;
    public String postObject(final String model_name,JSONObject postBody,boolean loader,APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader).execute(APIRoutes.postObjectAPI(model_name));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }

        return returnResponse;
    }


    public void getObjects(final String model_name, HashMap<String,Object> queryParams, boolean loader, APIResponse postTaskListener){
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"GET",loader).execute(APIRoutes.getObjectsAPI(model_name,queryParams));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public void updateObject(final String model_name, String id, JSONObject postBody, boolean loader, APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"UPDATE",body_part,loader).execute(APIRoutes.updateOrGetObjectAPI(model_name,id));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public void getPivot(final String model_name, HashMap<String,String> queryParams, boolean loader, APIResponse postTaskListener){
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"GET",loader).execute(APIRoutes.getPivotAPI(model_name,queryParams));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }

    public String shareCatalogue(final String customerId,JSONObject postBody,boolean loader,APIResponse postTaskListener){
        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader,60L).execute(APIRoutes.catalogueAPI(customerId));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }

        return returnResponse;
    }



    public String getAppConfigurationDetails(String userId, String apiKey ,APIResponse postTaskListener){
        if(base_url==null)
            getMetadata(context);

        sharedPreference.writeString(DaveSharedPreference.USER_ID,userId);
        sharedPreference.writeString(DaveSharedPreference.API_KEY, apiKey);
        if (CheckNetworkConnection.networkHasConnection(context)) {
            System.out.println("*****************Get APp Config Details:-------------"+APIRoutes.appConfigureAPI());
            new APICallAsyncTask(postTaskListener, context,"GET",true,"Checking for App Update...",10L).execute(APIRoutes.appConfigureAPI());
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }

        return returnResponse;
    }


    public void postConversation(String customerId, String conversation_id, JSONObject postBody,boolean loader,APIResponse postTaskListener){

        RequestBody body_part = RequestBody.create(MEDIA_TYPE_JSON, postBody.toString());
        if (CheckNetworkConnection.networkHasConnection(context)) {
            new APICallAsyncTask(postTaskListener, context,"POST",body_part,loader)
                    .execute(APIRoutes.conversationAPI(customerId,conversation_id));
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(context);
        }
    }
    //****************Custom  method End*************************
}






