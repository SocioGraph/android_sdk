package com.example.admin.daveai.expandViewRecycler.ViewItem;

public class ImageItem {

    public String imgUrl;
    public String shareText;

    public ImageItem(String imgUrl,String shareText) {
        this.imgUrl = imgUrl;
        this.shareText = shareText;
    }
}
