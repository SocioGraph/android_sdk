package com.example.admin.daveai.database.models;

/**
 * Created by soham on 14/9/18.
 */

public class SingletonTableRowModel {
    public String model_name = "";
    public String data_cached = "";
    public long last_synced;
    public String request_type = "";
    public String error_message = "";

    //insert data
    public SingletonTableRowModel(String model_name, String data_cached, long last_synced, String request_type) {
        this.model_name = model_name;
        this.data_cached = data_cached;
        this.last_synced = last_synced;
        this.request_type = request_type;
    }

    //update data
    public SingletonTableRowModel(String model_name, String data_cached, long last_synced, String request_type, String error_message) {
        this.model_name = model_name;
        this.data_cached = data_cached;
        this.last_synced = last_synced;
        this.request_type = request_type;
        this.error_message = error_message;
    }



}
