package com.example.admin.daveai.database.models;

public class MediaQueueTableRowModel {

    public int postId = 0;
    public String url = "";
    public String type = "";
    public String updateTableName = "";
    public String updateTableRowId = "";
    public String updateAttributeName = "";
    public String status = "";

    public MediaQueueTableRowModel(int postId, String url, String type, String updateTableName, String updateTableRowId, String updateAttributeName, String status) {
        this.postId = postId;
        this.url = url;
        this.type = type;
        this.updateTableName = updateTableName;
        this.updateTableRowId = updateTableRowId;
        this.updateAttributeName = updateAttributeName;
        this.status = status;
    }
}
