package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.GridviewImageItem;

import me.drakeet.multitype.ItemViewBinder;


public class GridImageItemViewBinder extends ItemViewBinder<GridviewImageItem, GridImageItemViewBinder.GridHolder> {

    private Context context;

    public GridImageItemViewBinder(Context context) {
        this.context = context;
    }

    @Override
    protected @NonNull
    GridImageItemViewBinder.GridHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.image_grid_layout, parent, false);
        return new GridImageItemViewBinder.GridHolder(root, context);
    }

    @Override
    protected void onBindViewHolder(@NonNull GridImageItemViewBinder.GridHolder holder, @NonNull GridviewImageItem gridviewImageItem) {

       // GridAdapter alertsAdapter = new GridAdapter(context, gridviewImageItem.getImageList(), gridviewImageItem.getProductDetails());
        GridAdapter alertsAdapter = new GridAdapter(context, gridviewImageItem.getGridImageDetails());
        holder.gridRecycleview.setAdapter(alertsAdapter);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull GridImageItemViewBinder.GridHolder holder) {
        holder.itemView.clearAnimation();
    }


    static class GridHolder extends RecyclerView.ViewHolder {

        RecyclerView gridRecycleview;
        LinearLayoutManager mLayoutManager;

        GridHolder(@NonNull View itemView, Context context) {
            super(itemView);
            gridRecycleview = itemView.findViewById(R.id.gridRecycleview);
            mLayoutManager = new GridLayoutManager(context, 3);
            gridRecycleview.setLayoutManager(mLayoutManager);

        }
    }
}

