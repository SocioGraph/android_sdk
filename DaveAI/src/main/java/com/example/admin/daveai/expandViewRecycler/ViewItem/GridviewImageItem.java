package com.example.admin.daveai.expandViewRecycler.ViewItem;

import org.json.JSONArray;


public class GridviewImageItem {

    private JSONArray jsonGridList;

    public GridviewImageItem(JSONArray jsonGridList) {
        this.jsonGridList = jsonGridList;
    }

    public JSONArray getGridImageDetails() {
        return jsonGridList;
    }

}
