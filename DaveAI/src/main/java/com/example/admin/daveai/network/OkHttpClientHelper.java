package com.example.admin.daveai.network;


import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


/**
 * Created by soham on 8/8/18.
 */

public class OkHttpClientHelper extends OkHttpClient{
    private static String TAG = "OkHttpClientHelper";
    private static OkHttpClientHelper okHttpClientHelper;


    public static OkHttpClientHelper getInstance(Long timeOut) {
            okHttpClientHelper = new OkHttpClientHelper(timeOut);
            Log.i(TAG,"Getting OkHttpClientHelper instance");
        return okHttpClientHelper;
    }

    public OkHttpClientHelper(Long timeOut) {
        this.setConnectionPool(new ConnectionPool(20,5L));
        this.setWriteTimeout(40, TimeUnit.SECONDS); // write timeout
        this.setConnectTimeout(timeOut, TimeUnit.SECONDS); // connect timeout
        this.setReadTimeout(30, TimeUnit.SECONDS);  // socket timeout
        //this.setRetryOnConnectionFailure(false);
        //this.setCache(null) ;  //new Cache(sContext.getCacheDir(),10*1024*1024)


    }

    public OkHttpClient getNewHttpClient(Long timeOut) {
        OkHttpClient client = new OkHttpClient();
                client.setFollowRedirects(true);
                client .setFollowSslRedirects(true);
                client.setRetryOnConnectionFailure(true);
                client.setCache(null);
                client.setConnectTimeout(timeOut, TimeUnit.SECONDS);
                client.setWriteTimeout(40, TimeUnit.SECONDS);
                client.setReadTimeout(30, TimeUnit.SECONDS);

        return client;
    }
}


 /*   private void makeServerCall(Context context, Request request, Long timeout, final APIResponse apiResponse) throws IOException {


       *//* Request request = new Request.Builder()
                .url(url)
                .build();*//*

        OkHttpClientHelper.getInstance(timeout).newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                new Handler().post(new Runnable() {
                    public void run() {
                        apiResponse.onTaskFailure(1,e.toString());

                    }
                });

            }

            @Override
            public void onResponse(Response response) throws IOException {
                new android.os.Handler().post(new Runnable() {
                    public void run() {
                        //do Stuff here
                        try {
                            int responseCode = response.code();
                            String responseString = response.body().string();
                            if(response.isSuccessful()) {
                                apiResponse.onTaskCompleted(responseString);
                            }
                            else {
                                if(responseCode == 400){
                                    //Error Syncing with Server: 400: <error message>
                                    String printError = new JSONObject(responseString).getString("error");
                                    apiResponse.onTaskFailure(responseCode,"Error Syncing with Server: "+responseCode+": "+ printError);

                                }else if( responseCode == 401 || responseCode == 403){
                                    //401, 403 It should be "Error Authenticating : 401: <Error message>"
                                    String printError = new JSONObject(responseString).getString("error");
                                    apiResponse.onTaskFailure(responseCode,"Error Authenticating : "+responseCode+": "+ printError);


                                }else if(responseCode == 404){
                                    apiResponse.onTaskFailure(responseCode, "404: Requested resource not found");

                                }
                                else if(responseCode ==500){
                                    apiResponse.onTaskFailure(responseCode, "500: Problem connecting with server. Some features may not work as expected!");
                                }
                                else if(responseCode ==502){
                                    apiResponse.onTaskFailure(responseCode, "502: Problem connecting with server. Some features may not work as expected!");

                                }
                                else if(responseCode ==504){
                                    apiResponse.onTaskFailure(responseCode, "504: Problem connecting with server. Some features may not work as expected!");

                                }else{
                                    apiResponse.onTaskFailure(responseCode, responseCode+": Problem connecting with server. Some features may not work as expected!");

                                }

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                    }
                });


            }
        });


    }*/
