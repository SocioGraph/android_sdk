package com.example.admin.daveai.daveUtil;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.admin.daveai.BuildConfig;
import com.example.admin.daveai.R;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static com.example.admin.daveai.others.DaveAIStatic.base_url;
import static com.example.admin.daveai.others.DaveAIStatic.enterprise_id;


public class MultiUtil {

    private static final String TAG = "MultiUtil";
    private Context context;
  

    public MultiUtil(Context context){
        this.context=context;
    }


    public static String getApplicationFilePath(Context context) {
        final File filesDir = context.getFilesDir();
        if (filesDir != null) {
            return filesDir.getAbsolutePath();
        }
        Log.d(TAG, "Couldn't retrieve ApplicationFilePath for : " + context.getPackageName());
        return "Couldn't retrieve ApplicationFilePath";
    }

    public static String methodToday(){
        Calendar cal = Calendar.getInstance();
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return df.format(cal.getTime());
    }

    public static String methodFirstDayOfMonth(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return df.format(cal.getTime());
    }



    public String getProviderAuthority() {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            String appId = bundle.getString("PROVIDER_AUTHORITY");

            Log.e(TAG,"getMetadata appId>>>>>>>>>>>>>>> "+appId);

            return appId;

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to Get URL OR ENTERPRISE From SP, NullPointer: " + e.getMessage());
        }

        return null ;
    }

    public static String getManifestMetadata(Context context, String keyName) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            return bundle.getString(keyName);

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());

        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());

        }
        catch (Exception e) {
            Log.e(TAG, "Failed to Get URL OR ENTERPRISE From SP, NullPointer: " + e.getMessage());
        }
        return null;
    }


    // Returns the URI path to the Bitmap displayed in specified ImageView
    public Uri getLocalBitmapUri(Context context,ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        Log.e(TAG,"getLocalBitmapUri>>>>>>>>>>>>>>>>> "+ drawable+"  "+bmp );
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            //Log.e(TAG,"drawable instanceof BitmapDrawable>>>>>>>>>>>>>>>>> "+ drawable+"  "+bmp );
        }
        else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Log.e(TAG,"Print Build version code version is >=24:-- "+Build.VERSION.SDK_INT +" Application Id:- "+BuildConfig.APPLICATION_ID);
                // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
                //bmpUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
                bmpUri = FileProvider.getUriForFile(context, getProviderAuthority(), file);

            } else {
                Log.e(TAG,"Print Build version code version is <=23 :-- "+Build.VERSION.SDK_INT);
                bmpUri = Uri.fromFile(file);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    //method to share image
    public void shareImage(Context context, Uri localBitmapUri){
        if (localBitmapUri != null) {

            Intent shareIntent = new Intent();
            shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_STREAM, localBitmapUri);
            shareIntent.setType("image/*");
            // Launch sharing dialog for image
            context.startActivity(Intent.createChooser(shareIntent, "Share Image"));

        } else {
            // ...sharing failed, handle error
            Log.e(TAG," Share Image is null :---------- "+localBitmapUri);
        }
    }
    //method to share image
    public void shareImage(Context context, Uri localBitmapUri,String shareText){
        if (localBitmapUri != null) {

            Intent shareIntent = new Intent();
            shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);

            shareIntent.putExtra(Intent.EXTRA_STREAM, localBitmapUri);

            // shareIntent.putExtra(Intent.EXTRA_SUBJECT, shareMsg);
            // shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMsg);

            shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareText);
            shareIntent.setType("image/*");
            //shareIntent.setType("text/html");

            // Launch sharing dialog for image
            context.startActivity(Intent.createChooser(shareIntent, "Share Image"));

        } else {
            // ...sharing failed, handle error
            Log.e(TAG," Share Image is null :---------- "+localBitmapUri);
        }
    }

    // Method to share either text or URL.
    private void shareImageUrl(Context context, String imageUrl) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Title Of The Post");
        // share.putExtra(Intent.EXTRA_TEXT, "http://www.codeofaninja.com");
        share.putExtra(Intent.EXTRA_TEXT, imageUrl);
        context.startActivity(Intent.createChooser(share, "Share Image!"));
    }

    /**
     * method to create temp file
     * @return create temp File
     * @throws IOException  to read/write a file but don't have permission.
            to write to a file but disk space was no longer available.
     */
    public File createImageFile() throws IOException {
        // File imageFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/picture.jpg");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File imageFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return imageFile;
    }

    public Uri getUriFromFile(File imageFile)  {
        Uri fileUri = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fileUri = FileProvider.getUriForFile(context, getProviderAuthority(), imageFile);
            //TODO:  Permission..
            context.getApplicationContext().grantUriPermission("com.android.camera", fileUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            fileUri = Uri.fromFile(imageFile); // convert path to Uri
        }
        return fileUri;
    }

    public Uri getImageUriFromBitmap(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Context inContext,Uri uri) {
        Cursor cursor = inContext.getContentResolver().query(uri, null, null, null, null);
        assert cursor != null;
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public static void setImageUsingGlide(Context context, String imageUrl, ImageView imageViewName){
        Glide.with(context)
                .load(imageUrl)
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.no_image)
                //.override(250, 250)
                .fitCenter()
                //.centerCrop()
                .into(imageViewName);

    }

    public static void setImageUsingPicasso(Context context, String imageUrl, ImageView imageViewName){
        Picasso.with(context)
                .load(imageUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.no_image)
                .resize(0, 200)
                .into(imageViewName);

    }

   /* public static JSONObject copySourceToTargetJSONObject(JSONObject source,JSONObject target) {


        Iterator<?> keys = source.keys();
        while(keys.hasNext() ) {
            String key = (String)keys.next();
            try {
                if( source.get(key) instanceof JSONObject) {
                    JSONObject xx = (JSONObject) source.get(key);
                    copySourceToTargetJSONObject(xx,target);

                }else if(source.get(key) instanceof JSONArray){
                    JSONArray jsonArray = (JSONArray) source.get(key);
                    for(int i = 0;i<jsonArray.length();i++){
                        String arrKey = jsonArray.get(i).toString();
                        if(!target.has(arrKey))
                            target.put(arrKey,source.getString(arrKey));
                    }
                }
                else {
                    if(!target.has(key))
                        target.put(key, source.get(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG,"copyAllOriginalToFinalJSONObject  :--------  "+source +"\n Final Jsonobject:-"+ target);

        return target;
    }*/



    public static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}

   /* private void runCropImage() {

        // create explicit intent
        Intent intent = new Intent(context, CropImage.class);

        // tell CropImage activity to look for image to crop
       // String filePath = ...;
        intent.putExtra(CropImage.IMAGE_PATH, filePath);

        // allow CropImage activity to rescale image
        intent.putExtra(CropImage.SCALE, true);

        // if the aspect ratio is fixed to ratio 3/2
        *//*intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);*//*

        // start activity CropImage with certain request code and listen
        // for result
        startActivityForResult(intent, PIC_CROP);
    }
*/
