package com.example.admin.daveai.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.example.admin.daveai.database.models.APIQueueTableRowModel;
import com.example.admin.daveai.database.models.MediaQueueTableRowModel;
import com.example.admin.daveai.database.models.MediaTableRowModel;
import com.example.admin.daveai.database.models.ModelTableRowModel;
import com.example.admin.daveai.database.models.ObjectsTableRowModel;
import com.example.admin.daveai.database.models.RecommendationsTableRowModel;
import com.example.admin.daveai.database.models.S3UploadFileModel;
import com.example.admin.daveai.database.models.SingletonTableRowModel;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.model.Model;
import com.example.admin.daveai.others.CacheModelPOJO;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.admin.daveai.daveUtil.DaveConstants.ERROR_MSG_ATTRIBUTE;


public class DatabaseManager implements DatabaseConstants{
//TODO For deleting from post list, here is the priority
/*1. If post, then delete everything from the queue and then add post
2. If patch then delete all gets from the queue and add patch
3. If delete then delete everything from the queue and then add delete
4. If get then do nothing....*/
        private Context context;
        private long queueSize = 0;
        private final  String TAG = getClass().getSimpleName();
//        private Integer mOpenCounter = 0;
        private static DatabaseManager instance;
        private static DatabaseHelper mDatabaseHelper;
        private SQLiteDatabase mDatabase = null;

    public static synchronized void initializeInstance(DatabaseHelper helper) {
        if (instance == null) {
            instance = new DatabaseManager();
            mDatabaseHelper = helper;
        }
    }

    public static synchronized DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager();
            mDatabaseHelper = DatabaseHelper.newInstance(context);
            instance.openDatabase();
            instance.forceUpdateQueueSize();
        }
        instance.context = context;
        return instance;
    }

    public synchronized SQLiteDatabase openDatabase() {
            if(mDatabase==null){
                // Opening new database
                Log.e(TAG,"openDatabase DATA BASE CREATED since null");
                mDatabase = mDatabaseHelper.getWritableDatabase();
            }
            if(!mDatabase.isOpen()) {
                Log.e(TAG,"openDatabase DATA BASE CREATED since closed");
                // Opening new database
                mDatabase = mDatabaseHelper.getWritableDatabase();
            }
        return mDatabase;
    }

    public synchronized void closeDatabase() {

            // Closing database
//            mDatabase.close();

    }



    public synchronized void upgradeTable(){
        String CREATE_POST_QUEUE_TABLE = "CREATE TABLE IF NOT EXISTS "+"api_queue_table"+" (" +
                POST_ID+" TEXT PRIMARY KEY, " +
                API_URL+" TEXT, " +
                API_METHOD+" TEXT, " + //POST, PATCH, GET, DELETE
                API_BODY+" TEXT, " +
                UPDATE_TABLE_NAME+" TEXT, " +
                UPDATE_TABLE_ROW_ID+" TEXT, " +
                STATUS+" TEXT, " +
                PRIORITY+" INT, " +
                CACHE_TABLE_TYPE+" INT, " +
                EXTRA_DATA+" TEXT  DEFAULT '', " +
                ON_ERROR+" TEXT" +
                ")";
        mDatabase.execSQL(CREATE_POST_QUEUE_TABLE);
        mDatabase.execSQL("INSERT INTO "+"api_queue_table"+" SELECT * FROM "+POST_QUEUE_TABLE_NAME+";");
    }

    public synchronized void renameTable(String oldName, String newName){
        if(isTableExists(oldName)) {
            mDatabase.execSQL("ALTER TABLE " + oldName + " RENAME TO " + newName);
        }
    }

    public boolean isTableExists(String tableName) {
        Cursor cursor = mDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '"+tableName+"'", null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public boolean isTableEmpty(String tableName) {
        Cursor cursor = getDataFromTable(tableName);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                cursor.close();
                return false;
            }
            cursor.close();
        }
        return true;
    }

    public long updateQueueSizeOnDelete(){
        queueSize--;
        return queueSize;
    }

    public long updateQueueSizeOnAdd(){
        queueSize++;
        return queueSize;
    }

    public long getQueueSize(){
        return queueSize;
    }

    public long forceUpdateQueueSize() {
        return forceUpdateQueueSize(-1);
       /* try {
            Cursor postQueueCursor = getDataFromTable(POST_QUEUE_TABLE_NAME);
            queueSize = postQueueCursor.getCount();
            return queueSize;
        }catch (Exception e){
            e.printStackTrace();
            queueSize = 0;
        }
        return 0;*/
    }

    public long forceUpdateQueueSize(int priority) {
        try {
            HashMap<String,Object> queryParam = new HashMap<>();
            if(priority != -1)
                queryParam.put(PRIORITY,String.valueOf(priority));

            Cursor postQueueCursor = getDataFromTableWhere(POST_QUEUE_TABLE_NAME,queryParam);
            queueSize = postQueueCursor.getCount();
            return queueSize;

        }catch (Exception e){
            e.printStackTrace();
            queueSize = 0;
        }
        return 0;
    }

    public long checkLoginQueueSize() {
        try {
            HashMap<String,Object> queryParam = new HashMap<>();
            queryParam.put(PRIORITY,String.valueOf(APIQueuePriority.LOGIN.getValue()));
            Cursor postQueueCursor = getDataFromTableWhere(POST_QUEUE_TABLE_NAME,queryParam);
            queueSize = postQueueCursor.getCount();
            return queueSize;

        }catch (Exception e){
            e.printStackTrace();
            queueSize = 0;
        }
        return -1;
    }

    public void dropTables(ArrayList<String> tableNames){
        for(int i = 0 ; i < tableNames.size() ; i++){
            dropTable(tableNames.get(i));
        }
    }

    public void dropTable(String tableName){
        Log.i(TAG,"Dropping table = "+tableName);
        if(isTableExists(tableName)) {
            mDatabase.execSQL("DROP TABLE " + tableName);
        }
    }


    public synchronized void insertAPIQueueData(APIQueueTableRowModel apiQueueTableRowModel){
        if(!isTableExists(POST_QUEUE_TABLE_NAME))
           createTable(POST_QUEUE_TABLE_NAME, CacheTableType.POST_QUEUE);
        Log.i(TAG,"inserting data into api queue = api = "+apiQueueTableRowModel.apiURL+"  method = "+apiQueueTableRowModel.apiMethod
                +"\n PostBody"+apiQueueTableRowModel.apiBody);
        String tableName = generateTableName(POST_QUEUE_TABLE_NAME,CacheTableType.POST_QUEUE);
        if(apiQueueTableRowModel.apiURL!= null) {
            ContentValues values = new ContentValues();
            values.put(API_URL, apiQueueTableRowModel.apiURL);
            values.put(API_METHOD, apiQueueTableRowModel.apiMethod);
            values.put(API_BODY, apiQueueTableRowModel.apiBody);
            values.put(UPDATE_TABLE_NAME, apiQueueTableRowModel.updateTableName);
            values.put(UPDATE_TABLE_ROW_ID, apiQueueTableRowModel.updateTableRowId);
            values.put(STATUS, apiQueueTableRowModel.status);
            values.put(PRIORITY, apiQueueTableRowModel.priority);
            values.put(CACHE_TABLE_TYPE, apiQueueTableRowModel.cacheTableType);
            values.put(ON_ERROR, apiQueueTableRowModel.onError);
            insertIntoTable(tableName,values);
            updateQueueSizeOnAdd();

        }
        closeDatabase();
    }

    public synchronized void insertS3UploadQueueData(S3UploadFileModel s3UploadFileModel){
        if(!isTableExists(S3_UPLOAD_QUEUE_TABLE_NAME)) {
            createTable(S3_UPLOAD_QUEUE_TABLE_NAME, DatabaseConstants.CacheTableType.S3_UPLOAD_QUEUE);
        }
        if(s3UploadFileModel.getLocal_path()!= null) {
            ContentValues values = new ContentValues();
            values.put(LOCAL_PATH, s3UploadFileModel.getLocal_path());
            values.put(UPLOAD_FILE_NAME, s3UploadFileModel.getUpload_file_name());
            values.put(UPLOAD_FOLDER_PATH, s3UploadFileModel.getUpload_folder_path());
            values.put(UPLOAD_BUCKET, s3UploadFileModel.getUpload_bucket_name());

            HashMap<String, Object> params = new HashMap<>();
            params.put(LOCAL_PATH, s3UploadFileModel.getLocal_path());
            Cursor c = getDataFromTable(S3_UPLOAD_QUEUE_TABLE_NAME, params, new HashMap<>(), "=", 0, "");

            if (c.getCount() == 0) {
                Log.e(TAG,"insert into "+S3_UPLOAD_QUEUE_TABLE_NAME+" called");
                insertIntoTable(S3_UPLOAD_QUEUE_TABLE_NAME, values);
            } else {
                updateRow(S3_UPLOAD_QUEUE_TABLE_NAME,values,LOCAL_PATH+"=?",new String[]{s3UploadFileModel.getLocal_path()});
                Log.e(TAG,"update into "+S3_UPLOAD_QUEUE_TABLE_NAME+" called");
            }
        }
        closeDatabase();
    }


    public synchronized void insertMediaMapData(MediaTableRowModel mediaTableRowModel){
        createTable(MEDIA_TABLE_NAME,CacheTableType.MEDIA_TABLE);
        if(mediaTableRowModel.url!= null) {
            ContentValues values = new ContentValues();
            values.put(URL, mediaTableRowModel.url);
            values.put(TYPE, mediaTableRowModel.type);
            values.put(LOCAL_PATH, mediaTableRowModel.localPath);
            values.put(LAST_SYNCED, mediaTableRowModel.lastSynced);

            HashMap<String, Object> params = new HashMap<>();
            params.put(URL, mediaTableRowModel.url);
            Cursor c = getDataFromTable(MEDIA_TABLE_NAME, params, new HashMap<>(), "=", 0, "");

            if (c.getCount() == 0) {
                insertIntoTable(MEDIA_TABLE_NAME, values);
            } else {
                updateRow(MEDIA_TABLE_NAME,values,URL+"=?",new String[]{mediaTableRowModel.url});
            }
        }
        closeDatabase();
    }


    public void deleteAllRowsInTable(String tableName){
        mDatabase.delete(tableName,null,null);
        closeDatabase();
    }
    public synchronized void insertMediaQueueData(MediaQueueTableRowModel mediaQueueTableRowModel){
        //Log.d(TAG,"INSERTING INTO MEDIA QUEUE");
        Cursor c = null;
        try {
            HashMap<String, Object> pars = new HashMap<>();
            pars.put(URL, mediaQueueTableRowModel.url);
            c = getDataFromTable(MEDIA_TABLE_NAME, pars, new HashMap<>(), "=", 0, "");
        }catch (Exception e){
            e.printStackTrace();
        }

       /* if(c==null){
            Log.e(TAG,"C IS NULL ");
        }else{
            Log.e(TAG,"C IS NOT NULL ==  "+c.getCount());
        }*/

        if(c!=null && c.getCount()==0) {
            Log.e(TAG, "added to media queue file = "+mediaQueueTableRowModel.url);
            createTable(MEDIA_QUEUE_TABLE_NAME, CacheTableType.MEDIA_QUEUE_TABLE);
            if (mediaQueueTableRowModel.url != null && !TextUtils.isEmpty(mediaQueueTableRowModel.url)) {

                HashMap<String, Object> params = new HashMap<>();
                params.put(URL, mediaQueueTableRowModel.url);
                params.put(UPDATE_TABLE_NAME, mediaQueueTableRowModel.updateTableName);
                params.put(UPDATE_TABLE_ROW_ID, mediaQueueTableRowModel.updateTableRowId);
                params.put(UPDATE_ATTRIBUTE_NAME, mediaQueueTableRowModel.updateAttributeName);

                Cursor c1 = getDataFromTableWhere(MEDIA_QUEUE_TABLE_NAME, params);
                int existingRequestCount = c1.getCount();
                c1.close();
                Log.e(TAG,"existingRequestCount = "+existingRequestCount);
                if (existingRequestCount == 0) {
                    ContentValues values = new ContentValues();
                    values.put(URL, mediaQueueTableRowModel.url);
                    values.put(TYPE, mediaQueueTableRowModel.type);
                    values.put(UPDATE_TABLE_NAME, mediaQueueTableRowModel.updateTableName);
                    values.put(UPDATE_TABLE_ROW_ID, mediaQueueTableRowModel.updateTableRowId);
                    values.put(STATUS, mediaQueueTableRowModel.status);
                    values.put(UPDATE_ATTRIBUTE_NAME, mediaQueueTableRowModel.updateAttributeName);
                    insertIntoTable(MEDIA_QUEUE_TABLE_NAME, values);
                }
            }
        }else{
            Log.e(TAG," insertMediaQueueData  C IS NULL ");
        }
    }

    public synchronized void updateAPIQueueData(APIQueueTableRowModel apiQueueTableRowModel){

        ContentValues values = new ContentValues();
        values.put(API_URL, apiQueueTableRowModel.apiURL);
        values.put(API_METHOD, apiQueueTableRowModel.apiMethod);
        values.put(API_BODY, apiQueueTableRowModel.apiBody);
        values.put(UPDATE_TABLE_NAME, apiQueueTableRowModel.updateTableName);
        values.put(UPDATE_TABLE_ROW_ID, apiQueueTableRowModel.updateTableRowId);
        values.put(STATUS, apiQueueTableRowModel.status);
        values.put(PRIORITY, apiQueueTableRowModel.priority);
        values.put(ON_ERROR, apiQueueTableRowModel.onError);

        updateRow(POST_QUEUE_TABLE_NAME, values, POST_ID+"=?",new String[]{String.valueOf(apiQueueTableRowModel.postId)});
    }
    public synchronized void updateMediaQueueData(MediaQueueTableRowModel mediaQueueTableRowModel){

        ContentValues values = new ContentValues();
        values.put(URL, mediaQueueTableRowModel.url);
        values.put(TYPE, mediaQueueTableRowModel.type);
        values.put(UPDATE_TABLE_NAME, mediaQueueTableRowModel.updateTableName);
        values.put(UPDATE_TABLE_ROW_ID, mediaQueueTableRowModel.updateTableRowId);
        values.put(STATUS, mediaQueueTableRowModel.status);
        values.put(UPDATE_ATTRIBUTE_NAME, mediaQueueTableRowModel.updateAttributeName);

        updateRow(POST_QUEUE_TABLE_NAME, values, POST_ID+"=?",new String[]{String.valueOf(mediaQueueTableRowModel.postId)});
    }

    public synchronized void insertObjectsData(String tableName, ObjectsTableRowModel coreTableRowModel){

        tableName = generateTableName(tableName,CacheTableType.OBJECTS);
        if(getMetaData(tableName)!=null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id);
            values.put(DATA_CACHED, coreTableRowModel.data_cached);
            values.put(LAST_SYNCED, coreTableRowModel.last_synced);
            insertOrUpdate(tableName,values);
        }
    }

    public synchronized void insertOrUpdate(String tableName, ContentValues contentValues) {

        CacheModelPOJO cacheModelPOJO = getMetaData(tableName);
        if (cacheModelPOJO != null) {

            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(OBJECT_ID,contentValues.getAsString(OBJECT_ID));
            Cursor c = getDataFromTableWhere(tableName,queryParams);
            if (c.getCount()>0){
                updateRow(tableName,contentValues,OBJECT_ID+"=?",new String[]{contentValues.getAsString(cacheModelPOJO.model_id_name)});
            }else{
                insertIntoTable(tableName,contentValues);
            }
            c.close();
        }
    }

    public synchronized void insertOrUpdateObjectData(String tableName, ObjectsTableRowModel coreTableRowModel){

        if(getMetaData(tableName)!= null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id);
            values.put(DATA_CACHED, coreTableRowModel.data_cached);
            values.put(LAST_SYNCED, coreTableRowModel.last_synced);
            if(coreTableRowModel.error_message!=null && !coreTableRowModel.error_message.isEmpty())
                values.put(ERROR_MESSAGE,coreTableRowModel.error_message);

            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(OBJECT_ID,coreTableRowModel.object_id);
           // Log.e(TAG,"insertOrUpdateObjectData Table Name :- "+tableName +" lastDataSync:-"+coreTableRowModel.last_synced);

            if (getDataFromTableWhere(tableName,queryParams).getCount()>0){
                Log.d(TAG,"Update Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.last_synced);
                updateRow(tableName,values,OBJECT_ID+"=?",new String[]{coreTableRowModel.object_id});
            }else{
                Log.d(TAG,"Insert  new Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.data_cached);
                insertIntoTable(tableName,values);
            }
        }else {
            Log.e(TAG,"Create Meta Data For modelName of Model:- "+tableName);
        }
    }

    public synchronized void insertObjectDataIfNotThere(String tableName, ObjectsTableRowModel coreTableRowModel){

        if(getMetaData(tableName)!= null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id);
            values.put(DATA_CACHED, coreTableRowModel.data_cached);
            values.put(LAST_SYNCED, coreTableRowModel.last_synced);
            if(coreTableRowModel.error_message!=null && !coreTableRowModel.error_message.isEmpty())
                values.put(ERROR_MESSAGE,coreTableRowModel.error_message);

            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(OBJECT_ID,coreTableRowModel.object_id);

            if (getDataFromTableWhere(tableName,queryParams).getCount()>0){
                /*//Log.i(TAG,"Update Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.data_cached);
                updateRow(tableName,values,OBJECT_ID+"=?",new String[]{coreTableRowModel.object_id});*/
            }else{
                // Log.i(TAG,"Insert  new Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.data_cached);
                insertIntoTable(tableName,values);
            }
        }else {
            Log.e(TAG,"Create Meta Data For modelName of Model:- "+tableName);
        }
    }

    public synchronized long getLastupdated(String modelName,CacheTableType cacheTableType){
        try {
            String query = "Select MAX(" + LAST_SYNCED + ") as "+LAST_SYNCED+" from " + generateTableName(modelName, cacheTableType);
            Cursor c = mDatabase.rawQuery(query, null);
            c.moveToFirst();
            for(int i = 0 ; i < c.getColumnNames().length ; i++){
                Log.d(TAG,"getLastupdated:-"+c.getColumnNames()[i]);
            }
            return c.getLong(c.getColumnIndex(LAST_SYNCED));
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public synchronized JSONObject getObjectData(String modelName ,String objectId){
        try {
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(OBJECT_ID, objectId);
            //Log.i(TAG,"getObjectData :---"+ modelName +" ObjectID:- "+ objectId+" queryParams="+queryParams);
            Cursor cursor = getDataFromTableWhere(modelName, queryParams);
            if(cursor!=null) {
                if (cursor.moveToFirst()) {
                    do {
                        return new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }

        }catch (Exception e){
            Log.e(TAG,"Error during getting Object Response from DB:-"+e.getMessage());
            e.printStackTrace();

        }
        return new JSONObject();

    }



  /*  public synchronized void updateObjectsData(String tableName, ObjectsTableRowModel coreTableRowModel,String key){

        if(getMetaData(tableName)!= null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id);
            values.put(DATA_CACHED, coreTableRowModel.data_cached);
            values.put(LAST_SYNCED, coreTableRowModel.last_synced);

            Log.i(TAG,"Update Objects Table row Object Details  :- "+coreTableRowModel.data_cached);
            updateRow(tableName,values,key+"=?",new String[]{coreTableRowModel.object_id});
        }
    }*/

    public synchronized void insertRecommendationsData(String tableName, RecommendationsTableRowModel coreTableRowModel){

        Log.e(TAG,"INSERTING RECOMMENDATION 1 = "+tableName);
        tableName = generateTableName(tableName,CacheTableType.RECOMMENDATION);
        Log.e(TAG,"INSERTING RECOMMENDATION 2 = "+tableName);
        /*if(getMetaData(tableName)!=null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id); // Contact Name
            values.put(DATA_CACHED, coreTableRowModel.data_cached); // Contact Phone
            values.put(LAST_SYNCED, coreTableRowModel.last_synced); // Contact Phone

            Log.e("LOGGER","INSERTING RECOMMENDATION DATA = "+coreTableRowModel.data_cached);
            insertIntoTable(tableName,values);
            Log.e("LOGGER","data inserted for tablename = " + tableName + " count = " +getDataFromTable(tableName).getCount());
        }*/



        if(getMetaData(tableName)!= null) {
            ContentValues values = new ContentValues();
            values.put(OBJECT_ID, coreTableRowModel.object_id); // Contact Name
            values.put(DATA_CACHED, coreTableRowModel.data_cached); // Contact Phone
            values.put(LAST_SYNCED, coreTableRowModel.last_synced); // Contact Phone


            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(OBJECT_ID,coreTableRowModel.object_id);
            if (getDataFromTableWhere(tableName,queryParams).getCount()>0){
                Log.e(TAG,"Update Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.data_cached);
                updateRow(tableName,values,OBJECT_ID+"=?",new String[]{coreTableRowModel.object_id});

            }else{
                Log.e(TAG,"Insert  new Object in Objects Table Name :- "+tableName +" details:-"+coreTableRowModel.data_cached);
                insertIntoTable(tableName,values);
            }
        }else {
            Log.e(TAG,"Create Meta Data For modelName of Model:- "+tableName);
        }
    }

    public interface OnAPIQueueListener{
        void isEmpty(boolean isEmpty,long count);
    }



    public interface OnAPIQueueEmptyListener{
        void isEmpty();
    }
    public int checkAPIQueueCount(String tableName,int priorityToCheck){
        int count = 0;
        HashMap<String,Object> params = new HashMap<>();
        params.put("priority",String.valueOf(priorityToCheck));
        Cursor postQueueCursor = DatabaseManager.getInstance(context).getDataFromTableWhere(tableName,params);
        if(postQueueCursor!=null) {
            count = postQueueCursor.getCount();
        }
        return count;
    }
    public int checkAPIQueueCount(String tableName){
        int count = 0;
        Cursor postQueueCursor = DatabaseManager.getInstance(context).getDataFromTable(tableName);
        if(postQueueCursor!=null) {
            count = postQueueCursor.getCount();
        }
        return count;
    }
    int count = 0;
    /**
     * runs a thread with intervals of @checkIntervals which has a minimum value of 1000
     * */
    public void alertOnAPIQueueIsEmpty(OnAPIQueueEmptyListener onAPIQueueEmptyListener,long checkIntervals){
        if(checkIntervals < 1000){
            checkIntervals = 1000;
        }
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
              count = checkAPIQueueCount(POST_QUEUE_TABLE_NAME);
              Log.e(TAG,"current api queue length = "+count);
              if(count == 0){
                  onAPIQueueEmptyListener.isEmpty();
                  this.cancel();
              }
            }
        }, 0, checkIntervals);
    }
    /**
     * runs a thread with intervals of @checkIntervals which has a minimum value of 1000
     * */
    public void alertOnAPIQueueIsEmpty(OnAPIQueueEmptyListener onAPIQueueEmptyListener,long checkIntervals,int priorityToCheck){
        if(checkIntervals < 1000){
            checkIntervals = 1000;
        }
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
                count = checkAPIQueueCount(POST_QUEUE_TABLE_NAME,priorityToCheck);
                if(count == 0){
                    onAPIQueueEmptyListener.isEmpty();
                    this.cancel();
                }
            }
        }, 0, checkIntervals);
    }
    /**
     * runs a thread with intervals of @checkIntervals which has a minimum value of 1000
     * */
    public void alertOnMediaQueueIsEmpty(OnAPIQueueEmptyListener onAPIQueueEmptyListener,long checkIntervals){
        if(checkIntervals < 1000){
            checkIntervals = 1000;
        }
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
                count = checkAPIQueueCount(MEDIA_QUEUE_TABLE_NAME);
                Log.e(TAG,"media queue length == "+count);
                if(count == 0){
                    onAPIQueueEmptyListener.isEmpty();
                    this.cancel();
                }
            }
        }, 0, checkIntervals);
    }

    /**
     * runs a thread with intervals of @checkIntervals which has a minimum value of 1000
     * */
    public void alertOnMediaQueueIsEmpty(OnAPIQueueListener onAPIQueueListener,long checkIntervals){
        if(checkIntervals < 1000){
            checkIntervals = 1000;
        }
        Handler handler = new Handler();
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //your method
                count = checkAPIQueueCount(MEDIA_QUEUE_TABLE_NAME);
                Log.e(TAG,"media queue length == "+count);
                handler.post(new Runnable() {
                    public void run() {
                        //do Stuff here
                        if(count == 0){
                            onAPIQueueListener.isEmpty(true,count);

                        }else {
                            onAPIQueueListener.isEmpty(false,count);
                        }
                    }
                });
                if(count == 0){
                    this.cancel();
                }

            }
        }, 0, checkIntervals);
    }




    public ArrayList<String> checkErrorRows(){
        ArrayList<String> errorModels = new ArrayList<>();
        HashMap<String,Object> queryParams = new HashMap<>();
        Cursor cursor = getDataFromTable(META_TABLE_NAME,queryParams,new HashMap<>(),"=",0,"");
        Cursor c = mDatabase.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);


        // Log.e(TAG," getMetaData META DATA FOR  Model NAme = "+modelName+" COunt:-" +cursor.getCount());
        if (cursor.moveToFirst()){
            do {
                if (c.moveToFirst()) {
                    while ( !c.isAfterLast() ) {
                        try {
                            if (c.getString(0).equals(cursor.getString(cursor.getColumnIndex(TABLE_NAME)))) {
                                if (isTableExists(cursor.getString(cursor.getColumnIndex(TABLE_NAME)))) {
                                    try {
                                        Cursor c1 = openDatabase().rawQuery("SELECT name FROM " + cursor.getString(cursor.getColumnIndex(TABLE_NAME)) + " WHERE " + ERROR_MESSAGE + " IS NOT NULL AND " + ERROR_MESSAGE + " != \"\"", null);
                                        if (c1 != null && c1.getCount() > 0) {
                                            errorModels.add(cursor.getString(cursor.getColumnIndex(TABLE_NAME)));
                                        }
                                    }catch (Exception e){

                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        c.moveToNext();

                    }
                }

            } while(cursor.moveToNext());
        }

        c.close();
        cursor.close();
        return errorModels;
    }


    public synchronized void insertORUpdateModelData(String tableName, ModelTableRowModel coreTableRowModel){

        try {
            if (getMetaData(tableName) != null) {
                ContentValues values = new ContentValues();
                //values.put(MODEL_TYPE,coreTableRowModel.model_type+coreTableRowModel.model_name);
                values.put(MODEL_TYPE, coreTableRowModel.model_type);
                values.put(MODEL_NAME, coreTableRowModel.model_name); // Contact Name
                values.put(DATA_CACHED, coreTableRowModel.data_cached); // Contact Phone
                values.put(LAST_SYNCED, coreTableRowModel.last_synced); // Contact Phone
                if(coreTableRowModel.error_message!=null && !coreTableRowModel.error_message.isEmpty())
                    values.put(ERROR_MESSAGE, coreTableRowModel.error_message); // error_message
                // insertIntoTable(tableName,values);

                HashMap<String, Object> queryParams = new HashMap<>();
                queryParams.put(MODEL_NAME, coreTableRowModel.model_name);

                if (getDataFromTableWhere(MODEL_TABLE_NAME, queryParams).getCount() > 0) {
                    Log.i(TAG, "Update  Model Details  :- " + coreTableRowModel.data_cached);
                    updateRow(MODEL_TABLE_NAME, values, MODEL_NAME + "=?", new String[]{coreTableRowModel.model_name});

                } else {
                    Log.i(TAG, "Insert model Details modelType :-" + coreTableRowModel.model_type +" modelName:-"+coreTableRowModel.model_name
                            +" last_synced:- "+coreTableRowModel.last_synced+" dataCached:-"+coreTableRowModel.data_cached);
                    insertIntoTable(MODEL_TABLE_NAME, values);
                }
            } else {
                Log.e(TAG, "Please create Meta Data For modelName of Model 1:- " + coreTableRowModel.model_name);
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during save or update model Response:---"+e.getMessage());
        }
    }


    public synchronized JSONObject getModelData(String modelName){
        try {
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(MODEL_NAME, modelName);
            Cursor cursor = getDataFromTableWhere(MODEL_TABLE_NAME, queryParams);
            //Log.e(TAG,"get Model From DB:----------"+cursor+" && "+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    return new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
                } while (cursor.moveToNext());
            }
            cursor.close();
            this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Model Resonse from DB:-"+e.getMessage());
        }
        return new JSONObject();

    }

    public synchronized String getModelNameBasedOnType(String modelType){
        try {
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(MODEL_TYPE, modelType);
            Cursor cursor = getDataFromTableWhere(MODEL_TABLE_NAME, queryParams);
            //Log.i(TAG,"get ModelName based on ModelType From DB:----------"+modelType+" && cursorCOunt "+cursor.getCount());
            if (cursor.moveToFirst()) {
                do {
                    return cursor.getString(cursor.getColumnIndex(MODEL_NAME));
                } while (cursor.moveToNext());
            }
            cursor.close();
            this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting Model Resonse from DB:-"+e.getMessage());
        }
        return null;

    }

    public synchronized void insertOrUpdateCustomSingleton(String tableName, SingletonTableRowModel coreTableRowModel){

        ContentValues values = new ContentValues();
        values.put(MODEL_NAME, coreTableRowModel.model_name);
        values.put(DATA_CACHED, coreTableRowModel.data_cached);
        values.put(LAST_SYNCED, coreTableRowModel.last_synced);
        values.put(REQUEST_TYPE, coreTableRowModel.request_type);

        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.put(MODEL_NAME,coreTableRowModel.model_name);

        if (getDataFromTableWhere(SINGLETON_TABLE_NAME,queryParams).getCount()>0){
            Log.i(TAG,"Update  Singelton Object Details  :- "+coreTableRowModel.model_name);
            updateRow(SINGLETON_TABLE_NAME,values,MODEL_NAME+"=?",new String[]{coreTableRowModel.model_name});

        }else{
            Log.i(TAG,"Insert Singleton Objects Table row Object Details  :- "+coreTableRowModel.model_name);
            insertIntoTable(SINGLETON_TABLE_NAME,values);
        }

    }

    public synchronized JSONObject updateSingletonCached(String tableName,String requestType,JSONObject updateParams){


        HashMap<String, Object> queryParams = new HashMap<>();
        queryParams.put(MODEL_NAME,tableName);
        Cursor c = getDataFromTableWhere(SINGLETON_TABLE_NAME,queryParams);

        Log.e(TAG,"updateSingletonCached:----"+tableName);
        if(c.getCount()>0){
            try {
                c.moveToFirst();
                JSONObject pJsonObject = new JSONObject(c.getString(c.getColumnIndex(DATA_CACHED)));
                Iterator<String> iter = updateParams.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = updateParams.get(key);
                        pJsonObject.put(key,value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                SingletonTableRowModel coreTableRowModel = new SingletonTableRowModel(
                        tableName, pJsonObject.toString(), System.currentTimeMillis(),requestType
                );
                insertOrUpdateSingleton(tableName,coreTableRowModel);
                return pJsonObject;

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return updateParams;
    }


    public synchronized JSONObject insertOrUpdateSingleton(String tableName, SingletonTableRowModel coreTableRowModel){
         // Log.e(TAG,"insertOrUpdateSingleton:---------"+tableName+"  "+getMetaData(tableName));
         JSONObject cJsonObject = new JSONObject();
         if(getMetaData(tableName)!= null) {
            ContentValues values = new ContentValues();
            values.put(MODEL_NAME, coreTableRowModel.model_name);
           // values.put(DATA_CACHED, coreTableRowModel.data_cached);
            values.put(LAST_SYNCED, coreTableRowModel.last_synced);
            values.put(REQUEST_TYPE, coreTableRowModel.request_type);
            HashMap<String, Object> queryParams = new HashMap<>();
            queryParams.put(MODEL_NAME,coreTableRowModel.model_name);
            if(coreTableRowModel.error_message!=null && !coreTableRowModel.error_message.isEmpty())
                 values.put(ERROR_MESSAGE, coreTableRowModel.error_message); // error_message

           /* if (getDataFromTableWhere(SINGLETON_TABLE_NAME,queryParams).getCount()>0){
                updateRow(SINGLETON_TABLE_NAME,values,MODEL_NAME+"=?",new String[]{coreTableRowModel.model_name});
            }*/
            try {
                cJsonObject = new JSONObject(coreTableRowModel.data_cached);
                Cursor c = getDataFromTableWhere(SINGLETON_TABLE_NAME,queryParams);
                if (c.getCount()>0){
                    Log.e(TAG,"Update  Singleton Object Details  :- "+coreTableRowModel.model_name);
                    c.moveToFirst();
                    JSONObject pJsonObject = new JSONObject(c.getString(c.getColumnIndex(DATA_CACHED)));
                    Iterator<String> iter = cJsonObject.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        Object value = cJsonObject.get(key);
                        pJsonObject.put(key,value);

                    }
                    values.put(DATA_CACHED, pJsonObject.toString());
                    updateRow(SINGLETON_TABLE_NAME,values,MODEL_NAME+"=?",new String[]{coreTableRowModel.model_name});
                    return pJsonObject;

                }
                else{
                    Log.e(TAG,"Insert Singleton Objects Table row Object Details  :- "+coreTableRowModel.model_name);
                    values.put(DATA_CACHED, coreTableRowModel.data_cached);
                    insertIntoTable(SINGLETON_TABLE_NAME,values);
                    return cJsonObject;
                }
            }catch (Exception e){
                 e.printStackTrace();
            }
         }else{
            Log.e(TAG,"Please create Meta Data For Singleton Object Row :- "+coreTableRowModel.model_name);
        }
        return cJsonObject;
    }

    public synchronized JSONObject getSingletonRow(String modelName){
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(MODEL_NAME,modelName);
            Cursor cursor = getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);
            if (cursor.getCount()==1) {
                cursor.moveToFirst();
                //Log.e(TAG,"singleton data found for "+modelName+" column index for data_cached = "+cursor.getColumnIndex(DATA_CACHED)+" cursor count = "+cursor.getCount());
                return new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
            }
            cursor.close();
            this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            return new JSONObject();
        }
        return new JSONObject();
    }

    public synchronized String getCustomSingletonRow(String modelName){
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(MODEL_NAME,modelName);
            Cursor cursor = getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);
            if (cursor.getCount()==1) {
                cursor.moveToFirst();
                //Log.e(TAG,"singleton data found for "+modelName+" column index for data_cached = "+cursor.getColumnIndex(DATA_CACHED)+" cursor count = "+cursor.getCount());
                return cursor.getString(cursor.getColumnIndex(DATA_CACHED));
            }
            cursor.close();
            this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public synchronized JSONObject getSingletonBasedOnRequestType(String requestType){
        try {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put(REQUEST_TYPE,requestType);
            Cursor cursor = getDataFromTableWhere(SINGLETON_TABLE_NAME,hashMap);
            if (cursor.getCount()==1) {
                cursor.moveToFirst();
                return new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
            }
            cursor.close();
            this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            return new JSONObject();
        }
        return new JSONObject();
    }

    public synchronized void deleteSingletonRow(String modelName){
        try {
            ArrayList<String> attrs = new ArrayList<>();
            attrs.add(MODEL_NAME);
            deleteRow(
                    SINGLETON_TABLE_NAME,
                    createWhereClause(attrs),
                    new String[]{String.valueOf(modelName)}
            );

        }catch (Exception e){
            e.printStackTrace();

        }

    }


    public synchronized long getModelMasterTimestamp(String modelName){
        CacheModelPOJO cacheModelPOJO = getMetaData(modelName);
        if(cacheModelPOJO!=null){
            return cacheModelPOJO.master_timestamp;
        }else{
            return 0;
        }
    }

    /**
    * @apiNote send CacheModelPOJO.isDefault = 0 to update the default timeout values, else the default data will remain.
    * */
    public synchronized void insertMetaData(CacheModelPOJO cacheModelPOJO){

        ContentValues values = new ContentValues();
        values.put(TABLE_NAME, cacheModelPOJO.modelName);
        values.put(HOT_UPDATE, cacheModelPOJO.hot_refresh_time_limit);
        values.put(COLD_UPDATE, cacheModelPOJO.cold_refresh_time_limit);
        values.put(MODEL_ID_NAME, cacheModelPOJO.model_id_name);
        values.put(IS_DEFAULT, cacheModelPOJO.isDefault);
        values.put(MASTER_TIMESTAMP, cacheModelPOJO.master_timestamp);

        if (getMetaData(cacheModelPOJO.modelName) == null) {
            Log.i(TAG,"insertMetaData:---"+cacheModelPOJO.modelName);
            insertIntoTable(META_TABLE_NAME, values);

        }else {
            Log.i(TAG,"UpdateMetaData:---"+cacheModelPOJO.modelName);
            updateRow(META_TABLE_NAME, values, TABLE_NAME + "=?", new String[]{cacheModelPOJO.modelName});
        }


    }

    public synchronized CacheModelPOJO getMetaData(String modelName){
        HashMap<String,Object> queryParams = new HashMap<>();
        queryParams.put(TABLE_NAME,modelName);
        Cursor cursor = getDataFromTable(META_TABLE_NAME,queryParams,new HashMap<>(),"=",0,"");

       // Log.e(TAG," getMetaData META DATA FOR  Model NAme = "+modelName+" COunt:-" +cursor.getCount());
        if (cursor.moveToFirst()){
                CacheModelPOJO cacheModelPOJO = new CacheModelPOJO(
                        modelName,
                        cursor.getLong(cursor.getColumnIndex(HOT_UPDATE)),
                        cursor.getLong(cursor.getColumnIndex(COLD_UPDATE)),
                        0,
                        cursor.getString(cursor.getColumnIndex(MODEL_ID_NAME)),
                        System.currentTimeMillis()
                );
                cacheModelPOJO.master_timestamp = cursor.getLong(cursor.getColumnIndex(MASTER_TIMESTAMP));
                cursor.close();
                return cacheModelPOJO;
        }
        cursor.close();
        return null;
    }



    /**
     * @apiNote Base insert api
     * */
    public synchronized void insertIntoTable(String tableName, ContentValues contentValues){
        Log.e(TAG,"insertIntoTable = "+tableName +""+ contentValues.get(LAST_SYNCED));
        /*Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    long row_id = mDatabase.insert(tableName, null, contentValues);
                    Log.e(TAG,"insertIntoTable for "+tableName+" row_id = "+row_id);
                    DatabaseManager.this.closeDatabase();
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e(TAG,"Error insertIntoTable :--------"+e.getMessage());
                }
            }
        };
        thread.run();*/

        try {
            long row_id = mDatabase.insert(tableName, null, contentValues);
            Log.e(TAG,"insertIntoTable for "+tableName+" row_id = "+row_id);
            DatabaseManager.this.closeDatabase();
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error insertIntoTable :--------"+e.getMessage());
        }

    }

    /**
     * @apiNote Base update api
     * */
    public synchronized void updateRow(String tableName, ContentValues contentValues, String query_keys, String[] values){
        SQLiteDatabase db = mDatabase;

       // Log.e(TAG,"updateRow:-    "+ tableName +" last Sync"+ contentValues.get(LAST_SYNCED));
        db.update(tableName, contentValues,query_keys,values);
    }

    public synchronized JSONArray getObjectsData(String modelName,long timestamp){
        try {
            Log.e(TAG,"getAllObjectsData ModelNAme:----"+ modelName+ "  timestamp = "+timestamp);
            Cursor cursor = getDataFromTable(modelName);
            if(cursor!=null) {
                Log.e("LOGGER","CURSOR COUNT = "+cursor.getCount());

                JSONArray dataJarr = new JSONArray();
                if (cursor.moveToFirst()) {
                    long count = 0;
                    do {
                       long lastSync =  cursor.getLong(cursor.getColumnIndex(LAST_SYNCED));
                       if(lastSync<timestamp)
                           dataJarr.put(new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED))));

                    } while (cursor.moveToNext());
                }
                cursor.close();


                Log.e(TAG,"get Total Objects Count model name  :-"+modelName+" Count:-" +dataJarr.length() +" TotalCount:-"+getDataFromTable(modelName).getCount()
                        );

                return dataJarr;
            }

        }catch (Exception e){
            e.printStackTrace();
            return new JSONArray();
        }
        return new JSONArray();
    }

    public synchronized JSONObject getAllObjectsData(String modelName, HashMap<String,Object> queryParam, HashMap<String,Object> cachedDataQueryParams, long page_size, long page_number){
        try {
            Log.e(TAG,"getAllObjectsData ModelNAme:----"+ modelName+ "  queryParam = "+queryParam+ "  cachedDataQueryParams = "+cachedDataQueryParams);
            Cursor cursor;
            if(page_number>0)
                page_number--;
            long startPosition = page_number*page_size;
            cursor = getDataFromTableWhere(generateTableName(modelName, CacheTableType.OBJECTS),queryParam,cachedDataQueryParams,(page_number+1)*page_size);
            if(cursor!=null) {
                Log.e("LOGGER","CURSOR COUNT = "+cursor.getCount());
                JSONArray dataJarr = new JSONArray();
                JSONObject ret = new JSONObject();
                if (cursor.moveToFirst()) {
                        long count = 0;
                        do {
                            if(count++>=startPosition) {
                                dataJarr.put(new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED))));
                            }
                        } while (cursor.moveToNext());
                }
                cursor.close();
                 /*  "is_first": false,
                "total_number": 11772,
                "page_size": 100,
                "page_number": 2,
                "is_last": false,*/

                ret.put("total_number",getDataFromTable(modelName).getCount());
                ret.put("page_size",page_size);
                ret.put("page_number",page_number+1);
                ret.put("data", dataJarr);

                Log.e(TAG,"get Total Objects Count model name  :-"+modelName+" Count:-" +dataJarr.length() +" TotalCount:-"+getDataFromTable(modelName).getCount()
                +" Response:-"+ret);

                return ret;
            }

        }catch (Exception e){
            e.printStackTrace();
            return new JSONObject();
        }
        return new JSONObject();
    }
/**
 * @apiNote updates json keys inside the data cached for an object id and updates the table.
 * */
    public synchronized JSONObject updateDataCached(String tableName, String objectId, JSONObject updateParams,String errorMsg){

        HashMap<String, Object> objectIdParam = new HashMap<>();
        objectIdParam.put(OBJECT_ID,objectId);
        Cursor c = getDataFromTableWhere(tableName,objectIdParam);
        Log.e(TAG,"updateDataCached:----"+tableName+" id:-"+objectId +" cursorCount:-"+ c.getCount());
        if(c.getCount()>0){
            try {
                c.moveToFirst();
                JSONObject pJsonObject = new JSONObject(c.getString(c.getColumnIndex(DATA_CACHED)));
                Iterator<String> iter = updateParams.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = updateParams.get(key);
                        pJsonObject.put(key,value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //pJsonObject.put(ERROR_MSG_ATTRIBUTE,errorMsg);
                if(errorMsg ==null || errorMsg.isEmpty()){
                    pJsonObject.remove(ERROR_MSG_ATTRIBUTE);
                }else {
                    pJsonObject.put(ERROR_MSG_ATTRIBUTE,errorMsg);
                }

                ObjectsTableRowModel coreTableRowModel = new ObjectsTableRowModel(
                        objectId, pJsonObject.toString(), System.currentTimeMillis(),errorMsg
                );
                insertOrUpdateObjectData(tableName,coreTableRowModel);
                return pJsonObject;

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return updateParams;
    }

    public synchronized JSONObject getInteractionsData(String stageName, HashMap<String,Object> queryParams){

        JSONObject finalResponse = new JSONObject();
        Log.i(TAG,"getInteractionsData StageNAme="+ stageName +""+ queryParams);
        try {
            finalResponse.put("__TOTAL__", new JSONObject());
            //todo crashing
            Cursor cursor = getDataFromTableWhere(generateTableName(INTERACTIONS_TABLE_NAME, CacheTableType.INTERACTIONS),new HashMap<>(),queryParams);
            if(cursor!=null) {
                //Log.i(TAG,"get InteractionsData Count="+ cursor.getCount());
                JSONObject stageTotal = new JSONObject();
                stageTotal.put("_count",cursor.getCount());
                JSONArray dataList = new JSONArray();
                if (cursor.moveToFirst()) {
                    do {
                        JSONObject objectDetails = new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
                        dataList.put(objectDetails);

                        JSONObject quantityAttr = objectDetails.getJSONObject("quantity_attributes");
                        Iterator<String> keys = quantityAttr.keys();
                        while(keys.hasNext()) {
                            String key = keys.next();
                            if(!quantityAttr.isNull(key)) {
                                if (stageTotal.has(key)) {
                                    if (stageTotal.get(key) instanceof Double)
                                        stageTotal.put(key, stageTotal.getDouble(key) + quantityAttr.getDouble(key));
                                    else if (stageTotal.get(key) instanceof Integer)
                                        stageTotal.put(key, stageTotal.getInt(key) + quantityAttr.getInt(key));

                                } else {
                                    stageTotal.put(key, quantityAttr.get(key));
                                }
                            }
                        }

                    } while (cursor.moveToNext());
                }
                cursor.close();
                this.closeDatabase();

                JSONObject totalJson = new JSONObject();
                totalJson.put(stageName,stageTotal);
                finalResponse.put("__TOTAL__", totalJson);
                finalResponse.put(stageName, dataList);
                return finalResponse;
            }

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"Error during getting interactions "+e.getMessage());
            return finalResponse;
        }
        return finalResponse;
    }

    public synchronized void insertOrUpdateInteractions(ObjectsTableRowModel coreTableRowModel){

        try {
            if(getMetaData(INTERACTIONS_TABLE_NAME)!= null) {
                ContentValues values = new ContentValues();
                values.put(OBJECT_ID, coreTableRowModel.object_id);
                //values.put(DATA_CACHED, coreTableRowModel.data_cached);
                values.put(LAST_SYNCED, coreTableRowModel.last_synced);

                HashMap<String, Object> queryParams = new HashMap<>();
                queryParams.put(OBJECT_ID,coreTableRowModel.object_id);

                Cursor cursor = getDataFromTableWhere(INTERACTIONS_TABLE_NAME,queryParams);
                if(cursor.getCount()>0){
                    JSONObject pJsonObject = new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED)));
                    JSONObject cJsonObject = new JSONObject(coreTableRowModel.data_cached);
                    Iterator<String> keys = cJsonObject.keys();
                    while(keys.hasNext()) {
                        String key = keys.next();
                        pJsonObject.put(key,cJsonObject.get(key));
                    }
                    values.put(DATA_CACHED, pJsonObject.toString());
                    Log.d(TAG,"Update  Object in INTERACTIONS Table Name  &&  details:-"+pJsonObject.toString());
                    updateRow(INTERACTIONS_TABLE_NAME,values,OBJECT_ID+"=?",new String[]{coreTableRowModel.object_id});

                }else{
                    Log.d(TAG,"Insert  new Object in INTERACTIONS Table Name  &&  details:-"+coreTableRowModel.data_cached);
                    values.put(DATA_CACHED, coreTableRowModel.data_cached);
                    insertIntoTable(INTERACTIONS_TABLE_NAME,values);
                }
            }else {
                Log.e(TAG,"Create Meta Data For modelName of Model:- "+ INTERACTIONS_TABLE_NAME);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }



    public synchronized JSONObject getRecommendationData(String category_id){
        try {

            Cursor cursor = getDataFromTable(generateTableName(category_id, CacheTableType.RECOMMENDATION));
            JSONArray dataJarr = new JSONArray();
            JSONObject ret = new JSONObject();
            if (cursor.moveToFirst()) {

                do {
                    dataJarr.put(new JSONObject(cursor.getString(cursor.getColumnIndex(DATA_CACHED))));
                } while (cursor.moveToNext());
            }
            cursor.close();


            return ret.put("recommendations",dataJarr);
        }catch (Exception e){
            e.printStackTrace();
            return new JSONObject();
        }
    }



    public synchronized Cursor getDataFromTable(String tableName){
        return getDataFromTable(tableName,new HashMap<>(),new HashMap<>(),"=",0,"");
    }

    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams){
        return getDataFromTable(tableName,queryParams,new HashMap<>(),"=",0,"");
    }
    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams,Map<String,Object> cachedDataQueryParams){
        return getDataFromTable(tableName,queryParams,cachedDataQueryParams,"=",0,"");
    }
    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams,Map<String,Object> cachedDataQueryParams, long limit){
        return getDataFromTable(tableName,queryParams,cachedDataQueryParams,"=",limit,"");
    }

    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams, long limit, String orderBy){
        return getDataFromTable(tableName,queryParams,new HashMap<>(),"=", limit, orderBy);
    }
    /**
     * @apiNote select <tableName> where <queryParam.key> <comparator> <queryParam.value>
     * */
    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams, String comparator, long limit, String orderBy){
        return getDataFromTable(tableName,queryParams,new HashMap<>(),comparator,limit,orderBy);
    }
    public synchronized Cursor getDataFromTableWhereCacheContains(String tableName,Map<String,Object> cachedDataQueryParams, long limit, String orderBy){
        return getDataFromTable(tableName,new HashMap<>(),cachedDataQueryParams,"=",limit,orderBy);
    }


    /**
     * @apiNote select <tableName> where <queryParam.key> <comparator> <queryParam.value>
     * */
    public synchronized Cursor getDataFromTableWhere(String tableName,Map<String,Object> queryParams, String comparator){

        return getDataFromTable(tableName,queryParams,new HashMap<>(),comparator,0,"");
    }

    public synchronized Cursor getDataFromTableWhereCacheContains(String tableName,Map<String,Object> cachedDataQueryParams){
        return getDataFromTable(tableName,new HashMap<>(),cachedDataQueryParams,"=",0,"");

    }


    /**
     * when calling select where, add ~ before key/NOT_EQUALS to check for not equals
     * when calling select where, add ~ after key to check for key like value
     * when calling select where, add other operators as last char of key to use different comparators
     *
     * */

    private String getComparator(String key){
        if(key.startsWith(TILDE)){
            return NOT_EQUALS;
        }else if(key.endsWith(TILDE)){
            return LIKE_OPERATOR;
        }else if(key.endsWith(NOT_TILDE)){
            return NOT_LIKE_OPERATOR;
        }else if(key.endsWith(EQUALS)){
            return EQUALS;
        }else if(key.endsWith(NOT_EQUALS)){
            return NOT_EQUALS;
        }else if(key.endsWith(LESS_THAN)){
            return LESS_THAN;
        }else if(key.endsWith(GREATER_THAN)){
            return GREATER_THAN;
        }else{
            return EQUALS;
        }
    }

    private String getKey(String key){
        if(key.startsWith(TILDE)){
            return key.substring(1);
        }else if(key.endsWith(TILDE)){
            return key.substring(0,key.length()-1);
        }else if(key.endsWith(NOT_TILDE)){
            return key.substring(0,key.length()-1);
        }else if(key.endsWith(EQUALS)){
            return key.substring(0,key.length()-1);
        }else if(key.endsWith(NOT_EQUALS)){
            return key.substring(0,key.length()-1);
        }else if(key.endsWith(LESS_THAN)){
            return key.substring(0,key.length()-1);
        }else if(key.endsWith(GREATER_THAN)){
            return key.substring(0,key.length()-1);
        }else{
            return key;
        }
    }

/**
 * @param tableName name of the table to run select query on
 * @param queryParams Map of key value of params to query for. If empty it will run select * tableName
 * @param cachedDataQueryParams Map of key value of params to query for. If empty it will run select * tableName
 * */
    public synchronized Cursor getDataFromTable(String tableName, Map<String,Object> queryParams,
                                                Map<String,Object> cachedDataQueryParams,String comparator,long limit, String orderBy){

       //Log.e(TAG,"getDataFromTable ---tableNAme:-"+tableName+" queryParams == "+queryParams+" cachedDataQueryParams"+cachedDataQueryParams);
       // Log.e(TAG,"getDataFromTable --- cachedDataQueryParams == "+cachedDataQueryParams);
        String query = "";



        if(queryParams.size()>0||cachedDataQueryParams.size()>0){
            query = " where ";
        }


        //openDatabase();

        ArrayList<Object> values = new ArrayList<>();
        Iterator iterator = queryParams.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String,Object> entry = (Map.Entry<String,Object>)iterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();

            String comp = getComparator(key);

            if(comp.equals(LIKE_OPERATOR)||comp.equals(NOT_LIKE_OPERATOR)){
                value = "%"+(String)value+"%";
            }

            query = query+" "+getKey(key)+" "+comp+" ?";
            if(iterator.hasNext()){
                query = query + " and ";
            }
            values.add(value);

            Log.d("QUERY_DATA",key+" "+getComparator(key)+" "+value);
        }

        if(!query.equals(" where ")&&cachedDataQueryParams.size()>0){
            query = query + " and ";
        }

        Iterator cachedDataQueryIterator = cachedDataQueryParams.entrySet().iterator();
        while(cachedDataQueryIterator.hasNext()){
            Map.Entry<String,Object> entry = (Map.Entry<String,Object>)cachedDataQueryIterator.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if(value instanceof List<?> || value instanceof ArrayList<?>){
                Model mod = new Model(context);

                try {
                    JSONObject modelJsn = getModelData(tableName);
                    Gson gson = new Gson();
                    mod = gson.fromJson(modelJsn.toString(),Model.class);
                }catch (Exception e){
                    e.printStackTrace();
                }
                Log.e(TAG,"attribute atype = "+mod.getAttributeAType(key));

                String queryAddition = "";
                for(int i = 0 ; i < ((List<Object>)value).size() ; i++ ){
                    if(i>0){
                        queryAddition +=" or ";
                    }
                    JSONObject keyValuePairJson = new JSONObject();
                    String keyValuePairStr = "";
                    try {
                        if(mod!=null&&mod.getAttributeAType(key)!=null&&!mod.getAttributeAType(key).isEmpty()
                                &&
                                (mod.getAttributeAType(key).equals("unicode")
                                ||mod.getAttributeAType(key).equals("float")
                                ||mod.getAttributeAType(key).equals("int")
                                ||mod.getAttributeAType(key).equals("date")
                                ||mod.getAttributeAType(key).equals("time")
                                ||mod.getAttributeAType(key).equals("datetime")
                                ||mod.getAttributeAType(key).equals("objectlist")
                                ||mod.getAttributeAType(key).equals("bool")
                                ||mod.getAttributeAType(key).equals("dict")
                                )){
                            keyValuePairJson.put(key, ((List<Object>) value).get(i));
                            keyValuePairStr = "%"+keyValuePairJson.toString().substring(1, keyValuePairJson.toString().length() - 1)+"%";
                        }else if(mod!=null&&mod.getAttributeAType(key)!=null&&!mod.getAttributeAType(key).isEmpty()
                                &&(mod.getAttributeAType(key).equals("list")||mod.getAttributeAType(key).equals("stringlist"))){
                            keyValuePairJson.put(key, "[%" + ((List<Object>) value).get(i) + "%]");
                            keyValuePairStr = "%\""+key+"\":[%\""+((List<Object>) value).get(i)+"\"%]%";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error during getDataFromTable:------------- " + e.getMessage());
                    }
                    queryAddition += DATA_CACHED+" LIKE ?";
                    values.add( keyValuePairStr);
                }
                query+=" ( "+queryAddition+" ) ";
                if (cachedDataQueryIterator.hasNext()) {
                    query = query + " and ";
                }
            }else {
                query = query + DATA_CACHED + " LIKE ?";

                if (cachedDataQueryIterator.hasNext()) {
                    query = query + " and ";
                }
                JSONObject keyValuePairJson = new JSONObject();
                String keyValuePairStr = "";
                try {
                    Model modelInstance = ModelUtil.getModelInstance(context,tableName);
                    if(modelInstance!= null){
                        if( modelInstance.getAttributeAType(key)!=null && !modelInstance.getAttributeAType(key).isEmpty()
                                &&(modelInstance.getAttributeAType(key).equals("unicode")
                                ||modelInstance.getAttributeAType(key).equals("float")
                                ||modelInstance.getAttributeAType(key).equals("int")
                                ||modelInstance.getAttributeAType(key).equals("date")
                                ||modelInstance.getAttributeAType(key).equals("time")
                                ||modelInstance.getAttributeAType(key).equals("datetime")
                                ||modelInstance.getAttributeAType(key).equals("objectlist")
                                ||modelInstance.getAttributeAType(key).equals("bool")
                                ||modelInstance.getAttributeAType(key).equals("dict")
                        )){
                            if(value.toString().startsWith(TILDE)){
                                value = "%"+value.toString().substring(1)+"%";
                            }
                            keyValuePairJson.put(key, value);
                            keyValuePairStr = "%"+keyValuePairJson.toString().substring(1, keyValuePairJson.toString().length() - 1)+"%";

                        }else if(modelInstance.getAttributeAType(key)!=null&&!modelInstance.getAttributeAType(key).isEmpty()&&(modelInstance.getAttributeAType(key).equals("list")||modelInstance.getAttributeAType(key).equals("stringlist"))){
                            if(value.toString().startsWith(TILDE)){
                                value = "%"+value.toString().substring(1)+"%";
                            }
                            keyValuePairJson.put(key, "[%" + ( value) + "%]");
                            keyValuePairStr = "%\""+key+"\":[%\""+(value)+"\"%]%";
                        }

                        keyValuePairJson.put(key, "[%" + ( value) + "%]");
                        keyValuePairStr = "%\""+key+"\":\""+(value)+"\"%";
                    }else{
                        keyValuePairJson.put(key, ( value));
                        keyValuePairStr = "%"+keyValuePairJson.toString().substring(1, keyValuePairJson.toString().length() - 1)+"%";
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(TAG, "Error during getDataFromTable:------------- " + e.getMessage());
                }
                values.add( keyValuePairStr );

                Log.d(TAG,DATA_CACHED+" = "+keyValuePairStr);
            }

        }

        Log.d(TAG,"  Method getDataFromTable:-  query = " + "SELECT * FROM " + tableName + query);

        String [] valuesArray = values.toArray(new String[values.size()]);
        String selectQuery = "SELECT * FROM "+tableName+query;

        if(!TextUtils.isEmpty(orderBy)){
            selectQuery = selectQuery + " ORDER BY "+orderBy;
        }

        if(limit>0){
            selectQuery = selectQuery + " LIMIT "+limit;
        }

        Log.d(TAG,"query = "+selectQuery+"  values count = "+valuesArray);

        for(int i = 0 ; i < valuesArray.length; i++){
            Log.d(TAG,"valuesArray["+i+"] = "+valuesArray[i]);
        }
        Cursor cursor = mDatabase.rawQuery(selectQuery,valuesArray);

        closeDatabase();

        return cursor;
    }

    public synchronized void createTable(String tableName, CacheTableType cacheTableType){
        String createTableQuery="";
        switch (cacheTableType)
        {
            case INTERACTIONS:
                createTableQuery = String.format(CREATE_INTERACTIONS_TABLE,generateTableName(tableName,cacheTableType));
                break;
            case SINGLETON:
                createTableQuery = CREATE_SINGLETON_TABLE;
                break;
            case META_DATA:
                createTableQuery = CREATE_META_TABLE;
                break;
            case RECOMMENDATION:
                createTableQuery = String.format(CREATE_RECOMMENDATION_TABLE,generateTableName(tableName,cacheTableType));
                break;
            case MODEL:
                createTableQuery = String.format(CREATE_MODEL_TABLE,generateTableName(tableName,cacheTableType));
                break;
            case OBJECTS:
                createTableQuery = String.format(CREATE_OBJECTS_TABLE,generateTableName(tableName,cacheTableType));
                break;
            case POST_QUEUE:
                createTableQuery = CREATE_POST_QUEUE_TABLE;
                break;
            case MEDIA_QUEUE_TABLE:
                createTableQuery = CREATE_MEDIA_QUEUE_TABLE;
                break;
            case MEDIA_TABLE:
                createTableQuery = CREATE_MEDIA_TABLE;
                break;
            case S3_UPLOAD_QUEUE:
                createTableQuery = CREATE_S3_UPLOAD_QUEUE;
                break;

        }
        Log.e(TAG,"createTable() query ==== "+createTableQuery);
        mDatabase.execSQL(createTableQuery);
        closeDatabase();
    }


    public String generateTableName(String tableName, CacheTableType cacheTableType){
        String finalTableName = "";
        switch (cacheTableType)
        {
            case INTERACTIONS:
                // Do Nothing
                break;
            case SINGLETON:
                // do nothing
                finalTableName = SINGLETON_TABLE_NAME;
                break;
            case META_DATA:
                // do nothing
                break;
            case RECOMMENDATION:
                finalTableName = "recommendation__"+tableName;
                break;
            case MODEL:
                finalTableName = MODEL_TABLE_NAME;
                break;
            case OBJECTS:
                //do nothing
                break;
        }
        if(finalTableName.isEmpty()){
            return tableName;
        }else {
            return finalTableName;
        }
    }


    public DatabaseConstants.CacheRefreshStatus updateStatus(String tableName, CacheTableType cacheTableType){
        return updateStatus(tableName,cacheTableType,new HashMap<String , Object>(),new HashMap<String, Object>(),0,0);
    }

    public DatabaseConstants.CacheRefreshStatus updateStatus(String tableName, CacheTableType cacheTableType, HashMap<String,Object> queryParams, HashMap<String,Object> cacheQueryParams,long page_size, long page_number) {
        Log.d(TAG, "updateStatus " + tableName + " :--" + queryParams +" "+cacheQueryParams );

        HashMap<String, Object> tempParams = queryParams;
        Map<String, Object> metaParam = new HashMap<>();
        if (cacheTableType == CacheTableType.RECOMMENDATION) {
            metaParam.put(TABLE_NAME, generateTableName(tableName, cacheTableType));
        } else {
            metaParam.put(TABLE_NAME, tableName);
        }
        Cursor metaEntry = getDataFromTableWhere(META_TABLE_NAME, metaParam);

        Log.d(TAG, "Check Meta Data Status " + tableName + " :--" + metaEntry.getCount());

        long hotUpdateLimit = 0;
        long coldUpdateLimit = 0;
        long masterTimeStamp = 0;
        if (metaEntry.getCount() > 0) {
            if (metaEntry.moveToFirst()) {
                do {
                    hotUpdateLimit = metaEntry.getLong(metaEntry.getColumnIndex(HOT_UPDATE));
                    coldUpdateLimit = metaEntry.getLong(metaEntry.getColumnIndex(COLD_UPDATE));
                    masterTimeStamp = metaEntry.getLong(metaEntry.getColumnIndex(MASTER_TIMESTAMP));

                } while (metaEntry.moveToNext());
            }
            Log.i(TAG, " updateStatus META_DATA for " + tableName + " hot = " + hotUpdateLimit + " cold = " + coldUpdateLimit);
        } else {
            return CacheRefreshStatus.NO_META_FOUND;
        }
        metaEntry.close();

        if (cacheTableType == CacheTableType.OBJECTS && cacheQueryParams.size() == 0) {
            /*String selectQuery = "SELECT * FROM " + META_TABLE_NAME + " where " + MASTER_TIMESTAMP + " < ? and " + TABLE_NAME + " = ?";

            String[] valuesArray = new String[]{String.valueOf(System.currentTimeMillis() - coldUpdateLimit), tableName};
            Cursor cur = mDatabase.rawQuery(selectQuery, valuesArray);*/
            long coldcalctime = System.currentTimeMillis() - coldUpdateLimit;
            Log.d(TAG,"mastertimestamp = "+masterTimeStamp+"  calcTimeStamp = "+coldcalctime);
            if (masterTimeStamp < coldcalctime) {
                Log.d(TAG,"Last Updated COLD_UPDATE_REQUIRED");
                return CacheRefreshStatus.COLD_UPDATE_REQUIRED;
            }
//            cur.close();
        }


        int hotCursorCount = 0;
        int coldCursorCount = 0;
        SQLiteDatabase db = mDatabase;
        String[] valuesArray = new String[]{String.valueOf(System.currentTimeMillis() - hotUpdateLimit)};
        String selectQuery = "SELECT * FROM " + generateTableName(tableName, cacheTableType) + " where " + LAST_SYNCED + " < ?";

        if (cacheTableType == CacheTableType.SINGLETON || cacheTableType == CacheTableType.MODEL) {
            valuesArray = new String[]{String.valueOf(System.currentTimeMillis() - hotUpdateLimit), tableName};
            selectQuery = selectQuery + " and " + MODEL_NAME + " = ?";
        }

        Log.d(TAG, "checkStatus query = " + selectQuery);
//        HashMap<String,Object> params = new HashMap<>();
        tempParams.put(LAST_SYNCED + "<", String.valueOf(System.currentTimeMillis() - hotUpdateLimit));
        if (cacheTableType == CacheTableType.SINGLETON || cacheTableType == CacheTableType.MODEL) {
            tempParams.put(MODEL_NAME, tableName);
        }

        // Log.d(TAG,"HOT UPDATE QUERY Started ");
        Cursor cursor = getDataFromTableWhere(generateTableName(tableName, cacheTableType), tempParams, cacheQueryParams, (page_number + 1) * page_size);
//        curse.close();
//        Cursor cursor = db.rawQuery(selectQuery,valuesArray);
        hotCursorCount = cursor.getCount();
        /*if(cursor.moveToFirst()) {
            do {
                Log.e(TAG, "last_sync = " + cursor.getLong(cursor.getColumnIndex(LAST_SYNCED) )+"     checking time === "+- Long.parseLong(tempParams.get(LAST_SYNCED+"<").toString()));
            } while (cursor.moveToNext());
        }*/
        Log.d(TAG, " updateStatus hot update " + tableName + " query count = " + cursor.getCount());
        if (cursor.getCount() > 0) {
            cursor.close();
            closeDatabase();
            if (hotUpdateLimit > 0) {
                return CacheRefreshStatus.HOT_UPDATE_REQUIRED;
            }
        }
        cursor.close();/*
        queryParams.clear();*/

        tempParams.put(LAST_SYNCED + "<", String.valueOf(System.currentTimeMillis() - coldUpdateLimit));

        String[] vals = new String[]{String.valueOf(System.currentTimeMillis() - coldUpdateLimit)};
        if (cacheTableType == CacheTableType.SINGLETON || cacheTableType == CacheTableType.MODEL) {
            vals = new String[]{String.valueOf(System.currentTimeMillis() - coldUpdateLimit), tableName};
            tempParams.put(MODEL_NAME, tableName);
        }
        Log.d(TAG, "COLD UPDATE QUERY Started ");

        Cursor coldCursor = getDataFromTableWhere(generateTableName(tableName, cacheTableType), tempParams, cacheQueryParams, (page_number + 1) * page_size);//db.rawQuery(selectQuery,vals);

        coldCursorCount = coldCursor.getCount();
        Log.d(TAG, " updateStatus cold update for " + generateTableName(tableName, cacheTableType) + " query count = " + coldCursor.getCount());

        if (coldCursor.getCount() > 0) {
            coldCursor.close();
            closeDatabase();
            return CacheRefreshStatus.COLD_UPDATE_REQUIRED;
        }
        coldCursor.close();
        if (hotCursorCount == 0 && coldCursorCount == 0) {
            Log.d(TAG, " updateStatus hotCursorCount = " + hotCursorCount + "  coldCursonCount = " + coldCursorCount + "   Table entry count for " + tableName + " = " + getDataFromTable(generateTableName(tableName, cacheTableType)).getCount());


            db = mDatabase;
            valuesArray = new String[]{};
            selectQuery = "SELECT * FROM " + generateTableName(tableName, cacheTableType);

            if (cacheTableType == CacheTableType.SINGLETON || cacheTableType == CacheTableType.MODEL) {
                valuesArray = new String[]{tableName};
                selectQuery = selectQuery + " where " + MODEL_NAME + " = ?";
            }
            Cursor finalCheckCursor = db.rawQuery(selectQuery, valuesArray);
            if (finalCheckCursor.getCount() > 0) {
                finalCheckCursor.close();
                return CacheRefreshStatus.NOT_REQUIRED;
            }
            finalCheckCursor.close();
            return CacheRefreshStatus.HOT_UPDATE_REQUIRED;
        }
        Log.d("DB_CACHE", tableName + " no update required");
        return CacheRefreshStatus.NOT_REQUIRED;

    }

    public String createWhereClause(ArrayList<String> attrs){
        String whereClause = "";
        for(int i = 0 ; i< attrs.size() ; i ++) {
            if (i == 0) {
                whereClause = attrs.get(i)+" = ? ";
            }else{
                whereClause = whereClause+" and "+attrs.get(i)+" = ? ";
            }
        }
        return whereClause;
    }

    /**
     * @apiNote Base delete api
     * */
    public synchronized void deleteRow(String tableName, String whereClause, String[] values){

        Log.i(TAG,"deleteRow   tableName: "+tableName +" whereClause:- "+ whereClause+ "values:-" + values);
        SQLiteDatabase db = mDatabase;
        db.delete(tableName,whereClause, values);
        this.closeDatabase();
    }

    public List<JSONObject> getDataListFromObjectsTable(String modelName){
        return getDataListFromObjectsTable(modelName,"","");
    }

    public List<JSONObject> getDataListFromObjectsTable(String modelName,String orderBy){
        return getDataListFromObjectsTable(modelName,"",orderBy);
    }

    public List<JSONObject> getDataListFromObjectsTable(String modelName ,String searchKey,String orderBy){
        List<JSONObject> objectsList = new ArrayList<>();
        try {

            HashMap<String, Object> hashMap = new HashMap<>();
            if(searchKey!=null && !searchKey.isEmpty()){
                hashMap.put(DATA_CACHED+TILDE,searchKey);
            }

           // Cursor cursor = getDataFromTable(generateTableName(modelName, CacheTableType.OBJECTS));
            Cursor cursor =  getDataFromTableWhere(modelName, hashMap,0,orderBy);
           /* while (cursor.moveToNext()) {
                objectsList.add(new JSONObject(cursor.getString(cursor.getColumnIndexOrThrow(DATA_CACHED))));

            }*/
            if (cursor.moveToFirst()) {
                do {
                    objectsList.add(new JSONObject(cursor.getString(cursor.getColumnIndexOrThrow(DATA_CACHED))));
                } while (cursor.moveToNext());
            }
            cursor.close();
            this.closeDatabase();


        }catch (Exception e){
                 Log.e("DB","Error during getting data from Objects table:- "+e.getMessage());
        }

        return objectsList;
    }

    public List<JSONObject> getDataListFromObjectsTable(String modelName ,HashMap<String,Object> queryParam){
        List<JSONObject> objectsList = new ArrayList<>();
        Cursor cursor =  getDataFromTableWhere(modelName, queryParam);
        while (cursor.moveToNext()) {
            String objectDetail = cursor.getString(cursor.getColumnIndexOrThrow(DATA_CACHED));
            try {
                objectsList.add(new JSONObject(objectDetail));
            } catch (JSONException e) {
                Log.e(TAG,"Error during getting data from Objects table:- "+e.getMessage());
            }
        }

        return objectsList;
    }



}
