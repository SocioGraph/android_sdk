package com.example.admin.daveai.network;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.daveUtil.DaveConstants;
import com.example.admin.daveai.daveUtil.MultiUtil;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import static com.example.admin.daveai.others.DaveAIHelper.currentOrderId;
import static com.example.admin.daveai.others.DaveAIStatic.base_url;
import static com.example.admin.daveai.others.DaveAIStatic.enterprise_id;




public class APIRoutes implements DaveConstants {

    private static final String TAG = "APIRoutes";
   // public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    public static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
    public static Locale locale = Locale.getDefault();
    private Context context;

   /* // static variable single_instance of type Singleton
    private static APIRoutes singleInstance = null;
    private APIRoutes() {}

    // static method to create instance of Singleton class
    public static APIRoutes getInstance(Context context) {
        if (singleInstance == null)
            singleInstance = new APIRoutes(context);
        return singleInstance;
    }*/

    public APIRoutes() {}
    public APIRoutes(Context context){
        this.context = context;
    }


    public static void getMetadata(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;

            DaveSharedPreference sharedPreferences = new DaveSharedPreference(context);
            String enterpriseId =  sharedPreferences.readString(DaveSharedPreference.EnterpriseId);
            if(enterpriseId!= null){
                enterprise_id = enterpriseId;
            }else {
                enterprise_id = bundle.getString("X-I2CE-ENTERPRISE-ID");
                sharedPreferences.writeString("enterprise_id",bundle.getString("X-I2CE-ENTERPRISE-ID"));
            }
            String  url = sharedPreferences.readString(DaveSharedPreference.BASE_URL);
            if(url!= null){
                base_url=url;

            }else {
                base_url = bundle.getString("BASE_URL");
                sharedPreferences.writeString("base_url",bundle.getString("BASE_URL"));

            }

            //Log.e(TAG,"getMetadata:- "+enterprise_id +" && baseURL == "+base_url);

        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e(TAG, "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        catch (Exception e) {
            Log.e(TAG, "Failed to Get URL OR ENTERPRISE From SP, NullPointer: " + e.getMessage());
        }
    }

    private HttpUrl.Builder addBaseUrlToAPI(String url) {
        return HttpUrl.parse(MultiUtil.getManifestMetadata(context,MANIFEST_BASE_URL)+url).newBuilder();
    }


    private static HttpUrl.Builder convertStringToAPI(String url) {
        return HttpUrl.parse(base_url+url).newBuilder();
    }


    public static String userLoginAPI() {
        return convertStringToAPI("/login/" + enterprise_id)
                .build().toString();
    }

    public static String modelNameAPI(String model_type) {
        return convertStringToAPI("/models/core/name")
                .addQueryParameter("model_type", model_type)
                .build().toString();
    }

    // https://test.iamdave.ai/models/core?model_type=customer
    public static String getCoreModel(String model_type) {
        return convertStringToAPI("/models/core")
                .addQueryParameter("model_type", model_type)
                .build().toString();
    }

    public static String modelAPI(String model_name) {
        return convertStringToAPI( "/model/" + model_name)
                .build().toString();
    }

    public static String postObjectAPI(String model_name) {
        return convertStringToAPI("/object/" + model_name)
                .build().toString();
    }
    public static String bulkDownloadAPI() {
        return convertStringToAPI("/bulk_download/dave")
                .build().toString();
    }
    public static String getBulkDownloadAPI(String id) {
        return convertStringToAPI("/bulk_download/dave/"+id)
                .build().toString();
    }
    public static String getBulkDownloadAPI(String id, String status) {
        return convertStringToAPI("/bulk_download/dave/"+id)
                .addQueryParameter("_status",status)
                .build().toString();
    }

    public static String getObjectAPI(String model_name, String id) {
        return convertStringToAPI("/object/" + model_name + "/" + id)
                .build().toString();
    }

    public static String updateOrGetObjectAPI(String model_name,String id) {
        return convertStringToAPI("/object/" + model_name+"/"+id)
                .build().toString();
    }

    public static String iUpdateObjectAPI(String model_name,String id) {
        return convertStringToAPI("/iupdate/" + model_name + "/" + id)
                .build().toString();
    }

    public static String objectsAPI(String model_name, int page_number) {
        return convertStringToAPI("/objects/" + model_name)
                .addQueryParameter("_page_number", String.valueOf(page_number))
                .build().toString();
    }

    public static String getObjectsAPI(String model_name, HashMap<String,Object> queryParam) {
        HttpUrl.Builder urlBuilder = convertStringToAPI("/objects/" + model_name);
        if(queryParam!=null && !queryParam.isEmpty()){
            Log.e(TAG,"Print api for get Objects:--------------"+queryParam+"== "+queryParam.size());
            for (Map.Entry entry : queryParam.entrySet()) {
                if(entry.getValue() instanceof ArrayList){
                    for(int i=0; i< ((ArrayList) entry.getValue()).size();i++){
                        urlBuilder.addQueryParameter(entry.getKey().toString(),((ArrayList) entry.getValue()).get(i).toString());
                    }
                }
                else
                    urlBuilder.addQueryParameter(entry.getKey().toString(), entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public String deleteObjectAPI(String modelName, String objectId, HashMap<String,String> queryParam) {
        HttpUrl.Builder urlBuilder = convertStringToAPI("/object/" + modelName+"/"+objectId);
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public static String objectsFilterAPI(String model_name, String searchKey) {
        return convertStringToAPI( "/objects/" + model_name)
                .addQueryParameter("_keyword", searchKey)
                .build().toString();
    }

    public static String interactionStagesAPI() {
        return convertStringToAPI("/objects/interaction_stage")
                .addQueryParameter("_sort_by", "default_positivity_score")
                .build().toString();
    }

    public static String interactionStageListAPI() {
        return convertStringToAPI("/objects/interaction_stage")
                .addQueryParameter("_sort_by", "default_positivity_score")
                .addQueryParameter("filter_attributes", "name")
                .addQueryParameter("filter_attributes", "quantity_attributes")
                .addQueryParameter("filter_attributes", "share_button_template")
                .addQueryParameter("filter_attributes", "share_button_name")
                .build().toString();
    }

    public static String interactionStageAPI(String customer_id, String stage) {
        return convertStringToAPI("/objects/interaction")
                .addQueryParameter("customer_id", customer_id)
                .addQueryParameter("stage", stage.toLowerCase(locale))
                .build().toString();
    }

    public static String interactionsAPI(Context context,String customerId, String stage) {


       // HttpUrl.Builder urlBuilder = convertStringToAPI("/interactions/" + customerModelName + "/" + customerId);
        HttpUrl.Builder urlBuilder = convertStringToAPI("/interactions/" + new ModelUtil(context).getCustomerModelName() + "/" + customerId);
        urlBuilder.addQueryParameter("_interaction_stage", stage.toLowerCase(locale));
        urlBuilder.addQueryParameter("_most_recent", Boolean.TRUE.toString());
        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
        if(daveAIPerspective.getInteraction_stage_attr_list()!=null && daveAIPerspective.getInteraction_stage_attr_list().size()>0){
            for (int j = 0; j < daveAIPerspective.getInteraction_stage_attr_list().size(); j++) {
               urlBuilder.addQueryParameter("_interaction_attributes", daveAIPerspective.getInteraction_stage_attr_list().get(j).toString());
            }
        }

        String cOrderId = currentOrderId.get(customerId);
        if(cOrderId!=null && !cOrderId.isEmpty()) {
            urlBuilder.addQueryParameter(daveAIPerspective.getInvoice_id_attr_name(),cOrderId);
           /* if (interactionFilterAttr != null && interactionFilterAttr.length() > 0) {
                Iterator<?> keys = interactionFilterAttr.keys();
                while (keys.hasNext()) {
                    String key = (String) keys.next();


                }
            }*/
        }
        return urlBuilder.build().toString();
    }


    public static String searchAPI() {
        return convertStringToAPI( "/search/dave")
                .build().toString();
    }

    public static String similarAPI(String product_id) {
        if(product_id!=null && !product_id.isEmpty()) {
            return convertStringToAPI("/similar_products/dave/" + product_id)
                    .build().toString();
        }else {
            return convertStringToAPI("/similar_products/dave")
                    .build().toString();
        }

    }

    public static String recommendationsAPI(String customer_id) {

        HttpUrl.Builder urlBuilder = convertStringToAPI("/recommendations/dave/" + customer_id);
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return  urlBuilder.build().toString();
    }

    public static String recomSimilarAPI(String customer_id, String product_id) {

        HttpUrl.Builder urlBuilder = convertStringToAPI("/recommendations/dave/" + customer_id + "/" + product_id);
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return  urlBuilder.build().toString();

    }

    public String recommendationAPI(String customer_id, String product_id) {

        HttpUrl.Builder urlBuilder;
        if (product_id != null && !product_id.isEmpty()) {
            urlBuilder =convertStringToAPI("/recommendations/dave/" + customer_id + "/" + product_id);
        } else {
            urlBuilder = convertStringToAPI("/recommendations/dave/" + customer_id);

        }
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
       // Log.d(TAG,"add defaultParams in recommendationsAPI***************:- " + defaultParams);
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return urlBuilder.build().toString();
    }

    public static String pivotAttributeAPI(String modelName,String attr_name) {
        return convertStringToAPI( "/pivot/"+ modelName+ "/" + attr_name)
                .build().toString();
    }



    public static String quantityPredictionsAPI(String productId, String customerId ,String nextStage) {
        return convertStringToAPI( "/quantity_predictions/dave/" + productId + "/" + customerId)
                 .addQueryParameter("stages",nextStage)
                .build().toString();
    }
    public static String priceRangeAPI(String actionName, String over) {
        return convertStringToAPI( "/pivot/product")
                .addQueryParameter("_action",actionName)
                .addQueryParameter("_over",over)
                .build().toString();
    }

    public static String uploadFileAPI() {
        return convertStringToAPI( "/upload_file")
                .build().toString();
    }

  /*  public static String getPerspectiveAPI(String perspective_name){
        return convertStringToAPI( "/perspective/dave/"+perspective_name)
                .build().toString();
    }*/

    public static String getPerspectiveAPI(String perspective_name){
        return convertStringToAPI( "/settings/perspective/"+perspective_name)
                .build().toString();
    }

    //https://api.iamdave.ai/perspective/dave/mobile_application
    public static String readPerspectiveAPI() {
        //return convertStringToAPI( "/perspective/dave/attributes/mobile_application")
        return convertStringToAPI( "/settings/perspective/mobile_application")
                .addQueryParameter("user_login_model_name",null)
                .addQueryParameter("user_login_id_attr_name",null)
                .addQueryParameter("user_profile_details_list",null)
                .addQueryParameter("recommendations_btn_name",null)
                //.addQueryParameter("no_of_recommendations", String.valueOf(10))
                .addQueryParameter("no_of_recommendations", null)
                .addQueryParameter("default_recommendation_params", null)
                .addQueryParameter("count_after_next_recommendations", null)
                .addQueryParameter("scan_btn_name",null)
                .addQueryParameter("scan_code_attribute_name",null)
                .addQueryParameter("customer_card_image",null)
                .addQueryParameter("customer_card_header",null)
                .addQueryParameter("customer_card_sub_header",null)
                .addQueryParameter("customer_card_right_attr",null)
                .addQueryParameter("customer_profile_title",null)
                .addQueryParameter("customer_details_view",null)
                .addQueryParameter("customer_sign_up_list",null)
                .addQueryParameter("customer_attr_hierarchy_first",null)
                .addQueryParameter("customer_variable_attr_title",null)
                .addQueryParameter("customer_variable_attr",null)
                .addQueryParameter(CUSTOMER_FILTER_LIST,null)
                .addQueryParameter(CUSTOMER_HIERARCHY_LIST,null)
                .addQueryParameter("product_card_image_view",null)
                .addQueryParameter("product_card_header",null)
                .addQueryParameter("product_card_sub_header",null)
                .addQueryParameter("product_card_right_attr",null)
                .addQueryParameter("product_expand_view_title",null)
                .addQueryParameter("product_add_edit_List",null)
                .addQueryParameter("product_expand_view",null)
                .addQueryParameter("product_category_hierarchy_title",null)
                .addQueryParameter("product_category_hierarchy",null)
                .addQueryParameter("product_similar_attributes",null)
                .addQueryParameter("product_variants_attributes",null)
                .addQueryParameter("product_price_attr",null)
                .addQueryParameter(PRODUCT_FILTER_LIST,null)
                .addQueryParameter("summation_attributes",null)
                .addQueryParameter("interaction_qty",null)
                .addQueryParameter("predicated_qty_attr",null)
                .addQueryParameter("interaction_stage_attr_list",null)
                .addQueryParameter("invoice_model_name",null)
                .addQueryParameter("invoice_id_attr_name",null)
                .addQueryParameter("invoice_checkout_attr_list",null)
                .addQueryParameter("invoice_close_attr_name",null)
                .addQueryParameter("invoice_card_header",null)
                .addQueryParameter("invoice_card_sub_header",null)
                .addQueryParameter("invoice_card_right_attr",null)
                .addQueryParameter("invoice_date_attr",null)
                .addQueryParameter("invoice_stage_attr",null)
                .addQueryParameter("invoice_show_zero_qty",null)
                .addQueryParameter("invoice_total_count_attr",null)
                .addQueryParameter("invoice_total_value_attr",null)
                .addQueryParameter(ENABLE_MANAGE_PRODUCT,null)
                .addQueryParameter(FORM_VIEW_BUTTON_NAME,null)
                .addQueryParameter(FORM_VIEW_ATTRIBUTE_LIST,null)
                .addQueryParameter("form_view_order_stage_name",null)
                .addQueryParameter(MENU_TITLE_FOR_SIGNUP,null)
                .addQueryParameter(MENU_TITLE_FOR_CUSTOMER_LIST,null)
                .addQueryParameter(MENU_TITLE_FOR_PRODUCT_LIST,null)
                .addQueryParameter("menu_title_for_order_list",null)
                .addQueryParameter(ENABLE_SET_DEFAULT_QUANTITY,null)
                .addQueryParameter(MANAGE_PRODUCT_FROM_CUSTOMER_PROFILE,null)
                .addQueryParameter(ADD_PRODUCT_TITLE,null)
                .addQueryParameter(COLD_UPDATE_TIME_LIMIT,null)
                .addQueryParameter(HOT_UPDATE_TIME_LIMIT,null)
                .build().toString();
    }




    public static String getPivotAPI(String model_name, HashMap<String,String> queryParam) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(base_url+"/pivot/" + model_name).newBuilder();
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public static String apiGetPivot(String model_name, HashMap<String,Object> queryParam) {
        HttpUrl.Builder urlBuilder = HttpUrl.parse(base_url+"/pivot/" + model_name).newBuilder();
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                if(entry.getValue() instanceof String)
                    urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
                else if(entry.getValue() instanceof ArrayList && entry.getValue()!=null ){
                    for(int i =0; i< ((ArrayList) entry.getValue()).size();i++)
                        urlBuilder.addQueryParameter(entry.getKey().toString(),((ArrayList) entry.getValue()).get(i).toString());

                }
            }
        }
        return urlBuilder.build().toString();
    }

    public static String nextInteractionAPI(String customerId,String productId) {
        return convertStringToAPI( "/next_interaction/product/" + productId + "/" + customerId)
                .build().toString();
    }

    public static String catalogueAPI(String customerId) {
        return convertStringToAPI("/catalog/dave/" + customerId)
                .build().toString();
    }

    public static String appConfigureAPI() {
         return convertStringToAPI( "/objects/app_configuration")
                .addQueryParameter("_sort_by","created_at")
                .addQueryParameter("_sort_reverse",Boolean.TRUE.toString())
                .addQueryParameter("_page_size", "1")
                .addQueryParameter("status", "app_build_complete")
                .build().toString();

    }



    public static String transferSessionAPI(String modelName, String oldObjectId, String newObjectId) {
        return convertStringToAPI( "/transfer_session/"+modelName+"/"+oldObjectId+"/"+newObjectId)
                .build().toString();

    }

    public static String conversationAPI(String customerId, String conversation_id) {
        return convertStringToAPI("/conversation/"+conversation_id+"/" + customerId)
                .build().toString();
    }

    public static String smsTemplateListAPI() {
       // return convertStringToAPI("/template-list/sms")
        return convertStringToAPI("/transaction-templates/dave")
                .build().toString();
    }


    public static String smsSendTemplateAPI() {
        return convertStringToAPI("/transaction/sms")
                .build().toString();
    }

    public static String mailSendTemplateAPI() {
        return convertStringToAPI("/transaction/mail")
                .build().toString();
    }

    public String optionsAPI(String optionUrl, HashMap<String,Object> queryParam) {
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(optionUrl);
        if(queryParam!=null && queryParam.size()>0){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public HttpUrl logoutAPI(){
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI("/logout");
        return urlBuilder.build();
    }

   /* public HttpUrl logoutAPI(String objectId){
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI("/logout/"+objectId);
        return urlBuilder.build();
    }*/


}
