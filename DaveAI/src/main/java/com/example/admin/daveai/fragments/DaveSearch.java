package com.example.admin.daveai.fragments;


import android.support.v4.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.admin.daveai.R;
import com.example.admin.daveai.network.APIResponse;
import com.example.admin.daveai.network.APIRoutes;
import com.example.admin.daveai.network.APICallAsyncTask;
import com.example.admin.daveai.network.CheckNetworkConnection;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import static com.example.admin.daveai.others.DaveAIStatic.base_url;


public class DaveSearch extends Fragment  {

    static final String TAG = "DaveSearch";
    AutoCompleteTextView autoCompleteTextView;
    ImageView search_btn;
    private ProgressDialog pDialog;
    OkHttpClient client;
    String statement_value = "";
    String search_url = "";
    String command = "";
    String model = "";
    String response = "";
    GradientDrawable gd = new GradientDrawable();
  /*  HorizontalBarChart barChart;
    ArrayList<String> bar_labels = new ArrayList<>();
    ArrayList<BarDataSet> dataSets = new ArrayList<>();
    ArrayList<BarEntry> valueSet = new ArrayList<>();*/
    ArrayList<String> sug_adapter = new ArrayList<>();
    int index_position=0;
    JSONArray ar_in = null;
    JSONArray index_pos = new JSONArray();
    JSONArray next = null;
    String previous_text="";
    String auto_text_value="";
    ArrayList<String> sugg_list = new ArrayList<>();
    ArrayAdapter<String>  adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        View view = inflater.inflate(R.layout.fragment_dave_search, container, false);

       /* View view;
        if(DaveSearchLayout!=0){
            view = inflater.inflate(DaveSearchLayout, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_dave_search, container, false);
        }*/

        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(view.getWindowToken(), 0);
        autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.auto_sugst);
        search_btn = (ImageView) view.findViewById(R.id.search_icon);
       // barChart = (HorizontalBarChart) view.findViewById(R.id.barchart);
        gd.setStroke(1, Color.BLACK);

        //  onCreateAutoSuggestionAPI();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        // getActivity().setTitle("Products");
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e("DEBUG", "onActivityCreated of DaveSearch");
        // onCreateAutoSuggestionAPI();
        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callClickOnSearchButton();
            }
        });
        autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    callClickOnSearchButton();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //barChart.setVisibility(View.GONE);
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(!s.toString().isEmpty()&& s.toString().contains("...")){
                    StringBuffer sb1 = new StringBuffer(s.toString());
                    sb1.replace(0,3,"");
                    autoCompleteTextView.setText(new StringBuilder().append(previous_text).append(sb1.toString()).toString());
                }
                statement_value =  autoCompleteTextView.getText().toString();
                getNextAutoSuggestion();

            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

    }

    @Override
    public void onPause() {
        Log.i("DEBUG", "OnPause of DaveSearch");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i("DEBUG", "onStop() of DaveSearch");
        super.onStop();

        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void onCreateAutoSuggestionAPI() {
        if (CheckNetworkConnection.networkHasConnection(getActivity())) {
            GetAutoSuggestionFromServer task = new GetAutoSuggestionFromServer(new TaskListener<String>() {
                @Override
                public void onFinished(String result) {
                    // Do Something after the task has finished
                    // autoCompleteTextView.setAdapter(adapter);
                    // autoCompleteTextView.setThreshold(1);
                }});
            task.execute();
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(getActivity());
        }

        APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Log.i(TAG, "<<<<<<<<<<<<<<<For plot graph >>>>>>>>>>>>>>>>>>>>>" + response);
                /*if(!response.isEmpty())
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject plot_data = jsonObject.getJSONObject("data");
                        valueSet.clear();
                        assert bar_labels != null;
                        bar_labels.clear();
                        Iterator<String> iter = plot_data.keys();
                        for (int i = 0; iter.hasNext(); i++) {
                            String bar_key = iter.next();
                            String bar_value = plot_data.get(bar_key).toString();
                            bar_labels.add(bar_key);
                            BarEntry v1e1 = new BarEntry(Float.valueOf(bar_value), i);
                            valueSet.add(v1e1);
                        }
                        plotBarGraph();

                    } catch (Exception e) {
                        Log.e(TAG, "Error in For plot graph" + e.getLocalizedMessage());
                    }*/
            }

            @Override
            public void onTaskFailure(int requestCode, String responseMsg) {

            }
        };
        // Create the async task and pass it the post task listener.
        new APICallAsyncTask(postTaskListener, getActivity(), "GET",true).execute(base_url + search_url);
    }

    private void callAutoSuggestionAPI() {
        if (CheckNetworkConnection.networkHasConnection(getActivity())) {

            GetAutoSuggestionFromServer task = new GetAutoSuggestionFromServer(new TaskListener<String>() {
                @Override
                public void onFinished(String result) {
                    // Do Something after the task has finished
                    // autoCompleteTextView.setAdapter(adapter);
                    // autoCompleteTextView.setThreshold(1);
                    autoCompleteTextView.showDropDown();
                }});
            task.execute();
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(getActivity());
        }
    }


    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
    }

    private void getNextAutoSuggestion(){

        if (!statement_value.equals("")) {
            try {
                for (int i = 0; i < ar_in.length(); i++) {
                    if(ar_in.get(i).equals(statement_value)){
                        index_position = i;
                        //  index_pos.put(next.get(index_position));
                        if(!index_pos.toString().contains(next.get(index_position).toString()))
                            index_pos.put(next.get(index_position));
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, " try Catch Error onTextChanged" + e.getLocalizedMessage());
            }
            boolean exist = false;
            for (int i=0;i<sug_adapter.size();i++){
                if (sug_adapter.get(i).contains(statement_value)) {
                    exist = true;
                    // Log.e(sug_adapter.get(i), "----+++++++++++++"+statement_value);
                }
            }
            if(!exist){
                Log.e(TAG, "----Search not found----"+statement_value);
                callAutoSuggestionAPI();
            }
        }else {
            statement_value = "";
            search_url = "";
            onCreateAutoSuggestionAPI();
        }
    }

 /*   private void plotBarGraph() {
        assert dataSets != null;
        dataSets.clear();

        barChart.setVisibility(View.VISIBLE);
        BarDataSet barDataSet = new BarDataSet(valueSet, "Brand 1");
        barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        dataSets.add(barDataSet);
        BarData bar_data = new BarData(bar_labels, dataSets);
        barChart.setData(bar_data);
        barChart.setDescription("Bar Chart");
        barChart.animateXY(2000, 2000);
        barChart.invalidate();
    }*/

/*    void callSearchFunctionAPI(){
       APIResponse postTaskListener = new APIResponse() {
            @Override
            public void onTaskCompleted(String response) {
                Log.i(TAG, "<<<<<<<<<<<<<<<For plot graph >>>>>>>>>>>>>>>>>>>>>" + response);
                if(!response.isEmpty())
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject plot_data = jsonObject.getJSONObject("data");
                        valueSet.clear();
                        assert bar_labels != null;
                        bar_labels.clear();
                        Iterator<String> iter = plot_data.keys();
                        for (int i = 0; iter.hasNext(); i++) {
                            String bar_key = iter.next();
                            String bar_value = plot_data.get(bar_key).toString();
                            bar_labels.add(bar_key);
                            BarEntry v1e1 = new BarEntry(Float.valueOf(bar_value), i);
                            valueSet.add(v1e1);
                        }
                        plotBarGraph();

                    } catch (Exception e) {
                        Log.e(TAG, "Error in For plot graph" + e.getLocalizedMessage());
                    }
            }

           @Override
           public void onTaskFailure(int requestCode, String responseMsg) {

           }
       };
        // Create the async task and pass it the post task listener.
        new APICallAsyncTask(postTaskListener, getActivity(), "GET",true).execute(base_url + search_url);
    }*/

    private  void callClickOnSearchButton(){
        if (CheckNetworkConnection.networkHasConnection(getActivity())) {
            GetAutoSuggestionFromServer task = new GetAutoSuggestionFromServer(new TaskListener<String>() {
                @Override
                public void onFinished(String result) {
                    if (!search_url.equals("")) {
                        if (command.equals("objects")) {
                            HashMap<String, String> intent_details = new HashMap<>();
                            intent_details.put("model_name", model);
                            intent_details.put("search_url", search_url);

                         /*   Intent intent = new Intent(getActivity(), CustomerDashboard.class);
                            intent.putExtra("search_message", intent_details);
                            startActivity(intent);*/
                        } else if (command.equals("unique")) {
                            hideSoftKeyboard();
                            if (CheckNetworkConnection.networkHasConnection(getActivity())) {
                                //callSearchFunctionAPI();
                            } else {
                                CheckNetworkConnection.showNetDisabledAlertToUser(getActivity());
                            }
                        }
                    }else {
                        Toast.makeText(getActivity(), "No Result Found", Toast.LENGTH_SHORT).show();
                    }
                }});
            task.execute();
        } else {
            CheckNetworkConnection.showNetDisabledAlertToUser(getActivity());
        }


    }

    private class GetAutoSuggestionFromServer extends AsyncTask<Void,Void,String> {

        private TaskListener<String> taskListener;//Call back interface
        GetAutoSuggestionFromServer(TaskListener<String> listener) {
            this.taskListener = listener;//Assigning call back interface through constructor
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... arg0) {
            try {
                JSONObject post_body = new JSONObject();
                post_body.put("statement",statement_value);
                // post_body.put("disable_previous",true);
                post_body.put("__index__", index_pos);
                Log.e(TAG, "post_body:-----" +post_body.toString());
              //  HttpUrl.Builder urlBuilder = HttpUrl.parse(APIRoutes.dave_auto_sugg_URL).newBuilder();
                client = new OkHttpClient();
              // response = ApiCall.POST(client, urlBuilder.build().toString(),post_body.toString());
              //  response = POST(client, urlBuilder.build().toString(),post_body.toString());
                response = POST(client, APIRoutes.searchAPI(),post_body.toString());
                Log.e(TAG, "GetAutoSuggestionFromServer Response" + response);

            } catch (Exception e) {
                Log.e(TAG, " Error in doInBackground" + e.getLocalizedMessage());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                previous_text=statement_value;
                Log.e(TAG, " +++++++++++++++previous_text:++++++++++++++" +previous_text);
                sug_adapter.clear();
                ar_in= null;
                sugg_list.clear();
                JSONObject jsonObject = new JSONObject(result);
                next = jsonObject.getJSONArray("__next__");
                ar_in = jsonObject.getJSONArray("values");
                for (int i = 0; i < ar_in.length(); i++) {
                    sug_adapter.add(ar_in.getString(i));
                    if(!statement_value.isEmpty()){
                        StringBuffer sb = new StringBuffer(ar_in.getString(i));
                        sb.replace(0,statement_value.length(),"...");
                        sugg_list.add(sb.toString());
                    }else {
                        sugg_list.add(ar_in.getString(i));
                    }
                }
                Log.e(TAG, " sug_adapter:-----" +sug_adapter);
                Log.e(TAG, " ++++sugg_list:--------+++++++" +sugg_list);
                search_url=jsonObject.getString("__url__");
                JSONObject query = jsonObject.getJSONObject("query");
                JSONArray index = query.getJSONArray("__index__");
                if(index.length()>0) {
                    command = index.getJSONObject(0).getString("key");
                    Log.i(TAG, "___________command________:---" + command);
                }
                if(index.length()>1) {
                    model = index.getJSONObject(1).getString("key");
                    Log.i(TAG, "___________model________:---" + model);
                }
            } catch (Exception e) {
                Log.e(TAG, " Error:-----" + e.getMessage());
            }
            //  if (!sug_adapter.isEmpty()&& sug_adapter.size()>0) {
            // ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),R.layout.dropdowm_layout, R.id.autoCompleteItem, sug_adapter);
            if (!sugg_list.isEmpty()&& sugg_list.size()>0) {
                System.out.println("<<<<<<<<<<<<getActivity()>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+getActivity());
                adapter = new ArrayAdapter<>(getActivity(),R.layout.dropdowm_layout, R.id.autoCompleteItem, sugg_list);
                adapter.notifyDataSetChanged();
                // adapter.setNotifyOnChange(true);
                autoCompleteTextView.setAdapter(adapter);
                autoCompleteTextView.setThreshold(1);
            }
            // In onPostExecute we check if the listener is valid
            if(this.taskListener != null) {
                // And if it is we call the callback function on it.
                this.taskListener.onFinished(search_url);
                // this.taskListener.onFinishedTask(sug_adapter.toString());
            }
        }
    }
    public interface TaskListener<K> {
        // void onFinished(String result);
        void onFinished(K result);
        // void onFinishedTask(K result);
    }

    //POST network request
    public static String POST(OkHttpClient client, String url, String body) throws IOException {

        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body_part = RequestBody.create(JSON, body);
        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .addHeader("X-I2CE-USER-ID", "rinki+beautywares@i2ce.in")
                .addHeader("X-I2CE-ENTERPRISE-ID", "beautywares")
                .addHeader("X-I2CE-APIRoutes-KEY", "cmlua2kgYmVhdXR5d2FyZXNAaTJjZSBpbjE0ODk1NjE5MjggMTM=")
                .post(body_part)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

   /* private void auto_suggestion_APICall(){
        JSONObject post_body = new JSONObject();
        HttpUrl.Builder urlBuilder = HttpUrl.parse(APIRoutes.dave_auto_sugg_URL).newBuilder();
        try {
            post_body.put("statement",statement_value);
            post_body.put("__index__", index_pos);
            Log.e(TAG, "post_body:-----" +post_body.toString());

        } catch (Exception e) {
            Log.e(TAG, " Error in doInBackground" + e.getLocalizedMessage());
        }
        APICallAsyncTask.OnTaskCompleted postTaskListener = new APICallAsyncTask.OnTaskCompleted() {
            @Override
            public void onTaskCompleted(String response) {
                Log.i(TAG, "<<<<<<<<<<<<<<<For plot graph >>>>>>>>>>>>>>>>>>>>>" + response);
                try {
                    previous_text=statement_value;
                    Log.e(TAG, " +++++++++++++++previous_text:++++++++++++++" +previous_text);
                    sug_adapter.clear();
                    ar_in= null;
                    sugg_list.clear();
                    JSONObject jsonObject = new JSONObject(response);
                    next = jsonObject.getJSONArray("__next__");
                    ar_in = jsonObject.getJSONArray("values");
                    for (int i = 0; i < ar_in.length(); i++) {
                        sug_adapter.add(ar_in.getString(i));
                        if(!statement_value.isEmpty()){
                            StringBuffer sb = new StringBuffer(ar_in.getString(i));
                            sb.replace(0,statement_value.length(),"...");
                            sugg_list.add(sb.toString());
                        }else {
                            sugg_list.add(ar_in.getString(i));
                        }
                    }
                    Log.e(TAG, " sug_adapter:-----" +sug_adapter);
                    Log.e(TAG, " ++++sugg_list:--------+++++++" +sugg_list);
                    search_url=jsonObject.getString("__url__");
                    JSONObject query = jsonObject.getJSONObject("query");
                    JSONArray index = query.getJSONArray("__index__");
                    if(index.length()>0) {
                        command = index.getJSONObject(0).getString("key");
                        Log.i(TAG, "___________command________:---" + command);
                    }
                    if(index.length()>1) {
                        model = index.getJSONObject(1).getString("key");
                        Log.i(TAG, "___________model________:---" + model);
                    }
                } catch (Exception e) {
                    Log.e(TAG, " Error:-----" + e.getMessage());
                }
                if (!sugg_list.isEmpty()&& sugg_list.size()>0) {
                    adapter = new ArrayAdapter<>(getActivity(),R.layout.dropdowm_layout, R.id.autoCompleteItem, sugg_list);
                    adapter.notifyDataSetChanged();
                    autoCompleteTextView.setAdapter(adapter);
                    autoCompleteTextView.setThreshold(1);
                }
            }
        };
        // Create the async task and pass it the post task listener.
        new APICallAsyncTask(postTaskListener, getActivity(), "POST",post_body.toString()).execute(urlBuilder.build().toString());
    }
*/

}






