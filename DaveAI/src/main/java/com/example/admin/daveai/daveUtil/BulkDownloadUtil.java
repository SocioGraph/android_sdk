package com.example.admin.daveai.daveUtil;


import java.util.ArrayList;
import java.util.HashMap;


public class BulkDownloadUtil {


    private long bulkDownloadTime = 10;
    private HashMap<String,Object> coreModelObjectsMap;
    private HashMap<String,Object> modelObjectsMap;

    private  HashMap<String, ArrayList<String>> modelHierarchyMap;
    private  HashMap<String, ArrayList<String>> modelFiltersMap;
    private boolean isConvertIntoInteractions = false;


    public long getBulkDownloadTime() {
        return bulkDownloadTime;
    }

    public void setBulkDownloadTime(long bulkDownloadTime) {
        this.bulkDownloadTime = bulkDownloadTime;
    }

    public HashMap<String, Object> getCoreModelObjectsMap() {
        return coreModelObjectsMap;
    }

    public void setCoreModelObjectsMap(HashMap<String, Object> coreModelObjectsMap) {
        this.coreModelObjectsMap = coreModelObjectsMap;
    }

    public HashMap<String, Object> getModelObjectsMap() {
        return modelObjectsMap;
    }

    public void setModelObjectsMap(HashMap<String, Object> modelObjectsMap) {
        this.modelObjectsMap = modelObjectsMap;
    }

    public HashMap<String, ArrayList<String>> getModelHierarchyMap() {
        return modelHierarchyMap;
    }

    public void setModelHierarchyMap(HashMap<String, ArrayList<String>> modelHierarchyMap) {
        this.modelHierarchyMap = modelHierarchyMap;
    }

    public HashMap<String, ArrayList<String>> getModelFiltersMap() {
        return modelFiltersMap;
    }

    public void setModelFiltersMap(HashMap<String, ArrayList<String>> modelFiltersMap) {
        this.modelFiltersMap = modelFiltersMap;
    }

    public boolean isConvertIntoInteractions() {
        return isConvertIntoInteractions;
    }

    public void setConvertIntoInteractions(boolean convertIntoInteractions) {
        isConvertIntoInteractions = convertIntoInteractions;
    }


}
