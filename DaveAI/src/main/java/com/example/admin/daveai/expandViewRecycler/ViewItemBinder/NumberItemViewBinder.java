package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.admin.daveai.R;
import com.example.admin.daveai.expandViewRecycler.ViewItem.NumberItem;

import me.drakeet.multitype.ItemViewBinder;

public class NumberItemViewBinder extends ItemViewBinder<NumberItem, NumberItemViewBinder.NumberHolder> {


    @NonNull
    @Override
    protected NumberHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return new NumberHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_number, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull NumberHolder holder, @NonNull NumberItem item) {
        holder.numberTextKey.setText(String.valueOf(item.getNumberKey()));
        holder.numberTextValue.setText(String.valueOf(item.getNumberValue()));
    }

    public class NumberHolder extends RecyclerView.ViewHolder {
        private TextView numberTextKey, numberTextValue;

        public NumberHolder(View itemView) {
            super(itemView);
            numberTextKey = itemView.findViewById(R.id.key_title);
            numberTextValue = itemView.findViewById(R.id.key_value);
        }
    }
}
