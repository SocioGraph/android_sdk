package com.example.admin.daveai.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.admin.daveai.R;
import com.example.admin.daveai.adapter.VisualizationAdapter;
import com.example.admin.daveai.others.MyScaleGestures;
import com.example.admin.daveai.others.OnPinchListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;


public class Visualization extends Activity {


    private final String TAG = getClass().getSimpleName();
    public static final String VISUALIZATION_VIEW_TYPE = "visualization_view_type";
    public static final String GRID_POSITION = "grid_position";
    public static final String GRID_LIST = "grid_list";
    public static final String IMAGE_URL = "image_url";


    private ViewPager visualizationViewPager;
    private RelativeLayout imageViewLayout;
    private ProgressBar progressBar;
    private ImageView imageView;

    private String strViewType = "";
    private String imageURL= "";
    JSONArray jsonGridList;
    int gridPosition ;

    // Used to detect pinch zoom gesture.
    private ScaleGestureDetector scaleGestureDetector = null;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public enum VisualisationType {

        IMAGE("image"),
        SLIDE_SHOW("slide_show"),
        THUMBNAILS("thumbnails")
        ;
        private final String type;

        /**
         * @param text
         */
        VisualisationType(final String text) {
            this.type = text;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return type;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualization);

        ImageView cross = findViewById(R.id.cross);
        visualizationViewPager=findViewById(R.id.visualizationViewPager);
        imageViewLayout=findViewById(R.id.imageViewLayout);
        imageView = findViewById(R.id.imageView);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        try {
            Bundle bundle = getIntent().getExtras();
            if(bundle!=null) {
                strViewType  = bundle.getString(VISUALIZATION_VIEW_TYPE);
                if(bundle.containsKey(GRID_POSITION)){
                    gridPosition = bundle.getInt(GRID_POSITION);
                }

                if(bundle.containsKey(GRID_LIST)){
                    String jsonArray = bundle.getString(GRID_LIST);
                    jsonGridList = new JSONArray(jsonArray);
                }

                if(bundle.containsKey(IMAGE_URL)){
                    imageURL = bundle.getString(IMAGE_URL);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });

        Log.e(TAG,"Print "+strViewType +"  "+ imageURL);

        if(strViewType!=null && !strViewType.isEmpty()){

            if(strViewType.equals(VisualisationType.IMAGE.toString())){
                visualizationViewPager.setVisibility(View.GONE);
                imageViewLayout.setVisibility(View.VISIBLE);
                if(imageURL!=null && !imageURL.isEmpty()) {
                    //initControls();
//                    imageView.setOnTouchListener(new MyScaleGestures(Visualization.this));
                    showViewTypeImage();
                }

            }else {
                visualizationViewPager.setVisibility(View.VISIBLE);
                imageViewLayout.setVisibility(View.GONE);

                VisualizationAdapter viewPagerAdapter = new VisualizationAdapter(Visualization.this, jsonGridList);
                visualizationViewPager.setAdapter(viewPagerAdapter);
                visualizationViewPager.setCurrentItem(gridPosition);

            }

        }

    }

    private void showViewTypeImage(){
        try {
            progressBar.setVisibility(View.VISIBLE);
            Log.i(TAG,"image loading from =***************  "+imageURL);
            if(imageURL.startsWith("http")) {

                Picasso.with(this)
                        .load(imageURL)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        //.fit()
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                progressBar.setVisibility(View.GONE);
                                Log.i(TAG, "PICASSO SUccess!!!");
                            }

                            @Override
                            public void onError() {
                                progressBar.setVisibility(View.GONE);
                                Log.e(TAG, "PICASSO ERROR!!!");
                            }
                        });
            }else{
                Glide.with(this)
                        .load(imageURL)
                        .asBitmap()
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.no_image)
                        .listener(new RequestListener<String, Bitmap>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                Log.e(TAG,"Glide error issue "+e+" target:----------"+ target+" isFirstResource:-----"+isFirstResource);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                progressBar.setVisibility(View.GONE);
                                Log.i(TAG,"Glide Success onResourceReady target:----------"+ resource+" isFirstResource:-----"+isFirstResource);
                                return false;
                            }
                        })
                        // .skipMemoryCache(false)
                        // .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(imageView);

               // imageView.setOnTouchListener(new MyScaleGestures(this));
            }



        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"Error showViewTypeImage() :- "+e.getLocalizedMessage());
        }

    }
/*

    private void initControls() {
        if(scaleGestureDetector == null)
        {
            // Create an instance of OnPinchListner custom class.
            OnPinchListener onPinchListener = new OnPinchListener(getApplicationContext(), imageView);

            // Create the new scale gesture detector object use above pinch zoom gesture listener.
            scaleGestureDetector = new ScaleGestureDetector(getApplicationContext(), onPinchListener);
        }
    }*/


    /*
    When the activity on touch event occurred, invoke scaleGestureDetector‘s onTouchEvent method
    to make it detect scale change gesture.
    */
    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        // Dispatch activity on touch event to the scale gesture detector.
        scaleGestureDetector.onTouchEvent(event);
        return true;
    }*/
}