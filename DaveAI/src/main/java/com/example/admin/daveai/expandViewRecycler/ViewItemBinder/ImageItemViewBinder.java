/*
 * Copyright 2016 drakeet. https://github.com/drakeet
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.admin.daveai.expandViewRecycler.ViewItemBinder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.admin.daveai.R;
import com.example.admin.daveai.activity.Visualization;
import com.example.admin.daveai.expandViewRecycler.ViewItem.ImageItem;
import com.example.admin.daveai.daveUtil.MultiUtil;

import me.drakeet.multitype.ItemViewBinder;

public class ImageItemViewBinder extends ItemViewBinder<ImageItem, ImageItemViewBinder.ImageHolder> {
    private Context context;

    public ImageItemViewBinder(Context context) {
        this.context = context;
    }

    @Override
    protected @NonNull
    ImageHolder onCreateViewHolder(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        View root = inflater.inflate(R.layout.item_image, parent, false);
        return new ImageHolder(root);
    }

    @Override
    protected void onBindViewHolder(@NonNull ImageHolder holder, @NonNull ImageItem imageContent) {
        if (imageContent.imgUrl != null) {
            //Log.e("imgBInd", "onBindViewHolder: " + imageContent.imgUrl);

            Glide.with(context)
                    .load(imageContent.imgUrl)
                    .asBitmap()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.no_image)
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.image);


        }

        holder.shareImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiUtil crudUtil = new MultiUtil(context);
                Uri bitmap = crudUtil.getLocalBitmapUri(context,holder.image);
                Log.e("ImageExpand","print bitmap in image :-------"+bitmap);

                if(bitmap!=null)
                    crudUtil.shareImage(context,bitmap,imageContent.shareText);
            }
        });

        holder.zoomImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle b=new Bundle();
                b.putString(Visualization.VISUALIZATION_VIEW_TYPE,Visualization.VisualisationType.IMAGE.toString());
                b.putString(Visualization.IMAGE_URL,imageContent.imgUrl);

                Intent intent = new Intent(context, Visualization.class);
                intent.putExtras(b);
                context.startActivity(intent);





            }
        });
    }

    class ImageHolder extends RecyclerView.ViewHolder {

        private @NonNull
        ImageView image, shareImage,zoomImage;


        ImageHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            shareImage = itemView.findViewById(R.id.shareImage);
            zoomImage = itemView.findViewById(R.id.zoomImage);
        }
    }
}
