package com.example.admin.daveai.network;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import com.example.admin.daveai.daveUtil.ModelUtil;
import com.example.admin.daveai.others.DaveAIPerspective;
import com.example.admin.daveai.others.DaveSharedPreference;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.MediaType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;




public interface DaveRoutes {


   // public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json");
    public static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    public static void getMetadata(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(
                    context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;

            DaveSharedPreference sharedPreferences = new DaveSharedPreference(context);
            String enterpriseId =  sharedPreferences.readString(DaveSharedPreference.EnterpriseId);
            if(enterpriseId == null){
                sharedPreferences.writeString(DaveSharedPreference.EnterpriseId,bundle.getString("X-I2CE-ENTERPRISE-ID"));
            }

            String  url = sharedPreferences.readString(DaveSharedPreference.BASE_URL);
            if(url == null){
                sharedPreferences.writeString(DaveSharedPreference.BASE_URL,bundle.getString("BASE_URL"));

            }

            //Log.e(TAG,"getMetadata:- "+enterprise_id +" && baseURL == "+base_url);

        } catch (PackageManager.NameNotFoundException e) {
            Log.e("DaveRoutes", "Failed to load meta-data, NameNotFound: " + e.getMessage());
        } catch (NullPointerException e) {
            Log.e("DaveRoutes", "Failed to load meta-data, NullPointer: " + e.getMessage());
        }
        catch (Exception e) {
            Log.e("DaveRoutes", "Failed to Get URL OR ENTERPRISE From SP, NullPointer: " + e.getMessage());
        }
    }

    static String getBaseURl(Context context){
        return new DaveSharedPreference(context).readString(DaveSharedPreference.BASE_URL);
    }

    static HttpUrl.Builder addBaseUrlToAPI(Context context ,String url) {
        return HttpUrl.parse(getBaseURl(context)+url).newBuilder();
    }

    static String userLoginAPI(Context context) {
        return addBaseUrlToAPI(context,"/login/" + new DaveSharedPreference(context).readString(DaveSharedPreference.EnterpriseId))
                .build().toString();
    }

    static String modelNameAPI(Context context,String model_type) {
        return addBaseUrlToAPI(context,"/models/core/name")
                .addQueryParameter("model_type", model_type)
                .build().toString();
    }

    // https://test.iamdave.ai/models/core?model_type=customer
    static String getCoreModel(Context context,String model_type) {
        return addBaseUrlToAPI(context,"/models/core")
                .addQueryParameter("model_type", model_type)
                .build().toString();
    }

    public static String modelAPI(Context context,String model_name) {
        return addBaseUrlToAPI(context, "/model/" + model_name)
                .build().toString();
    }

    public static String postObjectAPI(Context context,String model_name) {
        return addBaseUrlToAPI(context,"/object/" + model_name)
                .build().toString();
    }
    public static String bulkDownloadAPI(Context context) {
        return addBaseUrlToAPI(context,"/bulk_download/dave")
                .build().toString();
    }
    public static String getBulkDownloadAPI(Context context,String id) {
        return addBaseUrlToAPI(context,"/bulk_download/dave/"+id)
                .build().toString();
    }
    public static String getBulkDownloadAPI(Context context,String id, String status) {
        return addBaseUrlToAPI(context,"/bulk_download/dave/"+id)
                .addQueryParameter("_status",status)
                .build().toString();
    }

    public static String getObjectAPI(Context context,String model_name, String id) {
        return addBaseUrlToAPI(context,"/object/" + model_name + "/" + id)
                .build().toString();
    }

    public static String updateOrGetObjectAPI(Context context,String model_name,String id) {
        return addBaseUrlToAPI(context,"/object/" + model_name+"/"+id)
                .build().toString();
    }

    public static String iUpdateObjectAPI(Context context,String model_name,String id) {
        return addBaseUrlToAPI(context,"/iupdate/" + model_name + "/" + id)
                .build().toString();
    }

    public static String objectsAPI(Context context,String model_name, int page_number) {
        return addBaseUrlToAPI(context,"/objects/" + model_name)
                .addQueryParameter("_page_number", String.valueOf(page_number))
                .build().toString();
    }

    public static String getObjectsAPI(Context context,String model_name, HashMap<String,Object> queryParam) {
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/objects/" + model_name);
        if(queryParam!=null && !queryParam.isEmpty()){
            for (Map.Entry entry : queryParam.entrySet()) {
                if(entry.getValue() instanceof ArrayList){
                    for(int i=0; i< ((ArrayList) entry.getValue()).size();i++){
                        urlBuilder.addQueryParameter(entry.getKey().toString(),((ArrayList) entry.getValue()).get(i).toString());
                    }
                }
                else
                    urlBuilder.addQueryParameter(entry.getKey().toString(), entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    static String deleteObjectAPI(Context context, String modelName, String objectId, HashMap<String, String> queryParam) {
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/object/" + modelName+"/"+objectId);
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public static String objectsFilterAPI(Context context,String model_name, String searchKey) {
        return addBaseUrlToAPI( context,"/objects/" + model_name)
                .addQueryParameter("_keyword", searchKey)
                .build().toString();
    }

    public static String interactionStagesAPI(Context context) {
        return addBaseUrlToAPI(context,"/objects/interaction_stage")
                .addQueryParameter("_sort_by", "default_positivity_score")
                .build().toString();
    }

    public static String interactionStageListAPI(Context context) {
        return addBaseUrlToAPI(context,"/objects/interaction_stage")
                .addQueryParameter("_sort_by", "default_positivity_score")
                .addQueryParameter("filter_attributes", "name")
                .addQueryParameter("filter_attributes", "quantity_attributes")
                .addQueryParameter("filter_attributes", "share_button_template")
                .addQueryParameter("filter_attributes", "share_button_name")
                .build().toString();
    }

    public static String interactionStageAPI(Context context,String customer_id, String stage) {
        return addBaseUrlToAPI(context,"/objects/interaction")
                .addQueryParameter("customer_id", customer_id)
                .addQueryParameter("stage", stage.toLowerCase(Locale.getDefault()))
                .build().toString();
    }

    public static String interactionsAPI(Context context,String customerId, String stage) {

        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/interactions/" + new ModelUtil(context).getCustomerModelName() + "/" + customerId);
        urlBuilder.addQueryParameter("_interaction_stage", stage.toLowerCase(Locale.getDefault()));
        urlBuilder.addQueryParameter("_most_recent", Boolean.TRUE.toString());
        DaveAIPerspective daveAIPerspective = DaveAIPerspective.getInstance();
        if(daveAIPerspective.getInteraction_stage_attr_list()!=null && daveAIPerspective.getInteraction_stage_attr_list().size()>0){
            for (int j = 0; j < daveAIPerspective.getInteraction_stage_attr_list().size(); j++) {
               urlBuilder.addQueryParameter("_interaction_attributes", daveAIPerspective.getInteraction_stage_attr_list().get(j).toString());
            }
        }

       /* String cOrderId = currentOrderId.get(customerId);
        if(cOrderId!=null && !cOrderId.isEmpty()) {
            urlBuilder.addQueryParameter(daveAIPerspective.getInvoice_id_attr_name(),cOrderId);

        }*/
        return urlBuilder.build().toString();
    }


    public static String searchAPI(Context context) {
        return addBaseUrlToAPI(context, "/search/dave")
                .build().toString();
    }

    public static String similarAPI(Context context,String product_id) {
        if(product_id!=null && !product_id.isEmpty()) {
            return addBaseUrlToAPI(context,"/similar_products/dave/" + product_id)
                    .build().toString();
        }else {
            return addBaseUrlToAPI(context,"/similar_products/dave")
                    .build().toString();
        }

    }

    public static String recommendationsAPI(Context context,String customer_id) {

        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/recommendations/dave/" + customer_id);
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return  urlBuilder.build().toString();
    }

     static String recomSimilarAPI(Context context,String customer_id, String product_id) {

        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/recommendations/dave/" + customer_id + "/" + product_id);
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return  urlBuilder.build().toString();

    }

    static String recommendationAPI(Context context,String customer_id, String product_id) {

        HttpUrl.Builder urlBuilder;
        if (product_id != null && !product_id.isEmpty()) {
            urlBuilder =addBaseUrlToAPI(context,"/recommendations/dave/" + customer_id + "/" + product_id);
        } else {
            urlBuilder = addBaseUrlToAPI(context,"/recommendations/dave/" + customer_id);

        }
        HashMap<String, Object> defaultParams = DaveAIPerspective.getInstance().getDefault_recommendation_params();
       // Log.d(TAG,"add defaultParams in recommendationsAPI***************:- " + defaultParams);
        if(defaultParams!=null && !defaultParams.isEmpty()){
            for (Map.Entry<String, Object> entry : defaultParams.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return urlBuilder.build().toString();
    }

    public static String pivotAttributeAPI(Context context,String modelName,String attr_name) {
        return addBaseUrlToAPI(context, "/pivot/"+ modelName+ "/" + attr_name)
                .build().toString();
    }



    public static String quantityPredictionsAPI(Context context,String productId, String customerId ,String nextStage) {
        return addBaseUrlToAPI(context, "/quantity_predictions/dave/" + productId + "/" + customerId)
                 .addQueryParameter("stages",nextStage)
                .build().toString();
    }
    public static String priceRangeAPI(Context context,String actionName, String over) {
        return addBaseUrlToAPI(context, "/pivot/product")
                .addQueryParameter("_action",actionName)
                .addQueryParameter("_over",over)
                .build().toString();
    }

    public static String uploadFileAPI(Context context) {
        return addBaseUrlToAPI(context, "/upload_file")
                .build().toString();
    }

  /*  public static String getPerspectiveAPI(String perspective_name){
        return convertStringToAPI( "/perspective/dave/"+perspective_name)
                .build().toString();
    }*/

    public static String getPerspectiveAPI(Context context,String perspective_name){
        return addBaseUrlToAPI( context,"/settings/perspective/"+perspective_name)
                .build().toString();
    }


    public static String getPivotAPI(Context context,String model_name, HashMap<String,String> queryParam) {
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/pivot/" + model_name);
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    public static String apiGetPivot(Context context,String model_name, HashMap<String,Object> queryParam) {

        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/pivot/" + model_name);
        if(queryParam!=null){
            for (Map.Entry entry : queryParam.entrySet()) {
                if(entry.getValue() instanceof String)
                    urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
                else if(entry.getValue() instanceof ArrayList && entry.getValue()!=null ){
                    for(int i =0; i< ((ArrayList) entry.getValue()).size();i++)
                        urlBuilder.addQueryParameter(entry.getKey().toString(),((ArrayList) entry.getValue()).get(i).toString());

                }
            }
        }
        return urlBuilder.build().toString();
    }

    public static String nextInteractionAPI(Context context,String customerId,String productId) {
        return addBaseUrlToAPI(context, "/next_interaction/product/" + productId + "/" + customerId)
                .build().toString();
    }

    public static String catalogueAPI(Context context,String customerId) {
        return addBaseUrlToAPI(context,"/catalog/dave/" + customerId)
                .build().toString();
    }

    public static String appConfigureAPI(Context context) {
         return addBaseUrlToAPI(context, "/objects/app_configuration")
                .addQueryParameter("_sort_by","created_at")
                .addQueryParameter("_sort_reverse",Boolean.TRUE.toString())
                .addQueryParameter("_page_size", "1")
                .addQueryParameter("status", "app_build_complete")
                .build().toString();

    }



    public static String transferSessionAPI(Context context,String modelName, String oldObjectId, String newObjectId) {
        return addBaseUrlToAPI(context, "/transfer_session/"+modelName+"/"+oldObjectId+"/"+newObjectId)
                .build().toString();

    }

    public static String conversationAPI(Context context,String customerId) {
        return addBaseUrlToAPI(context,"/conversation/daveai_spar/" + customerId)
                .build().toString();
    }

    public static String smsTemplateListAPI(Context context) {
       // return convertStringToAPI("/template-list/sms")
        return addBaseUrlToAPI(context,"/transaction-templates/dave")
                .build().toString();
    }


    public static String smsSendTemplateAPI(Context context) {
        return addBaseUrlToAPI(context,"/transaction/sms")
                .build().toString();
    }

    public static String mailSendTemplateAPI(Context context) {
        return addBaseUrlToAPI(context,"/transaction/mail")
                .build().toString();
    }

    static String optionsAPI(Context context,String optionUrl, HashMap<String,Object> queryParam) {

        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,optionUrl);
        if(queryParam!=null && queryParam.size()>0){
            for (Map.Entry entry : queryParam.entrySet()) {
                urlBuilder.addQueryParameter(entry.getKey().toString(),entry.getValue().toString());
            }
        }
        return urlBuilder.build().toString();
    }

    static HttpUrl logoutAPI(Context context){
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI(context,"/logout");
        return urlBuilder.build();
    }

   /* public HttpUrl logoutAPI(String objectId){
        HttpUrl.Builder urlBuilder = addBaseUrlToAPI("/logout/"+objectId);
        return urlBuilder.build();
    }*/


}
