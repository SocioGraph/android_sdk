package com.example.admin.daveai.others;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.example.admin.daveai.broadcasting.PushNotifications;
import com.example.admin.daveai.broadcasting.S3UploadService;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;

import org.json.JSONObject;

import java.util.ArrayList;

public abstract class DaveApplication extends Application implements PushNotifications.PushNotificationsListener {


    private final String TAG = getClass().getSimpleName();

    public abstract void onCreateApplication();
    public abstract void onReceivedNotification(OSNotification notification, JSONObject jsonObject);
    public abstract void onOpenedNotification(OSNotificationOpenResult osNotificationOpenResult, JSONObject jsonObject);

    private static final Handler mHandler = new Handler();
    @Override
    public void onCreate() {
        super.onCreate();
        PushNotifications pushNotifications  = new PushNotifications(this,this);
        pushNotifications.startPushNotification();
        onCreateApplication();
    }

    @Override
    public void onNotificationReceived(OSNotification notification, JSONObject jsonObject) {
        onReceivedNotification(notification,jsonObject);

    }

    @Override
    public void onNotificationOpened(OSNotificationOpenResult osNotificationOpenResult, JSONObject jsonObject) {
        onOpenedNotification(osNotificationOpenResult,jsonObject);
    }

    /**
     * @param localDirName sdCard Folders ex: "/dave/logcat" will save logs in '[sdcard]/dave/logcat'
     * @param timePerFile interval for log posting.conver
     * @param s3Path folder path in s3 '[Folder]/[SubFolder]/[SubSubFolder]'
     * @param s3Bucket s3 bucket name ex: general-iamdave
     * @param s3Bucket s3 bucket name ex: general-iamdave
     * @param avoidStringList log lines with these phrases will not be saved
     * */
    public void uploadADBLogs(String localDirName, long timePerLogFile, String s3Path, String s3Bucket, ArrayList<String> avoidStringList){

        new DaveLogger().writeLogCat(this.getApplicationContext(),localDirName,timePerLogFile,s3Path,s3Bucket,avoidStringList);
        try {
            Intent msgIntent = new Intent(this, S3UploadService.class);
            this.startService(msgIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static final void runOnUiThread(Runnable runnable) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            runnable.run();
        } else {
            mHandler.post(runnable);
        }
    }

}
