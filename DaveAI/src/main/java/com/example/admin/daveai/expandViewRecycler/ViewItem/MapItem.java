package com.example.admin.daveai.expandViewRecycler.ViewItem;


public class MapItem {
    private double latitude;
    private double longitude;

    public MapItem(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
