package com.example.admin.daveai.others;

import android.content.Context;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;



public class DaveAIHelper {

    public static BeforePostInteractionListener listener = null;
    public static HashMap<String ,String> preOrderId = new HashMap<>();
    public static HashMap<String ,String> currentOrderId = new HashMap<>();
    private HashMap<String ,String> queryParamOfUrl;
    public static JSONObject interactionFilterAttr= new JSONObject();
    public static JSONObject interactionAttr = new JSONObject();


    public interface PostInteractionListener {
        public void postInteraction();
    }

    public interface BeforePostInteractionListener {
        public void beforePostInteraction(Context context, String customerId, PostInteractionListener interactionPoster);
    }

    public static void setBeforePostInteractionListener(BeforePostInteractionListener beforePostInteractionListener) {
        Log.e("SET DAVEHELPER"," beforePostInteractionListener :--------"+  beforePostInteractionListener);
        listener = beforePostInteractionListener;
    }


    public HashMap<String, String> getPreOrderId() {
        return preOrderId;
    }

    public void setPreOrderId(String keyName, String value) {
        preOrderId.put(keyName,value);
    }

    public HashMap<String, String> getCurrentOrderId() {
        return currentOrderId;
    }

    public void setCurrentOrderId(String keyName, String value) {
        System.out.println("******setCurrentOrderId**************  "+keyName+"  Value:- "+value);
        currentOrderId.put(keyName,value);
    }

    public HashMap<String, String> getQueryParamOfUrl() {
        return queryParamOfUrl;
    }

    public void setQueryParamOfUrl(String keyName, String value) {
        this.queryParamOfUrl.put(keyName,value);
    }

    public JSONObject getInteractionFilterAttr() {
        return interactionFilterAttr;
    }

    public void setInteractionFilterAttr(JSONObject interactionFilterAttr) {
        DaveAIHelper.interactionFilterAttr = interactionFilterAttr;
    }

    public void addInteractionFilterAttr(String keyName, Object value) {
        try {
            interactionFilterAttr.put(keyName,value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("******addInteractionFilterAttr**************  "+interactionFilterAttr);
    }

    public JSONObject getInteractionAttr() {
        return interactionAttr;
    }

    public void setInteractionAttr(JSONObject interactionAttr) {
        DaveAIHelper.interactionAttr = interactionAttr;
    }

    public void addInteractionAttr(String keyName, Object value) {
        try {
            interactionAttr.put(keyName,value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
