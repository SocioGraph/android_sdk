package com.example.admin.daveai.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.admin.daveai.R;
import java.util.ArrayList;


public class ViewPagerAdapter extends PagerAdapter {

    private final String TAG = getClass().getSimpleName();
    private Context context;
    private ArrayList<Object> imageList;
    private LayoutInflater layoutInflater;


    public ViewPagerAdapter(Context context,ArrayList<Object> imageUrlList) {
        this.context = context;
        this.imageList=imageUrlList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        View itemView = layoutInflater.inflate(R.layout.view_pager_item, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageView);
        if(imageList!=null && imageList.size()>0) {
            Object imageType = imageList.get(position);
           // Log.e(TAG,"imageType******************************************:- " + imageType);
            if(imageType instanceof String){
                Glide.with(context)
                        .load(imageType.toString())
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }
            else  if(imageType instanceof Integer){
                imageView.setImageResource((Integer) imageType);
            }
            else  if(imageType instanceof Uri){
                imageView.setImageURI((Uri) imageType);
            }
            else {
              //  Bitmap  uploadImage = Bitmap.createScaledBitmap((Bitmap) imageList.get(position), 250, 250, false);
                Bitmap  uploadImage = Bitmap.createScaledBitmap((Bitmap) imageType, 250, 250, false);
                imageView.setImageBitmap(uploadImage);
            }

        }
        container.addView(itemView,0);
        return itemView;
    }


    public void updateViewPagerList(ArrayList<Object> newItemList) {

        imageList.addAll(newItemList);
        Log.e(TAG," After  updateViewPagerList****************:- " + imageList.size());
        notifyDataSetChanged();

    }

    public void updateViewPagerListItem(Object newItem) {
        removeView(R.drawable.placeholder);
        imageList.add(0,newItem);

        //Log.e(TAG," updateViewPagerListItem****************:- " + imageList);
        notifyDataSetChanged();
    }

    private void removeView(Object objectValue) {
        if(imageList.contains(objectValue)) {
            imageList.remove(objectValue);
        }
        notifyDataSetChanged();
    }


}
